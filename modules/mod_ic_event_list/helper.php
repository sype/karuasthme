<?php
/**
 *	iCagenda Event List
 *----------------------------------------------------------------------------
 * @package     mod_ic_event_list
 * @copyright   Copyright (c)2012-2014 Cyril Rezé, Jooml!C - All rights reserved

 * @license     GNU General Public License version 3 or later; see LICENSE.txt
 * @author      Cyril Rezé (Lyr!C)
 * @link        http://www.joomlic.com
 *
 * @update		3.3.6 2014-05-13
 * @version		2.12
 *----------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.model');

//JModel::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_icagenda/models', 'iCagendaModel');
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_icagenda/models', 'iCagendaModel');

class modiCEventListHelper
{
    /**
     * Retrieves the list of events
     *
     * @param array $params An object containing the module parameters
     * @access public
     */
    public function getList($params)
    {
		$app	= JFactory::getApplication();
		$db		= JFactory::getDbo();

		// Get an instance of the generic events model
		//$model = JModel::getInstance('Events', 'iCagendaModel', array('ignore_request' => true));
		$model = JModelLegacy::getInstance('Events', 'iCagendaModel', array('ignore_request' => true));

		// Set application parameters in model
		$appParams = JFactory::getApplication()->getParams();
		$model->setState('params', $appParams);

		// Set the filters based on the module params
		$model->setState('filter.state', 1);

		$model->setState('list.select', 'a.*');

		// Access filter
		//$access = !JComponentHelper::getParams('com_icagenda')->get('show_noauth');
		//$authorised = JAccess::getAuthorisedViewLevels(JFactory::getUser()->get('id'));
		//$model->setState('filter.access', $access);

		// Set ordering
		$model->setState('list.ordering', 'a.next');

		$orderby= $params->get('orderby');
		if (($orderby == '2') OR (($orderby == NULL))) {
			$model->setState('list.direction', 'ASC');
		}
		if ($orderby == '1') {
			$model->setState('list.direction', 'DESC');
		}

//		$model->setState('list.start', 0);
//		$model->setState('list.limit', (int) $params->get('count', 5));

		// Category filter
		$catfilter = $params->get('mcatid');
//		if (($catfilter != '0') OR ($catfilter != NULL)) {
//			$model->setState('filter.category', $params->get('mcatid', array()));
//		}

		// Filter by language
		//$model->setState('filter.language',$app->getLanguageFilter());


		//	Retrieve Content
		$items = $model->getItems();

		return $items;
    }

	// Short Description
	public function descShort ($description, $paramlimit, $paramlimit_Content){
		$limit = '100';
		$limitGlobal = '0';
		if (isset($paramlimit)) $limitGlobal=$paramlimit;
		if ($limitGlobal == 0) {
			$limit = JComponentHelper::getParams('com_icagenda')->get('ShortDescLimit');
		}
		if ($limitGlobal == 1) {
			$customlimit=$paramlimit_Content;
			if (is_numeric($customlimit)){
				$limit=$paramlimit_Content;
			} else {
				$limit = JComponentHelper::getParams('com_icagenda')->get('ShortDescLimit');
			}
		}
		if (is_numeric($limit)) {
			$limit = $limit;
		} else {
			$limit = '1';
		}
		$readmore='';
		if ($limit <= 1) {
			$readmore='';
		} else {
			$readmore='&#46;&#46;&#46;';
		}
		$text=strip_tags($description);
//		$text=preg_replace('/<img[^>]*>/Ui', '', $description);
		if(strlen($text)>$limit){
			$string_cut=substr($text, 0,$limit);
			$last_space=strrpos($string_cut,' ');
			$string_ok=substr($string_cut, 0,$last_space);
			$text=$string_ok.'';
//			$text=strip_tags($text);
			$text=$text.$readmore;
		}else{
			$text=$text;
		}
		$textdesc = $text;
		return $textdesc;
	}

    public function getCatcolor($catid)
    {
		// Get Access Names
		$db = JFactory::getDBO();
		$db->setQuery(
			'SELECT `color`' .
			' FROM `#__icagenda_category`' .
			' WHERE `id` = '. (int) $catid
		);
		$cat_color=$db->loadObject()->color;
		return $cat_color;
	}

	// Function to convert font color, depending on category color
	public function getfontColor($catid){

		$col=self::getCatcolor($catid);

		$color = '';
		if (isset($col)) {$color = $col;}

		$hex_R = substr($color,1,2);
		$hex_G = substr($color,3,2);
		$hex_B = substr($color,5,2);
		$RGBhex = hexdec($hex_R).",".hexdec($hex_G).",".hexdec($hex_B);

		$RGB = explode(",",$RGBhex);
		$RGBa=$RGB[0];
		$RGBb=$RGB[1];
		$RGBc=$RGB[2];

		$somme = ($RGBa + $RGBb + $RGBc);
		if ($somme > '600') {
			$fcolor = '#111111';
		} else {
			$fcolor = 'white';
		}
		return $fcolor;
	}

	// function to get number of registered people to an event
	public function registered ($eventID){

		// Preparing connection to db
		$db = JFactory::getDBO();
		// Preparing the query
		$query = $db->getQuery(true);
		$query->select(' sum(r.people) AS registered')->from('#__icagenda_registration AS r')->where('(r.eventId='.(int)$eventID.') AND (r.state > 0)');
		$db->setQuery($query);
		$people = $db->loadObjectList();

		$nbreg = $people[0]->registered;

		return $nbreg;

	}

    public function getThumbnail($imageSet, $thumb_generator)
    {
		include_once JPATH_ROOT.'/media/com_icagenda/scripts/icthumb.php';

		// START iCthumb

		// Initialize Vars
		$Image_Link 			= '';
		$Thumb_Link 			= '';
		$Display_Thumb 			= false;
		$No_Thumb_Option		= false;
		$Default_Thumb			= false;
		$MimeTypeOK 			= true;
		$MimeTypeERROR			= false;
		$Invalid_Link 			= false;
		$Invalid_Img_Format		= false;
		$fopen_bmp_error_msg	= false;

		// SETTINGS ICTHUMB
		$FixedImageVar 			= $imageSet;

		// Set if run iCthumb
		if (($FixedImageVar) AND ($thumb_generator == 1)) {

			$params_media = JComponentHelper::getParams('com_media');
			$image_path = $params_media->get('image_path', 'images');

			// Set folder vars
			$fld_icagenda 		= 'icagenda';
			$fld_thumbs 		= 'thumbs';
			$fld_copy	 		= 'copy';

			// SETTINGS ICTHUMB
			$thumb_width		= '130';
			$thumb_height		= '50';
			$thumb_quality		= '80';
			$thumb_destination	= 'themes/w'.$thumb_width.'h'.$thumb_height.'q'.$thumb_quality.'_';

			// Get Image File Infos
			$url = $FixedImageVar;
			$decomposition = explode( '/' , $url );
			// in each parent
			$i = 0;
			while ( isset($decomposition[$i]) )
				$i++;
			$i--;
			$imgname = $decomposition[$i];
			$fichier = explode( '.', $decomposition[$i] );
			$imgtitle = $fichier[0];
			$imgextension = strtolower($fichier[1]); // fixed 3.1.10


//							$GblParams = JComponentHelper::getParams('com_icagenda');
//							$MaxWidth = $GblParams->get('Event_MaxWidth');
//							$MaxHeight = $GblParams->get('Event_MaxHeight');
//							$Quality = $GblParams->get('Event_Quality');

							// Get Image File Infos (minimum php 5.2) - Remove in 3.1.9
//							$path = pathinfo($item->image);
//							$imgname = $path['basename'];			// Get file base name
//							$imgdirname = $path['dirname'];		// Get image dirname
//							$imgextension = strtolower($path['extension']);	// Get image extension
//							$imgtitle = $path['filename'];			// Get image name

			// Clean file name
			jimport( 'joomla.filter.output' );
			$cleanFileName = JFilterOutput::stringURLSafe($imgtitle) . '.' . $imgextension;
			$cleanTitle = JFilterOutput::stringURLSafe($imgtitle);

//							$cleanFileName2 = cleanString($imgtitle) . '.' . $imgextension;
//							$cleanTitle2 = cleanString($imgtitle);
//							echo $cleanFileName.'<br />'.$cleanFileName2;

			// Paths to thumbs and copy folders
			$thumbsPath 			= $image_path.'/'.$fld_icagenda.'/'.$fld_thumbs.'/';
			$copyPath	 			= $image_path.'/'.$fld_icagenda.'/'.$fld_thumbs.'/'.$fld_copy.'/';

			// Image pre-settings
			$imageValue 			= $FixedImageVar;
			$Image_Link 			= $FixedImageVar;
			$Invalid_LinkMsg		= '<i class="icon-warning"></i><br /><span style="color:red;"><strong>' . JText::_('COM_ICAGENDA_INVALID_PICTURE_LINK') . '</strong></span>';
			$Wrong_img_format		= '<i class="icon-warning"></i><br/><span style="color:red;"><strong>' . JText::_('COM_ICAGENDA_NOT_AUTHORIZED_IMAGE_TYPE') . '</strong><br/>' . JText::_('COM_ICAGENDA_NOT_AUTHORIZED_IMAGE_TYPE_INFO') . '</span>';
			$fopen_bmp_error		= '<i class="icon-warning"></i><br/><span style="color:red;"><strong>' . JText::_('COM_ICAGENDA_PHP_ERROR_FOPEN_COPY_BMP') . '</strong><br/>' . JText::_('COM_ICAGENDA_PHP_ERROR_FOPEN_COPY_BMP_INFO') . '</span>';

			// Mime-Type pre-settings
			$errorMimeTypeMsg 		= '<i class="icon-warning"></i><br /><span style="color:red;"><strong>' . JText::_('COM_ICAGENDA_ERROR_MIME_TYPE') . '</strong><br/>' . JText::_('COM_ICAGENDA_ERROR_MIME_TYPE_NO_THUMBNAIL');

			// url to thumbnails already created
			$Thumb_Link 			= $image_path.'/'.$fld_icagenda.'/'.$fld_thumbs.'/'.$thumb_destination . $cleanFileName;
			$Thumb_aftercopy_Link 	= $image_path.'/'.$fld_icagenda.'/'.$fld_thumbs.'/'.$thumb_destination . $cleanTitle . '.jpg';

			// Check if thumbnails already created
			if ((file_exists(JPATH_ROOT . '/' . $Thumb_Link)) AND (!file_exists(JPATH_ROOT . '/' . $Thumb_aftercopy_Link))) {
				$Thumb_Link = $Thumb_Link;
				$Display_Thumb = true;
			}
			elseif (file_exists(JPATH_ROOT . '/' . $Thumb_aftercopy_Link)) {
				$Thumb_Link = $Thumb_aftercopy_Link;
				$Display_Thumb = true;
			}
			// if thumbnails not already created, create thumbnails
			else {

				if (filter_var($imageValue, FILTER_VALIDATE_URL)) {
					$linkToImage = $imageValue;
				} else {
					$linkToImage = JPATH_ROOT . '/' . $imageValue;
				}

				if (file_exists($linkToImage)) {

					// Test Mime-Type
					$fileinfos = getimagesize($linkToImage);
					$mimeType = $fileinfos['mime'];
					$extensionType = 'image/'.$imgextension;

					// SETTINGS ICTHUMB
					$errorMimeTypeInfo = '<span style="color:black;"><br/>' . JText::sprintf('COM_ICAGENDA_ERROR_MIME_TYPE_INFO', $imgextension, $mimeType);

					// Error message if Mime-Type is not the same as extension
					if (($imgextension == 'jpeg') OR ($imgextension == 'jpg')) {
						if (($mimeType != 'image/jpeg') AND ($mimeType != 'image/jpg')) {
							$MimeTypeOK 	= false;
							$MimeTypeERROR 	= true;
						}
					}
					elseif ($imgextension == 'bmp') {
						if (($mimeType != 'image/bmp') AND ($mimeType != 'image/x-ms-bmp')) {
							$MimeTypeOK 	= false;
							$MimeTypeERROR 	= true;
						}
					}
					else {
						if ($mimeType != $extensionType) {
							$MimeTypeOK 	= false;
							$MimeTypeERROR 	= true;
						}
					}
				}

				// If Error mime-type, no thumbnail creation
				if ($MimeTypeOK) {

					// Call function and create image thumbnail for events list in admin

					// If Image JPG, JPEG, PNG or GIF
					if (($imgextension == "jpg") OR ($imgextension == "jpeg") OR ($imgextension == "png") OR ($imgextension == "gif")) {

						$Thumb_Link = $Thumb_Link;

						if (!file_exists(JPATH_ROOT . '/' . $Thumb_Link)) {

							if (filter_var($imageValue, FILTER_VALIDATE_URL)) {

								if ((url_exists($imageValue)) AND ($fopen)) {

									$testFile = JPATH_ROOT . '/' . $copyPath . $cleanFileName;
									if (!file_exists($testFile)) {
										//Get the file
										$content = file_get_contents($imageValue);
										//Store in the filesystem.
										$fp = fopen(JPATH_ROOT . '/' . $copyPath . $cleanFileName, "w");
										fwrite($fp, $content);
										fclose($fp);
									}

									$linkToImage = JPATH_ROOT . '/' . $copyPath . $cleanFileName;
									$imageValue = $copyPath . $cleanFileName;

								} else {
									$linkToImage = $imageValue;
								}

							} else {
								$linkToImage = JPATH_ROOT . '/' . $imageValue;
							}

							if ((url_exists($linkToImage)) OR (file_exists($linkToImage))) {
								createthumb($linkToImage, JPATH_ROOT . '/' . $Thumb_Link, $thumb_width, $thumb_height, $thumb_quality);

							} else {
								$Invalid_Link = true;

							}
						}
					}

					// If Image BMP
					elseif ($imgextension == "bmp") {

						$Image_Link = $copyPath . $cleanTitle . '.jpg';

						$Thumb_Link = $Thumb_aftercopy_Link;

						if (!file_exists(JPATH_ROOT . '/' . $Thumb_Link)) {

							if (filter_var($imageValue, FILTER_VALIDATE_URL)) {

								if ((url_exists($imageValue)) AND ($fopen)) {

									$testFile = JPATH_ROOT . '/' . $copyPath . $cleanTitle . '.jpg';
									if (!file_exists($testFile)) {
										//Get the file
										$content = file_get_contents($imageValue);
										//Store in the filesystem.
										$fp = fopen(JPATH_ROOT . '/' . $copyPath . $cleanFileName, "w");
										fwrite($fp, $content);
										fclose($fp);
										$imageNewValue = JPATH_ROOT . '/' . $copyPath . $cleanFileName;
										imagejpeg(icImageCreateFromBMP($imageNewValue), JPATH_ROOT . '/' . $copyPath . $cleanTitle . '.jpg', 100);
										unlink($imageNewValue);
									}

								} else {
									$linkToImage = $imageValue;
								}

							} else {
								imagejpeg(icImageCreateFromBMP(JPATH_ROOT . '/' . $imageValue), JPATH_ROOT . '/' . $copyPath . $cleanTitle . '.jpg', 100);
							}

							$imageValue = $copyPath . $cleanTitle . '.jpg';
							$linkToImage = JPATH_ROOT . '/' . $imageValue;

							if (!$fopen) {
								$fopen_bmp_error_msg = true;
							}
							elseif ((url_exists($linkToImage)) OR (file_exists($linkToImage))) {
								createthumb($linkToImage, JPATH_ROOT . '/' . $Thumb_Link, $thumb_width, $thumb_height, $thumb_quality);
							}
							else {
								$Invalid_Link = true;
							}
						}
					}

					// If Not authorized Image Format
					else {
						if ((url_exists($linkToImage)) OR (file_exists($linkToImage))) {
							$Invalid_Img_Format = true;
						} else {
							$Invalid_Link = true;
						}
					}

					if (!$Invalid_Link) {
						$Display_Thumb = true;
					}
				}
				// If error Mime-Type
				else {
					if (($imgextension == "jpg") OR ($imgextension == "jpeg") OR ($imgextension == "png") OR ($imgextension == "gif") OR ($imgextension == "bmp")) {
						$MimeTypeERROR = true;
					} else {
						$Invalid_Img_Format = true;
						$MimeTypeERROR = false;
					}
				}
			}

		}
		elseif (($FixedImageVar) AND ($thumb_generator == 0)) {
			$No_Thumb_Option = true;
		}
		else {
			$Default_Thumb = true;
		}

		// END iCthumb



		// Set Thumbnail
		$default_thumbnail = 'media/com_icagenda/images/nophoto.jpg';
		if ($Invalid_Img_Format) {
			$thumb_img = $default_thumbnail;
		}

		if ($Invalid_Link) {
			$thumb_img = $default_thumbnail;
		}

		if ($MimeTypeERROR) {
			$thumb_img = $default_thumbnail;
		}

		if ($fopen_bmp_error_msg) {
			$thumb_img = $default_thumbnail;
		}

		if ($Display_Thumb) {
			$thumb_img = $Thumb_Link;
		}

		if ($No_Thumb_Option) {
			$thumb_img = $FixedImageVar;
		}

		if ($Default_Thumb) {
			if ($imageSet) {
				$thumb_img = $default_thumbnail;
			} else {
//				$thumb_img = '';  // A VOIR
				$imageSet = true;
				$thumb_img = $default_thumbnail;
			}
		}
			$baseURL = JURI::base();
			$subpathURL = JURI::base(true);
//echo $baseURL.'<br/>';
//echo $subpathURL.'<br/>';

		$thumb_img = ltrim($thumb_img, '/');
		return $baseURL.$thumb_img;
	}

}
