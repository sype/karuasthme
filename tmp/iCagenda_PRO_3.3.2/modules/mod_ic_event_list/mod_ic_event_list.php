<?php
/**
 *	iCagenda Event List
 *----------------------------------------------------------------------------
 * @package     mod_ic_event_list
 * @copyright   Copyright (c)2012-2014 Cyril Rezé, Jooml!C - All rights reserved

 * @license     GNU General Public License version 3 or later; see LICENSE.txt
 * @author      Cyril Rezé (Lyr!C)
 * @link        http://www.joomlic.com
 *
 * @update		3.3.0 2014-02-21
 * @version		2.9
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *----------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die();

// Test if translation is missing, set to en-GB by default
$language= JFactory::getLanguage();
//$language->load( 'mod_ic_event_list', JPATH_SITE.'/modules/mod_ic_event_list', 'en-GB', true );
//$language->load( 'mod_ic_event_list', JPATH_SITE.'/modules/mod_ic_event_list', null, true );
$language->load( 'mod_ic_event_list', JPATH_SITE, 'en-GB', true );
$language->load( 'mod_ic_event_list', JPATH_SITE, null, true );

// Include the class of the syndicate functions only once
require_once(dirname(__FILE__).'/helper.php');

// Load Needed Files
$document = JFactory::getDocument();

// Load CSS Files
$document->addStyleSheet( JURI::base( true ).'/media/com_icagenda/css/tipTip.css' );

// Get Component Global Options
$iCparams = JComponentHelper::getParams('com_icagenda');


// Load jQuery Files
if(version_compare(JVERSION, '3.0', 'lt')) {

	JHTML::_('behavior.mootools');

	// load jQuery, if not loaded before (NEW VERSION IN 1.2.6)
	$scripts = array_keys($document->_scripts);
	$scriptFound = false;


	for ($i = 0; $i < count($scripts); $i++) {
		if (stripos($scripts[$i], 'jquery.min.js') !== false) {
			$scriptFound = true;
		}
		// load jQuery, if not loaded before as jquery - added in 1.2.7
		if (stripos($scripts[$i], 'jquery.js') !== false) {
		    $scriptFound = true;
		}
	}

	// jQuery Library Loader
	if (!$scriptFound) {
		// load jQuery, if not loaded before
		if (!JFactory::getApplication()->get('jquery')) {
			JFactory::getApplication()->set('jquery', true);
			// add jQuery
			$document->addScript('https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
		    $document->addScript('components/com_icagenda/js/jquery.noconflict.js');
		}
	}
}
else {
	JHtml::_('behavior.formvalidation');
	JHtml::_('bootstrap.framework');
	JHtml::_('jquery.framework');
}
$document->addScript(JURI::base( true ).'/media/com_icagenda/js/jquery.tipTip.js');

$iCtip	 = array();
$iCtip[] = '	jQuery(document).ready(function(){';
$iCtip[] = '		jQuery(".icTip").tipTip({maxWidth: "200", defaultPosition: "top", edgeOffset: 1});';
$iCtip[] = '	});';

// Add the script to the document head.
JFactory::getDocument()->addScriptDeclaration(implode("\n", $iCtip));


$level = E_ALL & ~E_NOTICE & ~E_DEPRECATED;

if (version_compare(PHP_VERSION, '5.4.0-dev', '>='))
{
	// PHP 5.4 adds E_STRICT to E_ALL.
	// Our utf8 normalizer triggers E_STRICT output on PHP 5.4.
	// Unfortunately it cannot be made E_STRICT-clean while
	// continuing to work on PHP 4.
	// E_STRICT is defined starting with PHP 5
	if (!defined('E_STRICT'))
	{
		define('E_STRICT', 2048);
	}
	$level &= ~E_STRICT;
}
error_reporting($level);

// Static call to the class
$list = modiCEventListHelper::getList($params);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

// Get Module Params
$limitcount=$params->get('count', 5);
$monthFormat=$params->get('monthFormat', 2);
$iCmenuitem=$params->get('iCmenuitem');
$dp_city = $params->get('dp_city', '');
$dp_shortDesc = $params->get('dp_shortDesc', '');
$dp_dateTime=$params->get('dp_dateTime', '');
$dp_regInfos=$params->get('dp_regInfos', '');
$paramlimit=$params->get('paramlimit', '');
$paramlimit_Content=$params->get('paramlimit_Content');
$column=$params->get('column', 'col1');
//$cropped_thumb=$params->get('cropped_thumb');

// Current Date control (UTC)
$day= date('d');
$m= date('m');
$y= date('y');
$toDay=mktime(0,0,0,$m,$day,$y);

// Itemid Request (automatic detection of the first iCagenda menu-link, by menuID)
$x='';
$lang = JFactory::getLanguage();
$langcur = $lang->getTag();
$langcurrent = $langcur;


//		$db		= JFactory::getDbo();
//		$query	= $db->getQuery(true);
//		$query->select('a.title, a.published, a.id, a.params')
//			->from('`#__menu` AS a')
//			->where( "(link = 'index.php?option=com_icagenda&view=list') AND (published > 0)" );
//		$db->setQuery($query);
//		$link = $db->loadObjectList();

//		foreach ($link as $l){
//			if ($l->published == '1') {
//				$link_params = json_decode( $l->params, true );

//				foreach ($link_params as $key => $value){
//					if ($key == 'mcatid') {
//						print_r($value);
//					}
//				}
//			}
//		}

$db = JFactory::getDbo();
$query	= $db->getQuery(true);
$query->select('id AS idm, params AS params')->from('#__menu')->where( "(link = 'index.php?option=com_icagenda&view=list') AND (published > 0) AND (language = '$langcurrent') " );

$db->setQuery($query);
$idm=$db->loadResult();
$allidm=$db->loadList();
$mItemid=$idm;



if ($mItemid == NULL) {
	$db = JFactory::getDbo();
	$query	= $db->getQuery(true);
	$query->select('id AS noidm')->from('#__menu')->where( "(link = 'index.php?option=com_icagenda&view=list') AND (published > 0) AND (language = '*') " );
	$db->setQuery($query);
	$noidm=$db->loadResult();
	$allnoidm=$db->loadObject()->noidm;
	$noidm=$noidm;
}
$nolink = '';
if ($noidm == NULL && $mItemid == NULL) {
	$nolink = 1;
}
if ($nolink == 1) {
	do {
		echo '<div style="color:#a40505; text-align: center;"><b>info :</b></div><div style="color:#a40505; font-size: 0.8em; text-align: center;">'.JText::_( 'MOD_IC_EVENT_LIST_MENULINK_UNPUBLISHED_MESSAGE' ).'</div>';
	} while ($x > 0);
}

if(is_numeric($iCmenuitem)) {
	$lien = $iCmenuitem;
} else {
	if ($mItemid == NULL) {
		$lien = $noidm;
	}
	else {
		$lien = $mItemid;
	}
}


// Static call to the framework module processor which will
// Include the layout file (procedural, not a class)
jimport( 'joomla.application.module.helper' );
$layout= $params->get('layout');
$time = '1';
$time = $params->get('time');

// Get Array of categories to be displayed
$all_cat = '0';
$cat_filter = $params->get('mcatid', '0');

if (!is_array($cat_filter)) {
	$catfilter = array($cat_filter);
} else {
	$catfilter = $cat_filter;
}
if (in_array('0', $catfilter)) {
	$all_cat =  '1';
}


// Load List
$i='';
$nb=0;
?>
<!--
 * iCagenda PRO Event List Module by Jooml!C
 *
 * @copyright	Copyright (c)2012-2014 JOOMLIC - All rights reserved.
 * @version		2.8
 *
-->
<?php

// Get Time Format setting
$timeformat = $iCparams->get('timeformat');

// Check if GD is enabled
if (extension_loaded('gd') && function_exists('gd_info')) {
	$thumb_generator = $iCparams->get('thumb_generator', 1);
} else {
	$thumb_generator = 0;
	//JError::raiseWarning('101', JText::_('COM_ICAGENDA_PHP_ERROR_GD'));
}

// Get limit Short Description Global Option
$limitShortDesc = $iCparams->get('ShortDescLimit');

// Check if fopen is allowed
$fopen = true;
$result = ini_get('allow_url_fopen');
if (empty($result))
{
	//JError::raiseWarning('101', JText::_('COM_ICAGENDA_PHP_ERROR_FOPEN'));
	$fopen = false;
}
//echo $datenext;

	echo '<div class="ic_eventlist'.$moduleclass_sfx.'">';
	echo '<div class="section group">';
	if($list) {
	foreach ($list as $item) :
		if($item->approval == 0) {

			$evtParams = '';
			$evtParams = new JRegistry($item->params);

			$catid			= $item->catid;
			$cat_color		= modiCEventListHelper::getCatcolor($catid);

			$font_color		= modiCEventListHelper::getfontColor($catid);

			$imageSet		= $item->image;
			$thumb_img		= modiCEventListHelper::getThumbnail($imageSet, $thumb_generator);

//			$descShort		= modiCEventListHelper::descShort($description, $paramlimit, $paramlimit_Content);

			$eventID		= $item->id;
			$nextDate		= $item->next;

			$eventTitle		= $item->title;
			$eventCity		= $item->city;

			if ($timeformat == 1) {
				$lang_time = strftime("%H:%M", strtotime("$nextDate"));
			} else {
				$lang_time = strftime("%I:%M %p", strtotime("$nextDate"));
			}

			$nextTime		= JText::_($lang_time);
			$displayTime	= $item->displaytime;

			$description	= $item->desc;
			$shortDesc		= modiCEventListHelper::descShort($description, $paramlimit, $paramlimit_Content);

			if (empty($dp_city) OR $dp_city == 0) {
				$display_city = false;
			} else {
				$display_city = true;
			}

			if (empty($dp_shortDesc) OR $dp_shortDesc == 0) {
				$display_shortDesc = false;
			} else {
				$display_shortDesc = true;
			}

			if (empty($dp_dateTime) OR $dp_dateTime == 0) {
				$display_dateTime = false;
			} elseif ($dp_dateTime == 1) {
				$display_dateTime = true;
				$display_date = true;
				$display_Time = true;
			} elseif ($dp_dateTime == 2) {
				$display_dateTime = true;
				$display_date = true;
				$display_Time = false;
			} elseif ($dp_dateTime == 3) {
				$display_dateTime = true;
				$display_date = false;
				$display_Time = true;
			}

			if (empty($dp_regInfos)) {
				$registered		= false;
				$maxTickets		= false;
				$TicketsLeft	= false;
			} else {
				$registered		= modiCEventListHelper::registered($eventID);
				$get_maxTickets	= $evtParams->get('maxReg');
				if ($get_maxTickets < 1) {
					$maxTickets		= false;
					$TicketsLeft	= false;
				} else {
					$maxTickets		= $evtParams->get('maxReg');
					$TicketsLeft	= ($get_maxTickets - $registered);
				}
			}

			$access='0';
			$control='';

			// Access Control
			$user = JFactory::getUser();
			$userLevels = $user->getAuthorisedViewLevels();
			if(version_compare(JVERSION, '3.0', 'lt')) {
				$userGroups = $user->getAuthorisedGroups();
			} else {
				$userGroups = $user->groups;
			}
			$access=$item->access;
			if ($access == '0') { $access='1'; }

//			foreach ($userLevels as $level){
//				if ($level == $access) {
//					$control=$access;
//				}
//			}
			if (in_array($access, $userLevels)
				OR in_array('8', $userGroups))
			{
				$control=$access;
			}

			// Language Control
			$lang = JFactory::getLanguage();
			$eventLang = '';
			$langTag = '';
			$langTag = $lang->getTag();

			if(isset($item->language)) $eventLang=$item->language;
			if($eventLang=='') $eventLang=$langTag;
			if($eventLang=='*') {
				$eventLang=$langTag;
			}

			$cat_item = $item->catid;

			if ( (in_array($cat_item, $catfilter)) OR ($all_cat == '1') ) {
			if ($control == $access) {
				if ($eventLang == $langTag) {

					$datenext=strtotime($item->next);
					$urlEvent=JRoute::_( 'index.php?option=com_icagenda&amp;view=list&amp;layout=event&amp;id=' . (int)$item->id . '&amp;Itemid=' . (int)$lien);
					$ymd_next = date('Y-m-d', $datenext);
//					$ymd_today = date('Y-m-d', $toDay);
					$ymd_today = JHtml::date($toDay , 'Y-m-d', true);


					$nextDay = date('d', strtotime($item->next));
					if ($monthFormat == 1) {
						$nextMonth = date('m', strtotime($item->next));
					}
					if ($monthFormat == 2) {
						$nextMonth = JText::_(strftime("%B", strtotime("$item->next")).'_SHORT');
					}

					if ($time == '3') {
//						if (($ymd_next <= $ymd_today)) {
//							$eventDisplay=FALSE;
//						} else {
//							$eventDisplay=TRUE;

							if (($ymd_next == $ymd_today)) {
								$nb=($nb+1);
							}
							if (($ymd_next == $ymd_today)) {
								if($nb <= $limitcount) {
									// Style of layout
									$style=str_replace('_:', '', $layout);
//									$style=$layout;
//									JHTML::_('stylesheet', $style.'_style.css', 'modules/mod_ic_event_list/css/');
									$document->addStyleSheet( JURI::base().'modules/mod_ic_event_list/css/'.$style.'_style.css' );
									require(JModuleHelper::getLayoutPath('mod_ic_event_list',$layout ));
								}
							}
//						}
					}

					if ($time == '2') {
//						if (($ymd_next <= $ymd_today)) {
//							$eventDisplay=FALSE;
//						} else {
//							$eventDisplay=TRUE;

							if (($ymd_next > $ymd_today)) {
								$nb=($nb+1);
							}
							if (($ymd_next > $ymd_today)) {
								if($nb <= $limitcount) {
									// Style of layout
									$style=str_replace('_:', '', $layout);
//									$style=$layout;
//									JHTML::_('stylesheet', $style.'_style.css', 'modules/mod_ic_event_list/css/');
									$document->addStyleSheet( JURI::base().'modules/mod_ic_event_list/css/'.$style.'_style.css' );
									require(JModuleHelper::getLayoutPath('mod_ic_event_list',$layout ));
								}
							}
//						}
					}

					if ($time == '1') {
//						if (($ymd_next < $toDay)) {
//							$eventDisplay=FALSE;
//						} else {
//							$eventDisplay=TRUE;

							if (($ymd_next >= $ymd_today)) {
								$nb=($nb+1);
							}
							if (($ymd_next >= $ymd_today)) {
								if($nb <= $limitcount) {
									// Style of layout
									$style=str_replace('_:', '', $layout);
//									$style=$layout;
//									JHTML::_('stylesheet', $style.'_style.css', 'modules/mod_ic_event_list/css/');
									$document->addStyleSheet( JURI::base().'modules/mod_ic_event_list/css/'.$style.'_style.css' );
									require(JModuleHelper::getLayoutPath('mod_ic_event_list',$layout ));
								}
							}
//						}
					}

					if ($time == '0') {
						if (($ymd_next < $ymd_today)) {

							if (($ymd_next < $ymd_today)) {
								$nb=($nb+1);
							}
							if (($ymd_next < $ymd_today)) {
								if($nb <= $limitcount) {
									// Style of layout
									$style=str_replace('_:', '', $layout);
//									JHTML::_('stylesheet', $style.'_style.css', 'modules/mod_ic_event_list/css/');
									$document->addStyleSheet( JURI::base().'modules/mod_ic_event_list/css/'.$style.'_style.css' );
									require(JModuleHelper::getLayoutPath('mod_ic_event_list',$layout ));
								}
							}
						}
					}

					if (($time == '') OR ($time == NULL) OR ($time == '4')) {
						$nb=($nb+1);

						if (($nb != 0)) {
							if($nb <= $limitcount) {
								// Style of layout
								$style=str_replace('_:', '', $layout);
//								JHTML::_('stylesheet', $style.'_style.css', 'modules/mod_ic_event_list/css/');
								$document->addStyleSheet( JURI::base().'modules/mod_ic_event_list/css/'.$style.'_style.css' );
								require(JModuleHelper::getLayoutPath('mod_ic_event_list',$layout ));
							}
						}
					}
				}
			}
			}
		}
	endforeach;
	}
	echo '</div>';
	echo '</div>';
	echo '<div style="clear:both"></div>';

if (($nb == '0')) {
	$cattext = '';
	if ($time == '3') { $ordertext = 'today'; }
	if ($time == '2') { $ordertext = 'future'; }
	if ($time == '1') { $ordertext = 'future'; }
	if ($time == '0') { $ordertext = 'past'; }
	if (($time == '') OR ($time == NULL) OR ($time == '4')) { $ordertext = 'all'; }
//	if (($catfilter != '0') OR ($catfilter != NULL)) {
//		$cattext = '_CATEGORY';
//	}
	do {
		echo '<div class="noEvent">'.JText::_( 'MOD_IC_EVENT_LIST_'.strtoupper($ordertext).'_NO_EVENT_TEXT' ).'</div>';
//		echo '<div class="noEvent">'.JText::_( 'MOD_IC_EVENT_LIST_'.strtoupper($ordertext).'_NO_EVENT_TEXT'.$cattext ).'</div>';
	} while ($i < 0);
}
