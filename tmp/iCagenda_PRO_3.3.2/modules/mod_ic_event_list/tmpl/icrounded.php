<?php
/**
 *------------------------------------------------------------------------------
 *  iCagenda Event List Module by Jooml!C
 *------------------------------------------------------------------------------
 * @package     com_icagenda
 * @copyright   Copyright (c) 2013 Cyril Rezé, Jooml!C - All rights reserved
 *
 * @license     GNU General Public License version 3 or later; see LICENSE.txt
 * @author      Cyril Rezé (Lyr!C)
 * @link        http://www.joomlic.com
 *
 * @layout		icrounded
 * @version		2.7 2014-01-03
 * @since       1.0
 *------------------------------------------------------------------------------
*/

// No direct access to this file
defined('_JEXEC') or die(); ?>

<div class="icrounded_eventlist col <?php echo $column; ?>">

	<a href="<?php echo $urlEvent; ?>">
	<div class="eventDiv">
			<!--[if lte IE 9]>
				<a href="<?php echo $urlEvent; ?>"><span>
			<![endif]-->

		<div class="dateImgBox">

			<div class="dateDiv">
				<div class="iCday"><?php echo $nextDay; ?></div>
				<div class="iCmonth"><?php echo $nextMonth; ?></div>
			</div>

			<?php if ($imageSet): ?>
			<div class="imageDiv">
				<div class="evtImg" style="background-image:url(<?php echo $thumb_img; ?>);"> </div>
			</div>
			<?php endif; ?>

		</div>

		<div class="titleSpan">
			<!--[if lte IE 9]>
				<a href="<?php echo $urlEvent; ?>">
			<![endif]-->
			<?php echo htmlspecialchars($eventTitle); ?>
			<!--[if lte IE 9]>
				</a>
			<![endif]-->
		</div>

		<?php if ($display_city): ?>
		<?php if ($eventCity): ?>
		<div class="cityDiv"><?php echo htmlspecialchars($eventCity); ?><?php if ($display_dateTime): ?>,<?php endif; ?></div>
		<?php endif; ?>
		<?php endif; ?>

		<?php if ($display_dateTime): ?>
			<?php if ($display_date): ?>
				<div class="datetimeDiv">
					<?php echo JHtml::date($nextDate , JText::_('DATE_FORMAT_LC3')); ?>
				</div>
			<?php endif; ?>
			<?php if ($display_Time): ?>
				<?php if ($displayTime): ?>
					<div class="datetimeDiv">
						<?php echo JText::_( 'MOD_IC_EVENT_LIST_AT_TIME' ); ?> <?php
						echo $nextTime;
						?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<br />
		<?php endif; ?>

		<?php if ($display_shortDesc): ?>
			<?php if ($shortDesc): ?>
				<span class="descSpan"><?php echo $shortDesc; ?><span>
			<?php endif; ?>
		<?php endif; ?>

		<?php if ($maxTickets): ?>
		<span class="icTip iCreg available" title="<?php echo JText::_( 'MOD_IC_EVENT_LIST_SEATS_NUMBER' ); ?>"><?php echo $maxTickets; ?></span>
		<?php endif; ?>

		<?php if ($TicketsLeft): ?>
		<span class="icTip iCreg ticketsleft" title="<?php echo JText::_( 'MOD_IC_EVENT_LIST_SEATS_AVAILABLE' ); ?>"><?php echo $TicketsLeft; ?></span>
		<?php endif; ?>

		<?php if ($registered): ?>
		<span class="icTip iCreg registered" title="<?php echo JText::_( 'MOD_IC_EVENT_LIST_ALREADY_BOOKED' ); ?>"><?php echo $registered; ?></span>
		<?php endif; ?>

			<!--[if lte IE 9]>
				</span></a>
			<![endif]-->
	</div>
	</a>

</div>
