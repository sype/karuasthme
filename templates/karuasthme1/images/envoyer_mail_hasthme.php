﻿<?php
function decoder($texte){
        $texte = utf8_decode($texte); // converti en iso-8859-1
        $texte = stripslashes($texte); // élimine les anti-slashs d'échappement
        $texte = nl2br($texte); // converti les retours  en <br />
        $texte = trim($texte); // élimine les '\n', '\r', '\t' etc
        return $texte;
}
 
$erreurPHP="";

/* Paragraphe à decommenter pour tester le script php seul, sans le fla
$_POST['sujet'] = 'formulaire Test';
$_POST['messageHtml'] = '<html><body>Salut<br /> mon cher<br /><br />Bye</body></html>';
$_POST['messageText'] = "Salut  mon cher  Bye";
$_POST['expediteur'] = "expediteur@fai.fr";
$_POST['mailReponse'] = "expediteur@fai.fr";
*/

if(isset($_POST['sujet']))$sujet = $_POST['sujet'];
else $erreurPHP .= "Variable 'sujet' non transmise\n";
 
if(isset($_POST['messageHtml']))$messageHtml = $_POST['messageHtml'];
else $erreurPHP .= "Variable 'message' non transmise\n";

if(isset($_POST['messageText']))$messageTexte = $_POST['messageText'];
else $erreurPHP .= "Variable 'message' non transmise\n";
 
if(isset($_POST['expediteur']))$expediteur = $_POST['expediteur'];
else $erreurPHP .= "Variable 'expediteur' non transmise\n";
 
if(isset($_POST['mailReponse']))$mailReponse = $_POST['mailReponse'];
else $erreurPHP .= "Variable 'mailReponse' non transmise\n";
 
if($erreurPHP != "") echo utf8_encode("erreurPHP=".$erreurPHP);
else
{
	$to = "jack.goth@wanadoo.fr";
	$sujet = decoder($sujet);
	$expediteur = decoder($expediteur);
	$messagehtml = decoder($messageHtml);
	$messageTexte = utf8_decode($messageTexte);
	
	 //-----------------------------------------------
     //GENERE LA FRONTIERE DU MAIL ENTRE TEXTE ET HTML
     //-----------------------------------------------

     $frontiere = '-----=' . md5(uniqid(rand()));

     //-----------------------------------------------
     //HEADERS DU MAIL
     //-----------------------------------------------

     $headers = 'From: '.$expediteur.' <'.$mailReponse.'>'."\n";
     $headers .= 'Return-Path: <'.$mailReponse.'>'."\n";
     $headers .= 'MIME-Version: 1.0'."\n";
     $headers .= 'Content-Type: multipart/alternative; boundary="'.$frontiere.'"'."\n";
	 $headers .= 'Content-Transfer-Encoding: 8bit';

     //-----------------------------------------------
     //MESSAGE TEXTE
     //-----------------------------------------------
     $message = "This is a multi-part message in MIME format\n\n";

     $message = "--$frontiere\n";
     $message .= "Content-Type: text/plain; charset=UTF-8\n";
     $message .= "Content-Transfer-Encoding: 8bit\n\n";
     $message .= $messageTexte."\n\n";

     
	 //-----------------------------------------------
     //MESSAGE HTML
     //-----------------------------------------------
     $message .= "--$frontiere\n";
     $message .= "Content-Type: text/html; charset=UTF-8\n";
     $message .= "Content-Transfer-Encoding: 8bit\n\n";
     $message .= $messageHtml."\n\n";

     $message .= "--$frontiere--\n"; 

	$mail_OK = mail($to, $sujet, $message, $headers) ;
	
 
	if (!$mail_OK)
	{
		$erreurPHP .= "Problème lors de l'envoi du mail";
		
	}
	echo utf8_encode("erreurPHP=".$erreurPHP);
}
?>