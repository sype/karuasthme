<?php
//Script permettant de récupérer la liste des suvis

//connection bd
$patId = $_GET['id'];
include('config.php');

//Récupération nom et prénom patient
$req = "SELECT * FROM patient WHERE idPatient=".$patId.";";
if($result = mysql_query($req)) {
		while($ligne = mysql_fetch_array($result)){
				$fName = $ligne['prenomPatient'];
				$lName = $ligne['nomPatient'];
				}
}
?>
<!--Titre section-->
<div class="sectionTitle">Liste de suivi pour <?php echo htmlentities(ucfirst($fName)." ".strtoupper($lName)); ?></div>
<?php
//Récupération des suivi
$req = "SELECT `idFiche`, `questA_1`, `questA_2`, `questA_3`, `questA_4`, `questA_5`, `questA_6`, `questB_1`, `questB_2`, `questB_3`, `questB_4`, `questB_5`, DATE_FORMAT(DATE,'%d/%m/%Y %H:%i:%s') AS date, `idPatient` FROM fiches WHERE idPatient=".$patId.";";
$pos = true;
if($result = mysql_query($req)):
		while($ligne = mysql_fetch_array($result)):
		//construction des tableaux des suivis
		?>
			   <table class="suivi" cellspacing="0" cellpadding="0">
				 <tr>
				   <td class="suiviInfo"><span style="font-weight:bold">Examen du <?php echo  $ligne['date']; ?><!--</span><span id="delBut"><a href="#" onclick="msgBox('getContain(2, <?php echo  $ligne["idFiche"]; ?>)','getContain(1, <?php echo  $ligne["idPat"] ?>)');"><img style="border:none" align="right" src="images/del.gif" width="14" height="14"></a></span>--></td>
						</tr>
						<tr>
						<td>
						<table>
						<tr>
						<td>
						Questionnaire de suivi
						</td>
						</tr>
						<tr>
						<td>
						Vider les poumons : <span style="font-weight:bold">
						<?php if($ligne['questA_1']){echo "oui";}else{echo "non";} ?>
                        </span>
						</td>
						</tr>
						<tr>
						<td>
						Inhaler : <span style="font-weight:bold">
						<?php if($ligne['questA_2']){echo "oui";}else{echo "non";} ?>
						</span>
						</td>
						</tr>
						<tr>
						<td>
						Bloquer : <span style="font-weight:bold">
						<?php if($ligne['questA_3']){echo "oui";}else{echo "non";} ?>
						</span>
						</td>
						</tr>
						<tr>
						<td>
						Sait-il différencier son traitement de fond de son traitement de secours ? <span style="font-weight:bold">
						<?php if($ligne['questA_4']){echo "oui";}else{echo "non";} ?>
						</td>
						</tr>
						<tr>
						<td>
						Sait-il gérer une exacerbation PAR LES PLANS D'ACTION ? <span style="font-weight:bold">
						<?php if($ligne['questA_5']){echo "oui";}else{echo "non";} ?>
						</span>
						</td>
						</tr>
						<tr>
						<td>
						Maitrise t-il son peakflow ? <span style="font-weight:bold">
						<?php if($ligne['questA_6']){echo "oui";}else{echo "non";} ?>
						</span>
						</td>
						</tr>
						<tr>
						<td>
						Test de contrôle de l'asthme : <span style="font-weight:bold;<?php $val =  $ligne['questB_1']+$ligne['questB_2']+$ligne['questB_3']+$ligne['questB_4']+$ligne['questB_5'];
						if($val>0 && $val<15){
							echo "color:red;";
						}					
						if($val>=15 && $val<20){
							echo "color:#FF6600;";
						}
						if($val>=20 && $val<=25){
							echo "color:green;";
						}
						?>
                        ">
						<?php echo $val; ?>
                        </span>
						</td>
						</tr>
						</table>
						</td>
						</tr>
						</table>
                        <?php endwhile; ?>
                        <br/>
                        <br/>
                        <div id="legende">Test de contrôle de l'asthme : 0 < 
                          <span style="color:red">Non controlé</span> < 15 < 
                          <span style="color:#FF6600">Perfectible</span> < 20 < 
                          <span style="color:green">Controlé</span> <= 25
                        </div>
                        <?php
						else:
						//TO DO
						endif;
						mysql_close();
?>