<?php
//header('Content-type: text/html; charset=utf-8');
$id = $_GET['id'];

//Importation librairie FPDF
require_once('class/fpdf.php');
require_once('class/fpdi.php');
include('config.php');


$req = "SELECT nomMed, prenomMed, prenomPatient, nomPatient, `questA_1` , `questA_2` , `questA_3` , `questA_4` , `questA_5` , `questA_6` , `questB_1` , `questB_2` , `questB_3` , `questB_4` , `questB_5`, DATE_FORMAT(DATE,'%d/%m/%Y %H:%i:%s') AS date FROM `patient` INNER JOIN  fiches On fiches.idPatient = patient.idPatient INNER JOIN medecin ON patient.idMed = medecin.idMed WHERE idFiche = ".$id .";" ;

$cmpt = 0;

//Récupération et montage du PDF du suivi
if($result = mysql_query($req)){
	while($ligne = mysql_fetch_array($result)){ 
	       $pdf=new FPDI();
           $pdf->AddPage();
		   $pdf->Image('class/pdf/question.jpg', 0, 0); 
		   $pdf->SetFont('Arial','B',10);
		   $pdf->Text(5, 5, utf8_decode("Médecin : ".$ligne['prenomMed']." ".strtoupper($ligne['nomMed'])));
		   $pdf->Text(5, 10, utf8_decode("Patient : ".$ligne['prenomPatient']." ".strtoupper($ligne['nomPatient'])));
		   $pdf->Text(5, 15, "Date : ".$ligne['date']);
		   $pdf->SetFont('Arial','B',12);
		   $pdf->SetTextColor(220,50,50);
		   if($ligne['questA_1']){
		       $pdf->Text(122, 85, "X");
		   }else{
		         $pdf->Text(167, 85, "X");
		   }
		   if($ligne['questA_2']){
		       $pdf->Text(122, 98, "X");
		   }else{
		         $pdf->Text(167, 98, "X");
		   }
		   if($ligne['questA_3']){
		       $pdf->Text(123, 111, "X");
		   }else{
		         $pdf->Text(167, 111, "X");
		   }
		   if($ligne['questA_4']){
		       $pdf->Text(170, 121, "X");
		   }else{
		         $pdf->Text(187, 121, "X");
		   }
		   if($ligne['questA_5']){
		       $pdf->Text(151, 140, "X");
		   }else{
		         $pdf->Text(183, 140, "X");
		   }
		   if($ligne['questA_6']){
		       $pdf->Text(97, 212, "X");
		   }else{
		         $pdf->Text(138, 212, "X");
		   }
           $pdf->AddPage();
		   $pdf->Image('class/pdf/controle.jpg', 0, 0); 
		   $pdf->SetFont('Arial','B',25);
		   $pdf->Text(179, 106, $ligne['questB_1']);
		   $pdf->Text(179, 133, $ligne['questB_2']);
		   $pdf->Text(179, 171, $ligne['questB_3']);
		   $pdf->Text(179, 203, $ligne['questB_4']);
		   $pdf->Text(179, 230, $ligne['questB_5']);
		   $total = $ligne['questB_1']+$ligne['questB_2']+$ligne['questB_3']+$ligne['questB_4']+$ligne['questB_5'];
		   //Détection type asthme
			if($total>0 && $total<15){
				$pdf->SetTextColor(255,0,0);
				$type = "non controlé";
			}					
			if($total>=15 && $total<20){
				$pdf->SetTextColor(255,102,0);
				$type = "perfectible";
			}
			if($total>=20 && $total<=25){
				$pdf->SetTextColor(0,128,0);
				$type = "controlé";
			}
		   $pdf->Text(179, 251, $total);
		   $pdf->Text(25, 251, "Asthme ".utf8_decode($type));
		   $cmpt++;
		   }
}
if($cmpt!==0){

//Envoi du PDF au navigateur
$pdf->Output('newpdf', 'I'); 
}

?>