<?php
include('config.php');
header("Content-Type: text/xml;charset=ISO-8859-1;");
echo "<?xml version='1.0'?>";
?>
<rapport>
<etat><?php
$nbSendedMail = 0;
$baseSite = "http://www.karuasthme.org/";
$req = "SELECT nomMed, prenomMed, mailMed FROM medecin;";
if($result = mysql_query($req)){
	while($ligne = mysql_fetch_array($result)){ 
			 //Envoie du mail d'info
			 	$headers = "Reply-to: \"Karu Asthme\" <contact@karuasthme.org>\n"; 
				$headers .= "From: \"Karu Asthme\"<contact@karuasthme.org>\n";
     			$headers .= "MIME-Version: 1.0\n";
				$headers .= "Content-Type: text/html; charset=\"utf-8\"\n";
				$headers .= "Content-Transfer-Encoding: quoted-printable";
				$msg = "Bonjour ".ucfirst($ligne['prenomMed'])." ".strtoupper($ligne['nomMed']).",<br/><br/>Pour vous connecter à \"Gestion Patient\" utilisez dorénavant votre adresse et votre mot de passe reçu dans un précédent mail.<br/><br/>";
				$msg .= "Vous pouvez vous connecter dès maintenant en cliquant sur le lien ci-dessous ou en le copiant dans votre navigateur :<br/><br/>";
				$msg .= "<a href&#61;\"".$baseurl."\">".$baseurl."</a>";
				$msg .= "<br/><br/>Cordialement,";
				$msg .= "<br/><br/>Karu Asthme.";
				if((strpbrk ($ligne['mailMed'], "@")))
				{
					if(mail($ligne['mailMed'], 'Accès Espace Professionnel', $msg, $headers)){
						$nbSendedMail++;
					}
				}					
	}
}
echo $nbSendedMail;
?></etat>
</rapport>