<?php session_start();
include 'config.php';

//Récupération de l'id du patient s'il ne s'agit pas d'un nouveau
if($_GET['id']!=0){
	$idPatient = $_GET['id'];
	$enabled = "disabled";
	$req = "SELECT * FROM patient, inclusion WHERE idPatient = $idPatient AND patient.idIncl=inclusion.idIncl;";
	$result = mysql_query($req);
	$ligne = mysql_fetch_array($result);
	$newPat = TRUE;
	
	//Réccupération avant de la date de naissance pour formatage
	$dateNais = explode("-", $ligne['dateNais']);
	$dateIncl = explode("-", $ligne['dateIncl']);
	
}else{
	$enabled = "enabled";
	$newPat = FALSE;
}
?>
<form id="inclusion" name="inclusion" method="post" action="new_patient.php" >
<table class="listPat" style="margin:10px" align="center" width="650px">
<tr>
<td align="center">
<div style="border-bottom:2px dotted #ff7700;padding-bottom:5px;margin:5px" id="title">
Demande d'inclusion <?php if($newPat): ?>faite le <?php echo $dateIncl[2]."/".$dateIncl[1]."/".$dateIncl[0]; endif;?> - Etat : <?php if($ligne['idGesPat']!=0 ):?>Patient inclus<?php else:?>En cours d'inclusion<?php endif;?>
</div><br/>
</td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
<td class="legend2">
N° Sécu*
</td>
<td>
<input <?php echo $enabled; ?> id="noSecu" onkeyup="verifDigit(this);" type="text" name="noSecu" maxlength="15" style="width:118px" value="<?php if($newPat){echo $ligne['noSecuPat'];}?>"/>
</td>
</tr>
<tr>
<td class="legend2">
Lieu de l'inclusion
</td>
<td>
<select name="reseau">
		<OPTION VALUE="PaP" selected="selected">Pointe a Pitre</OPTION>
		<OPTION VALUE="BT">Basse Terre</OPTION>
</select>
</td>
</tr>
<tr>
<td class="legend2">
Civilité
</td>
<td>
<select <?php echo $enabled; ?> name="civilite">
		<OPTION <?php if($newPat){if($ligne['civilitePat'] == 'Mr'){echo "selected";};}?> VALUE="Mr">Mr</OPTION>
		<OPTION <?php if($newPat){if($ligne['civilitePat'] == 'Mme'){echo "selected";};}?> VALUE="Mme">Mme</OPTION>
		<OPTION <?php if($newPat){if($ligne['civilitePat'] == 'Mlle'){echo "selected";};}?> VALUE="Mlle">Mlle</OPTION>
		<OPTION <?php if($newPat){if($ligne['civilitePat'] == 'Enfant'){echo "selected";};}?> VALUE="Enfant">Enfant</OPTION>
</select>
</td>
</tr>
<tr>
<td class="legend2">
Nom*
</td>
<td>
<input <?php echo $enabled; ?> id="nom" type="text" name="nom" maxlength="128" size="32" value="<?php if($newPat){echo utf8_decode($ligne['nomPatient']);}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Prenom*
</td>
<td>
<input <?php echo $enabled; ?> id="prenom" type="text" name="prenom" maxlength="128" size="32" value="<?php if($newPat){echo utf8_decode($ligne['prenomPatient']);}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Rue
</td>
<td>
<input <?php echo $enabled; ?> id="adresse" type="text" name="adresse" maxlength="256" size="32" value="<?php if($newPat){echo utf8_decode($ligne['ruePat']);}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Ville
</td>
<td>
<input <?php echo $enabled; ?> id="ville" type="text" name="ville" maxlength="128" size="32" value="<?php if($newPat){echo utf8_decode($ligne['villePat']);}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Code Postal*
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" id="cp" type="text" name="cp" maxlength="5" size="5" value="<?php if($newPat){echo $ligne['cpPat'];}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Date de naissance*
</td>
<td>
<input <?php echo $enabled; ?> id="JJ" onkeyup="verifDigit(this);" type="text" name="JJ" maxlength="2" style="width:17px" value="<?php if($newPat){echo $dateNais[2];}?>"/>&nbsp;/&nbsp;<input <?php echo $enabled; ?> id="MM" onkeyup="verifDigit(this);" type="text" name="MM" maxlength="2" style="width:17px" value="<?php if($newPat){echo $dateNais[1];}?>"/>&nbsp;/&nbsp;<input <?php echo $enabled; ?> id="AA" onkeyup="verifDigit(this);" type="text" name="AA" maxlength="4" style="width:34px" value="<?php if($newPat){echo $dateNais[0];}?>"/>
</td>
<td>
JJ/MM/AAAA
</td>
</tr>
<tr>
<td class="legend2">
Coordonné téléphonique*<br/>(fixe ou portable)
</td>
<td>
<input <?php echo $enabled; ?> id="tel" onkeyup="verifDigit(this);" type="text" name="tel" maxlength="10" style="width:77px" value="<?php if($newPat){echo $ligne['telPat'];}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Sévérité
</td>
<td>
<input <?php echo $enabled; ?> id="severite" type="radio" name="severite" value="2" <?php if($newPat){if($ligne['severitePat'] == 2){echo "checked";};}?> />&nbsp;2&nbsp;&nbsp;<input <?php echo $enabled; ?> id="severite" type="radio" name="severite" value="3" <?php if($newPat){if($ligne['severitePat'] == 3){echo "checked";};}?> />&nbsp;3&nbsp;&nbsp;<input <?php echo $enabled; ?> id="severite" type="radio" name="severite" value="4" <?php if($newPat){if($ligne['severitePat'] == 4){echo "checked";};}?> />&nbsp;4
</td>
</tr>
<tr>
<td class="legend2">
Nombre de gênes/Mois
</td>
<td>
<input <?php echo $enabled; ?> id="nbgene" onkeyup="verifDigit(this);" type="text" name="nbgene" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['nbGene'];}?>" />
</td>
</tr>
</table>
</td>
</tr>
<td>
<table width="100%">
<tr>
<td  class="legend2" width="33%">
Traitement de crise*
</td>
<td class="legend2" width="33%">
Traitement de fond*
</td>
<td class="legend2" width="33%">
Autres
</td>
</tr>
<tr>
<td width="33%">
<textarea <?php echo $enabled; ?> id="traitCrise" rows="4" name="traitCrise" ><?php if($newPat){echo utf8_decode($ligne['traitCrise']);}?></textarea>
</td>
<td width="33%">
<textarea <?php echo $enabled; ?> id="traitFond" rows="4" name="traitFond"><?php if($newPat){echo utf8_decode($ligne['traitFond']);}?></textarea>
</td>
<td width="33%">
<textarea <?php echo $enabled; ?> id="autres" rows="4" name="autres"><?php if($newPat){echo utf8_decode($ligne['traitAutre']);}?></textarea>
</td>
</tr>
<tr>
<td class="legend2">
Consultations aux Urgences dans l'année précédente
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" onclick="change(this,'');" type="text" name="consulPrec" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['consulPrec'];}else{echo 0;}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Hospitalisations dans l'année précédente
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" type="text" name="hospPrec" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['hospPrec'];}else{echo 0;}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Nombre de consultations non programmées
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" type="text" name="consultNoProg" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['consultNoProg'];}else{echo 0;}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Nombre d'arrêts de travail
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" type="text" name="nbStopTrav" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['nbStopTrav'];}else{echo 0;}?>" />
</td>
<td class="legend2">
Durée
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" type="text" name="dureeStopTrav" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['dureeStopTrav'];}else{echo 0;}?>" />
</td>
</tr>
<tr>
<td class="legend2">
Nombre d'absence scolaire
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" type="text" name="nbStopSchool" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['nbStopSchool'];}else{echo 0;}?>" />
</td>
<td class="legend2">
Durée
</td>
<td>
<input <?php echo $enabled; ?> onkeyup="verifDigit(this);" type="text" name="dureeStopSchool" maxlength="3" size="3" value="<?php if($newPat){echo $ligne['dureeStopSchool'];}else{echo 0;}?>" />
</td>
</tr>
</table>
</td>
<tr>
<td align="center">
<br /><br /><input <?php if($newPat){echo 'style="display:none"';} ?> type="button" onclick="verifChamps('noSecu;nom;prenom;cp;JJ;MM;AA;traitCrise;traitFond;tel', 'document.inclusion.submit();');" value="Demander l'inclusion" /><br /><br />
</td>
</tr>
</table>
</form>