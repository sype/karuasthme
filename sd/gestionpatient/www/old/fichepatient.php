<?php

$id = $_GET['idPatient'];
include('config.php');

$req = "SELECT* FROM patient, inclusion WHERE idPatient = ".$id." AND patient.idIncl = inclusion.idIncl";

header("Content-Type: text/xml;charset=ISO-8859-1;");
echo "<?xml version='1.0'?>";
?>
<liste>
<?php
$cmpt = 0;
$tab = array("\'", "<br/>");
$tab2 = array("'", "\n");
if($result = mysql_query($req)):
	while($ligne = mysql_fetch_array($result)):
?>
		   <patient>
		   <noSecu><?php echo $ligne['noSecuPat']; ?></noSecu>
		   <civilite><?php echo $ligne['civilitePat']; ?></civilite>
		   <nom><?php echo utf8_decode($ligne['nomPatient']); ?></nom>
		   <prenom><?php echo utf8_decode($ligne['prenomPatient']); ?></prenom>
		   <rue><?php echo utf8_decode($ligne['ruePat']); ?></rue>
		   <cp><?php echo $ligne['cpPat']; ?></cp>
		   <ville><?php echo utf8_decode($ligne['villePat']); ?></ville>
		   <dateNais><?php echo $ligne['dateNais']; ?></dateNais>
		   <tel><?php echo $ligne['telPat']; ?></tel>
		   <port><?php echo $ligne['portPat']; ?></port>
		   <severite><?php echo $ligne['severitePat']; ?></severite>
		   <nbGene><?php echo $ligne['nbGene']; ?></nbGene>
		   <traitCrise><?php echo str_replace($tab, $tab2, utf8_decode($ligne['traitCrise'])); ?></traitCrise>
		   <traitFond><?php echo str_replace($tab, $tab2, utf8_decode($ligne['traitFond'])); ?></traitFond>
		   <traitAutre><?php echo str_replace($tab, $tab2, utf8_decode($ligne['traitAutre'])); ?></traitAutre>
		   <consulPrec><?php echo $ligne['consulPrec']; ?></consulPrec>
		   <hospPrec><?php echo $ligne['hospPrec']; ?></hospPrec>
		   <idMed><?php echo $ligne['idMed']; ?></idMed>
		   <consultNoProg><?php echo $ligne['consultNoProg']; ?></consultNoProg>
		   <nbStopTrav><?php echo $ligne['nbStopTrav']; ?></nbStopTrav>
		   <dureeStopTrav><?php echo $ligne['dureeStopTrav']; ?></dureeStopTrav>
		   <nbStopSchool><?php echo $ligne['nbStopSchool']; ?></nbStopSchool>
		   <dureeStopSchool><?php echo $ligne['dureeStopSchool']; ?></dureeStopSchool>
		   </patient>
<?php
		   $cmpt++;
    endwhile;
endif;
if($cmpt == 0):
endif;
?>
</liste>