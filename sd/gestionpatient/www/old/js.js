﻿//Change le curseur en fonction de type
function cursor(type){
	switch(type){
		case 1:
		document.body.style.cursor='pointer';
		break;
		
		case 0:
		document.body.style.cursor='default';
		break;
	}
}

//Change la page en fonction de state:page en cours et butt:boutton appuyé
function change(state, butt){
	if(state==0 && butt==0){
		document.location.href="accueil.php"
	}else if(state==0 && butt==1){
		document.getElementById('title').innerHTML = "Test de controle de l'asthme";
		document.getElementById('subTitle').innerHTML = "découvrez si votre asthme est controlé ou non";
		document.getElementById('divButtonRight').innerHTML = "<br/><br/><br/>terminé";
		document.getElementById('divButtonLeft').innerHTML = "<br/><br/><br/>suivi patient";
		document.getElementById('questionPartA').style.display = 'none';
		document.getElementById('questionPartB').style.display = 'block'
		document.getElementById('state').value = 1;
	}else if(state==1 && butt==0){
		document.getElementById('divButtonRight').innerHTML = "<br/><br/><br/>test de contrôle";
		document.getElementById('divButtonLeft').innerHTML = "<br/><br/><br/>annuler";
		document.getElementById('title').innerHTML = "Questionnaire de suivi du patient asthmatique";
		document.getElementById('subTitle').innerHTML ="Par le médecin traitant";
		document.getElementById('questionPartB').style.display = 'none';
		document.getElementById('questionPartA').style.display = 'block';
		document.getElementById('state').value = 0;
	}else if(state==1 && butt==1){
		var st1 = false;
		var st2  = false;
		var i = 1;
		//Vérifie si toutes les questions ont été traitées
		for(i = 1;i<7;i++){
			if(!document.getElementById('questA_'+i).checked && !document.getElementById('questA_'+i+'b').checked){
				st1 = true;
			}
		}
		for(i = 1;i<6;i++){
			if(document.getElementById('questB_'+i).value == 0){
				st2 = true;
			}
		}
		if(st1 || st2){
			alert('Veuillez vous assurer d\'avoir répondu à toutes les questions avant de valider');
		}else{
			document.suivi.submit();
		}
	}
	
}

//Gère le questionnaire partie B
//state:position de la souris, id:id de la div, part:question en cours, no: valeur de la réponse
function quest(state, id, part, no){
	switch(state){
		case 1:
		cursor(1);
		document.getElementById(id).style.backgroundColor = bgColor(no);
		break;
		case 0:
		cursor(0);
		document.getElementById(id).style.backgroundColor = "";
		break;
		case 2:
		old = 'divQuest'+part+'_';
		if(document.getElementById('quest'+part)){
			old+=document.getElementById('quest'+part).value;
		}
		if(document.getElementById(old)){
			document.getElementById(old).style.backgroundColor = "";
		}
		document.getElementById(id).style.backgroundColor = bgColor(no);
		document.getElementById('quest'+part).value = no;
		document.getElementById('pointQuest'+part).innerHTML = no;
		sum = 0;
		for(var i=1;i<6;i++){
			sum += parseInt(document.getElementById('questB_'+i).value);
		}
		document.getElementById('total').innerHTML = sum;
		asthmeState(sum);
		break;
	}
}


//Permet de changer le background de la div en fonction de no
function bgColor(no){
	switch(no){
		case 1:
		color = "#ff0000";
		break;
		case 2:
		color = "#FF6600";
		break;
		case 3:
		color = "#FFCC00";
		break;
		case 4:
		color = "#FFFF33";
		break;
		case 5:
		color = "#00CC00";
		break;
	}
	return color;
}

//Effectue la création de l'objet XMLHttpRequest
function startAjax(){
	var xhr_object = null;
	//Création de l'objet XMLHttpRequest (AJAX) en fct du navigateur 
	if(window.XMLHttpRequest)// Firefox
	return xhr_object = new XMLHttpRequest();
	else if(window.ActiveXObject)// Internet Explorer   
	return xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non supporté par le navigateur   
	alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");   
	return;
	}
}

//Affiche le contenu en fonction de numCont et transmet la variable id au script php et chp
function getContain(numCont,id,text){
	var content;
	switch(numCont){
		case 0:
		content = "liste_patient.php";
		break;
		case 1:
		content = "liste_suivi.php";
		break;
		case 2:
		content = "del_suivi.php";
		break;
		case 3:
		content = "configuration.php";
		break;
		case 4:
		content = "inclusion.php";
		break;
		case 5:
		content = "new_patient.php";
		break;
		case 6:
		content = "form_inclusion.php";
		break;
		case 7:
		content = "document.php";
		break;
	}
	if(text){
		chp = '&text='+text;
		}else{
				chp='';
		}
	var xhr_object = startAjax();
	xhr_object.open("POST", content+"?id="+id+chp, true);  
	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 1) {
			document.getElementById('content').innerHTML = '<img style="margin-left:200px" align="middle" src="images/loading.gif" width="320" height="320">';
		}
		if(xhr_object.readyState == 4) {
			document.getElementById('content').innerHTML = xhr_object.responseText;
		}
	}
	xhr_object.send(null);
}

//fonction de demande de confirmation
function msgBox(action,next){
	if(confirm('Veuillez confirmer')){
		eval(action);
		eval(next);
	}
}

//fonction qui determine le type d'asthme
function asthmeState(val){
	box = document.getElementById('asthmeState');
	box.style.padding = '2px';
	if(val>0 && val<15){
		box.style.color = 'red';
		box.innerHTML = 'Non controlé';
	}
	if(val>=15 && val<20){
		box.style.color = '#FF6600';
		box.innerHTML = 'Perfectible';
	}
	if(val>=20 && val<=25){
		box.style.color = 'green';
		box.innerHTML = 'Controlé';
	}
}

//fonction de vérification de champs
function verifChamps(chp,action){
	var tabChp = chp.split(";");
	var i = 0;
	var state = 0;
	for(i; i<tabChp.length;i++){
		document.getElementById(tabChp[i]).style.border = "1px solid gray";
		if(document.getElementById(tabChp[i]).value == ""){
			document.getElementById(tabChp[i]).style.border = "1px solid red";
			state = 1;
		}
	}
	if(state == 1){
		alert('Veuillez renseigner le(s) champs encadré(s) en rouge');
	}else{
		eval(action);
	}
}

//fonction qui oblige l'insertion d'un caractère numérique
function verifDigit(chp){
	 var car = chp.value.charAt(chp.value.length-1);
	if(isNaN(car)){
		chp.value = chp.value.replace(car,"");
	}
}

//Compare deux champs
function compare(a, b, msg, action){
	if(document.getElementById(a).value != document.getElementById(b).value){
		alert(msg);
	}else{
		eval(action);
	}
}
