<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Karu Astme - Gestion Patient</title>
</head>
<link href="style.css" rel="stylesheet" type="text/css"/>
<script src="js.js" language="JavaScript" type="text/JavaScript"></script>
<body>
<form id="login" name="login" method="post" action="login.php" onkeypress="if(event.keyCode == 13){this.valid.click()}">
<table class="loginTable" align="center" width="300px">
<tr>
<td align="center" style="border-bottom:2px dotted #999999;border-top:2px dotted #999999;">
<span style="color:#0066FF;font-weight:bold">Karu Astme - Gestion Patient</span>
</td>
</tr>
<tr>
<td>
<table>
<tr>
<td>Email :</td>
<td><input id="email" style="width:200px;" type="text" name="email" maxlength="256" size="16"/></td>
</tr>
<tr>
<td>Mot de passe :</td>
<td><input id="pwd" style="width:125px;" type="password" name="pwd" maxlength="128" size="18"/></td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="center">
<br/><input id="valid" type="button" value="Connexion" onclick="verifChamps('email;pwd', 'document.login.submit()');" />
</td>
</tr>
<tr>
<td align="center">
<a href="forget.php">Identifiants oubliés ?</a>
</td>
</tr>
<tr>
<td align="center">
<br/><span style="font-size:10px;color:#999999;text-decoration:none">©2010 <a target="_blank" style="text-decoration:none;color:#999999" href="http://www.karuasthme.org">Karu Asthme</a> - Réalisation : <a target="_blank" style="text-decoration:none;color:#999999" href="http://www.pisystems.fr">PI Systems</a></span>
</td>
</tr>
</table>
</form>
</body>
</html>
