<?php 
session_start(); 
if(isset($_SESSION['userId'])){
	$userId = $_SESSION['userId'];
	$userName = $_SESSION['userName'];
	}else{
		header('location:index.php');
}

include("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $sitename; ?> - <?php echo htmlentities($userName); ?></title>
<link href="style.css" rel="stylesheet" type="text/css"/>
<script src="js.js" language="JavaScript" type="text/JavaScript"></script>
</head>

<body>
<table class="generalTable" width="750" align="center" cellspacing="0" cellpadding="0">
<tr>
<td height="80">
<!-- En tête informations -->
<table  width="100%">
<tr>
<td align="left" style="font-weight:bold">Utilisateur : <span style="color:#006699"><?php echo ucfirst($userName); ?></span><br/><br/></td>
<td align="right">
<span style="font-weight:bold;color:#0066FF;border-bottom:2px dotted #0066FF;"><?php echo $sitename; ?></span>
</td>
</tr>
</table>
<!-- Menu -->
<table class="menu" width="100%">
<tr>
<td align="center">
<a href="#" onclick="getContain(0,<?php echo $userId; ?>);">Patients</a>
</td>
<td align="center">
<a href="#" onclick="getContain(4,<?php echo $userId; ?>);">Inclusion</a>
</td>
<td align="center">
<a href="#" onclick="getContain(7,<?php echo $userId; ?>);">Documents</a>
</td>
<td align="center">
<a href="#" onclick="getContain(3,<?php echo $userId; ?>);">Configuration</a>
</td>
<td align="center">
<a href="logout.php">Déconnexion</a>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td height="100%">
<!--Div qui va contenir le contenu chargé en AJAX-->
<div id="content">
</div>
</td>
</tr>
<tr>
<td align="center">
<br/><span style="font-size:10px;color:#999999;text-decoration:none">©2010 <a target="_blank" style="text-decoration:none;color:#999999" href="http://www.karuasthme.org">Karu Asthme</a> - Réalisation : <a target="_blank" style="text-decoration:none;color:#999999" href="http://www.pisystems.fr">PI Systems</a></span>
</td>
</tr>
</table>
</body>
<script language="JavaScript" type="text/JavaScript">getContain(0,<?php echo $userId; ?>);</script>
</html>
