<?php
/**
 * Tool SLS_HttpRequest - Http Request Treatment
 *  
 * @author Florian Collot
 * @copyright SillySmart
 * @package Sls.Generics.Url
 * @since 1.0  
 */
class SLS_HttpRequest 
{
	private $_params;
					
	/**
	 * Constructor
	 *
	 * @access public
	 * @since 1.0
	 */
	public function __construct()
	{
		$this->_params = array_merge($_POST,$_GET,$_FILES);		
		
		// We test if we have an extension in the URL
		if (substr($this->_params['smode'], (strlen($this->_params['smode'])-5), 1) == "." || substr($this->_params['smode'], (strlen($this->_params['smode'])-4), 1) == ".") 
		{
			(substr($this->_params['smode'], (strlen($this->_params['smode'])-4), 1) == ".") ? $this->_params['smode'] = substr($this->_params['smode'], 0, (strlen($this->_params['smode'])-4)) : false;
			(substr($this->_params['smode'], (strlen($this->_params['smode'])-5), 1) == ".") ? $this->_params['smode'] = substr($this->_params['smode'], 0, (strlen($this->_params['smode'])-5)) : false;
		}
		$explode = explode("/", $this->_params['smode']);
		$this->_params['smode'] = array_shift($explode);
		if (count($explode) > 1)
		{
			$tabletmp = array_chunk($explode, 2);
			foreach($tabletmp as $value) 			
				$this->_params[$value[0]] = $value[1];
		}		
	}
	
	/**
	 * Return a value
	 *
	 * @access public
	 * @param string $key the parameter key
	 * @param string $type the type of the key requested. should be POST, GET, FILE
	 * @return string $param the parameter
	 * @since 1.0
	 */
	public function getParam($key, $type=null)
	{
		if ($type == null)	
		{	
			if(isset($this->_params[$key]))
				return $this->_params[$key];
		}
		else 
		{
			$type = strtoupper($type);
			if ($type != 'POST' && $type != 'GET' && $type != 'FILE')
				SLS_Tracing::addTrace(new Exception("The parameter 'type' should be 'POST', 'GET' or 'FILE'. Current value = ".var_dump($type)));
			else 
			{
				switch($type)
				{
					case 'POST':
						return $_POST[$key];
						break;
					case 'GET':
						return $_GET[$key];
						break;
					case 'FILE':
						return $_FILE[$key];
						break;
				}
			}
		}		
	}
	
	/**
	 * Return all parameters (GET, POST or FILES)
	 *
	 * @access public	 
	 * @param string $type the type you want ('ALL','POST','GET','FILES')
	 * @return array $params array of all paramters
	 * @since 1.0
	 * @example 
	 * var_dump($this->_http->getParams());
	 * // will produce :
	 * array(
  	 * 		"mode"		=> "Home",
  	 * 		"smode"		=> "Welcome",
  	 * 		"..."		=> "..."
	 * )
	 */
	public function getParams($type='all') 
	{
		$type = strtoupper($type);
		if ($type != 'ALL' && $type != 'POST' && $type != 'GET' && $type != 'FILES')
			SLS_Tracing::addTrace(new Exception("To use the method SLS_HttpRequest::getParams(), you need to specify a correct type of value ('all', 'post', 'get' or 'files')"));
		else
		{
			if ($type == 'ALL') 
				return $this->_params;
			elseif ($type == 'POST')
				return $_POST;
			elseif ($type == 'GET')
				return $_GET;
			elseif ($type == 'FILES')
				return $_FILES;
			else 
				return $this->_params;
		}
	}
}
?>