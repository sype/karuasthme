<?
/**
 * Tool SLS_Tracing - Application Logging
 *  
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package Sls.Generics.Tools
 * @since 1.0 
 */ 
class SLS_Tracing 
{	
	static $_exceptionThrown = false;
	
	/**
	 * Add a trace in the application :
	 * - If server is in production, we write into the logs
	 * - If server is in development, output is used	 
	 *
	 * @access public static
	 * @param Exception $exception the raised exception contained the message to trace
	 * @param bool $stack flag if we want the stacktrace call
	 * @param string $complementary html complementary description
	 * @since 1.0
	 */
	public static function addTrace($exception,$stack=true,$complementary="")
	{		
		$generic = SLS_Generic::getInstance();
		$e = $exception->getTrace();	
					
		// If logs
		if ($generic->isProd())
		{
			$nbOccurencesFiles = 0;
			$nbMaxLines = 2000;
			$directory = date("Y-m");
			$fileName = date("Y-m-d");
			$filePath = ""; 
			
			$traces = SLS_Tracing::stackTracing($e,false);
			
			// If month directory doesn't exists, create it			
			if (!file_exists($generic->getPathConfig("logs").$directory))			
				mkdir($generic->getPathConfig("logs").$directory,0777);					
			
			// Count the number of hits of log file			
			$handle = opendir($generic->getPathConfig("logs").$directory);
			while (false !== ($file = readdir($handle)))			
				if (SLS_String::startsWith($file,$fileName))
					$nbOccurencesFiles++;
	    	closedir($handle);
	    
	    	// If the current file log doesn't exists, create it		    
		    if ($nbOccurencesFiles == 0)
		    {
		    	touch($generic->getPathConfig("logs").$directory."/".$fileName."_0.log");
		    	$filePath = $generic->getPathConfig("logs").$directory."/".$fileName."_0.log";
		    }
		    // Else, locate it
		    else	    
		    	$filePath = $generic->getPathConfig("logs").$directory."/".$fileName."_".($nbOccurencesFiles-1).".log";
		    
		    // If the max number of lines has been reach, increase the file log version		    
		    if (SLS_String::countLines(file_get_contents($filePath)) >= $nbMaxLines)
		    {
		    	touch($generic->getPathConfig("logs").$directory."/".$fileName."_".$nbOccurencesFiles.".log");
		    	$filePath = $generic->getPathConfig("logs").$directory."/".$fileName."_".$nbOccurencesFiles.".log";
		    }
		    
		    // Then, if and only if the file log has been located, increased or created : write into the logs		    
		    if (is_file($filePath))
		    {
		    	$oldContentLog = file_get_contents($filePath);
		    	$newContentLog = date("Y-m-d H:i:s").' - '.$exception->getMessage()."\n";
		    	
		    	for ($i=0 ; $i<count($traces) ; $i++)	    	
		    		$newContentLog .= "    ".trim($traces[$i]["call"]." ".$traces[$i]["file"])."\n";
		    		
		    	file_put_contents($filePath,$newContentLog.$oldContentLog); 
		    }	    
		}
		
		// Else output
		else 
		{	  
			$traces = SLS_Tracing::stackTracing($e,true);  	
			if (!SLS_Tracing::$_exceptionThrown)
			{
		    	echo 
					'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n".
					'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">'."\n".
						'<head>'."\n".
						'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'."\n";
				SLS_Tracing::makeCss();
				SLS_Tracing::loadMootools();
				echo 
						'</head>'."\n".
						'<body>'."\n";
			}
			echo 
						'<div class="mainTitle">'.date("Y-m-d H:i:s").' - '.$exception->getMessage().''."\n".
						'<div>in '.SLS_Tracing::getTraceInfo($e[0],true).'</div></div>'."\n";
			
			if ($stack)
			{
				echo '<div class="stackTrace">'."\n";
				for($i=0 ; $i<count($traces) ; $i++)
				{
					$style = ($i<(count($traces)-1)) ? 'border-bottom:1px dotted darkgray;padding-bottom:2px;' : '';
					echo '<div style="'.$style.'">'.$traces[$i]["call"].'<span>'.$traces[$i]["file"].'</span></div>'."\n";
				}		
				echo '</div>'."\n";
			}
			if ($complementary != "")
			{
				echo $complementary;
			}
			
			//echo 
					'</body>'."\n".
			 	'</html>'."\n";
			SLS_Tracing::$_exceptionThrown = true;
		}
	}
	
	/**
	 * Display All Traces
	 *
	 * @access public static
	 * @since 1.0
	 */
	public static function displayTraces()
	{
		if (SLS_Tracing::$_exceptionThrown && !SLS_Generic::getInstance()->isProd())
		{
			echo 
					'</body>'."\n".
			 	'</html>'."\n";
			die();
		}
	}
	
	/**
	 * Get the stack tracing of the exception	 
	 *
	 * @access public static
	 * @param array $trace trace array recovered at the Exception raise
	 * @param bool $html if we want a HTML render (version dev)
	 * @return array $stack strack tracing shaped (html or plain)
	 * @since 1.0
	 */
	public static function stackTracing($trace,$html=false)
	{
		$stack = array();						
		foreach ($trace as $k => $line) 
		{
			$callInfo = ($k < count($trace) - 1) ? SLS_Tracing::getTraceInfo($trace[$k + 1],$html) : "";
			$stack[$k]["call"] = $callInfo . ' (' .basename($line['file']) .':'.$line['line'].')';
			$stack[$k]["file"] = 'in file '.$line['file'].' at line '.$line['line'];
		}
		return $stack;
	}
	
	/**
	 * Get trace infos	 
	 *
	 * @access public static
	 * @param array $traceElt associative array contained trace informations recovered by the raise of the exception
	 * @param bool $html if we want a HTML render (version dev)
	 * @return string $trace trace infos shaped (html ou plain)
	 * @since 1.0
	 */
	public static function getTraceInfo($traceElt,$html=false) 
	{
		$info = $traceElt['class'] . $traceElt['type'] . $traceElt['function'];
		$info .= '(';
		if ($traceElt['args']) 
		{			
			for ($i = 0; $i < count($traceElt['args']); $i++) 
			{
				$arg = $traceElt['args'][$i];
				$value = (is_array($arg)) ? "<pre>".SLS_String::printArray($arg)."</pre>" : $arg;
				if ($html)
					$info .= is_object($arg) ? get_class($arg) : (is_string($value) && SLS_String::startsWith(SLS_String::trimString($value), "<")) ? gettype($arg) : '<a href="#" onclick="return false;" class="tooltipB" title="'.gettype($arg).'" rel="'.$value.'">'.gettype($arg).'</a>';
				else
					$info .= is_object($arg) ? get_class($arg) : ''.gettype($arg).'';
				if ($i < count($traceElt['args']) - 1)
					$info .= ', ';				
			}
		}
		$info .= ')';
		return $info;
	}

	/**
	 * Construct the css in the output case	 
	 *
	 * @access public static
	 * @since 1.0
	 */
	public static function makeCss()
	{
		echo 
			'<style type="text/css">
			body {
				font-family:Verdana;
			}
			pre {
				font-family:Verdana;
				font-size:12px;
				margin: 0 40px;
				padding: 10px;
			}
			h2 {
				font-size:18px;
				font-weight:bold;
			}
			div.mainTitle {
				display:block;
				font-size:18px;
				font-weight:bold;
				margin-bottom:10px;
			}
			div.mainTitle div {
				display:block;
				font-family:Verdana;
				font-size:12px;
				font-style: italic;
				color:#000080;
			}
			div.stackTrace {
				display:block;
				margin: 0 40px;
				padding: 10px;
				border:1px solid #000;
			}
			div.stackTrace div {
				display:block;
				font-family:Verdana;
				font-size:12px;
				padding-top:2px;
			}
			div.stackTrace div span{
				display:block;
				margin-left:10px;
				font-family:Verdana;
				font-size:12px;
				font-style: italic;
				color:gray;
			}			
			.custom_tip .tip {
				color: #000;
				width: 400px;
				z-index: 13000;
			}		 
			.custom_tip .tip-title {
				font-weight: bold;
				font-size: 11px;
				margin: 0;
				color: #3E4F14;
				padding: 8px 8px 4px;
				background: #C3DF7D;
				border-bottom: 1px solid #B5CF74;
			}			 
			.custom_tip .tip-text {
				font-size: 11px;
				padding: 4px 8px 8px;
				background: #CFDFA7;
			}
			.dp-highlighter{font-family:"Consolas","Courier New",Courier,mono,serif;font-size:12px;background-color:#E7E5DC;width:99%;overflow:auto;margin:18px 0 18px 0 !important;padding-top:1px}.dp-highlighter ol,.dp-highlighter ol li,.dp-highlighter ol li span{margin:0;padding:0;border:none}.dp-highlighter a,.dp-highlighter a:hover{background:none;border:none;padding:0;margin:0}.dp-highlighter .bar{padding-left:45px}.dp-highlighter.collapsed .bar,.dp-highlighter.nogutter .bar{padding-left:0px}.dp-highlighter ol{list-style:decimal;background-color:#fff;margin:0px 0px 1px 45px !important;padding:0px;color:#5C5C5C}.dp-highlighter.nogutter ol,.dp-highlighter.nogutter ol li{list-style:none !important;margin-left:0px !important}.dp-highlighter ol li,.dp-highlighter .columns div{list-style:decimal-leading-zero;list-style-position:outside !important;border-left:3px solid #6CE26C;background-color:#F8F8F8;color:#5C5C5C;padding:0 3px 0 10px !important;margin:0 !important;line-height:14px}.dp-highlighter.nogutter ol li,.dp-highlighter.nogutter .columns div{border:0}.dp-highlighter .columns{background-color:#F8F8F8;color:gray;overflow:hidden;width:100%}.dp-highlighter .columns div{padding-bottom:5px}.dp-highlighter ol li.alt{background-color:#FFF;color:inherit}.dp-highlighter ol li span{color:black;background-color:inherit}.dp-highlighter.collapsed ol{margin:0px}.dp-highlighter.collapsed ol li{display:none}.dp-highlighter.printing{border:none}.dp-highlighter.printing .tools{display:none !important}.dp-highlighter.printing li{display:list-item !important}.dp-highlighter .tools{padding:3px 8px 3px 10px;font:9px Verdana,Geneva,Arial,Helvetica,sans-serif;color:silver;background-color:#f8f8f8;padding-bottom:10px;border-left:3px solid #6CE26C}.dp-highlighter.nogutter .tools{border-left:0}.dp-highlighter.collapsed .tools{border-bottom:0}.dp-highlighter .tools a{font-size:9px;color:#a0a0a0;background-color:inherit;text-decoration:none;margin-right:10px}.dp-highlighter .tools a:hover{color:red;background-color:inherit;text-decoration:underline}.dp-about{background-color:#fff;color:#333;margin:0px;padding:0px}.dp-about table{width:100%;height:100%;font-size:11px;font-family:Tahoma,Verdana,Arial,sans-serif !important}.dp-about td{padding:10px;vertical-align:top}.dp-about .copy{border-bottom:1px solid #ACA899;height:95%}.dp-about .title{color:red;background-color:inherit;font-weight:bold}.dp-about .para{margin:0 0 4px 0}.dp-about .footer{background-color:#ECEADB;color:#333;border-top:1px solid #fff;text-align:right}.dp-about .close{font-size:11px;font-family:Tahoma,Verdana,Arial,sans-serif !important;background-color:#ECEADB;color:#333;width:60px;height:22px}.dp-highlighter .comment,.dp-highlighter .comments{color:#008200;background-color:inherit}.dp-highlighter .string{color:blue;background-color:inherit}.dp-highlighter .keyword{color:#069;font-weight:bold;background-color:inherit}.dp-highlighter .preprocessor{color:gray;background-color:inherit}
			</style>
			';		
	}
	
	/**
	 * Load Mootools in the output case	 
	 *
	 * @access public static
	 * @since 1.0
	 */
	public static function loadMootools()
	{
		$generic = SLS_Generic::getInstance();
		echo '<script src="'.$generic->getProtocol().'://'.$generic->getSiteConfig("domainName").'/'.$generic->getPathConfig("coreJsStatics").'mootools.js" type="text/javascript"></script>'."\n";		
		echo "<script>\n
					window.addEvent('domready', function(){\n
						var customTipsB = $$('.tooltipB');\n
						var toolTipsB = new Tips(customTipsB, {\n
						    className: 'custom_tip'\n
						});\n
					});\n
					</script>
					\n";
		echo '<script src="'.$generic->getProtocol().'://'.$generic->getSiteConfig("domainName").'/'.$generic->getPathConfig("coreJsStatics").'hightlight.js" type="text/javascript"></script>'."\n";		
	}
}
?>