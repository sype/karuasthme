<?php
/**
 * SLS_BoRights Tool - Check authentification and authorizations in customer back-office
 *  
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package SLS.Generics.Tools.SLS_Bo
 * @since 1.0 
 */
class SLS_BoRights
{
	/**
	 * Check if an admin is authorized to log in
	 *
	 * @access public static
	 * @param string $login login
	 * @param string $pwd password
	 * @return bool true if connected, else false
	 * @since 1.0
	 */
	public static function connect($login,$pwd)
	{
		$generic = SLS_Generic::getInstance();
		$session = $generic->getObjectSession();
		$sessionToken = substr(substr(sha1($generic->getSiteConfig("privateKey")),12,31).substr(sha1($generic->getSiteConfig("privateKey")),4,11),6);
		
		$pathsHandle = file_get_contents($generic->getPathConfig("configSls")."/rights.xml");
		$xmlRights = new SLS_XMLToolbox($pathsHandle);		
		$result = array_shift($xmlRights->getTagsAttributes("//sls_configs/entry[@login='".($login)."' and @password='".sha1($pwd)."']",array("right")));
		
		if (!empty($result))
		{
			$session->setParam("SLS_BO_VALID_".$sessionToken,"true");	
			$session->setParam("SLS_BO_USER_".$sessionToken,$login);
			$session->setParam("SLS_BO_USER",$login);
			$session->setParam("SLS_BO_RIGHT_".$sessionToken,$result["attributes"][0]["value"]);
			return true;
		}
		else
			return false;
	}
	
	/**
	 * Disconnect an admin
	 * 
	 * @access public static
	 * @since 1.0
	 */
	public static function disconnect()
	{		
		$generic = SLS_Generic::getInstance();
		$session = $generic->getObjectSession();
		$sessionToken = substr(substr(sha1($generic->getSiteConfig("privateKey")),12,31).substr(sha1($generic->getSiteConfig("privateKey")),4,11),6);
		
        $session->delParam("SLS_BO_VALID_".$sessionToken);
        $session->delParam("SLS_BO_USER_".$sessionToken);
        $session->delParam("SLS_BO_RIGHT_".$sessionToken);
	}
	
	/**
	 * Check if an admin is logged
	 *
	 * @access public static
	 * @return bool true if logged, else false
	 * @since 1.0
	 */
	public static function isLogged()
	{
		$generic = SLS_Generic::getInstance();
		$session = $generic->getObjectSession();		
		$sessionToken = substr(substr(sha1($generic->getSiteConfig("privateKey")),12,31).substr(sha1($generic->getSiteConfig("privateKey")),4,11),6);
		
		if ($session->getParam("SLS_SESSION_VALID_".$sessionToken) == "true")
		{
			$session->setParam("SLS_BO_VALID_".$sessionToken,"true");
			$session->setParam("SLS_BO_USER_".$sessionToken,$session->getParam('SLS_SESSION_USER_'.$sessionToken));
			$session->setParam("SLS_BO_RIGHT_".$sessionToken,"RAMD");
		}		
		if ($session->getParam("SLS_BO_VALID_".$sessionToken) == "true")
			return true;
		else
			return false;
	}
	
	/**
	 * Check if the current admin is authorized to access on this back-office action
	 *
	 * @access public static
	 * @param string $type the type of action ('List'|'Add'|'Modify'|'Delete')
	 * @return int $authorized -1 if not logged, 0 is not authorized, 1 if ok
	 * @since 1.0
	 */
	public static function isAuthorized($type="List")
	{
		$generic = SLS_Generic::getInstance();
		$session = $generic->getObjectSession();
		$sessionToken = substr(substr(sha1($generic->getSiteConfig("privateKey")),12,31).substr(sha1($generic->getSiteConfig("privateKey")),4,11),6);
		$prefix = "L";
		
		switch ($type)
		{
			case "List": 	$prefix = "L"; break;
			case "Add": 	$prefix = "A"; break;
			case "Modify": 	$prefix = "M"; break;
			case "Delete": 	$prefix = "D"; break;
			default:		$prefix = "L"; break;
		}
		
		if (self::isLogged())
		{			
			$right = $session->getParam("SLS_BO_RIGHT_".$sessionToken);
			if ($right == "GOD")
				return 1;
			
			$authorized = 0;
			for($i=0 ; $i<strlen($right) ; $i++)
			{
				if ($right{$i} == $prefix)
				{
					$authorized = 1;
					break;
				}
			}
			return $authorized;
		}
		else
			return -1;
	}
	
	/**
	 * Returns all the bo listing actions
	 * 
	 * @access public static
	 * @return array $actions all the actions with key label and key url
	 * @since 1.0
	 */
	public static function getDistinctActions()
	{
		$generic = SLS_Generic::getInstance();
		$xmlController = $generic->getControllersXML();	
		
		$result = array_shift($xmlController->getTagsAttribute("//controllers/controller[@isBo='true']","name"));
		$controller = $result["attribute"];
				
		$result = $xmlController->getTagsAttribute("//controllers/controller[@isBo='true']/scontrollers/scontroller","name");
		$actions = array();
		$actionB = array("label" 	=> "Connection",
					     "url" 		=> $generic->getProtocol()."://".$generic->getSiteConfig("domainName")."/".$controller."/Connection.sls");
		
		for($i=0 ; $i<$count=count($result) ; $i++)
		{
			$attribute = $result[$i]["attribute"];
			if (SLS_String::startsWith($attribute,"List"))
			{
				$arrayTmp = array();
				$className = SLS_String::substrAfterFirstDelimiter($attribute,"List");
				$db = SLS_String::substrBeforeFirstDelimiter($className,"_");
				$name = SLS_String::substrAfterFirstDelimiter($className,"_");
				
				$generic->useModel($name,$db);
				$object = new $className();
				
				$label = $object->getTableComment($object->getTable(),strtolower($db));
				
				if (empty($label))
					$label = $name;				
				
				$arrayTmp["label"] 	= $label;
				$arrayTmp["url"]	= $generic->getProtocol()."://".$generic->getSiteConfig("domainName")."/".$controller."/".$attribute.".sls";
				array_push($actions,$arrayTmp);
			}
		}
		
		$translation = array_shift($xmlController->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='Translation']"));
		$upload = array_shift($xmlController->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='FileUpload']"));
		if (!empty($translation))
		{
			$arrayTmp["label"] 	= "Translations";
			$arrayTmp["url"]	= $generic->getProtocol()."://".$generic->getSiteConfig("domainName")."/".$controller."/Translation.sls";
			array_push($actions,$arrayTmp);
		}
		if (!empty($upload))
		{
			$arrayTmp["label"] 	= "FileUpload";
			$arrayTmp["url"]	= $generic->getProtocol()."://".$generic->getSiteConfig("domainName")."/".$controller."/FileUpload.sls";
			array_push($actions,$arrayTmp);
		}
		
		usort($actions,"SLS_BoRights::boActionsSort");
		array_push($actions,$actionB);
		
		$arrayTmp["label"] 	= "Disconnection";
		$arrayTmp["url"]	= $generic->getProtocol()."://".$generic->getSiteConfig("domainName")."/".$controller."/Disconnection.sls";
		array_push($actions,$arrayTmp);
		
		return $actions;
	}
	
	/**
	 * Callback function to order array of bo actions
	 *
	 * @param array $a
	 * @param array $b
	 * @return int
	 */
	public static function boActionsSort($a,$b)
	{
		return (strcasecmp($a["label"],$b["label"])>0) ? 1 : 0;
	}
	
	/**
	 * If action bo exist
	 *	 
	 * @param string $alias the alias of the db
	 * @param string $model the model to check
	 * @param string $action the action to test
	 * @return bool true if exist, else false
	 */
	public static function boActionExist($alias,$model,$action)
	{		
		$controllersXML = SLS_Generic::getInstance()->getControllersXML();		
		$test = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='".ucfirst($action).ucfirst($alias)."_".SLS_String::tableToClass($model)."']"));		
		return (empty($test)) ? false : true;
	}
}
?>