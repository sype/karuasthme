<?php
/**
 * SLS_BoAdd Tool - Generate back-office adding
 *  
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package SLS.Generics.Tools.SLS_Bo
 * @since 1.0 
 */
class SLS_BoAdd
{		
	private $oController;
	private $model;
	private $db;
	private $_generic;
	private $http;
	private $xml;
	private $urlToRedirect;
	public $id;
			
	/**
	 * Constructor
	 *
	 * @access public
	 * @param Object $oController reference of the current running controller
	 * @param string $model the model you want to allow adding
	 * @param SLS_XMLToolBox $xml the reference of the xmltoolbox
	 * @param string $alias the alias of the db
	 * @since 1.0
	 */
	public function __construct($oController,$model,$alias,$xml)
	{
		$this->oController = $oController;
		$this->model = $model;
		$this->db = $alias;
		$this->xml = $xml;
		$this->_generic = SLS_Generic::getInstance();
		$this->http = $this->_generic->getObjectHttpRequest();
		$this->xmlType = new SLS_XMLToolbox(file_get_contents($this->_generic->getPathConfig("configSls")."/types.xml"));		
		$this->xmlFk = new SLS_XMLToolbox(file_get_contents($this->_generic->getPathConfig("configSls")."/fks.xml"));
	}
	
	/**
	 * Set the url on which you want to go after the adding success
	 *
	 * @access public
	 * @param string $url the url you want to go (website root if empty)
	 * @since 1.0
	 */
	public function setUrlToRedirect($url="")
	{
		if ($url != -1)
		{
			if (empty($url))
				$this->urlToRedirect = $this->_generic->getProtocol()."://".$this->_generic->getSiteConfig("domainName");
			else
				$this->urlToRedirect = $url;
		}
		else
			$this->urlToRedirect = "";
	}
	
	/**
	 * Construct the add
	 *
	 * @access public
	 * @since 1.0
	 */
	public function constructAdd()
	{				
		$this->_generic->useModel(SLS_String::tableToClass($this->model),$this->db);
		$class = ucfirst($this->db)."_".SLS_String::tableToClass($this->model);
		$object = new $class();
		$error = false;
		$firstLangError = "";
		$errors = array();
		$columns = array();
		
		$this->xml->startTag("bo_add");
		
		// If reload
		if ($this->http->getParam("reload_add") == "true")
		{
			// Multilanguage case
			if ($object->isMultilanguage())
			{
				$langs 			= $this->_generic->getObjectLang()->getSiteLangs();
				$defaultLang 	= $this->_generic->getSiteConfig("defaultLang");
				$pk 			= $object->giveNextId();
				$params 		= $this->http->getParams();
				$clones			= array();
								
				// Foreach langs, catch errors
				foreach($langs as $lang)
				{					
					$object_{$lang} = new $class();
					$object_{$lang}->setModelLanguage($lang);
					
					foreach($params as $key => $value)
					{
						if (SLS_String::endsWith($key,"_".$lang))
						{							
							$functionName 	= "set".SLS_String::fullTrim(ucwords(SLS_String::stringToUrl(str_replace("_"," ",SLS_String::substrBeforeLastDelimiter($key,"_"))," ",false)),"");
							$columns[$key] 	= SLS_String::trimSlashesFromString($value);
							
							if (!$object_{$lang}->$functionName($columns[$key]))
								$error = true;
							
							// If current lang is the default lang
							if ($lang == $defaultLang)
							{
								// If we found a no-multilanguage file's type
								$result = array_shift($this->xmlType->getTagsAttribute("//sls_configs/entry[@table='".$object_{$lang}->getDatabase()."_".$object_{$lang}->getTable()."' and @column='".SLS_String::substrBeforeLastDelimiter($key,"_")."' and @multilanguage = 'false']","multilanguage"));
								if (!empty($result) && !$error)
								{
									$clones[SLS_String::substrBeforeLastDelimiter($key,"_")] = $object_{$lang}->__get(SLS_String::substrBeforeLastDelimiter($key,"_"));									
								}
								// If we found a no-multilanguage fk
								$result = array_shift($this->xmlFk->getTagsAttribute("//sls_configs/entry[@tableFk='".$object_{$lang}->getDatabase()."_".$object_{$lang}->getTable()."' and @columnFk='".SLS_String::substrBeforeLastDelimiter($key,"_")."' and @multilanguage = 'false']","multilanguage"));
								if (!empty($result) && !$error)
								{
									$clones[SLS_String::substrBeforeLastDelimiter($key,"_")] = $object_{$lang}->__get(SLS_String::substrBeforeLastDelimiter($key,"_"));									
								}
							}
						}
					}
					
					if ($error && $firstLangError == "")
						$firstLangError = $lang;
				}
				
				// If no errors, it's very cool, good admin =)
				if (!$error)
				{
					// Fill clones
					foreach ($langs as $lang)
					{
						if ($lang != $defaultLang)
						{
							foreach($clones as $key => $value)
							{
								$functionName 	= "set".SLS_String::fullTrim(ucwords(SLS_String::stringToUrl(str_replace("_"," ",$key)," ",false)),"");
								$object_{$lang}->$functionName($value);								
							}
						}
					}
					
					foreach ($langs as $lang)
					{						
						$object_{$lang}->create($pk);
						$object_{$lang}->save();						
					}
					$this->id = $pk;
				}
				else
				{					
					foreach($langs as $lang)											
						$errors[$lang] = $object_{$lang}->getErrors();	
				}
			}
			
			// Unilanguage case, less generic !
			else
			{
				$lang = $this->_generic->getSiteConfig("defaultLang");
				$objectNl = new $class();
				$params = $this->http->getParams();
				
				// Catch errors
				foreach($params as $key => $value)
				{			
					if (SLS_String::endsWith($key,"_".$lang))
					{							
						$functionName = "set".SLS_String::fullTrim(ucwords(SLS_String::stringToUrl(str_replace("_"," ",SLS_String::substrBeforeLastDelimiter($key,"_"))," ",false)),"");						
						$columns[$key] = SLS_String::trimSlashesFromString($value);
						
						if (!$objectNl->$functionName($columns[$key]))
							$error = true;
					}
				}
				
				// If no errors
				if (!$error)
				{
					$objectNl->create();
					$objectNl->save();
					$this->id = $objectNl->__get($objectNl->getPrimaryKey());
				}				
				else
				{					
					$firstLangError = $lang;
					$errors[$lang] = $objectNl->getErrors();
				}
			}
			
			// If any errors, redirect on the user request page
			if (!$error && !empty($this->urlToRedirect))
			{				
				header("Location: ".$this->urlToRedirect);
			}
			// Else, form the xml of the user
			else
			{
				$this->xml->startTag("reload");
				$this->xml->addFullTag("error",($error) ? "true" : "false",true);
				$this->xml->addFullTag("first_lang_error",$firstLangError,true);
				$this->xml->startTag("properties");				
				foreach($columns as $column => $value)
				{
					$property 		= SLS_String::substrBeforeLastDelimiter($column,"_");
					$lang 			= SLS_String::substrAfterLastDelimiter($column,"_");					
					$currentError 	= (empty($errors[$lang][$property])) ? "" : $GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_'.$errors[$lang][$property]];					
					$this->xml->startTag("property");
					$this->xml->addFullTag("name",$column,true);
					$this->xml->addFullTag("column",$property,true);
					$this->xml->addFullTag("lang",$lang,true);
					$this->xml->addFullTag("value",$value,true);
					$this->xml->addFullTag("error",$currentError,true);					
					$this->xml->endTag("property");
				}
				$this->xml->endTag("properties");
				$this->xml->endTag("reload");
			}
		}
		
		// Get all properties, labels, values, types & length
		$properties = $this->getProperties();
		
		$this->xml->addFullTag("table",$this->model,true);
		$this->xml->addFullTag("description",$object->getTableComment($object->getTable(),$this->db),true);
		$this->xml->addFullTag("class",SLS_String::tableToClass($this->model),true);
		$this->xml->addFullTag("pk",$object->getPrimaryKey(),true);
		$this->xml->addFullTag("multilanguage",$object->isMultilanguage(),true);
		$this->xml->startTag("langs");		
		if ($object->isMultilanguage())
		{
			$langs = $this->_generic->getObjectLang()->getSiteLangs();
			foreach($langs as $lang)
				$this->xml->addFullTag("lang",$lang,true);
		}
		else
			$this->xml->addFullTag("lang",$this->_generic->getSiteConfig("defaultLang"),true);
		$this->xml->endTag("langs");
		
		$this->xml->startTag("properties");
		for($i=0 ; $i<$count=count($properties) ; $i++)
		{
			$this->xml->startTag("property");
			foreach($properties[$i] as $key => $value)
				if ($key != "form_values")
					$this->xml->addFullTag($key,$value,true);
			
			$tmp = $properties[$i]["form_values"];			
			if (is_array($tmp) && !empty($tmp) && $properties[$i]["form_type"] != "radio")
			{
				$this->xml->startTag("values");
				for($j=0 ; $j<$countj=count($tmp) ; $j++)
				{
					$this->xml->startTag("value");
					$this->xml->addFullTag("id",$tmp[$j]["id"],true);
					$this->xml->addFullTag("label",$tmp[$j]["label"],true);
					$this->xml->addFullTag("lang",$tmp[$j]["lang"],true);
					$this->xml->endTag("value");
				}
				$this->xml->endTag("values");
			}
			else if ($properties[$i]["form_type"] == "radio")
			{
				$this->xml->startTag("values");
				for($j=0 ; $j<$countj=count($tmp) ; $j++)
				{						
					$this->xml->addFullTag("value",$tmp[$j],true);
				}
				$this->xml->endTag("values");
			}
			else
				$this->xml->addFullTag("values",(empty($tmp)) ? "-1" : $tmp,true);
			$this->xml->endTag("property");
		}
		$this->xml->endTag("properties");
		$this->xml->endTag("bo_add");
		
		return $this->xml;
	}
	
	/**
	 * Get all the columns from the current table
	 *
	 * @access public
	 * @return array all the columns informations
	 * @since 1.0
	 */
	public function getProperties()
	{
		$properties = array();
		$sql = SLS_Sql::getInstance();
		
		$this->_generic->useModel(SLS_String::tableToClass($this->model),$this->db,"user");
		$class = ucfirst($this->db)."_".SLS_String::tableToClass($this->model);
		$currentModel = new $class();
		$defaultMultilanguage = ($currentModel->isMultilanguage()) ? "true" : "false";
				
		$columns = $sql->showColumns($currentModel->getTable());
		
		for($i=0 ; $i<$count=count($columns) ; $i++)
		{
			if ($columns[$i]->Key != "PRI")
			{
				$column = $columns[$i]->Field;
				$type = (strpos($columns[$i]->Type, "(")) ? SLS_String::substrBeforeFirstDelimiter(strtolower($columns[$i]->Type), "(") : $columns[$i]->Type;
				
				if (($type == "float" || $type == "double" || $type == "decimal") && (strpos($type[$i]->Type, "(")))
				{
					$length = SLS_String::substrBeforeFirstDelimiter(SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ')'), '('), ",");
					$values = array();
					$form = "input_text";
					$align = "right";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$date = "false";
					$defaultValue = $columns[$i]->Default;
				}
				else if ($type == "enum" || $type == "set")
				{
					$length = -1;
					$values = explode("','",SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, "')"), "('"));
					$form = "radio";
					$align = "left";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$date = "false";
					$defaultValue = $columns[$i]->Default;
				}
				else if (SLS_String::endsWith($type,"text"))
				{
					$length = -1;
					$values = array();
					$form = "textarea";
					$align = "left";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$date = "false";
					$defaultValue = $columns[$i]->Default;
				}
				else 
				{					
					if (strpos($columns[$i]->Type, "("))
					{
						$length = SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ")"), "(");
						$values = array();
						$form = "input_text";
						$align = "left";
						$multilanguage = $defaultMultilanguage;
						$fk = -1;
						$date = "false";
						$defaultValue = $columns[$i]->Default;
					}
					if(SLS_String::endsWith($type, "int"))
					{
						$length = SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ")"), "(");
						$values = array();
						$form = "input_text";
						$align = "left";
						$multilanguage = $defaultMultilanguage;
						$fk = -1;
						$date = "false";
						$defaultValue = $columns[$i]->Default;
					}
					else if ($type == "date" || $type == "datetime" || $type == "timestamp" || $type == "time" || $type == "year")
					{
						$length = -1;						
						$form = "input_text";
						$align = "right";
						$multilanguage = $defaultMultilanguage;
						$fk = -1;
						$date = $type;
						$defaultValue = $columns[$i]->Default;
						
						switch ($type)
						{
							case "date":		$values = date("Y-m-d"); break;
							case "datetime":	$values = date("Y-m-d H:i:s"); break;
							case "timestamp": 	$values = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")); break;
							case "time":		$values = date("H:i:s"); break;
							case "year":		$values = date("Y"); break;
						}
					}
				}
				$this->_generic->useModel(SLS_String::tableToClass($this->model),$this->db);
				$class = ucfirst($this->db)."_".SLS_String::tableToClass($this->model);
				$currentModel = new $class();
				
				// Search type file
				$result = array_shift($this->xmlType->getTagsAttribute("//sls_configs/entry[@table='".$this->db."_".$currentModel->getTable()."' and @column='".$column."' and (@type='file_img' or @type='file_all')]","multilanguage"));
				if (!empty($result))
				{					
					$values = array();
					$form = "input_file";
					$align = "left";
					$multilanguage = $result["attribute"];
					$fk = -1;
					$date = "false";
					$defaultValue = $columns[$i]->Default;
				}
				
				// Search type password								
				$result = $this->xmlType->getTags("//sls_configs/entry[@table='".$this->db."_".$currentModel->getTable()."' and @column='".$column."' and @type='password']");
				if (!empty($result))
				{
					$values = array();
					$form = "input_password";
					$align = "left";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$date = "false";
					$defaultValue = $columns[$i]->Default;
				}
				
				// Search FK
				$res = $this->xmlFk->getTagsByAttributes("//sls_configs/entry",array("tableFk","columnFk"),array($this->db."_".$currentModel->getTable(),$column));				
				if (!empty($res))
				{
					$resultFk = array_shift($this->xmlFk->getTagsAttribute("//sls_configs/entry[@tableFk='".$this->db."_".$currentModel->getTable()."' and @columnFk='".$column."' and @multilanguage = 'false']","multilanguage"));
					$labelPk = substr($res,(strpos($res,'labelPk="')+9),(strpos($res,'" tablePk="')-(strpos($res,'labelPk="')+9)));					
					$tableTm = substr($res,(strpos($res,'tablePk="')+9),(strpos($res,'"/>')-(strpos($res,'tablePk="')+9)));							
					$tablePk = SLS_String::substrAfterFirstDelimiter($tableTm,"_");
					$dbPk 	 = SLS_String::substrBeforeFirstDelimiter($tableTm,"_");			
					
					$form = "select";
					$align = "right";
					$multilanguage = (!empty($resultFk)) ? "false" : $defaultMultilanguage;
					$length = -1;
					$fk = $tablePk;
					$date = "false";
					$defaultValue = $columns[$i]->Default;
										
					$this->_generic->useModel($tablePk,$dbPk,"user");
					$class = ucfirst($dbPk)."_".SLS_String::tableToClass($tablePk);
					$object = new $class();
					$pk = $object->getPrimaryKey();
					
					$values = array();
					$arrayTmp = $object->searchModels();
					if ($columns[$i]->Null == "YES")
					{
						$tmp = array("id"	=>"",
									 "label"=>"",
									 "lang" =>"");
						array_push($values,$tmp);
					}
					for($j=0 ; $j<$count=count($arrayTmp) ; $j++)
					{
						$lang = (isset($arrayTmp[$j]->pk_lang)) ? $arrayTmp[$j]->pk_lang : "";
						$tmp = array("id"	=>$arrayTmp[$j]->$pk,
									 "label"=>$arrayTmp[$j]->$labelPk,
									 "lang" =>$lang);
						array_push($values,$tmp);
					}
				}
				$comment = ($columns[$i]->Comment == "") ? $column : $columns[$i]->Comment;
				$nullable = ($columns[$i]->Null == "NO") ? "false" : "true";
				$currentColumn = array("form_column"		=>$column,
								   	   "form_type" 			=> $form,
								   	   "form_align" 		=> $align,
								   	   "form_multilanguage" => $multilanguage,
								   	   "form_length"		=> $length,
								   	   "form_label" 		=> $comment,
									   "form_default"		=> $defaultValue,
									   "form_date"			=> $date,
								   	   "form_values"		=> $values,
								   	   "foreign_key"		=> $fk,
								   	   "form_null"			=> $nullable);
				array_push($properties,$currentColumn);
			}
		}
		return $properties;
	}
}
?>