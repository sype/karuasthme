<?php
/**
 * SLS_BoDelete Tool - Generate back-office deleting
 *  
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package SLS.Generics.Tools.SLS_Bo
 * @since 1.0 
 */
class SLS_BoDelete
{		
	private $oController;
	private $model;
	private $db;
	private $_generic;
	private $http;	
	private $urlToRedirect;
	private $refSmod;
			
	/**
	 * Constructor
	 *
	 * @access public
	 * @param object $oController reference of the current running controller
	 * @param string $model the model you want to allow adding	 
	 * @param string $alias the alias of the db
	 * @since 1.0
	 */
	public function __construct($oController,$model,$alias)
	{
		$this->oController = $oController;
		$this->model = $model;
		$this->db = $alias;
		$this->_generic = SLS_Generic::getInstance();
		$this->http = $this->_generic->getObjectHttpRequest();
	}
	
	/**
	 * Set the url on which you want to go after the adding success
	 *
	 * @access public
	 * @param string $mod the generic mod (domainName if empty)
	 * @param string $smod the generic smod (domainName if empty)
	 * @since 1.0
	 */
	public function setUrlToRedirect($mod,$smod)
	{
		$session = SLS_Generic::getInstance()->getObjectSession();
		$arrayPost = array();
		$this->refSmod = $smod;
		$post = $session->getParam("SLS_PREVIOUS_POST_".$smod);
		
		// If we have previous post
		if (!empty($mod) && !empty($smod) && !empty($post))
		{
	        $postExploded = explode("/",$post);
	        $postExploded = array_chunk($postExploded,2);	        
	        for($i=0 ; $i<$count=count($postExploded) ; $i++)        
	        	if (is_array($postExploded[$i]) && count($postExploded[$i]) == 2)        	
	        		array_push($arrayPost,array("key" => $postExploded[$i][0],"value" => $postExploded[$i][1]));
		}
		
		// If not url to redirect
		if (empty($mod) || empty($smod))
			$this->urlToRedirect = $this->_generic->getProtocol()."://".$this->_generic->getSiteConfig("domainName");
		else
			$this->urlToRedirect = $this->_generic->getFullPath($mod,$smod,$arrayPost);		
	}
	
	/**
	 * Construct the delete
	 *
	 * @access public
	 * @since 1.0
	 */
	public function constructDelete()
	{
		$class = ucfirst($this->db)."_".SLS_String::tableToClass($this->model);
		$this->_generic->useModel(SLS_String::tableToClass($this->model),$this->db);
		$object = new $class();
				
		// Recover id
		$ids = $this->http->getParam("id");
		
		$ids = (SLS_String::contains($ids,"|")) ? explode("|",$ids) : array($ids);
		
		foreach($ids as $id)
		{
			if ($object->getModel($id) === true)
			{
				if ($object->isMultilanguage())
					$langs = $this->_generic->getObjectLang()->getSiteLangs();
				else
					$langs = array($this->_generic->getSiteConfig("defaultLang"));
				
				foreach($langs as $lang)
				{
					$object_{$lang} = new $class();
					$object_{$lang}->setModelLanguage($lang);
					if ($object_{$lang}->getModel($id) === true)
						$object_{$lang}->delete();
				}
			}
		}
		$session = SLS_Generic::getInstance()->getObjectSession();
		$session->delParam("SLS_PREVIOUS_POST_".$this->refSmod);
		header("Location: ".$this->urlToRedirect);
		die();
	}
}
?>