<?php
/**
 * SLS_BoEdit Tool - Generate back-office editing
 *  
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package SLS.Generics.Tools.SLS_Bo
 * @since 1.0 
 */
class SLS_BoEdit
{		
	private $oController;
	private $model;
	private $db;
	private $_generic;
	private $http;
	private $xml;
	private $urlToRedirect;
	private $refSmod;
				
	/**
	 * Constructor
	 *
	 * @access public
	 * @param Object $oController reference of the current running controller
	 * @param string $model the model you want to allow adding
	 * @param SLS_XMLToolBox $xml the reference of the xmltoolbox
	 * @param string $alias the alias of the db
	 * @since 1.0
	 */
	public function __construct($oController,$model,$alias,$xml)
	{
		$this->oController = $oController;
		$this->model = $model;
		$this->db = $alias;
		$this->xml = $xml;
		$this->_generic = SLS_Generic::getInstance();
		$this->http = $this->_generic->getObjectHttpRequest();
		$this->xmlType = new SLS_XMLToolbox(file_get_contents($this->_generic->getPathConfig("configSls")."/types.xml"));		
		$this->xmlFk = new SLS_XMLToolbox(file_get_contents($this->_generic->getPathConfig("configSls")."/fks.xml"));
	}
	
	/**
	 * Set the url on which you want to go after the adding success
	 *
	 * @access public
	 * @param string $mod the generic mod (domainName if empty)
	 * @param string $smod the generic smod (domainName if empty)
	 * @since 1.0
	 */
	public function setUrlToRedirect($mod,$smod)
	{
		if ($mod != -1 && $smod != -1)
		{
			$session = $this->_generic->getObjectSession();
			$arrayPost = array();
			$this->refSmod = $smod;
			$post = $session->getParam("SLS_PREVIOUS_POST_".$smod);
			
			// If we have previous post
			if (!empty($mod) && !empty($smod) && !empty($post))
			{
		        $postExploded = explode("/",$post);
		        $postExploded = array_chunk($postExploded,2);	        
		        for($i=0 ; $i<$count=count($postExploded) ; $i++)        
		        	if (is_array($postExploded[$i]) && count($postExploded[$i]) == 2)        	
		        		array_push($arrayPost,array("key" => $postExploded[$i][0],"value" => $postExploded[$i][1]));
			}
			
			// If not url to redirect
			if (empty($mod) || empty($smod))
				$this->urlToRedirect = $this->_generic->getProtocol()."://".$this->_generic->getSiteConfig("domainName");
			else
				$this->urlToRedirect = $this->_generic->getFullPath($mod,$smod,$arrayPost);
		}
		else
			$this->urlToRedirect = "";
	}
	
	/**
	 * Construct the edit
	 *
	 * @access public
	 * @since 1.0
	 */
	public function constructEdit()
	{
		$class = ucfirst($this->db)."_".SLS_String::tableToClass($this->model);
		$this->_generic->useModel(SLS_String::tableToClass($this->model),$this->db);
		$object = new $class();
		$error = false;
		$firstLangError = "";
		$errors = array();
		$columns = array();
		
		$this->xml->startTag("bo_edit");
		
		// Recover id
		$id = $this->http->getParam("id");
		
		// If reload
		if ($this->http->getParam("reload_edit") == "true")
		{
			// Multilanguage case
			if ($object->isMultilanguage())
			{
				$langs 			= $this->_generic->getObjectLang()->getSiteLangs();
				$defaultLang 	= $this->_generic->getSiteConfig("defaultLang");
				$pk 			= $object->giveNextId();
				$params 		= $this->http->getParams();
				$clones			= array();
				
				// Foreach langs, catch errors
				foreach($langs as $lang)
				{					
					$object_{$lang} = new $class();
					$object_{$lang}->setModelLanguage($lang);
					$object_{$lang}->getModel($id);
					
					foreach($params as $key => $value)
					{						
						if (SLS_String::endsWith($key,"_".$lang))
						{						
							$functionName 	= "set".SLS_String::fullTrim(ucwords(SLS_String::stringToUrl(str_replace("_"," ",SLS_String::substrBeforeLastDelimiter($key,"_"))," ",false)),"");						
							$columns[$key] 	= SLS_String::trimSlashesFromString($value);														
							$result = array_shift($this->xmlType->getTagsAttribute("//sls_configs/entry[@table='".$object_{$lang}->getDatabase()."_".$object_{$lang}->getTable()."' and @column='".SLS_String::substrBeforeLastDelimiter($key,"_")."' and (@type = 'file_all' or @type = 'file_img')]","multilanguage"));
							$noSet = (!empty($result) && (empty($columns[$key]) || (is_array($params[$key]) && $columns[$key]["error"] == "4"))) ? true : false;
								
							if (!$noSet && !$object_{$lang}->$functionName($columns[$key]))
								$error = true;
								
							// If current lang is the default lang
							if ($lang == $defaultLang)
							{
								// If we found a no-multilanguage file's type
								$result = array_shift($this->xmlType->getTagsAttribute("//sls_configs/entry[@table='".$object_{$lang}->getDatabase()."_".$object_{$lang}->getTable()."' and @column='".SLS_String::substrBeforeLastDelimiter($key,"_")."' and @multilanguage = 'false']","multilanguage"));
								if (!empty($result) && !$error && (!empty($columns[$key]) && $columns[$key]["error"] != "4"))
								{
									$clones[SLS_String::substrBeforeLastDelimiter($key,"_")] = $object_{$lang}->__get(SLS_String::substrBeforeLastDelimiter($key,"_"));									
								}
								// If we found a no-multilanguage fk
								$result = array_shift($this->xmlFk->getTagsAttribute("//sls_configs/entry[@tableFk='".$object_{$lang}->getDatabase()."_".$object_{$lang}->getTable()."' and @columnFk='".SLS_String::substrBeforeLastDelimiter($key,"_")."' and @multilanguage = 'false']","multilanguage"));
								if (!empty($result) && !$error)
								{
									$clones[SLS_String::substrBeforeLastDelimiter($key,"_")] = $object_{$lang}->__get(SLS_String::substrBeforeLastDelimiter($key,"_"));									
								}
							}
						}
					}
					
					if ($error && $firstLangError == "")
						$firstLangError = $lang;
				}
				// If no errors, it's very cool, good admin =)
				if (!$error)
				{
					// Fill clones
					foreach ($langs as $lang)
					{
						if ($lang != $defaultLang)
						{
							foreach($clones as $key => $value)
							{
								$functionName 	= "set".SLS_String::fullTrim(ucwords(SLS_String::stringToUrl(str_replace("_"," ",$key)," ",false)),"");
								$object_{$lang}->$functionName($value);								
							}
						}
					}
					
					foreach ($langs as $lang)
					{
						$object_{$lang}->save();
					}
				}
				else
				{
					foreach($langs as $lang)
					{
						$errors[$lang] = $object_{$lang}->getErrors();
					}
				}
			}
			
			// Unilanguage case, less generic !
			else
			{
				$lang = $this->_generic->getSiteConfig("defaultLang");
				$objectNl = new $class();
				$objectNl->getModel($id);
				$params = $this->http->getParams();
				
				// Catch errors
				foreach($params as $key => $value)
				{			
					if (SLS_String::endsWith($key,"_".$lang))
					{							
						$noSet = (is_array($params[$key]) && $params[$key]["error"] == "4") ? true : false;
						
						$functionName = "set".SLS_String::fullTrim(ucwords(SLS_String::stringToUrl(str_replace("_"," ",SLS_String::substrBeforeLastDelimiter($key,"_"))," ",false)),"");						
						$columns[$key] = SLS_String::trimSlashesFromString($value);
												
						if (!$noSet && !$objectNl->$functionName($columns[$key]))
							$error = true;
					}
				}
				
				// If no errors
				if (!$error)					
					$objectNl->save();				
				else
				{
					$firstLangError = $lang;
					$errors[$lang] = $objectNl->getErrors();
				}
			}
			
			// If any errors, redirect on the user request page
			if (!$error && !empty($this->urlToRedirect))
			{
				$session = $this->_generic->getObjectSession();
				$session->delParam("SLS_PREVIOUS_POST_".$this->refSmod);
				header("Location: ".$this->urlToRedirect);				
			}
			// Else, form the xml of the user
			else
			{
				$this->xml->startTag("reload");
				$this->xml->addFullTag("id",$id,true);
				$this->xml->addFullTag("error",($error) ? "true" : "false",true);
				$this->xml->addFullTag("first_lang_error",$firstLangError,true);
				$this->xml->startTag("properties");
				foreach($columns as $column => $value)
				{
					$property 		= SLS_String::substrBeforeLastDelimiter($column,"_");
					$lang 			= SLS_String::substrAfterLastDelimiter($column,"_");					
					$currentError 	= (empty($errors[$lang][$property])) ? "" : $GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_'.$errors[$lang][$property]];					
					
					if (is_array($value) && (empty($value) || $value["error"]["error"] == "4"))
					{
						$object_{$lang} = new $class();
						$object_{$lang}->setModelLanguage($lang);
						if ($object_{$lang}->getModel($id) === true)
							$value = $object_{$lang}->__get($property);
					}
					
					$this->xml->startTag("property");
					$this->xml->addFullTag("name",$column,true);
					$this->xml->addFullTag("column",$property,true);
					$this->xml->addFullTag("lang",$lang,true);
					$this->xml->addFullTag("value",$value,true);
					$this->xml->addFullTag("error",$currentError,true);					
					$this->xml->endTag("property");
				}
				$this->xml->endTag("properties");
				$this->xml->endTag("reload");
			}
		}
		// Else, form the actual model
		else
		{
			$langs 	= $this->_generic->getObjectLang()->getSiteLangs();
			$columns = array();
			
			// If multilanguage
			if ($object->isMultilanguage())
			{
				foreach($langs as $lang)
				{
					$object_{$lang} = new $class();
					$object_{$lang}->setModelLanguage($lang);
					$object_{$lang}->getModel($id);
					$props = $object_{$lang}->getParams();
					
					foreach($props as $key => $value)					
						if ($key != $object_{$lang}->getPrimaryKey() && $key != "pk_lang")
							$columns[$key."_".$lang] = $value;
				}
			}
			// Else, just recover the model
			else
			{
				$lang = $this->_generic->getSiteConfig("defaultLang");
				$objectN1 = new $class();
				$objectN1->getModel($id);
				$props = $objectN1->getParams();
				foreach($props as $key => $value)				
					if ($key != $objectN1->getPrimaryKey() && $key != "pk_lang")
						$columns[$key."_".$lang] = $value;				
			}
			
			$this->xml->startTag("reload");
			$this->xml->addFullTag("id",$id,true);
			$this->xml->addFullTag("error","false",true);
			$this->xml->startTag("properties");				
			foreach($columns as $column => $value)
			{
				$property 		= SLS_String::substrBeforeLastDelimiter($column,"_");
				$lang 			= SLS_String::substrAfterLastDelimiter($column,"_");				
				$this->xml->startTag("property");
				$this->xml->addFullTag("name",$column,true);
				$this->xml->addFullTag("column",$property,true);
				$this->xml->addFullTag("lang",$lang,true);
				$this->xml->addFullTag("value",$value,true);
				$this->xml->addFullTag("error","",true);					
				$this->xml->endTag("property");
			}
			$this->xml->endTag("properties");
			$this->xml->endTag("reload");
		}
		
		// Get all properties, labels, values, types & length
		$properties = $this->getProperties();
		
		$this->xml->addFullTag("table",$this->model,true);
		$this->xml->addFullTag("description",$object->getTableComment($object->getTable(),$this->db),true);
		$this->xml->addFullTag("class",SLS_String::tableToClass($this->model),true);
		$this->xml->addFullTag("pk",$object->getPrimaryKey(),true);
		$this->xml->addFullTag("multilanguage",$object->isMultilanguage(),true);
		$this->xml->startTag("langs");		
		if ($object->isMultilanguage())
		{
			$langs = $this->_generic->getObjectLang()->getSiteLangs();
			foreach($langs as $lang)
				$this->xml->addFullTag("lang",$lang,true);
		}
		else
			$this->xml->addFullTag("lang",$this->_generic->getSiteConfig("defaultLang"),true);
		$this->xml->endTag("langs");
		
		$this->xml->startTag("properties");
		for($i=0 ; $i<$count=count($properties) ; $i++)
		{
			$this->xml->startTag("property");
			foreach($properties[$i] as $key => $value)
				if ($key != "form_values" && $key != "form_thumbs")
					$this->xml->addFullTag($key,$value,true);
			
			$tmp = $properties[$i]["form_values"];
			$tmpThumbs = $properties[$i]["form_thumbs"];
			if (is_array($tmp) && !empty($tmp) && $properties[$i]["form_type"] != "radio")
			{
				$this->xml->startTag("values");
				for($j=0 ; $j<$countj=count($tmp) ; $j++)
				{
					$this->xml->startTag("value");
					$this->xml->addFullTag("id",$tmp[$j]["id"],true);
					$this->xml->addFullTag("label",$tmp[$j]["label"],true);
					$this->xml->addFullTag("lang",$tmp[$j]["lang"],true);
					$this->xml->endTag("value");
				}
				$this->xml->endTag("values");
			}
			else if (is_array($tmpThumbs) && !empty($tmpThumbs) && $properties[$i]["form_type"] != "radio")
			{
				$this->xml->startTag("thumbs");
				for($j=0 ; $j<$countj=count($tmpThumbs) ; $j++)
				{
					$this->xml->startTag("thumb");
					foreach($tmpThumbs[$j] as $key => $value)
						$this->xml->addFullTag($key,$value,true);
					$this->xml->endTag("thumb");
				}
				$this->xml->endTag("thumbs");
			}
			else if ($properties[$i]["form_type"] == "radio")
			{
				$this->xml->startTag("values");
				for($j=0 ; $j<$countj=count($tmp) ; $j++)
				{						
					$this->xml->addFullTag("value",$tmp[$j],true);
				}
				$this->xml->endTag("values");
			}
			else
			{
				$this->xml->addFullTag("values",(empty($tmp)) ? "-1" : $tmp,true);
				$this->xml->addFullTag("thumbs",(empty($tmpThumbs)) ? "-1" : $tmpThumbs,true);
			}
			$this->xml->endTag("property");
		}
		$this->xml->endTag("properties");
		$this->xml->endTag("bo_edit");
		
		return $this->xml;
	}
	
	/**
	 * Get all the columns from the current table
	 *
	 * @access public
	 * @return array $properties all the columns informations
	 * @since 1.0
	 */
	public function getProperties()
	{
		$properties = array();
		$sql = SLS_Sql::getInstance();
		
		$this->_generic->useModel(SLS_String::tableToClass($this->model),$this->db);
		$class = ucfirst($this->db)."_".SLS_String::tableToClass($this->model);
		$currentModel = new $class();
		$columns = $sql->showColumns($currentModel->getTable());
		$defaultMultilanguage = ($currentModel->isMultilanguage()) ? "true" : "false";
		
		for($i=0 ; $i<$count=count($columns) ; $i++)
		{
			if ($columns[$i]->Key != "PRI")
			{
				$column = $columns[$i]->Field;
				$type = (strpos($columns[$i]->Type, "(")) ? SLS_String::substrBeforeFirstDelimiter(strtolower($columns[$i]->Type), "(") : $columns[$i]->Type;
				
				if (($type == "float" || $type == "double" || $type == "decimal") && (strpos($type[$i]->Type, "(")))
				{
					$length = SLS_String::substrBeforeFirstDelimiter(SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ')'), '('), ",");
					$values = array();
					$form = "input_text";
					$align = "right";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$typeFile = -1;
					$thumbs = array();
					$date = "false";
				}
				else if ($type == "enum" || $type == "set")
				{
					$length = -1;
					$values = explode("','",SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, "')"), "('"));
					$form = "radio";
					$align = "left";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$typeFile = -1;
					$thumbs = array();
					$date = "false";
				}
				else if (SLS_String::endsWith($type,"text"))
				{
					$length = -1;
					$values = array();
					$form = "textarea";
					$align = "left";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$typeFile = -1;
					$thumbs = array();
					$date = "false";
				}
				else 
				{					
					if (strpos($columns[$i]->Type, "("))
					{
						$length = SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ")"), "(");
						$values = array();
						$form = "input_text";
						$align = "left";
						$multilanguage = $defaultMultilanguage;
						$fk = -1;
						$typeFile = -1;
						$thumbs = array();
						$date = "false";
					}
					if(SLS_String::endsWith($type, "int"))
					{
						$length = SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ")"), "(");
						$values = array();
						$form = "input_text";
						$align = "left";
						$multilanguage = $defaultMultilanguage;
						$fk = -1;
						$typeFile = -1;
						$thumbs = array();
						$date = "false";
					}
					else if ($type == "date" || $type == "datetime" || $type == "timestamp" || $type == "time" || $type == "year")
					{
						$length = -1;
						$form = "input_text";
						$align = "right";
						$multilanguage = $defaultMultilanguage;
						$fk = -1;
						$typeFile = -1;
						$thumbs = array();
						$date = $type;
						
						switch ($type)
						{
							case "date":		$values = date("Y-m-d"); break;
							case "datetime":	$values = date("Y-m-d H:i:s"); break;
							case "timestamp": 	$values = mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")); break;
							case "time":		$values = date("H:i:s"); break;
							case "year":		$values = date("Y"); break;
						}
					}
				}
				
				// Search type file
				$result = array_shift($this->xmlType->getTagsAttributes("//sls_configs/entry[@table='".$this->db."_".$currentModel->getTable()."' and @column='".$column."' and (@type='file_img' or @type='file_all')]",array("type","multilanguage","thumbs")));				
				if (!empty($result) && count($result["attributes"]) == 3)
				{
					$length = -1;
					$values = array();
					$form = "input_file";
					$align = "left";
					$multilanguage = $result["attributes"][1]["value"];
					$fk = -1;
					$typeFile = $result["attributes"][0]["value"];
					$thumbs = unserialize(str_replace("||#||",'"',$result["attributes"][2]["value"]));
					$date = "false";
				}
				
				// Search type password
				$this->_generic->useModel(SLS_String::tableToClass($this->model),$this->db);
				$class = ucfirst($this->db)."_".SLS_String::tableToClass($this->model);
				$currentModel = new $class();
				$result = $this->xmlType->getTags("//sls_configs/entry[@table='".$this->db."_".$currentModel->getTable()."' and @column='".$column."' and @type='password']");
				if (!empty($result))
				{
					$values = array();
					$form = "input_password";
					$align = "left";
					$multilanguage = $defaultMultilanguage;
					$fk = -1;
					$typeFile = -1;
					$thumbs = array();
					$date = "false";
				}
				
				// Search FK
				$res = $this->xmlFk->getTagsByAttributes("//sls_configs/entry",array("tableFk","columnFk"),array($this->db."_".$currentModel->getTable(),$column));
				if (!empty($res))
				{
					$resultFk = array_shift($this->xmlFk->getTagsAttribute("//sls_configs/entry[@tableFk='".$this->db."_".$currentModel->getTable()."' and @columnFk='".$column."' and @multilanguage = 'false']","multilanguage"));
					$labelPk = substr($res,(strpos($res,'labelPk="')+9),(strpos($res,'" tablePk="')-(strpos($res,'labelPk="')+9)));					
					$tableTm = substr($res,(strpos($res,'tablePk="')+9),(strpos($res,'"/>')-(strpos($res,'tablePk="')+9)));							
					$tablePk = SLS_String::substrAfterFirstDelimiter($tableTm,"_");
					$dbPk 	 = SLS_String::substrBeforeFirstDelimiter($tableTm,"_");			
					
					$form = "select";
					$align = "right";
					$multilanguage = (!empty($resultFk)) ? "false" : $defaultMultilanguage;
					$length = -1;
					$fk = $tablePk;
					$typeFile = -1;
					$thumbs = array();
					$date = "false";
										
					$this->_generic->useModel($tablePk,$dbPk,"user");
					$class = ucfirst($dbPk)."_".SLS_String::tableToClass($tablePk);
					$object = new $class();
					$pk = $object->getPrimaryKey();
					
					$values = array();
					$arrayTmp = $object->searchModels();
					if ($columns[$i]->Null == "YES")
					{
						$tmp = array("id"	=>"",
									 "label"=>"",
									 "lang" =>"");
						array_push($values,$tmp);
					}
					for($j=0 ; $j<$count=count($arrayTmp) ; $j++)
					{
						$lang = (isset($arrayTmp[$j]->pk_lang)) ? $arrayTmp[$j]->pk_lang : "";
						$tmp = array("id"	=>$arrayTmp[$j]->$pk,
									 "label"=>$arrayTmp[$j]->$labelPk,
									 "lang" =>$lang);
						array_push($values,$tmp);
					}
				}
				$comment = ($columns[$i]->Comment == "") ? $column : $columns[$i]->Comment;
				$nullable = ($columns[$i]->Null == "NO") ? "false" : "true";
				$currentColumn = array("form_column"		=>$column,
								   	   "form_type" 			=> $form,
								   	   "form_align" 		=> $align,
								   	   "form_multilanguage" => $multilanguage,
								   	   "form_type_file"		=> $typeFile,
								   	   "form_thumbs"		=> $thumbs,
								   	   "form_length"		=> $length,
								   	   "form_label" 		=> $comment,
									   "form_date"			=> $date,
								   	   "form_values"		=> $values,
								   	   "foreign_key"		=> $fk,
								   	   "form_null"			=> $nullable);
				array_push($properties,$currentColumn);
			}
		}
		return $properties;
	}
}
?>