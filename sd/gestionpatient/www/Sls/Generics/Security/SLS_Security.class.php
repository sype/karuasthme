<?
/**
 * Manage Security
 *  
 * @author Florian Collot
 * @copyright SillySmart
 * @package Sls.Generics.Security
 * @since 1.0 
 */
class SLS_Security 
{
	private static $_instance;
	
	/**
	 * Contructor
	 *
	 * @access public
	 * @since 1.0
	 */
	public function __construct() {}
	
	/**
	 * Singleton
	 * 
	 * @access public static
	 * @return SLS_Security $instance SLS_Security instance
	 * @since 1.0
	 */
	public static function getInstance() 
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new SLS_Security();
			}
		return self::$_instance;
	}	
	
	/**
	 * Symmetric encryption
	 *
	 * @access public static	 
	 * @param string $text the text to encrypt
	 * @param string $key the private key (default Project Private key)
	 * @return string $encrypted the text encrypted
	 * @see SLS_Security::decrypte
	 * @since 1.0
	 * @example 
	 * $crypted = SLS_Security::getInstance()->crypte("sillysmart");
     * var_dump($crypted);
     * // will produce : "VyUA[..]UgCCA="
	 */	
	public static function crypte($Texte,$Cle="")
	{
		if ($Cle == "")
			$Cle = SLS_Generic::getInstance()->getSiteConfig('privateKey');
		srand((double)microtime()*1000000);
		$CleDEncryptage = md5(rand(0,32000) );
		$Compteur=0;
		$VariableTemp = ""; 
		for ($Ctr=0; $Ctr<strlen($Texte); $Ctr++)  
		{
			if ($Compteur==strlen($CleDEncryptage))
				$Compteur=0;
			$VariableTemp.= substr($CleDEncryptage,$Compteur,1).(substr($Texte,$Ctr,1) ^ substr($CleDEncryptage,$Compteur,1) );
			$Compteur++;
		}
		return base64_encode(SLS_Security::generationCle($VariableTemp,$Cle));
	}

	/**
	 * Symmetric encryption
	 *
	 * @access public static	 
	 * @param string $text the text to encrypt
	 * @param string $key the private key (default Project Private key)
	 * @return string $decrypted the text decrypted
	 * @see SLS_Security::crypte
	 * @since 1.0
	 * @example 
	 * $uncrypted = SLS_Security::getInstance()->decrypte("VyUA[..]UgCCA=");
     * var_dump($uncrypted);
     * // will produce : "sillysmart"
	 */	
	public static function decrypte($Texte,$Cle="")
	{
		if ($Cle == "")
			$Cle = SLS_Generic::getInstance()->getSiteConfig('privateKey');
		$Texte = SLS_Security::generationCle(base64_decode($Texte),$Cle);
		$VariableTemp = "";
		for ($Ctr=0;$Ctr<strlen($Texte);$Ctr++)
		{
			$md5 = substr($Texte,$Ctr,1);
			$Ctr++;
			$VariableTemp.= (substr($Texte,$Ctr,1) ^ $md5);
		}
		return $VariableTemp;
	}

	/**
	 * Generate a key based on private key
	 * 
	 * @access public static
	 * @param string $text the text
	 * @param string $privateKey the private key
	 * @return string $publicKey the public key
	 * @since 1.0
	 */
	public static function generationCle($Texte,$CleDEncryptage="")
	{
		$CleDEncryptage = md5($CleDEncryptage);
  		$Compteur=0;
  		$VariableTemp = "";
		for ($Ctr=0;$Ctr<strlen($Texte);$Ctr++) 
		{
			if ($Compteur==strlen($CleDEncryptage))
				$Compteur=0;
			$VariableTemp.= substr($Texte,$Ctr,1) ^ substr($CleDEncryptage,$Compteur,1);
			$Compteur++;
		}
  		return $VariableTemp;
  	}
}
?>