<?
/**
 * SQL_Sql class - SQL Mother
 *  
 * @author Laurent Bientz
 * @author Florian Collot
 * @copyright SillySmart
 * @package Sls.Generics.Sql
 * @since 1.0 
 */
class SLS_Sql 
{
	private static $_instance;
	private $_dbh = array();
	private $_currentDb = "";
	private $_generic;

	/**
	 * Constructor, instanciate PDO class to prepare connection(s)
	 * 
	 * @access public
	 * @since 1.0
	 */
	public function __construct() 
	{
		$this->_generic = SLS_Generic::getInstance();
		$needed = array("mysql", "PDO", "pdo_mysql");
		$php = get_loaded_extensions();
		
		foreach ($needed as $phpPre)		
			if (!in_array($phpPre, $php))
				SLS_Tracing::addTrace(new Exception("You need PHP Extension : ".$phpPre));
	}
	
	/**
	 * Singleton 
	 *
	 * @access public static
	 * @return SLS_Sql $instance SLS_Sql instance
	 * @since 1.0
	 */
	public static function getInstance() 
	{
		if (is_null(self::$_instance)) 		
			self::$_instance = new SLS_Sql();		
		return self::$_instance;
	}
	
	/**
	 * Return all db alias
	 *
	 * @access public	 
	 * @return array $dbs all db alias
	 * @see SLS_Sql::getCurrentDb
	 * @see SLS_Sql::changeDb
	 * @since 1.0
	 * @example 
	 * var_dump(SLS_Sql::getInstance()->getDbs());
	 * // will produce :
	 * array("db1","..","dbN")
	 */
	public function getDbs()
	{
		$dbs = array();
		$results = $this->_generic->getDbXML()->getTagsAttribute("//dbs/db","alias");
		foreach($results as $result)
			array_push($dbs,$result["attribute"]);
		return $dbs;
	}
	
	/**
	 * Return the current db alias
	 *
	 * @access public
	 * @return string $db the current db
	 * @see SLS_Sql::getDbs
	 * @see SLS_Sql::changeDb
	 * @since 1.0
	 */
	public function getCurrentDb()
	{
		return $this->_currentDb;
	}
	
	/**
	 * Change the current database on which PDO is connected
	 *
	 * @access public
	 * @param string $db alias db name
	 * @return bool $changed true if database changed, else false
	 * @see SLS_Sql::getCurrentDb
	 * @see SLS_Sql::getDbs
	 * @since 1.0
	 */
	public function changeDb($db)
	{
		// Get all databases
		$dbs = $this->getDbs();
		
		$db = strtolower($db);
		
		// If db doesn't exist
		if (!in_array($db,$dbs))
			return false;			
		else
		{
			// If PDO reference doesn't already exist
			if (!array_key_exists($key,$this->_dbh))
			{		
				$dsn = "mysql:host=".$this->_generic->getDbConfig("host",$db).";dbname=".$this->_generic->getDbConfig("base",$db);
				try
				{
					$this->_dbh[$db] = new PDO($dsn, $this->_generic->getDbConfig("user",$db), $this->_generic->getDbConfig("pass",$db));
					$this->_dbh[$db]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);					
					$this->_currentDb = $db;
					$this->setCharset($db);
				}
				catch(Exception $e)
				{
					SLS_Tracing::addTrace($e,true);
					return false;
				}
			}
			return true;
		}
	}
	
	/**
	 * Set the charset for the current connection
	 *
	 * @param string $db the db alias
	 */
	public function setCharset($db)
	{		
		$result = array_shift($this->_generic->getDbXML()->getTagsAttribute("//dbs/db[@alias='".$db."']","charset"));
		if (!empty($result["attribute"]))		
			$this->update("SET NAMES '".$result["attribute"]."';");
	}
	
	/**
	 * Ping a MySQL Connection
	 *
	 * @access public
	 * @param string $host the host
	 * @param string $db the db
	 * @param string $user the username
	 * @param string $pass the userpassword
	 * @return mixed $success true if connection succeed, else error message
	 * @since 1.0
	 */
	public function pingConnection($host,$db,$user,$pass)
	{		
		try
		{
			$pdo = new PDO("mysql:host=".$host.";dbname=".$db, $user, $pass);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return true;
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
	}
	
	/**
	 * Sql Select	 
	 *
	 * @access public
	 * @param string $query the sql query
	 * @return array $results array of PDO objects
	 * @see SLS_Sql::insert
	 * @see SLS_Sql::update
	 * @see SLS_Sql::delete
	 * @since 1.0
	 */
	public function select($query) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();		
		try
		{
			$statement = $this->_dbh[$this->_currentDb]->query($query);
			$table = array();
			while ($result = $statement->fetchObject()) 		
				$table[] = $result;		
			return $table;
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Sql Insert
	 *
	 * @access public
	 * @param string $insert the sql query
	 * @return int $insert_id the last insert id
	 * @see SLS_Sql::select
	 * @see SLS_Sql::update
	 * @see SLS_Sql::delete
	 * @since 1.0
	 */
	public function insert($insert) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();		
		try
		{
			$this->_dbh[$this->_currentDb]->exec($insert);
			return $this->_dbh[$this->_currentDb]->lastInsertId();		
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Sql Update
	 *
	 * @access public
	 * @param string $update the sql query
	 * @see SLS_Sql::select
	 * @see SLS_Sql::insert
	 * @see SLS_Sql::delete
	 * @since 1.0
	 */
	public function update($update) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();		
		try
		{
			$this->_dbh[$this->_currentDb]->exec($update);
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Sql Delete
	 *
	 * @access public
	 * @param string $delete the sql query
	 * @see SLS_Sql::select
	 * @see SLS_Sql::insert
	 * @see SLS_Sql::update
	 * @since 1.0
	 */
	public function delete($delete) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();		
		try
		{
			$this->_dbh[$this->_currentDb]->exec($delete);
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Return all the columns of one table
	 *
	 * @access public	 
	 * @param string $table the table name
	 * @return array $columns array of PDO objects
	 * @since 1.0
	 * @example 
	 * var_dump(SLS_Sql::getInstance()->showColumns("table"));
	 * // will produce :
	 * array(
  	 *		0 => object(stdClass){
     *								"Field"			=> "table_id",
     *								"Type"			=> "bigint(20)",
     *								"Collation"		=> NULL,
     *								"Null"			=> "NO",
     *								"Key"			=> "PRI",
     *								"Default"		=> NULL,
     *								"Extra"			=> "auto_increment",
     *								"Privileges"	=> "select,insert,update,references",
     *								"Comment"		=> "Id"
     * 							},
     * 		1 => ...
  	 * )
	 */
	public function showColumns($table) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();		
		try
		{	
			$statement = $this->_dbh[$this->_currentDb]->query("SHOW FULL COLUMNS FROM `".$table."` FROM `".$this->_generic->getDbConfig("base",$this->_currentDb)."`");		
			$cols = array();
			while ($result = $statement->fetchObject()) 		
				$cols[] = $result;		
			return $cols;
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Get all the columns name of one table	 
	 *
	 * @access public	 
	 * @param string $table table name
	 * @return array $columns array of table columns
	 * @since 1.0
	 * @example 
	 * var_dump(SLS_Sql::getInstance()->getColumnsName("user"));
	 * // will produce
	 * array("user_id", "user_email", "user_pwd")
	 */
	public function getColumnsName($table) 
	{
		$columns = $this->showColumns($table);
		if ($columns == false)
			return false;
		else
		{
			$cols = array();		
			foreach ($columns as $col)
				array_push($cols, $col->Field);		
			return $cols;
		}
	}

	/**
	 * Insert blank row in the multilanguage case	 
	 *
	 * @access public
	 * @param string $table table name
	 * @param int $pkMultiLang one of the PKs described the entity in the case of multilanguage content (empty by default)
	 * @param string $modelLang the current language of the model
	 * @param bool $respectDefaultValue true => respect defaults values, false => erase defaults values by ''
	 * @param array $exclu fields to exclude
	 * @since 1.0
	 */
	public function insertMultiLanguageRow($table,$pkMultiLang,$modelLang,$respectDefaultValue=true,$exclu=array())
	{
		$columns = $this->showColumns($table);
		if ($columns == false)
			return false;
		else
		{
			$sqlColumn = "";
			$sqlValues = "";			
			for($i=0;$i<count($columns);$i++) {
				if (!in_array($columns[$i]->Field, $exclu)) 
				{					
					if (strtolower($columns[$i]->Key) == "pri" && $columns[$i]->Field != "pk_lang")
					{
						$sqlColumn .= "`".$columns[$i]->Field."`,";
						$sqlValues .= $pkMultiLang.",";	
					}
					else if (strtolower($columns[$i]->Key) == "pri" && $columns[$i]->Field == "pk_lang")
					{
						$sqlColumn .= "`".$columns[$i]->Field."`,";
						$sqlValues .= "'".$modelLang."',";	
					}									
					else if ($columns[$i]->Default == "" || !$respectDefaultValue)
					{
						if (stristr($columns[$i]->Type, "char") !== false or stristr($columns[$i]->Type, "text") !== false) 
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'',";
						}
						elseif (stristr($columns[$i]->Type, "int") !== false)
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'0',";	
						}
						elseif (strtolower($columns[$i]->Type) == "date")
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'0000-00-00',";	
						}
						elseif (strtolower($columns[$i]->Type) == "datetime")
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'0000-00-00 00:00:00',";	
						}
					}
				}
			}
			$sqlColumn = substr($sqlColumn, 0, (strlen($sqlColumn)-1));
			$sqlValues = substr($sqlValues, 0, (strlen($sqlValues)-1));
			$sql = "INSERT INTO `".$table."` (".$sqlColumn.") VALUES (".$sqlValues.")";			
			try
			{
				return $this->insert($sql);
			}
			catch (Exception $e)
			{			
				SLS_Tracing::addTrace($e,true);
				return false;
			}
		}		
	}
	
	/**
	 * Insert blank row in auto increment case	 
	 * 
	 * @access public
	 * @param string $table table name
	 * @param bool $respectDefaultValue true => respect defaults values, false => erase defaults values by ''
	 * @param array $exclu fields to exclude
	 * @return int $idInserted the id generated
	 * @since 1.0
	 */
	public function insertBlankRow($table,$respectDefaultValue=true,$exclu=array()) 
	{
		$columns = $this->showColumns($table);
		if ($columns == false)
			return false;
		else
		{
			$sqlColumn = "";
			$sqlValues = "";
			for($i=1;$i<count($columns);$i++) {
				if (!in_array($columns[$i]->Field, $exclu)) 
				{		
					if ($columns[$i]->Default == "" || !$respectDefaultValue)
					{
						if (stristr($columns[$i]->Type, "char") !== false or stristr($columns[$i]->Type, "text") !== false) 
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'',";	
						}
						elseif (stristr($columns[$i]->Type, "int") !== false)
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'0',";	
						}
						elseif (strtolower($columns[$i]->Type) == "date")
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'0000-00-00',";	
						}
						elseif (strtolower($columns[$i]->Type) == "datetime")
						{
							$sqlColumn .= "`".$columns[$i]->Field."`,";
							$sqlValues .= "'0000-00-00 00:00:00',";	
						}
					}
				}
			}
			$sqlColumn = substr($sqlColumn, 0, (strlen($sqlColumn)-1));
			$sqlValues = substr($sqlValues, 0, (strlen($sqlValues)-1));
			$sql = "INSERT INTO `".$table."` (".$sqlColumn.") VALUES (".$sqlValues.")";		
			try
			{
				return $this->insert($sql);
			}
			catch (Exception $e)
			{			
				SLS_Tracing::addTrace($e,true);
				return false;
			}
		}
	}
	
	/**
	 * Escape dangerous caracters into the sql query	 	 
	 *
	 * @access public
	 * @param string $string the value to escape
	 * @return string $valueSecure the value escaped
	 * @since 1.0
	 */
	public function quote($string) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();		
		try
		{
			return $this->_dbh[$this->_currentDb]->quote($string);
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Update one table	 
	 *
	 * @access public
	 * @param string $table table name
	 * @param int $id the pk
	 * @param array $array the values
	 * @return bool $updated true if updated, else false
	 * @since 1.0
	 */
	public function updateTable($table, $id, $array) 
	{
		if (!$this->tableExists($table))
			return false;
		if (!is_numeric($id))
			return false;
		if (!is_array($array) || count($array) == 0)
			return false;
			
		$cols = $this->getColumnsName($table);
		$insertSql = '';
		foreach ( $array as $key => $value) 		
			$insertSql .= ' `'.$key.'`='.$this->quote($value).',';		
		if (!empty($insertSql)) 
		{	
			$insertSql = substr($insertSql, 0, (strlen($insertSql)-1));
			$sql = "UPDATE `".$table."` SET ".$insertSql." WHERE `".$cols[0]."` = ".$this->quote($id)." LIMIT 1";
			try
			{
				$this->update($sql);
			}
			catch (Exception $e)
			{			
				SLS_Tracing::addTrace($e,true);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Return status & informations regarding one or more table
	 *
	 * @access public
	 * @param string $table the table to list
	 * @return array $infos array of table(s) informations
	 * @since 1.0
	 */
	public function showTables($table=false) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();	
		
		$sql = "SHOW TABLE STATUS FROM `".$this->_generic->getDbConfig("base",$this->_currentDb)."`";		
		try
		{
			$statement = $this->_dbh[$this->_currentDb]->query($sql);
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
		
		$return = array();
		while ($result = $statement->fetchObject()) 
		{
			if ($table === false) 			
				$return[] = $result;			
			else  
			{
				if ($result->Name == $table) 
				{
					$return = $result;
					break;
				}
			}				
		}
		return $return;
	}
	
	/**
	 * Check if a table exists	 
	 *
	 * @access public
	 * @param string $table the table to check
	 * @return bool $exists true if ok, else false
	 * @since 1.0
	 */
	public function tableExists($table) 
	{
		// If no active connexion
		if (empty($this->_dbh))
			$this->connectToDefaultDb();
			
		$result = $this->showTables($table);
		if (is_array($result) && count($result) == 0) 		
			return false;		
		return true;
	}
	
	/**
	 * Validate an order by	 
	 *
	 * @access public
	 * @param string $asc the order
	 * @return bool $valid true if of, else false
	 * @since 1.0
	 */
	public function validateAsc($asc) 
	{
		if (strtoupper($asc) != "ASC" && strtoupper($asc) != "DESC")
			return false;
		else 
			return strtoupper($asc);
	}
	
	/**
	 * Force connexion on default database
	 *
	 * @access public
	 * @since 1.0
	 */
	public function connectToDefaultDb()
	{
		$result = array_shift($this->_generic->getDbXML()->getTagsAttribute("//dbs/db[@isDefault='true']","alias"));		
		$this->changeDb($result["attribute"]);
	}
}
?>