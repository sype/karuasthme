<?
/**
 * SLS_MemberSession class - Manage Member session
 *  
 * @author Florian Collot
 * @copyright SillySmart
 * @package Sls.Generics.Session
 * @since 1.0 
 */
class SLS_MemberSession
{
	private static $_instance;
	private $_generic;
	private $_session;
	private $_cookie;
	private $_login = "";
	private $_account_id = 0;
	
	/**
	 * Constructor
	 *
	 * @access public	 
	 * @since 1.0	 
	 */
	public function __construct() 
	{
		$this->_generic = SLS_Generic::getInstance();
		$this->_session = $this->_generic->getObjectSession();
		$this->_cookie = $this->_generic->getObjectCookie();
	}
	
	/**
	 * Singleton
	 *
	 * @access public static	 
	 * @return SLS_MemberSession $instance SLS_MemberSession instance
	 * @since 1.0
	 */
	public static function getInstance() 
	{
		if (is_null(self::$_instance))
			self::$_instance = new SLS_MemberSession();		
		return self::$_instance;
	}
	
	/**
	 * Check if a member is logged 
	 *
	 * @access public
	 * @return int $logged -1 not logged, 0 not logged but already visited, 1 logged
	 * @since 1.0
	 */
	public function isLogged() 
	{
		// If we find the user in the session		
		if ($this->_session->getParam('isLogged') === true)  
		{
			// If its last action have more than one hour
			if ($this->_session->getParam('lastAction') < (time()-3600)) 
			{
				// If we find in the session his login, we set the class variable and we return 0 (session expired)				
				$testLogin = $this->_session->getParam("login");
				if (!empty($testLogin))
				{
					$this->_login = $this->_session->getParam("login");
					return 0;
				}
				// Else, the PHP session have totaly expired
				else 				
					return -1;				
			}
			// If his last action is less than one hour, we take his login, his account_id and we return 1			
			else 
			{
				$this->_login = $this->_session->getParam("login");
				$this->_account_id = $this->_session->getParam("account_id");
				$this->keepAlive();
				return 1;
			}
		}
		// If the user is unknown in the session, we search in the cookies		
		else 
		{
			// If we find in the cookie the information that we show that he already comes in this website			
			if ($this->_cookie->getParam('hasConnection') === true)
			{
				// If we find his login, we return 0 (user known but need to reauthenticate)				
				$testLogin = $this->_cookie->getParam("login"); 
				if (!empty($testLogin))
				{
					$this->_session->setParam("login", $this->_cookie->getParam("login"));
					$this->_login = $this->_session->getParam("login");
					return 0;
				}
				// Else, we return -1 (invalid session)				
				else				
					return -1;				
			}
			// If we don't find any cookie, we return -1 (invalid session)			
			else			
				return -1;			
		}
	}
	
	/**
	 * Renew the session at each page - execute only when the user is logged	 
	 * 
	 * @access public
	 * @param string $login the login
	 * @param int $account the account_id
	 * @return bool true if ok, else false
	 * @since 1.0
	 */
	public function keepAlive($login=false,$account=false) 
	{
		if ($login === false)	
		{	
			if (empty($this->_account_id) || empty($this->_login)) 			
				return false;			
		}
		else 
		{
			if (empty($login) || empty($account) ||!is_numeric($account))			
				return false;			
			else 
			{
				$this->_login = $login;
				$this->_account_id = $account;
			}
		}
		$this->_session->setParam('isLogged', true);
		$this->_session->setParam('lastAction', time());
		$this->_session->setParam('login', $this->_login);
		$this->_session->setParam('account_id', $this->_account_id);
		$this->_cookie->put('hasConnection', true);
		$this->_cookie->put('login', $this->_login);
		$this->_cookie->set();
		return true;
	}
		
	/**
	 * Get the login
	 *
	 * @access public
	 * @return string $login the login
	 * @since 1.0
	 */
	public function getLogin()
	{
		return $this->_login;
	}
	
	/**
	 * Get the account_id
	 *
	 * @access public
	 * @return int $account_id the account_id
	 * @since 1.0
	 */
	public function getAccountId() 
	{
		return $this->_account_id;
	}
	
	/**
	 * Start the session for a user who is authenticating
	 *
	 * @access public
	 * @param string $login the login
	 * @param int $accountId the account_id
	 * @return bool
	 * @since 1.0
	 */
	public function initSession($login, $accountId) 
	{
		if (empty($login) || empty($accountId) ||!is_numeric($accountId))
			return false;		
		$this->keepAlive($login, $accountId);
		return true;
	}
	
	/**
	 * Tracking - Set the previous page	 
	 *
	 * @access public
	 * @since 1.0
	 */
	public function setTrackingPage() 
	{
		$http = $this->_generic->getObjectHttpRequest();
		$params = $http->getParams();
		$strPost = "";
		foreach($params as $key => $value)
			if ($key != "mode" && $key != "smode" && !empty($value))
				$strPost .= $key."/".$value."/";
		
		$this->_session->setParam('previousPage', $_SERVER['REQUEST_URI']);
		$this->_session->setParam('previousController', $this->_generic->getGenericControllerName());
		$this->_session->setParam('previousScontroller', $this->_generic->getGenericScontrollerName());
		$this->_session->setParam("previousMode", $http->getParam("mode"));
		$this->_session->setParam("previousSmode", $http->getParam("smode"));
		$this->_session->setParam("previousPost", $strPost);
		$this->_session->setParam("previousMore", substr($_SERVER['QUERY_STRING'],strlen("mode=".$http->getParam("mode")."&smode=".$http->getParam("smode"))));
	}
	
	/**
	 * Tracking - Get the previous page
	 *
	 * @access public
	 * @return string
	 * @since 1.0
	 */
	public function getPreviousPage()
	{
		return $this->_session->getParam("previousPage");
	}
	
	/**
	 * Kill the current session
	 *
	 * @access public
	 * @since 1.0
	 */
	public function killSession()
	{
		$this->_session->destroy();
	}
	
	/**
	 * Reroute the user towards the login page
	 * 	 
	 * @access public
	 * @param string $controller controller name for the redirection, false if no redirection
	 * @param string $scontroller scontroller name for the redirection, false if no redirection
	 * @param string $arg optionnal arguments "key/value/key2/value2"
	 * @since 1.0
	 */
	public function needConnection($controller, $scontroller, $arg=false)
	{
		$controllers = $this->_generic->getTranslatedController("Profil", "Login");
		if ($arg !== false)
			$urlAdd = $arg."/";
		else 	
			$urlAdd = "";
		if ($controller !== false and $scontroller !== false)
		{
			$controllers2 = $this->_generic->getTranslatedController($controller, $scontroller);
			$this->_generic->redirect($controllers['controller']."/".$controllers['scontroller']."/Error/Need_Connection/Redirect/".$controllers2['controller']."|".$controllers2['scontroller']."/".$urlAdd."index.html");
		}
		else
			$this->_generic->redirect($controllers['controller']."/".$controllers['scontroller']."/Error/Need_Connection/".$urlAdd."index.html");		
	}
}
?>