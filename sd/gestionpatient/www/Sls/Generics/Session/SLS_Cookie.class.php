<?php
/**
 * SLS_Cookie class - Manage cookie
 *  
 * @author Florian Collot
 * @copyright SillySmart
 * @package Sls.Generics.Session
 * @since 1.0 
 */
class SLS_Cookie
{
	var $_name  = "";
	var $_val   = array();
	var $_expires;
	var $_dir   = '/';
	var $_site  = '';
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @since 1.0
	 */
	public function __construct() {}
	
	/**
	 * Create a new cookie
	 *
	 * @access public
	 * @param string $cname the name of the cookie
	 * @param int $cexpires the expiration date
	 * @param string $cdir the path
	 * @param string $csite the site name
	 * @since 1.0
	 */
	public function newCookie($cname, $cexpires="", $cdir="/", $csite="")
	{
		$this->_name=$cname;
		
		if($cexpires)    
			$this->_expires=$cexpires;    
		else    
			$this->_expires=time() + 60*60*24*30*12; // ~12 months
		
		$this->_dir=$cdir;
		$this->_site=$csite;
		$this->_val=array();
		$this->extract();
	}
	
	/**
	 * Get a cookie
	 *
	 * @access public
	 * @param string $cname the name of the cookie
	 * @since 1.0
	 */
	public function extract($cname="")
	{
		if(!isset($_COOKIE))
		{
			global $_COOKIE;
			$_COOKIE=$GLOBALS["HTTP_COOKIE_VARS"];
		}
		
		if(empty($cname) && isset($this))    
			$cname=$this->_name;    
		else
			$this->_name = $cname;  
		
		if(!empty($_COOKIE[$cname]))
		{		
			if(get_magic_quotes_gpc())      
				$_COOKIE[$cname]=stripslashes($_COOKIE[$cname]);			
			$arr=unserialize($_COOKIE[$cname]);
			
			if($arr!==false && is_array($arr))
			{
				foreach($arr as $var => $val)
				{
					$_COOKIE[$var]=$val;
					if(isset($GLOBALS["PHP_SELF"]))	       
						$GLOBALS[$var]=$val;         	
				}
			}
			if(isset($this)) 
				$this->_val=$arr;
		}
		// remove the site cookie from the global scope.
		unset($_COOKIE[$cname]);
		unset($GLOBALS[$cname]);
	}
	
	/**
	 * Search into cookie a value described by a given key
	 *
	 * @access public
	 * @param string $key the key to search
	 * @return string $string the value
	 * @see SLS_Cookie::getKey
	 * @since 1.0
	 */
	public function getParam($key) 
	{
		$string = "";
		if(isset($this->_val))  	
			foreach ($this->_val as $var => $value)  		
				if ($var == $key)  			
					$string = $value;
		return $string;
	}
	
	/**
	 * Search into cookie a key described by a given value
	 *
	 * @access public
	 * @param string $value the value to search
	 * @return array $return the key
	 * @see SLS_Cookie::getParam
	 * @since 1.0
	 */
	public function getKey($value) 
	{
		$return = array();
		if(isset($this->_val))
			foreach ($this->_val as $var => $val)  		
				if ($val == $value)
					array_push($return, $var);  	
		return $return;
	}
	
	/**
	 * Push a couple key/value into the cookie   
	 *
	 * @access public
	 * @param string $var the key
	 * @param string $value the value
	 * @since 1.0
	 */
	public function put($var, $value)
	{
		$_COOKIE[$var]=$value;
		$this->_val["$var"]=$value;
		
		if(isset($GLOBALS["PHP_SELF"]))    
			$GLOBALS[$var]=$value;
		
		if(empty($value))
			unset($this->_val[$var]); 
	}
	
	/**
	 * Clear the current object, if u want to delete the cookie, call the function set() after a call to clear()   
	 *
	 * @access public
	 * @since 1.0
	 */
	public function clear()
	{
		$this->_val=array();
	}
	
	/**
	 * Write the cookie
	 *
	 * @access public
	 * @since 1.0
	 */
	public function set()
	{
		if(empty($this->_val))        	
			$cookie_val="";     
		else
			$cookie_val=serialize($this->_val);    
		if(strlen($cookie_val)>4*1024)
			SLS_Tracing::addTrace(new Exception("The cookie $this->_name exceeds the specification for the maximum cookie size.  Some data may be lost", E_USER_WARNING));
		
		try {	
			setcookie($this->_name, $cookie_val, $this->_expires, $this->_dir, $this->_site);
		}
		catch (Exception $e)
		{
			SLS_Tracing::addTrace($e);
		}
	}
	
	/**
	 * Delete the current cookie
	 *
	 * @access public
	 * @since 1.0
	 */
	public function delete()
	{
		$cookie_val = "";
		setcookie($this->_name,"",0);
	}  
}
?>