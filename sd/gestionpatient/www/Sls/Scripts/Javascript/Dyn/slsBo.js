var page = null;
window.addEvent('domready', function(){
	new SlsBo(page);
});
var SlsBo = new Class ({
	Implements: [Events, Options],
	options : {
	},
	
	initialize:function(type, options){
		this.type = type;
		switch(this.type){
			case 'edit':
				this.langSelected = null;
				this.initEdit();
				break;
			case 'login':
				this.initLogin();
				break;	
			case 'listing':
				this.initGroupBySelect();
				this.initExportSelect();
				this.initListing();
				break;
			default:
				break;
		}
		this.initOpenWrappers();
	},
	
	initEdit : function(){
		$$('div.grey_wrapper_no_padding ul.langs li a').each(function(element, index){
			if (element.get('class') == 'selected')
				this.langSelected = element.get('id').split('_')[0];
			/*if (index == 0){
				element.set('class', 'selected');
				this.langSelected = element.get('id').split('_')[0];
			}*/
			element.addEvent('click', function(e, element){
				this.editSwitchLang(element);
				e.stop();
			}.bindWithEvent(this, [element]))
		}.bind(this));
		$$('div.grey_wrapper_no_padding div.edit_form div.langs_form_wrapper').each(function(element, index){			
			if (element.get('id') == this.langSelected+'_form_wrapper')
				element.setStyle('display', 'block');
			else
				element.setStyle('display', 'none');
		}.bind(this));
	},
	
	editSwitchLang : function(element){
		var rel = element.get('id').split('_')[0];
		if (rel == this.langSelected)
			return false;
		$(rel+'_form_wrapper').setStyle('display', 'block');
		$(this.langSelected+'_form_wrapper').setStyle('display', 'none');
		$(this.langSelected+'_form_menu').set('class', '');
		$(rel+'_form_menu').set('class', 'selected');
		this.langSelected = rel;
	},
	
	initLogin : function(){
		
	},
	
	initOpenWrappers : function(){
		$$('a.open_wrapper_close').each(function(element, index){
			var id = element.get('id').split('_')[1];
			var h_calc = $('wrap_'+id).getFirst('div.h_calc');
			if ($chk($('wrap_'+id)) && $chk(h_calc)) {
				$('wrap_'+id).setStyles({
					'height' : '0px',
					'overflow' : 'hidden'
				});
				element.addEvent('click', function(e, id, h_calc){
					this.openWrapper(id, h_calc);
					e.stop();
				}.bindWithEvent(this, [id, h_calc]));
			}
		}.bind(this));
	},
	
	openWrapper : function(id, h_calc){
		var y = h_calc.getSize()['y'];
		var morph = new Fx.Morph($('wrap_'+id), {
			onComplete : function(){
				$('wrap_'+id).setStyles({
					'height' : 'auto',
					'overflow' : 'visible'
				});
				$('but_'+id).set('class', 'open_wrapper');
				$('but_'+id).removeEvents('click');
				$('but_'+id).addEvent('click', function(e, id, h_calc){
					this.closeWrapper(id, h_calc);
					e.stop();
				}.bindWithEvent(this, [id, h_calc]));
			}.bind(this)
		});
		morph.start({
			'height' : y.toString()+"px"
		});
	},
	
	closeWrapper : function(id, h_calc){
		var morph = new Fx.Morph($('wrap_'+id), {
			onStart : function(){
				$('wrap_'+id).setStyle('overflow', 'hidden');
			},
			onComplete : function(){
				$('but_'+id).set('class', 'open_wrapper_close');
				$('but_'+id).removeEvents('click');
				$('but_'+id).addEvent('click', function(e, id, h_calc){
					this.openWrapper(id, h_calc);
					e.stop();
				}.bindWithEvent(this, [id, h_calc]));
			}.bind(this)
		});
		morph.start({
			'height' : "0px"
		});
	},
	
	initGroupBySelect : function(){
		if (!$chk($('bo_listing_group_by')) || !$chk($('boForm')))
			return;
		//new Selects($('boForm'), $('bo_listing_group_by'));
	},
	
	initExportSelect : function(){
		if (!$chk($('listing_select_export')) || !$chk($('export_form')))
			return;
		new Selects($('export_form'), $('listing_select_export'),{
			'gap' : {
 					'left' : 80,  
 					'width': -80, 
 					'top'  : 25, 
 					'rTop': 0, 
 					'rLeft': 80 
			 	},
			 onSelect : function(element) {
			 		window.location = $('export_form').get('action')+'/Export_Type/'+element.get('href');
			 		Selects.elements['listing_select_export'].reset();
			 	}
		});
	},
	
	initListing : function(){
		$$('table.listing tr.clear td').each(function(element, index){
			var tr = element.getParent('tr');
			var check = tr.getFirst('td').getFirst('input');
			if (element.get('class') != 'check') {
				element.addEvent('click', function(e, element, tr, check){
					if (check.get('checked') == true){
						check.set('checked', false);
						this.updateDeleteListing();
						this.updateEditListing();
					}
					else{
						check.set('checked', true);
						this.updateDeleteListing();
						this.updateEditListing();
					}
					e.stop();
				}.bindWithEvent(this, [element, tr, check]));
			}
			else {
				check.addEvent('change', function(){
					this.updateDeleteListing();
					this.updateEditListing();
				}.bind(this));
			}
		}.bind(this));
		$$('table.listing tr.dark td').each(function(element, index){
			var tr = element.getParent('tr');
			var check = tr.getFirst('td').getFirst('input');
			if (element.get('class') != 'check') {
				element.addEvent('click', function(e, element, tr, check){
					if (check.get('checked') == true){
						check.set('checked', false);
						this.updateDeleteListing();
						this.updateEditListing();
					}
					else{
						check.set('checked', true);
						this.updateDeleteListing();
						this.updateEditListing();
					}
					e.stop();
				}.bindWithEvent(this, [element, tr, check]));
			}
			else {
				check.addEvent('change', function(){
					this.updateDeleteListing();
					this.updateEditListing();
				}.bind(this));
			}
		}.bind(this));
	},
	
	updateEditListing : function(){
		var inputs = $$('table.listing tr td.check input.filter_checkbox');
		var el = $$('ul.listing_actions .edit')[0];
		var selected = 0;
		for (var i=0;i<inputs.length;i++){
			if (inputs[i].get('checked'))
				selected++;
		}
		if (selected == 1){
			new Element('a', {
				'href' : '',
				'class': 'edit',
				'html' : el.get('html'),
				'events': {
					'click' : function(e) {
						var inputs = $$('table.listing tr td.check input.filter_checkbox');
						for (var i=0;i<inputs.length;i++){
							if (inputs[i].get('checked')){
								window.location = modify+inputs[i].get('id')+'.sls';
								break;
							}
						}
						e.stop();
					}
				}
			}).replaces(el);
		}
		else {
			new Element('span', {
				'class': 'edit',
				'html' : el.get('html')
			}).replaces(el);
		}
	},
	
	updateDeleteListing : function(){
		var inputs = $$('table.listing tr td.check input.filter_checkbox');
		var el = $$('ul.listing_actions .delete')[0];
		var selected = 0;
		for (var i=0;i<inputs.length;i++){
			if (inputs[i].get('checked'))
				selected++;
		}
		if (selected > 0){
			new Element('a', {
				'href' : '',
				'class': 'delete',
				'html' : el.get('html'),
				'events' : {
					'click' : function(e){
						var inputs = $$('table.listing tr td.check input.filter_checkbox');
						var ids = "";
						for (var i=0;i<inputs.length;i++){
							if (inputs[i].get('checked')){
								ids  += inputs[i].get('id')+'|';
							}
						}
						if (confirm('Are you sure to delete this records ?')){
							window.location = del+ids+'.sls';
						}
						e.stop();
					}
				}
			}).replaces(el);
		}
		else {
			new Element('span', {
				'class': 'delete',
				'html' : el.get('html')
			}).replaces(el);
		}
	}
	
});