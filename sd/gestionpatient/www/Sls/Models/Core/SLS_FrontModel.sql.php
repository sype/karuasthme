<?php
/**
 * Mother class of SQL models
 * 
 * @author Florian Collot
 * @author Laurent Bientz
 * @copyright SillySmart
 * @package Sls.Models.Core  
 * @see SLS_Frontmodel
 * @since 1.0
 */
class SLS_FrontModelSql
{
	// Class variables	
	protected $_db;
	protected $_table;
	protected $_primaryKey;
	protected $_isMultilanguage = false;
	protected $_modelLanguage = "";	
	protected $_generic;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @param string $table SQL table describing current child model
	 * @param string $primaryKey SQL primary key describing current child model
	 * @param bool $multilanguage true if the we have multilanguage content, else false
	 * @since 1.0
	 */
	public function __construct($table,$primaryKey,$multilanguage=false) 
	{
		$this->_isMultilanguage = ($multilanguage) ? true : false;
		
		$this->_db = SLS_Sql::getInstance();
		$this->_generic = SLS_Generic::getInstance();
		$this->_table = $table;
		$this->_primaryKey = $primaryKey;
	}
	
	/**
	 * Set the current language of the model
	 *
	 * @access public
	 * @param string $lang the current language of the model (generic lang if not filled)
	 * @since 1.0
	 */
	public function setModelLanguage($lang="")
	{
		if ($this->_isMultilanguage)
		{
			$this->_modelLanguage = (empty($lang)) ? $this->_generic->getObjectLang()->getLang() : $lang;
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * Get an object model
	 *
	 * @access public
	 * @param int $id the id of the model (PK)
	 * @return bool true if ok, else false
	 * @since 1.0
	 */
	public function getModel($id) 
	{		
		$sql = "SELECT * FROM `".$this->_table."` WHERE ".$this->_primaryKey." = ".$this->_db->quote($id)." ";
		if ($this->_isMultilanguage)
			$sql .= "AND pk_lang = ".$this->_db->quote($this->_modelLanguage)." ";
		$sql .= " LIMIT 1";
		return array_shift($this->_db->select($sql));
	}
	
	/**
	 * Create child model
	 *
	 * @access public
	 * @param int $pkMultiLang one of the PKs described the entity in the case of multilanguage content (empty by default)
	 * @see SLSFrontModelSql::delete
	 * @see SLSFrontModelSql::save
	 * @since 1.0
	 */
	public function create($pkMultiLang="")
	{
		if (empty($pkMultiLang))
			return $this->_db->insertBlankRow($this->_table, true);
		else
			return $this->_db->insertMultiLanguageRow($this->_table,$pkMultiLang,$this->_modelLanguage);			
	}
	
	/**
	 * Get child model entity described by primary key
	 *
	 * @access public
	 * @param int $id primary key of wanted model
	 * @return array PDO Object
	 * @since 1.0
	 */
	public function get($id)
	{		
		if (!is_numeric($id))
			return false;
		try
		{				
			$sql = "SELECT * FROM `".$this->_table."` WHERE ".$this->_primaryKey."=".$this->_db->quote($id)." ";			
			if ($this->_isMultilanguage)
				$sql .= "AND pk_lang = ".$this->_db->quote($this->_modelLanguage)." ";			
			$sql .= "LIMIT 1";			
			return $this->_db->select($sql);
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
		
	/**
	 * Update the current child model
	 * 	 
	 * @access public
	 * @param int $id primary key of the current model
	 * @param array $array associative array with modified keys/values
	 * @return bool true if updated, else false
	 * @see SLSFrontModelSql::create
	 * @see SLSFrontModelSql::delete
	 * @since 1.0
	 */
	public function save($id, $array) 
	{
		if (!is_numeric($id))
			return false;
		if ( $this->get($id) === false)
			return false;
		if (!is_array($array) || count($array) == 0)
			return false;		
		$insertSql = '';
		foreach ($array as $key => $value) 
		{
			if (in_array($key,$this->_db->getColumnsName($this->_table)))
				$insertSql .= ' '.$key.'='.$this->_db->quote($value).',';
			else
				SLS_Tracing::addTrace(new Exception("Warning: Column ".$key." doesn't exist in table `".$this->_table."`"),true);
		}
		if (!empty($insertSql)) 
		{	
			$insertSql = substr($insertSql, 0, (strlen($insertSql)-1));
			$sql = "UPDATE `".$this->_table."` SET ".$insertSql." WHERE ".$this->_primaryKey." = ".$this->_db->quote($id)." ";
			if ($this->_isMultilanguage)
				$sql .= "AND pk_lang = ".$this->_db->quote($this->_modelLanguage)." ";
			$sql .= " LIMIT 1";
			try
			{	
				$this->_db->update($sql);
			}
			catch (Exception $e)
			{			
				SLS_Tracing::addTrace($e,true);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Delete child model
	 *
	 * @access public
	 * @param int $id primary key of the current model
	 * @return bool true if deleted, else false
	 * @see SLSFrontModelSql::create
	 * @see SLSFrontModelSql::save
	 * @since 1.0
	 */
	public function delete($id) 
	{
		if ($this->get($id) === false)
			return false;
		$sql = "DELETE FROM `".$this->_table."` WHERE ".$this->_primaryKey."=".$this->_db->quote($id)." ";
		if ($this->_isMultilanguage)
			$sql .= "AND pk_lang = ".$this->_db->quote($this->_modelLanguage)." ";
		$sql .= " LIMIT 1";
		try
		{
			$this->_db->delete($sql);
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}		
		return true;
	}
	
	/**
	 * Search of n objects of models
	 *
	 * @access public
	 * @param string $table the current table to list (default: empty => current model)
	 * @param array $joins the table(s) to join with current table (default: empty => no join)
	 * If you want to natural join: 
	 * array("table_2","table_3","...","table_n") will give 'SELECT * FROM table_1 NATURAL JOIN table_2 NATURAL JOIN table_3 ... NATURAL JOIN table_n'
	 * If you want to join with a specific column: 
	 * array(0=>array("table"=>"table_2","column"=>"column_2"),1=>array("table"=>"table_3","column"=>"column_3"))	 
	 * @param array $clause the clause wanted (default: empty => no clause)
	 * array
	 * (
	 *		[0] => array
	 *				(
	 *					["column"] 	= "column_1",
	 *					["value"] 	= "value_1",
	 *					["mode"]	= "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 *		[1] => array
	 *				(
	 *					["column"] 	= "user_department",
	 *					["value"] 	= "75",
	 *					["mode"]	= "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 * )
	 * @param array $group a group by (default: empty => no group by)
	 * array("column_1","column_2","...","column_n")
	 * @param mixed $order the order you want (default: empty => ORDER BY primary key ASC)
	 * array
	 * (
	 * 		[0] => array
	 * 				(
	 * 					["column"] 	= "column_1",
	 * 					["order"]	= "asc"
	 * 				)
	 * 		[1] => array
	 * 				(
	 * 					["column"] 	= "column_2",
	 * 					["order"]	= "desc"
	 * 				)
	 * )
	 * or
	 * string "rand()"
	 * @param array $limit the limit you want (default: empty for all recordsets)
	 * array("start" => "10", "length" => "30")	 
	 * @return array PDO Object
	 * @since 1.0
	 */
	public function searchModels($table="",$joins=array(),$clause=array(),$group=array(),$order=array(),$limit=array())
	{
		$allowToJoin = false;
		
		/**
		 * TABLE NAME
		 */
		// If table name haven't been filled, try to recover table name from the current model
		if (empty($table))
		{
			$table = (empty($this->_table)) ? substr(get_class($this),0,strlen(get_class($this))-3) : $this->_table;
		}
		// If table name is again empty, throw a Sls Exception
		if (empty($table))
		{
			SLS_Tracing::addTrace(new Exception("Error: Table's name has been omitted"),true);
			return false;
		}
		// If table name filled but table doesn't exists in MySQL, throw a Sls Exception
		else if (!$this->_db->tableExists($table))
		{
			SLS_Tracing::addTrace(new Exception("Error: Table `".$table."` doesn't exist in database `".($this->_db->getCurrentDb())."`"),true);
			return false;
		}
		/**
		 * /TABLE NAME
		 */
				
		
		/**
		 * JOINS
		 */
		// Get all the columns of the current table
		$columnsMain = $this->_db->getColumnsName($table);
		$joinMain = $table;
		
		// If we want to join tables
		if (is_array($joins) && !empty($joins))
		{
			// Foreach tables to join
			foreach ($joins as $currentJoin)
			{
				// If we want joins with the clause "using"
				if (is_array($currentJoin))
					$currentJoin = $currentJoin["table"];
				
				// If the table to join doesn't exists in MySQL, throw a Sls Exception
				if (!$this->_db->tableExists($currentJoin))
					SLS_Tracing::addTrace(new Exception("Warning: Table `".$currentJoin."` to join with `".$joinMain."` doesn't exist in database `".(SLS_Generic::getInstance()->getDbConfig("base"))."`"),true);			
				// Else check if we can join
				else 
				{
					// Get all the columns of the table to join
					$columnsJoin = $this->_db->getColumnsName($currentJoin);
					
					// If we want joins with the clause "using", allow to join and merge the columns of the table to join with all the columns already listed
					if (is_array($currentJoin))
					{
						$allowToJoin = true;
						$columnsMain = array_merge($columnsMain,$columnsJoin);	
					}
					// Else if we want a "NATURAL JOIN", check if we have a common key
					else
					{
						// Foreach columns of the current table
						foreach($columnsMain as $tMain)
						{
							// Foreach columns of the table to join
							foreach($columnsJoin as $tJoin)
							{
								// If we have a common column, allow to join and merge the columns of the table to join with all the columns already listed
								if ($tJoin == $tMain)
								{
									$allowToJoin = true;
									$columnsMain = array_merge($columnsMain,$columnsJoin);
								}
							}
						}
					}
					
					// If we can't join, throw a Sls Exception
					if (!$allowToJoin)
					{
						SLS_Tracing::addTrace(new Exception("Warning: Table `".$currentJoin."` to join with `".$joinMain."` doesn't have a common key"),true);
						$joins = array();
						break;
					}
					
					// Move the reference table
					$joinMain = $currentJoin;
				}
			}
		}
		
		// Build the start of the query
		$sql = "SELECT * FROM `".$table."` ";
		
		// If we can join, build the join
		if ($allowToJoin && is_array($joins) && !empty($joins))
		{
			foreach ($joins as $currentJoin)
			{
				if (is_array($currentJoin))
					$sql .= "JOIN ".$currentJoin["table"]." USING(".$currentJoin["column"].") ";
				else
					$sql .= "NATURAL JOIN ".$currentJoin." ";
			}
		}
		/**
		 * /JOINS
		 */
		
		
		/**
		 * CLAUSE
		 */
		// If we want a clause where
		if (!empty($clause))
		{			
			$lastSucceeded = false;
			// Foreach items to restrict
			for($i=0 ; $i<count($clause) ; $i++)
			{
				// If column on which you want to apply a restrict clause is in the list of columns collected on the tables
				$columnName = (SLS_String::contains($clause[$i]["column"],".")) ? SLS_String::substrAfterLastDelimiter($clause[$i]["column"],".") : $clause[$i]["column"];
				if (in_array($columnName,$columnsMain))
				{
					$sql .= ($lastSucceeded) ? " AND " : " WHERE ";
					
					// Build the correct statement
					switch($clause[$i]["mode"])
					{
						case "like":
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])."%"));
							break;
						case "notlike":
							$sql .= ("LOWER(".$clause[$i]["column"].") NOT LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])."%"));
							break;							
						case "beginwith":
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote(strtolower($clause[$i]["value"])."%"));
							break;
						case "endwith":
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])));
							break;
						case "equal":
							$sql .= ("".$clause[$i]["column"]." = ".$this->_db->quote($clause[$i]["value"]));	
							break;
						case "notequal":
							$sql .= ("".$clause[$i]["column"]." != ".$this->_db->quote($clause[$i]["value"]));	
							break;
						case "lt":
							$sql .= ("".$clause[$i]["column"]." < ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "le":
							$sql .= ("".$clause[$i]["column"]." <= ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "ge":
							$sql .= ("".$clause[$i]["column"]." >= ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "gt":
							$sql .= ("".$clause[$i]["column"]." > ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "null":
							$sql .= ("".$clause[$i]["column"]." IS NULL ");
							break;
						case "notnull":
							$sql .= ("".$clause[$i]["column"]." IS NOT NULL ");
							break;
						default:
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])."%"));
							break;
					}						
					$lastSucceeded = true;
				}
				// Else, throw a Sls Exception
				else
					SLS_Tracing::addTrace(new Exception("Warning: Column ".$clause[$i]["column"]." in clause where doesn't exist in table `".$table."` (and collected tables to join)"),true);
			}
		}
		/**
		 * /CLAUSE
		 */
		
		
		/**
		 * GROUP BY
		 */
		// If we want a group by
		if (!empty($group) && is_array($group))
		{
			$lastSucceeded = false;
			// Foreach items to group
			foreach($group as $colGroup)			
			{
				// If column on which you want to apply a group by statement is in the list of columns collected on the tables
				$columnName = (SLS_String::contains($colGroup,".")) ? SLS_String::substrAfterLastDelimiter($colGroup,".") : $colGroup;
				if (in_array($columnName,$columnsMain))
				{
					$sql .= ($lastSucceeded) ? ", " : " GROUP BY ";
					$sql .= $colGroup." ";
					$lastSucceeded = true;
				}
				// Else, throw a Sls Exception
				else
					SLS_Tracing::addTrace(new Exception("Warning: Column ".$columnName." in group by statement doesn't exist in table `".$table."` (and collected tables to join)"),true);
			}
		}
		/**
		 * /GROUP BY
		 */
		
		
		/**
		 * ORDER
		 */
		// If we haven't fill the order to apply, order by pk asc
		if (empty($order))
		{			
			// If the table want is different with current model, get the primary key of this table
			if ($this->_table != $table)
			{				
				$className = SLS_String::tableToClass($table);
				$this->_generic->useModel($className,$this->_db);
				$object = new $className();
				
				$pk = $object->getPrimaryKey();
			}
			// Else, just get current pk
			else
				$pk = $this->_primaryKey;
			$order = array(array("column" => $pk, "order" => "asc"));			
		}
		// If we want to order by rand()
		if ($order == "rand()")
		{
			$sql .= " ORDER BY rand() ";
		}
		// Else, construct the order statement
		else
		{
			$lastSucceeded = false;
			// Foreach items to order
			for($i=0 ; $i<$count=count($order) ; $i++)					
			{
				$columnName = (SLS_String::contains($order[$i]["column"],".")) ? SLS_String::substrAfterLastDelimiter($order[$i]["column"],".") : $order[$i]["column"];
				// If column on which you want to order is in the list of columns collected on the tables
				if (in_array($columnName,$columnsMain))
				{
					$sql .= ($lastSucceeded) ? ", " : " ORDER BY ";
					$sql .= $order[$i]["column"];
					$sql .= ($this->_db->validateAsc($order[$i]["order"])) ? " ".strtoupper($order[$i]["order"])." " : " ASC ";
					$lastSucceeded = true;
				}
				// Else, throw a Sls Exception
				else
					SLS_Tracing::addTrace(new Exception("Warning: Column ".$columnName." in order by statement doesn't exist in table `".$table."` (and collected tables to join)"),true);
			}
		}
		/**
		 * /ORDER
		 */
		
		
		/**
		 * LIMIT
		 */
		// If we want to limit recordsets
		if (!empty($limit) && is_array($limit))
		{			
			if (is_numeric($limit["start"]) && is_numeric($limit["length"]) && $limit["start"] >= 0 && $limit["length"] > 0)
				$sql .= " LIMIT ".$limit["start"].",".$limit["length"];
		}
		/**
		 * /LIMIT
		 */		
		
		
		// Try to execute the built query
		try
		{
			return $this->_db->select($sql);
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Count the number of objects of models
	 *	 
	 * @access public
	 * @param string $table the current table to list (default: empty => current model)
	 * @param array $joins the table(s) to join with current table (default: empty => no join)
	 * If you want to natural join: 
	 * array("table_2","table_3","...","table_n") will give 'SELECT * FROM table_1 NATURAL JOIN table_2 NATURAL JOIN table_3 ... NATURAL JOIN table_n'
	 * If you want to join with a specific column: 
	 * array(0=>array("table"=>"table_2","column"=>"column_2"),1=>array("table"=>"table_3","column"=>"column_3"))	 
	 * @param array $clause the clause wanted (default: empty => no clause)
	 * array
	 * (
	 *		[0] => array
	 *				(
	 *					["column"] 	= "column_1",
	 *					["value"] 	= "value_1",
	 *					["mode"]	= "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 *		[1] => array
	 *				(
	 *					["column"] 	= "user_department",
	 *					["value"] 	= "75",
	 *					["mode"]	= "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 * )
	 * @param array $group a group by (default: empty => no group by)
	 * array("column_1","column_2","...","column_n")
	 * @return int the number of objects within limit
	 * @since 1.0
	 */
	public function countModels($table,$joins=array(),$clause=array(),$group=array())
	{
		$allowToJoin = false;
		
		/**
		 * TABLE NAME
		 */
		// If table name haven't been filled, try to recover table name from the current model
		if (empty($table))
		{
			$table = (empty($this->_table)) ? substr(get_class($this),0,strlen(get_class($this))-3) : $this->_table;
		}
		// If table name is again empty, throw a Sls Exception
		if (empty($table))
		{
			SLS_Tracing::addTrace(new Exception("Error: Table's name has been omitted"),true);
			return false;
		}
		// If table name filled but table doesn't exists in MySQL, throw a Sls Exception
		else if (!$this->_db->tableExists($table))
		{
			SLS_Tracing::addTrace(new Exception("Error: Table `".$table."` doesn't exist in database `".(SLS_Generic::getInstance()->getDbConfig("base"))."`"),true);
			return false;
		}
		/**
		 * /TABLE NAME
		 */

		
		/**
		 * JOINS
		 */
		// Get all the columns of the current table
		$columnsMain = $this->_db->getColumnsName($table);
		$joinMain = $table;
		
		// If we want to join tables
		if (is_array($joins) && !empty($joins))
		{
			// Foreach tables to join
			foreach ($joins as $currentJoin)
			{
				// If we want joins with the clause "using"
				if (is_array($currentJoin))
					$currentJoin = $currentJoin["table"];
				
				// If the table to join doesn't exists in MySQL, throw a Sls Exception
				if (!$this->_db->tableExists($currentJoin))
					SLS_Tracing::addTrace(new Exception("Warning: Table `".$currentJoin."` to join with `".$joinMain."` doesn't exist in database `".(SLS_Generic::getInstance()->getDbConfig("base"))."`"),true);			
				// Else check if we can join
				else 
				{
					// Get all the columns of the table to join
					$columnsJoin = $this->_db->getColumnsName($currentJoin);
					
					// If we want joins with the clause "using", allow to join and merge the columns of the table to join with all the columns already listed
					if (is_array($currentJoin))
					{
						$allowToJoin = true;
						$columnsMain = array_merge($columnsMain,$columnsJoin);	
					}
					// Else if we want a "NATURAL JOIN", check if we have a common key
					else
					{
						// Foreach columns of the current table
						foreach($columnsMain as $tMain)
						{
							// Foreach columns of the table to join
							foreach($columnsJoin as $tJoin)
							{
								// If we have a common column, allow to join and merge the columns of the table to join with all the columns already listed
								if ($tJoin == $tMain)
								{
									$allowToJoin = true;
									$columnsMain = array_merge($columnsMain,$columnsJoin);
								}
							}
						}
					}
					
					// If we can't join, throw a Sls Exception
					if (!$allowToJoin)
					{
						SLS_Tracing::addTrace(new Exception("Warning: Table `".$currentJoin."` to join with `".$joinMain."` doesn't have a common key"),true);
						$joins = array();
						break;
					}
					
					// Move the reference table
					$joinMain = $currentJoin;
				}
			}
		}
		
		// Build the start of the query
		$sql = "SELECT COUNT(*) AS total FROM `".$table."` ";
		
		// If we can join, build the join
		if ($allowToJoin && is_array($joins) && !empty($joins))
		{
			foreach ($joins as $currentJoin)
			{
				if (is_array($currentJoin))
					$sql .= "JOIN ".$currentJoin["table"]." USING(".$currentJoin["column"].") ";
				else
					$sql .= "NATURAL JOIN ".$currentJoin." ";
			}
		}
		/**
		 * /JOINS
		 */
				
		
		/**
		 * CLAUSE
		 */
		// If we want a clause where
		if (!empty($clause))
		{			
			$lastSucceeded = false;
			// Foreach items to restrict
			for($i=0 ; $i<count($clause) ; $i++)
			{
				// If column on which you want to apply a restrict clause is in the list of columns collected on the tables
				$columnName = (SLS_String::contains($clause[$i]["column"],".")) ? SLS_String::substrAfterLastDelimiter($clause[$i]["column"],".") : $clause[$i]["column"];				
				if (in_array($columnName,$columnsMain))
				{
					$sql .= ($lastSucceeded) ? " AND " : " WHERE ";
					
					// Build the correct statement
					switch($clause[$i]["mode"])
					{
						case "like":
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])."%"));
							break;
						case "notlike":
							$sql .= ("LOWER(".$clause[$i]["column"].") NOT LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])."%"));
							break;							
						case "beginwith":
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote(strtolower($clause[$i]["value"])."%"));
							break;
						case "endwith":
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])));
							break;
						case "equal":
							$sql .= ("".$clause[$i]["column"]." = ".$this->_db->quote($clause[$i]["value"]));	
							break;
						case "notequal":
							$sql .= ("".$clause[$i]["column"]." != ".$this->_db->quote($clause[$i]["value"]));	
							break;
						case "lt":
							$sql .= ("".$clause[$i]["column"]." < ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "le":
							$sql .= ("".$clause[$i]["column"]." <= ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "ge":
							$sql .= ("".$clause[$i]["column"]." >= ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "gt":
							$sql .= ("".$clause[$i]["column"]." > ".$this->_db->quote($clause[$i]["value"]));
							break;
						case "null":
							$sql .= ("".$clause[$i]["column"]." IS NULL ");
							break;
						case "notnull":
							$sql .= ("".$clause[$i]["column"]." IS NOT NULL ");
							break;
						default:
							$sql .= ("LOWER(".$clause[$i]["column"].") LIKE ".$this->_db->quote("%".strtolower($clause[$i]["value"])."%"));
							break;
					}						
					$lastSucceeded = true;
				}
				// Else, throw a Sls Exception
				else
					SLS_Tracing::addTrace(new Exception("Warning: Column ".$clause[$i]["column"]." in clause where doesn't exist in table `".$table."` (and collected tables to join)"),true);
			}
		}
		/**
		 * /CLAUSE
		 */
		
		
		/**
		 * GROUP BY
		 */
		// If we want a group by
		if (!empty($group))
		{
			$lastSucceeded = false;
			// Foreach items to group
			foreach($group as $colGroup)			
			{
				// If column on which you want to apply a group by statement is in the list of columns collected on the tables
				if (in_array($colGroup,$columnsMain))
				{
					$sql .= ($lastSucceeded) ? ", " : " GROUP BY ";
					$sql .= $colGroup." ";
					$lastSucceeded = true;
				}
				// Else, throw a Sls Exception
				else
					SLS_Tracing::addTrace(new Exception("Warning: Column ".$colGroup." in group by statement doesn't exist in table `".$table."` (and collected tables to join)"),true);
			}
		}
		/**
		 * /GROUP BY
		 */
		
		
		// Try to execute the built query
		try
		{
			$result =  array_shift($this->_db->select($sql));
			return $result->total;			
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
	
	/**
	 * Give the next id from the current model
	 *
	 * @access public
	 * @return array PDO
	 * @since 1.0
	 */
	public function giveNextId()
	{		
		$sql = "SELECT ".$this->_primaryKey." AS nextId FROM `".$this->_table."` ORDER BY ".$this->_primaryKey." DESC LIMIT 1";
		try
		{			
			return array_shift($this->_db->select($sql));			
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}		
	}
	
	/**
	 * Get the comment of a table
	 *
	 * @access public	 
	 * @param string $table the table name
	 * @param string $db the db alias (if empty, current db)
	 * @return string $comment the comment on the table
	 * @see SLS_FrontModelSql::setTableComment
	 * @see SLS_FrontModelSql::getColumnComment
	 * @see SLS_FrontModelSql::setColumnComment
	 * @since 1.0
	 */
	public function getTableComment($table,$db)
	{
		$comment = $table;

		// If need to switch db
		if ($this->_db->getCurrentDb() != $db)
			$this->_db->changeDb($db);
			
		// If table exists
		if ($this->_db->tableExists($table))
		{			
			$infos = $this->_db->showTables($table);			
			if (!empty($infos))
				$comment = $infos->Comment;
		}
		return $comment;
	}
	
	/**
	 * Set the comment of a table
	 *
	 * @access public	 
	 * @param string $comment the comment to save
	 * @param string $table the table name
	 * @param string $db the db alias (if empty, current db)
	 * @return bool true if saved, else false
	 * @see SLS_FrontModelSql::getTableComment
	 * @see SLS_FrontModelSql::getColumnComment
	 * @see SLS_FrontModelSql::setColumnComment
	 * @since 1.0
	 */
	public function setTableComment($comment,$table,$db)
	{
		// If need to switch db
		if ($this->_db->getCurrentDb() != $db)
			$this->_db->changeDb($db);
			
		// If table exists
		if ($this->_db->tableExists($table))
		{			
			$comment = str_replace("'","''",$comment);			
			$sql = "ALTER TABLE `".$table."` COMMENT = '".$comment."'";			
			$this->_db->update($sql);
			return true;
		}
	}
	
	/**
	 * Get the comment of a column
	 *
	 * @access public
	 * @param string $column the column name
	 * @param string $table the table name
	 * @param string $db the db alias (if empty, current db)
	 * @return string $comment the comment on a column
	 * @since 1.0
	 */
	public function getColumnComment($column,$table,$db)
	{
		$comment = $column;
		
		// If need to switch db
		if ($this->_db->getCurrentDb() != $db)
			$this->_db->changeDb($db);
		
		// If table exists
		if ($this->_db->tableExists($table))
		{
			$columns = $this->_db->showColumns($table);
			
			for($i=0 ; $i<$count=count($columns) ; $i++)		
				if ($columns[$i]->Field == $column)
					$comment = $columns[$i]->Comment;
		}
		
		return $comment;
	}
	
	/**
	 * Set the comment of a column
	 *
	 * @access public
	 * @param string $column the column name
	 * @param string $comment the comment to save
	 * @param string $table the table name
	 * @param string $db the db alias (if empty, current db)
	 * @return bool true if saved, else false
	 * @see SLS_FrontModelSql::getTableComment
	 * @see SLS_FrontModelSql::getColumnComment
	 * @see SLS_FrontModelSql::getColumnComment
	 * @since 1.0
	 */
	public function setColumnComment($column,$comment,$table,$db)
	{
		// If need to switch db
		if ($this->_db->getCurrentDb() != $db)
			$this->_db->changeDb($db);
		
		// If the table doesn't exists
		if (!$this->_db->tableExists($table))
			return false;
		
		$type = "";
		$cols = $this->_db->showColumns($table);
		$columns = array();
		for($i=0 ; $i<$count=count($cols) ; $i++)
			array_push($columns,$cols[$i]->Field);
		
		$comment = str_replace("'","''",$comment);
		
		// If column doesn't exists in this table
		if (!in_array($column,$columns))
			return false;
		
		// Search the column type
		for($i=0 ; $i<$count=count($cols) ; $i++)		
			if ($cols[$i]->Field == $column)
			{
				$type = $cols[$i]->Type;
				$null = ($cols[$i]->Null=="NO") ? "NOT NULL" : "NULL";
				$ai = ($cols[$i]->Extra=="auto_increment") ? "AUTO_INCREMENT" : "";
			}
		
		// If the column type doesn't found
		if (empty($type) || empty($null))
			return false;		
		else
		{			
			$sql = "ALTER TABLE `".$table."` MODIFY `".$column."` ".$type." ".$null." ".$ai." COMMENT '".$comment."'";
			$this->_db->update($sql);
			return true;
		}
	}
	
	/**
	 * Check if a recordset described by a column doesn't already exists
	 *
	 * @access public
	 * @param string $column the column name
	 * @param string $value the column value
	 * @param string $table the table to check (current table model if empty)
	 * @param string $excludedColumn the column name to exclude
	 * @param string $excludedValue the column value to exclude
	 * @return bool $isUnique true if no recordset has been found, else false
	 * @since 1.0
	 */
	public function isUnique($column,$value,$table,$excludedColumn="",$excludedValue="")
	{
		$sql = "SELECT COUNT(*) AS total FROM `".$this->_table."` WHERE ".$column." = ".$this->_db->quote($value)." ";
		if (!empty($excludedColumn) && !empty($excludedValue))		
			$sql .= "AND ".$excludedColumn." != ".$this->_db->quote($excludedValue)." ";		
		try
		{
			$result =  array_shift($this->_db->select($sql));
			return $result->total;
		}
		catch (Exception $e)
		{			
			SLS_Tracing::addTrace($e,true);
			return false;
		}
	}
}
?>