<?
/**
 * Mother class of models
 * 
 * @author Florian Collot
 * @author Laurent Bientz
 * @copyright SillySmart
 * @package Sls.Models.Core 
 * @see SLS_FrontmodelSql
 * @since 1.0  
 */
class SLS_FrontModel
{	
	// Class variables	 
	protected $_update_array = array();
	protected $_generic;
	protected $_sql;
	protected $_table;
	protected $_db;
	protected $_primaryKey;
	protected $_isMultilanguage = false;
	protected $_modelLanguage = "";
		 
	/**
	 * Constructor
	 *
	 * @access public
	 * @param bool $multilanguage true if we have multilanguage content, else false
	 * @since 1.0
	 */
	public function __construct($multilanguage=false)
	{
		$this->_isMultilanguage = ($multilanguage) ? true : false;
		
		$this->beSurInitDbInfos();		
		$this->_generic = SLS_Generic::getInstance();
		$sql = get_class($this)."Sql";
		$this->_sql = new ${sql}($this->_table, $this->_primaryKey, $this->_isMultilanguage);
	}
	
	/**
	 * Set the current language of the model
	 *
	 * @access public
	 * @param string $lang the current language of the model (default lang if not filled)
	 * @since 1.0
	 */
	public function setModelLanguage($lang="")
	{
		if ($this->_isMultilanguage)
		{
			$this->_sql->setModelLanguage($lang);
			$this->_modelLanguage = (empty($lang)) ? $this->_generic->getObjectLang()->getLang() : $lang;			
			return true;
		}
		else 
			return false;
	}
	
	/**
	 * Get an object model
	 *
	 * @access public
	 * @param int $id the id of the model (PK)
	 * @return bool true if ok, else false
	 * @since 1.0
	 */
	public function getModel($id)
	{		
		if ($this->_isMultilanguage && empty($this->_modelLanguage))			
			$this->setModelLanguage();
			
		$object = $this->_sql->getModel($id);
		
		if (!is_numeric($id))
			return false;
		if (!is_object($object))
			return false;
		
		foreach ($object as $key=>$value)						
			$this->{__.$key} = $value;
				
		return true;
	}
		
	/**
	 * Create child model
	 * 
	 * @access public
	 * @param int $pkMultiLang one of the PKs described the entity in the case of multilanguage content (empty by default)
	 * @see SLSFrontModel::save
	 * @see SLSFrontModel::delete
	 * @since 1.0
	 */
	public function create($pkMultiLang="") 
	{
		if ($this->_isMultilanguage)
		{
			if (empty($this->_modelLanguage))
				$this->setModelLanguage();
						
			$this->{__.$this->_primaryKey} = (empty($pkMultiLang)) ? $this->giveNextId() : $pkMultiLang;;				
			$this->_sql->create($this->{__.$this->_primaryKey});
		}
		else
			$this->{__.$this->_primaryKey} = $this->_sql->create();
	}
	
	/**
	 * Delete child model
	 *
	 * @access public
	 * @return bool true if deleted, else false
	 * @see SLSFrontModel::create
	 * @see SLSFrontModel::save
	 * @since 1.0
	 */
	public function delete() 
	{
		if ($this->_isMultilanguage && empty($this->_modelLanguage))			
			$this->setModelLanguage();
		if (!is_numeric($this->{__.$this->_primaryKey}))
			return false;
		$this->_sql->delete($this->{__.$this->_primaryKey});
		return true;
	}	
		
	/**
	 * The current child model is multilanguage ?
	 *
	 * @access public
	 * @return bool true if yes, else false
	 * @since 1.0
	 */
	public function isMultilanguage()
	{
		return $this->_isMultilanguage;
	}
	
	/**
	 * Getter of the database of the current table
	 *
	 * @access public
	 * @return string $db the database alias
	 * @since 1.0
	 * @example 
	 * // if the current database alias named "main"
	 * var_dump($model->getDatabase());
	 * // will produce : "main"
	 */
	public function getDatabase()
	{
		return $this->_db;
	}
	
	/**
	 * Getter of the Primary Key of the current table
	 *
	 * @access public
	 * @return string $pk the primary key
	 * @since 1.0
	 * @example 
	 * // if the current model has a primary key named "user_id"
	 * var_dump($object->getPrimaryKey());
	 * // will produce : "user_id"
	 */
	public function getPrimaryKey()
	{
		return $this->_primaryKey;
	}
	
	/**
	 * Getter of the current table
	 *
	 * @access public
	 * @return string $table the table name
	 * @since 1.0
	 * @example 
	 * // if the current model is map on a table named "user"
	 * var_dump($object->getTable());
	 * // will produce : "user"
	 */
	public function getTable()
	{
		return $this->_table;
	}
	
	/**
	 * Getter of the errors of the current model
	 *
	 * @access public
	 * @return array $typeErrors all the errors
	 * @since 1.0
	 */
	public function getErrors()
	{
		return $this->_typeErrors;
	}
	
	/**
	 * Getter of one error 
	 *
	 * @access public
	 * @param string $key the property wanted
	 * @return string the error wanted
	 * @since 1.0
	 */
	public function getError($key)
	{
		return (isset($this->_typeErrors[$key])) ? $this->_typeErrors[$key] : "";
	}
	
	/**
	 * Generic getter
	 *
	 * @access public
	 * @param string $var wanted class variable 
	 * @return string $value value of the wanted class variable
	 * @since 1.0
	 */
	public function __get($var) 
	{
		if (isset($this->{__.$var}))
			return $this->{__.$var};
		else
			SLS_Tracing::addTrace(new Exception("Error: Class variable `".$var."` doesn't exists in this child model in get context"),true);
	}
	
	/**
	 * Generic setter
	 *
	 * @access public
	 * @param string $var class variable to set
	 * @param mixed $value value of class variable you want to set
	 * @since 1.0
	 */
	public function __set($var, $value)
	{
		try
		{
			$this->{__.$var} = $value;
			$this->_update_array[$var] = $value;
		}			
		catch (Exception $e)
		{
			SLS_Tracing::addTrace(new Exception("Error: Class variable `".$var."` doesn't exists in this child model in set context"),true);		
		}
	}
	
	/**
	 * Getter returning all class variables representing database entity	 
	 *
	 * @access public
	 * @return array $params associative array with all class variables
	 * @since 1.0
	 * @example 
	 * // if the current model is map on a table named "user"
	 * var_dump($this->_generic->getParams());
	 * // will produce :
	 * array(
	 * 		"user_id"	=> 1,
	 * 		"user_email"=> "laurent@sillysmart.org",
	 * 		"user_pwd"	=> "password"
	 * )
	 */
	public function getParams()
	{
		// Temp unset of non SQL class variables
		$genericTmp = $this->_generic;
		$sqlTmp = $this->_sql;
		$updateArray = $this->_update_array;		
		$this->_generic = null;
		$this->_sql = null;
		$this->_update_array = null;
		
		eval ('$params = '.  var_export($this, true) . ';');
		
		// Reset of non SQL class variables
		$this->_generic = $genericTmp; 
		$this->_sql = $sqlTmp;
		$this->_update_array = $updateArray;
		
		return $params;	
	}
	
	/**
	 * Static function used by getParams	 
	 *
	 * @access public static
	 * @param array $an_array
	 * @return array
	 * @since 1.0
	 */
	public static function __set_state($an_array)
  	{
		$array = array();
		foreach($an_array as $key=>$value)
			(substr($key, 0, 2) === "__") ? $array[substr($key, 2)] = $value : false;		
		return $array;
	}
	
  	/**
   	 * Check that class variables describing sql table and its primary key have been filled	 
	 *
	 * @access protected
	 * @since 1.0
	 */
	protected function beSurInitDbInfos() 
	{
	  if (null === $this->_table)	  
	  	SLS_Tracing::addTrace(new Exception("Error: Class variable describing current table haven't been filled"),true);
	  if (null === $this->_primaryKey)	  
	  	SLS_Tracing::addTrace(new Exception("Error: Class variable describing the primary key of the current table hasn't been filled"),true);	  
	}
  
	/**
	 * Update the current child model	 
	 *
	 * @access public
	 * @return bool true if updated, else false
	 * @see SLSFrontModel::create
	 * @see SLSFrontModel::delete
	 * @since 1.0
	 */
	public function save() 
	{		
		if ($this->_isMultilanguage && empty($this->_modelLanguage))			
			$this->setModelLanguage();
		if (count($this->_update_array) == 0)
			return true;
		if (!is_numeric($this->{__.$this->_primaryKey}))
			return false;		
		$this->_sql->save($this->{__.$this->_primaryKey}, $this->_update_array);	
		return true;		
	}
	
	/**
	 * Search of n objects of models
	 *
	 * @access public
	 * @param string $table the current table to list (default: empty => current model)
	 * @param array $joins the table(s) to join with current table (default: empty => no join)
	 * If you want to natural join: 
	 * <code>array("table_2","table_3","...","table_n")</code> will give 'SELECT * FROM table_1 NATURAL JOIN table_2 NATURAL JOIN table_3 ... NATURAL JOIN table_n'
	 * If you want to join with a specific column: 
	 * <code>array(0=>array("table"=>"table_2","column"=>"column_2"),1=>array("table"=>"table_3","column"=>"column_3"))</code>
	 * @param array $clause the clause wanted (default: empty => no clause)
	 * <code>
	 * array
	 * (
	 *		[0] => array
	 *				(
	 *					["column"] = "column_1",
	 *					["value"] = "value_1",
	 *					["mode"] = "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 *		[1] => array
	 *				(
	 *					["column"] = "user_department",
	 *					["value"] = "75",
	 *					["mode"] = "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 * )
	 * </code>
	 * @param array $group a group by (default: empty => no group by)
	 * <code>array("column_1","column_2","...","column_n")</code>
	 * @param mixed $order the order you want (default: empty => ORDER BY primary key ASC)
	 * <code>
	 * array
	 * (
	 * 		[0] => array
	 * 				(
	 * 					["column"] = "column_1",
	 * 					["order"] = "asc"
	 * 				)
	 * 		[1] => array
	 * 				(
	 * 					["column"] = "column_2",
	 * 					["order"] = "desc"
	 * 				)
	 * )
	 * </code>
	 * or
	 * string "rand()"
	 * @param array $limit the limit you want (default: empty for all recordsets)
	 * <code>array("start" => "10", "length" => "30")</code>
	 * @return array $objects array of PDO objects
	 * @see SLS_FrontModel::countModels
	 * @since 1.0
	 * @example 
	 * // Find all recordsets
	 * $object->searchModels();
	 * @example 
	 * // Find all recordsets where column_name like 'is'
	 * $object->searchModels("table_name",array(),array(0=>array("column" => "colu_name", "value" => "is", "mode" => "like")));
	 */
	public function searchModels($table="",$joins=array(),$clause=array(),$group=array(),$order=array(),$limit=array())
	{
		return $this->_sql->searchModels($table,$joins,$clause,$group,$order,$limit);
	}
	
	/**
	 * Count the number of objects of models
	 *	 
	 * @access public
	 * @param string $table the current table to list (default: empty => current model)
	 * @param array $joins the table(s) to join with current table (default: empty => no join)
	 * If you want to natural join: 
	 * <code>array("table_2","table_3","...","table_n")</code> will give 'SELECT * FROM table_1 NATURAL JOIN table_2 NATURAL JOIN table_3 ... NATURAL JOIN table_n'
	 * If you want to join with a specific column: 
	 * <code>array(0=>array("table"=>"table_2","column"=>"column_2"),1=>array("table"=>"table_3","column"=>"column_3"))</code>
	 * @param array $clause the clause wanted (default: empty => no clause)
	 * <code>
	 * array
	 * (
	 *		[0] => array
	 *				(
	 *					["column"] = "column_1",
	 *					["value"] = "value_1",
	 *					["mode"] = "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 *		[1] => array
	 *				(
	 *					["column"] = "user_department",
	 *					["value"] = "75",
	 *					["mode"] = "like" or "notlike" or "beginwith" or "endwith" or "equal" or "notequal" or "lt" or "le" or "ge" or "gt" or "null" or "notnull"
	 *				)
	 * )
	 * </code>
	 * @param array $group a group by (default: empty => no group by)
	 * <code>array("column_1","column_2","...","column_n")</code>
	 * @return int $nbResults the number of objects within limit
	 * @see SLS_FrontModel::searchModels
	 * @since 1.0
	 */
	public function countModels($table,$joins=array(),$clause=array(),$group=array())
	{
		return $this->_sql->countModels($table,$joins,$clause,$group);
	}
	
	/**
	 * Give the next id from the current model
	 *
	 * @access public
	 * @return int $nextId the next id
	 * @since 1.0
	 */
	public function giveNextId()
	{
		$result = $this->_sql->giveNextId();
		
		return (empty($result)) ? 1 : ($result->nextId+1);
	}
	
	/**
	 * Get the comment of a table
	 *
	 * @access public	 
	 * @param string $table the table name (current child model if empty)
	 * @param string $db the db alias (if empty, current db)
	 * @return string $comment the comment on the table
	 * @see SLS_FrontModel::setTableComment
	 * @see SLS_FrontModel::getColumnComment
	 * @see SLS_FrontModel::setColumnComment
	 * @since 1.0
	 */
	public function getTableComment($table="",$db="")
	{
		$table = (empty($table)) ? $this->_table : $table;
		
		return $this->_sql->getTableComment($table,$db);
	}
	
	/**
	 * Set the comment of a table
	 *
	 * @access public	 
	 * @param string $comment the comment to save
	 * @param string $table the table name (current child model if empty)
	 * @param string $db the db alias (if empty, current db)
	 * @return bool true if saved, else false
	 * @see SLS_FrontModel::getTableComment
	 * @see SLS_FrontModel::getColumnComment
	 * @see SLS_FrontModel::setColumnComment
	 * @since 1.0
	 */
	public function setTableComment($comment,$table="",$db="")
	{
		$table = (empty($table)) ? $this->_table : $table;
		
		return $this->_sql->setTableComment($comment,$table,$db);
	}
	
	/**
	 * Get the comment of a column
	 *
	 * @access public
	 * @param string $column the column name
	 * @param string $table the table name (current child model if empty)
	 * @param string $db the db alias (if empty, current db)
	 * @return string $comment the comment on a column
	 * @see SLS_FrontModel::getTableComment
	 * @see SLS_FrontModel::setTableComment
	 * @see SLS_FrontModel::setColumnComment
	 * @since 1.0
	 */
	public function getColumnComment($column,$table="",$db="")
	{
		$table = (empty($table)) ? $this->_table : $table;
		
		return $this->_sql->getColumnComment($column,$table,$db);
	}
	
	/**
	 * Set the comment of a column
	 *
	 * @access public
	 * @param string $column the column name
	 * @param string $comment the comment to save
	 * @param string $table the table name (current child model if empty)
	 * @param string $db the db alias (if empty, current db)
	 * @return bool true if saved, else false
	 * @see SLS_FrontModel::getTableComment
	 * @see SLS_FrontModel::setTableComment
	 * @see SLS_FrontModel::getColumnComment
	 * @since 1.0
	 */
	public function setColumnComment($column,$comment,$table="",$db="")
	{
		$table = (empty($table)) ? $this->_table : $table;
		
		return $this->_sql->setColumnComment($column,$comment,$table,$db);
	}
	
	/**
	 * Check if a recordset described by a column doesn't already exists
	 *
	 * @access public
	 * @param string $column the column name
	 * @param string $value the column value
	 * @param string $table the table to check (current table model if empty)
	 * @param string $excludedColumn the column name to exclude
	 * @param string $excludedValue the column value to exclude
	 * @return bool $isUnique true if no recordset has been found, else false
	 * @since 1.0
	 */
	public function isUnique($column,$value,$table="",$excludedColumn="",$excludedValue="")
	{
		$table = (empty($table)) ? $this->_table : ((SLS_Sql::getInstance()->tableExists($table)) ? $table : $this->_table);
		
		if (!in_array($column,SLS_Sql::getInstance()->getColumnsName($table)))
		{
			SLS_Tracing::addTrace(new Exception("Error: Column ".$column." doesn't exist in table `".$table."`"),true);
			return false;
		}
		if (!empty($excludedColumn) && !empty($excludedValue))
		{
			if (!in_array($excludedColumn,SLS_Sql::getInstance()->getColumnsName($table)))
			{
				$excludedColumn = "";
				$excludedValue = "";
				SLS_Tracing::addTrace(new Exception("Warning: Column ".$excludedColumn." to exclude for the unique recordset test doesn't exist in table `".$table."` - exclude ommited"),true);
			}
		}
			
		return ($this->_sql->isUnique($column,$value,$table,$excludedColumn,$excludedValue)==0) ? true : false;
	}
}
?>