<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="GenerateMetas">
		<xsl:if test="//root/Statics/Sls/session/params/param[name='lastSide']/value = 'user'">
			<meta http-equiv="Content-Language" content="{//root/Statics/Sls/session/params/param[name='sls_user']/value}" /> 
		</xsl:if>
		<xsl:if test="//root/Statics/Sls/session/params/param[name='lastSide']/value != 'user'">
			<meta http-equiv="Content-Language" content="{//root/Statics/Sls/session/params/param[name='sls_management']/value}" /> 
		</xsl:if>
		<meta name="author" content="{//root/ControllerParams/metas/author}" />
		<meta name="copyright" content="{//root/ControllerParams/metas/copyright}" />
		<meta name="description" content="{//root/ControllerParams/metas/description}" />
		<meta name="keywords" content="{//root/ControllerParams/metas/keywords}" />
		<meta name="robots" content="{//root/ControllerParams/metas/robots}" />
		<title><xsl:value-of select="//root/ControllerParams/PageTitle" /></title>
		<xsl:if test="//root/ControllerParams/metas/favicon != ''">
			<link rel="shortcut icon" href="{//root/ControllerParams/metas/favicon}" />
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>