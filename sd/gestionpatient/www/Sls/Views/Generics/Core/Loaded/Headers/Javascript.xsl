<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="LoadGenericJavascript">
		<xsl:if test="ControllerParams/BuildConfigsJsVars = 1 or ControllerParams/JsMultiLang = 1">
			<script type="text/javascript">
				var slsBuild = {<xsl:if test="ControllerParams/BuildConfigsJsVars = 1"><xsl:for-each select="//Statics/Sls/Configs/*"><xsl:variable name="firstPos" select="position()" /><xsl:variable name="nodeName" select="name()" /><xsl:if test="count(*[@js='true'])!=0">'<xsl:call-template name="protectString"><xsl:with-param name="str" select="$nodeName" /><xsl:with-param name="type">'</xsl:with-param></xsl:call-template>' :{<xsl:variable name="nbItems" select="count(*[@js='true'])" /><xsl:for-each select="*[@js='true']"><xsl:variable name="childName" select="name()" />'<xsl:call-template name="protectString"><xsl:with-param name="str" select="$childName" /><xsl:with-param name="type">'</xsl:with-param></xsl:call-template>':'<xsl:call-template name="protectString"><xsl:with-param name="str" select="." /><xsl:with-param name="type">'</xsl:with-param></xsl:call-template>'<xsl:if test="$nbItems != position()">,</xsl:if></xsl:for-each>}<xsl:if test="count(//Statics/Sls/Configs/*) != $firstPos or //ControllerParams/JsMultiLang = 1">,</xsl:if></xsl:if></xsl:for-each></xsl:if><xsl:if test="ControllerParams/JsMultiLang = 1">'langs' : {<xsl:for-each select="//Statics/Sls/Langs/js/sentence">'<xsl:call-template name="protectString"><xsl:with-param name="str" select="name" /><xsl:with-param name="type">'</xsl:with-param></xsl:call-template>' : '<xsl:call-template name="protectString"><xsl:with-param name="str" select="value" /><xsl:with-param name="type">'</xsl:with-param></xsl:call-template>'<xsl:if test="position() != count(//Statics/Sls/Langs/js/sentence)">,</xsl:if></xsl:for-each>}</xsl:if>};			
			</script>
			<xsl:if test="ControllerParams/BuildConfigsJsVars = 1">
				<xsl:for-each select="//Statics/Sls/JsStatics/filesCoreStatics/file">
					<xsl:variable name="index" select="position()" />
					<script type="text/javascript" src="{//Statics/Sls/JsStatics/filesCoreStatics/file[$index]}"></script>				
				</xsl:for-each>			
			</xsl:if>
		</xsl:if>
		<xsl:if test="ControllerParams/LoadStaticsJs = 1">	
			<xsl:for-each select="//Statics/Sls/JsStatics/filesStatics/file">
				<xsl:variable name="index" select="position()" />
				<script type="text/javascript" src="{//Statics/Sls/JsStatics/filesStatics/file[$index]}"></script>				
			</xsl:for-each>			
		</xsl:if>
		<xsl:if test="ControllerParams/LoadDynsJs = 1">	
			<xsl:for-each select="//Statics/Sls/JsStatics/filesDyn/file">
				<xsl:variable name="index" select="position()" />
				<script type="text/javascript" src="{//Statics/Sls/JsStatics/filesDyn/file[$index]}"></script>				
			</xsl:for-each>			
		</xsl:if>
		
	</xsl:template>
</xsl:stylesheet>