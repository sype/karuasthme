<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<!-- 
	 	- Function BoListing
	 	- Generic's generation of back-office listing
	 	- Don't change anything
	-->
	<xsl:template name="BoListing">
		<xsl:text disable-output-escaping="yes"><![CDATA[
		<script type="text/javascript">
			function reloadFormBo(column,order,form){form=(form==null)?"boForm":form;document.getElementById("start").value=0;document.getElementById("column").value=column;document.getElementById("order").value=order;document.getElementById(form).submit()}function showMoreFilter(current,total){for(i=1;i<=total;i++)document.getElementById("filter_more_"+i).style.visibility='hidden';if(current+1<=total)document.getElementById("filter_more_"+(parseInt(current)+1)).style.visibility='visible';document.getElementById("filter_div_"+(parseInt(current)+1)).style.display='block'}function confirmDelete(link){if (confirm("Are you sure to delete these records ?"))window.location = link;else return false;}function checkNullable(id){if(document.getElementById('filter_value_'+id).value=='NULL'||document.getElementById('filter_value_'+id).value=='NOTNULL')document.getElementById('filter_value_'+id).value='';document.getElementById('filter_value_'+id).style.visibility='visible';if(document.getElementById('filter_exact_'+id).value=='null'||document.getElementById('filter_exact_'+id).value=='notnull'){document.getElementById('filter_value_'+id).style.visibility='hidden';document.getElementById('filter_value_'+id).value=document.getElementById('filter_exact_'+id).value.toUpperCase();}}			
			var page = 'listing';
			function display_filters_groupby(id){ document.getElementById(id).style.display = (document.getElementById(id).style.display == 'block') ? 'none' : 'block'; }
			</script>
		]]></xsl:text>
		<script type="text/javascript">
			var modify = '<xsl:value-of select="concat($sls_url_domain,'/',//page/actions/modify/mod,'/',//page/actions/modify/smod,'/id/')" />';
			var del = '<xsl:value-of select="concat($sls_url_domain,'/',//page/actions/delete/mod,'/',//page/actions/delete/smod,'/id/')" />';
			function areaHover(id,part,action,old){document.getElementById(id).src = (action == 'hover') ? (part == 'top') ? '<xsl:value-of select="concat($sls_url_img_core,'Buttons/arrow_ordering_top.png')" />' : '<xsl:value-of select="concat($sls_url_img_core,'Buttons/arrow_ordering_bottom.png')" />' : old;}
		</script>
		<xsl:variable name="actionForm">
			<xsl:choose>
				<xsl:when test="//page/actionForm != ''"><xsl:value-of select="//page/actionForm" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="concat($sls_url_domain, '/', //Statics/Sls/Params/param[name='mode']/value, '/', //Statics/Sls/Params/param[name='smode']/value, '.html')" /></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<div class="grey_wrapper">
			<form id="boForm" action="{$actionForm}" method="post">
				<input type="hidden" name="reload" 		id="reload" 	value="true" />
				<input type="hidden" name="column" 		id="column" 	value="{//page/column}" />
				<input type="hidden" name="order"  		id="order"  	value="{//page/order}" />
				<input type="hidden" name="start"  		id="start"  	value="{//page/start}" />
				<input type="hidden" name="length" 		id="length" 	value="{//page/length}" />
				<input type="hidden" name="total"  		id="total"  	value="{//page/total}" />
				<fieldset class="top_border">
					<legend class="open_wrapper_close" onclick="display_filters_groupby('filters');" style="cursor:pointer;">Filters</legend>
					<div class="content display_none" id="filters">
						<xsl:if test="count(//page/filters/filter[name != 'pk_lang' and value != '']) &gt; 0"><xsl:attribute name="class">content</xsl:attribute><xsl:attribute name="style">display:block;</xsl:attribute></xsl:if>						
						<xsl:for-each select="//page/filters/filter[value != '']">
							<xsl:variable name="currentName" select="name" />							
							<div id="filter_div_{position()}" class="row_center">
								<label for="filter_name_{position()}" class="label_filter">Filter #<xsl:value-of select="position()" /></label>								
								<select name="filter_name_{position()}" id="filter_name_{position()}" class="select_filter">
									<xsl:for-each select="//page/filters/filter">
										<option value="{name}">
											<xsl:if test="$currentName = name">
												<xsl:attribute name="selected">selected</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="desc" />
										</option>
									</xsl:for-each>
								</select>
								<select name="filter_exact_{position()}" id="filter_exact_{position()}" class="select_filter" onchange="checkNullable({position()})">
									<option value="like">
										<xsl:if test="exact = 'like'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Like
									</option>
									<option value="notlike">
										<xsl:if test="exact = 'notlike'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Not Like
									</option>
									<option value="beginwith">
										<xsl:if test="exact = 'beginwith'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Begin With
									</option>
									<option value="endwith">
										<xsl:if test="exact = 'endwith'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										End With
									</option>
									<option value="equal">
										<xsl:if test="exact = 'equal'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Equal
									</option>
									<option value="notequal">
										<xsl:if test="exact = 'notequal'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Not Equal
									</option>
									<option value="lt">
										<xsl:if test="exact = 'lt'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Lower than
									</option>
									<option value="le">
										<xsl:if test="exact = 'le'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Lower than or equal
									</option>
									<option value="ge">
										<xsl:if test="exact = 'ge'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Greater than or equal
									</option>
									<option value="gt">
										<xsl:if test="exact = 'gt'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Greater than
									</option>
									<option value="null">
										<xsl:if test="exact = 'null'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Is Null
									</option>
									<option value="notnull">
										<xsl:if test="exact = 'notnull'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										Is Not Null
									</option>
								</select>
								<input type="text" name="filter_value_{position()}" id="filter_value_{position()}" value="{value}" class="input_filter">
									<xsl:if test="exact = 'null' or exact = 'notnull'">
										<xsl:attribute name="style">visibility:hidden;</xsl:attribute>
									</xsl:if>
								</input>
								<xsl:variable name="toDisplay">
									<xsl:choose>
										<xsl:when test="position() = count(//page/filters/filter[value != '']) and position() &lt; count(//page/filters/filter)">visible</xsl:when>
										<xsl:otherwise>hidden</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>								
								<a href="javascript:showMoreFilter({position()},{count(//page/filters/filter)-1})" id="filter_more_{position()}" style="visibility:{$toDisplay};" title="Add filter" class="add_filter">Add filter</a>
							</div>								
						</xsl:for-each>
						<xsl:variable name="nbReloaded" select="count(//page/filters/filter[value != ''])" />						
						<xsl:for-each select="//page/filters/filter[value = '']">
							<xsl:variable name="toDisplay">
								<xsl:choose>
									<xsl:when test="count(//page/filters/filter[value != '']) = 0 and position() = 1">block</xsl:when>
									<xsl:otherwise>none</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>							
							<div style="display:{$toDisplay};" id="filter_div_{position()+$nbReloaded}" class="row_center">
								<label for="filter_name_{position()+$nbReloaded}" class="label_filter">Filtre #<xsl:value-of select="position()+$nbReloaded" /></label>
								<select name="filter_name_{position()+$nbReloaded}" id="filter_name_{position()+$nbReloaded}" class="select_filter">
									<xsl:for-each select="//page/filters/filter">
										<option value="{name}">											
											<xsl:value-of select="desc" />
										</option>
									</xsl:for-each>
								</select>						
								<select name="filter_exact_{position()+$nbReloaded}" id="filter_exact_{position()+$nbReloaded}" class="select_filter"  onchange="checkNullable({position()+$nbReloaded})">
									<option value="like">Like</option>
									<option value="notlike">Not Like</option>
									<option value="beginwith">Begin With</option>
									<option value="endwith">End With</option>
									<option value="equal">Equal</option>
									<option value="notequal">Not Equal</option>
									<option value="lt">Lower than</option>
									<option value="le">Lower than or equal</option>
									<option value="ge">Greater than or equal</option>
									<option value="gt">Greater than</option>
									<option value="null">Is Null</option>
									<option value="notnull">Is Not Null</option>
								</select>
								<input type="text" name="filter_value_{position()+$nbReloaded}" id="filter_value_{position()+$nbReloaded}" value="" class="input_filter" />
								<xsl:variable name="displayMore">
									<xsl:choose>
										<xsl:when test="position()+$nbReloaded &lt; count(//page/filters/filter)">visible</xsl:when>
										<xsl:otherwise>hidden</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>								
								<a href="javascript:showMoreFilter({position()+$nbReloaded},{count(//page/filters/filter)-1})" id="filter_more_{position()+$nbReloaded}" style="visibility:{$displayMore};" title="Add filter" class="add_filter">Add filter</a>
							</div>							
						</xsl:for-each>						
					</div>
				</fieldset>
				<fieldset class="top_border">
					<legend class="open_wrapper_close" onclick="display_filters_groupby('group_by');" style="cursor:pointer;">Group by</legend>
					<div class="content display_none" id="group_by">
						<xsl:if test="count(//page/groups/group[selected='true']) &gt; 0"><xsl:attribute name="class">content</xsl:attribute><xsl:attribute name="style">display:block;</xsl:attribute></xsl:if>						
						<div class="row_center">
							<label for="bo_listing_group_by" class="label_filter">Group</label>
							<select name="group_filter[]" multiple="multiple" size="2" class="select_filter" id="bo_listing_group_by">
								<xsl:for-each select="//page/groups/group">
									<option value="{name}">
										<xsl:if test="selected = 'true'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
										<xsl:value-of select="desc" />
									</option>
								</xsl:for-each>
							</select>							
						</div>
						<div class="row">
							<div class="login_submit">
								<div class="left"></div>
								<div class="middle">
									<input type="submit" value="Filter" onclick="document.getElementById('start').value=0;" />
								</div>
								<div class="right"></div>
							</div>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		
		<h1 class="main">Results for : <span class="light"><xsl:value-of select="//page/description" /> -  <xsl:value-of select="//page/total" /> result<xsl:if test="//page/total &gt; 1">s</xsl:if></span></h1>
		<ul class="listing_actions">
			<xsl:if test="count(//page/actions/add) &gt; 0">
				<li><a href="{concat($sls_url_domain,'/',//page/actions/add/mod,'/',//page/actions/add/smod)}" title="{concat('Add a new ',//page/description)}" class="add">Add a new record</a></li>
			</xsl:if>
			<xsl:if test="count(//page/actions/modify) &gt; 0">					
				<li><span class="edit">Edit</span></li>
			</xsl:if>
			<li>
				<xsl:choose>
					<xsl:when test="count(//page/actions/delete) &gt; 0"><xsl:attribute name="style">display:block</xsl:attribute></xsl:when>
					<xsl:otherwise><xsl:attribute name="style">display:none</xsl:attribute></xsl:otherwise>
				</xsl:choose>
				<span class="delete">Delete</span>
			</li>			
		</ul>
			
		<xsl:if test="count(//entities/entity) = 0">
			<ul class="error" style="display:block;float:left;margin-top:20px;">
				<li>No results for the search criteria.</li>
			</ul>
		</xsl:if>
			
		<xsl:if test="count(//entities/entity) &gt; 0 and count(//page/columns/column) &gt; 0">	
			<table class="listing" cellpadding="0" cellspacing="0">
				<tr>
					<th>&#160;</th>
					<xsl:for-each select="//page/columns/column">
						<th valign="middle">							
							<xsl:value-of select="desc" />
							<xsl:variable name="imgSrc"><xsl:choose><xsl:when test="name = //page/column and //page/order = 'asc'">arrow_ordering_top</xsl:when><xsl:when test="name = //page/column and //page/order = 'desc'">arrow_ordering_bottom</xsl:when><xsl:otherwise>arrow_ordering</xsl:otherwise></xsl:choose></xsl:variable>
							<map name="{concat('ordering',position())}" style="display:inline;border:none;">
								<area shape="rect" coords="0,0,10,8" href="#" onclick="reloadFormBo('{name}','asc')" title="Order Asc" onmouseover="areaHover('{concat('map_img_',position())}','top','hover','{concat($sls_url_img_core,'Buttons/',$imgSrc,'.png')}')" onmouseout="areaHover('{concat('map_img_',position())}','top','out','{concat($sls_url_img_core,'Buttons/',$imgSrc,'.png')}')" />
								<area shape="rect" coords="0,8,10,16" href="#" onclick="reloadFormBo('{name}','desc')" title="Order Desc" onmouseover="areaHover('{concat('map_img_',position())}','bottom','hover','{concat($sls_url_img_core,'Buttons/',$imgSrc,'.png')}')" onmouseout="areaHover('{concat('map_img_',position())}','bottom','out','{concat($sls_url_img_core,'Buttons/',$imgSrc,'.png')}')" />
							</map>							
							<img src="{concat($sls_url_img_core,'Buttons/',$imgSrc,'.png')}" id="{concat('map_img_',position())}" usemap="#{concat('ordering',position())}" style="border:none;margin:0px 0 0 10px;" align="absmiddle" />
						</th>
					</xsl:for-each>
				</tr>
				<xsl:for-each select="//entities/entity">
					<xsl:variable name="currentPosition" select="position()" />
					<xsl:variable name="trClass">
						<xsl:if test="position() mod 2  = 0">dark</xsl:if>
						<xsl:if test="position() mod 2 != 0">clear</xsl:if>
					</xsl:variable>
					<tr class="{$trClass}">
						<td class="check"><input type="checkbox" id="{//entities/entity[$currentPosition]/*[name() = //page/pk]}" class="filter_checkbox" /></td>
						<xsl:for-each select="//page/columns/column">
							<xsl:variable name="currentColumn" select="name" />
							<td>									
								<xsl:value-of select="//entities/entity[$currentPosition]/*[name() = $currentColumn]" disable-output-escaping="yes" />
							</td>
						</xsl:for-each>
					</tr>						
				</xsl:for-each>
			</table>
			
			<div id="footer">
				<div class="left">
					<xsl:variable name="nbPages" select="ceiling(//page/total div //page/length)" />
					<xsl:if test="(//page/start div //page/length) &gt; 1">
						<a href="#" onclick="document.getElementById('start').value = 0; document.getElementById('boForm').submit();" title="First page" class="pager">&lt;&lt;</a>
					</xsl:if>
					<xsl:if test="//page/start &gt; 0">
						<a href="#" onclick="document.getElementById('start').value = parseInt(document.getElementById('start').value) - parseInt(document.getElementById('length').value); document.getElementById('boForm').submit();" title="Previous" class="pager">Previous</a>
					</xsl:if>
					<xsl:value-of select="php:functionString('SLS_String::paginate',//page/start,//page/length,//page/total,'boForm')" disable-output-escaping="yes" />
					<xsl:if test="(//page/start + //page/length) &lt; (//page/total)">
						<a href="#" onclick="document.getElementById('start').value = parseInt(document.getElementById('start').value) + parseInt(document.getElementById('length').value); document.getElementById('boForm').submit();" title="Next" class="pager">Next</a>
					</xsl:if>					
					<xsl:if test="((ceiling(//page/total div //page/length)) - (ceiling(//page/start div //page/length) + 1)) &gt; 1">
						<a href="#" onclick="document.getElementById('start').value = ((Math.ceil(parseInt(document.getElementById('total').value) / parseInt(document.getElementById('length').value))-1) * document.getElementById('length').value).toString(); document.getElementById('boForm').submit();" title="Last" class="pager">&gt;&gt;</a>
					</xsl:if>					
					<span class="total">(<xsl:value-of select="//page/start + 1" />-<xsl:choose><xsl:when test="(//page/start + //page/length) &lt; (//page/total)"><xsl:value-of select="//page/start + //page/length" /></xsl:when><xsl:otherwise><xsl:value-of select="//page/total" /></xsl:otherwise></xsl:choose>/<xsl:value-of select="//page/total" />) | By page :</span>					
					<xsl:if test="//page/length != 20"><a href="#" onclick="document.getElementById('start').value=0;document.getElementById('length').value=20;document.getElementById('boForm').submit();" title="20" class="pager">20</a></xsl:if>
					<xsl:if test="//page/length = 20"><span>20</span></xsl:if>
					<xsl:if test="//page/length != 40"><a href="#" onclick="document.getElementById('start').value=0;document.getElementById('length').value=40;document.getElementById('boForm').submit();" title="40" class="pager">40</a></xsl:if>
					<xsl:if test="//page/length = 40"><span>40</span></xsl:if>
					<xsl:if test="//page/length != 60"><a href="#" onclick="document.getElementById('start').value=0;document.getElementById('length').value=60;document.getElementById('boForm').submit();" title="60" class="pager">60</a></xsl:if>
					<xsl:if test="//page/length = 60"><span>60</span></xsl:if>
					<xsl:if test="//page/length != 80"><a href="#" onclick="document.getElementById('start').value=0;document.getElementById('length').value=80;document.getElementById('boForm').submit();" title="80" class="pager">80</a></xsl:if>
					<xsl:if test="//page/length = 80"><span>80</span></xsl:if>
					<xsl:if test="//page/length != 100"><a href="#" onclick="document.getElementById('start').value=0;document.getElementById('length').value=100;document.getElementById('boForm').submit();" title="100" class="pager">100</a></xsl:if>
					<xsl:if test="//page/length = 100"><span>100</span></xsl:if>
				</div>
				<div class="right">
					<form id="export_form" method="post" action="{//View/url_export}">
						<label for="listing_select_export" class="display_none">Export this data</label>
						<div class="select_export">
							<select name="Export_Type" class="select_filter" id="listing_select_export" onchange="document.getElementById('export_form').submit();" size="2">
								<option value="excel">Excel</option>
								<option value="csv">Csv</option>
								<option value="html">Html</option>
								<option value="txt">Txt</option>
							</select>
						</div>
						<input type="submit" value="Export" class="display_none" />
					</form>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>