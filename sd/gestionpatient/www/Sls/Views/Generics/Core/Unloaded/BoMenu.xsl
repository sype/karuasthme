<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<!-- 
	 	- Function BoMenu
	 	- Generic's generation of back-office menu
	 	- Don't change anything
	-->
	<xsl:template name="BoMenu">

		<div class="grey_wrapper">
			<fieldset class="no_border">
				<legend>Category (<xsl:value-of select="count(//View/bo_actions/bo_action[label != 'Connection' and label != 'Disconnection'])" />)</legend>
				<div class="content" id="wrap_categorieswrapper">
					<div class="h_calc">
						<div id="menu">
							<xsl:variable name="lines" select="ceiling(count(//View/bo_actions/bo_action[label != 'Connection' and label != 'Disconnection']) div 4)" />
							<xsl:call-template name="displayMenu">
								<xsl:with-param name="i" select="'1'" />
								<xsl:with-param name="lines" select="$lines" />
							</xsl:call-template>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</xsl:template>
	
	<xsl:template name="displayMenu">
		<xsl:param name="i" />
		<xsl:param name="lines" />
		<xsl:variable name="start" select="($i - 1)" />
		<xsl:variable name="end" select="($i + $lines)" />		
		<div class="menu_col">
			<ul class="float_horizontal">
				<xsl:for-each select="//View/bo_actions/bo_action[label != 'Connection' and label != 'Disconnection' and (position() &gt; $start and position() &lt; $end)]">
					<xsl:variable name="urlList" select="url" />
					<xsl:variable name="urlAdd" select="concat('http://',//Statics/Sls/Configs/site/domainName,'/',//Statics/Sls/Params/param[name='genericmode']/value,'/',php:functionString('str_replace','List','Add',php:functionString('SLS_String::substrBeforeLastDelimiter',php:functionString('SLS_String::substrAfterLastDelimiter',url,concat(//Statics/Sls/Params/param[name='genericmode']/value,'/')),'.')))" />
					<xsl:variable name="urlModify" select="concat('http://',//Statics/Sls/Configs/site/domainName,'/',//Statics/Sls/Params/param[name='genericmode']/value,'/',php:functionString('str_replace','List','Modify',php:functionString('SLS_String::substrBeforeLastDelimiter',php:functionString('SLS_String::substrAfterLastDelimiter',url,concat(//Statics/Sls/Params/param[name='genericmode']/value,'/')),'.')))" />
					<xsl:variable name="urlCurrent" select="concat('http://',//Statics/Sls/Configs/site/domainName,'/',//Statics/Sls/Params/param[name='genericmode']/value,'/',//Statics/Sls/Params/param[name='genericsmode']/value)" />										
					<li>
						<a href="{url}">							
							<xsl:if test="//page/actionForm = url or php:functionString('SLS_String::startsWith',$urlCurrent,$urlList) = 'true' or php:functionString('SLS_String::startsWith',$urlCurrent,$urlAdd) = 'true' or php:functionString('SLS_String::startsWith',$urlCurrent,$urlModify) = 'true'"><xsl:attribute name="class">selected</xsl:attribute></xsl:if>							
							<xsl:value-of select="label" />
						</a>
						 
					</li>
				</xsl:for-each>
			</ul>
		</div>
		
		<xsl:if test="ceiling($i div $lines) &lt; 4">
			<xsl:call-template name="displayMenu">
				<xsl:with-param name="i" select="$i + $lines" />
				<xsl:with-param name="lines" select="$lines" />
			</xsl:call-template>
		</xsl:if>		
	</xsl:template>
	
</xsl:stylesheet>