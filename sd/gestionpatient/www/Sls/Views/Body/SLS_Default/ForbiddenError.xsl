<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="ForbiddenError">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<div id="rightSide">
				<div id="container">
					<h1>Sorry, You have not enough rights to access this area</h1>
					
				</div>	
				
			</div>
			
		</div>
		
	</xsl:template>
</xsl:stylesheet>