<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Error">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<div id="rightSide">
				<div id="container">
					<h1>Sorry, the wanted action doesn't exist</h1>
					<xsl:if test="count(//actions/action) &gt; 0">
						<h2>You maybe search :</h2>
						<ul>
							<!-- Don't change anything below -->
							<xsl:for-each select="//actions/action">
								<li><a href="{href}" title="{label}"><xsl:value-of select="label" /></a></li>
							</xsl:for-each>
							<!-- /Don't change anything below -->
						</ul>
					</xsl:if>
					
					
				</div>	
				
			</div>
			
		</div>
		
	</xsl:template>
</xsl:stylesheet>