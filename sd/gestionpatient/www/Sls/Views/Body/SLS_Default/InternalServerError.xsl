<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="InternalServerError">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<div id="rightSide">
				<div id="container">
					<h1>Sorry, The server have encountered a problem... Error 500</h1>
					
				</div>	
				
			</div>
			
		</div>
		
	</xsl:template>
</xsl:stylesheet>