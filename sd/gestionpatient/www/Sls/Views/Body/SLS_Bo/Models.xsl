<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Models">
	<xsl:text disable-output-escaping="yes"><![CDATA[		
		<script type="text/javascript">
			function confirmDelete(link){if (confirm("Are you sure to delete this record ?"))window.location = link;else return false;}
		</script>
		]]></xsl:text>
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Manage your Models</h1>
					<xsl:if test="count(//View/models/model) = 0">
						You don't have any models.<br />
					</xsl:if>
					<xsl:if test="count(//View/models/model) &gt; 0">
						<table cellpadding="5" cellspacing="0" border="1">
							<tr style="background-color:#E9E9E9;color:#000;">
								<th>Class</th>
								<th>Database</th>
								<th>Table</th>
								<th>PK</th>
								<th>Columns</th>
								<th>Modify</th>
								<th>Delete</th>
							</tr>
							<xsl:for-each select="//View/models/model">
								<tr>
									<td><xsl:value-of select="class" /></td>
									<td><xsl:value-of select="db" /></td>
									<td><xsl:value-of select="table" /></td>
									<td><xsl:value-of select="pk" /></td>
									<td align="center"><xsl:value-of select="nbColumns" /></td>
									<td align="center"><a href="{concat(//View/edit,'/name/',db,'_',table)}" title="Modify"><img src="{concat($sls_url_img_core_icons,'edit16.png')}" title="Modify" alt="Modify" style="border:0" /></a></td>
									<td align="center"><a href="#" onclick="confirmDelete('{concat(//View/delete,'/name/',db,'_',table)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete" alt="Delete" style="border:0" /></a></td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
					<a href="{//Links/Link[Name='GENERATE']/Href}" title="Generate your models">Generate models</a>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>