<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="GlobalSettings">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Edit Your Settings</h1>
					<h2>Global Settings</h2>
					<fieldset>
						<legend>Global Settings</legend>
						<xsl:if test="count(//View/errors/error) &gt; 0">
							<div style="display:block;margin-bottom:10px;margin-top:10px;color:red;font-weight:bold;">
								<xsl:for-each select="//View/errors/error">
									<xsl:value-of select="." /><br />
								</xsl:for-each>
							</div>
						</xsl:if>
						<form action="" name="" enctype="multipart/form-data" method="post">
							<input type="checkbox" name="export" id="export" />
							<label for="export">Export the configuration</label><br /><br /><br />
							<label for="domain">Domain Name :</label>
							<input type="text" name="domain" id="domain" value="{//View/current_values/domain}" /><br />
							
							<label for="project">Project Name :</label>
							<input type="text" name="project" id="project" value="{//View/current_values/project}" /><br />
							
							<label for="extension">Default Extension :</label>
							<input type="text" name="extension" id="extension" value="{//View/current_values/extension}" /><br />
							
							<label for="charset">Default Charset :</label>
							<select name="charset" id="charset">
								<xsl:for-each select="//View/charsets/charset">
									<option value="{.}">
										<xsl:if test="//View/current_values/charset = .">
											<xsl:attribute name="selected" select="'selected'" />
										</xsl:if>
										<xsl:value-of select="." />
									</option>
								</xsl:for-each>
							</select><br />
							
							<label for="lang">Default Langage :</label>
							<select name="lang" id="lang">
								<xsl:for-each select="//View/langs/lang">
									<option value="{.}">
										<xsl:if test="//View/current_values/lang = .">
											<xsl:attribute name="selected" select="'selected'" />
										</xsl:if>
										<xsl:value-of select="." />
									</option>
								</xsl:for-each>
							</select><br />
														
							<input type="checkbox" name="domainSessionActive" id="session" onclick="displaySessionDomain()" style="float:left;">
								<xsl:if test="//View/current_values/domain_session != ''">
									<xsl:attribute name="checked">checked</xsl:attribute>
								</xsl:if>
							</input>
							<label for="session" style="float:left;margin-left:2px;">Allow sharing session</label>&#160;
							<div id="domainSession">								
								<xsl:attribute name="style">
									<xsl:if test="//View/current_values/domain_session != ''">float:left;display:inline;</xsl:if>
									<xsl:if test="//View/current_values/domain_session = ''">float:left;display:none;</xsl:if>
								</xsl:attribute>
								&#160;=> Domain pattern:&#160;<input type="text" name="domainSession" value="{//View/current_values/domain_session}" />
							</div>
							<br /><br />
							
							<input type="hidden" name="reload" value="true" />
							<input type="submit" value="Confirm Changes" />
						</form>
					</fieldset>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>