<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="AddDomainName">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Global Settings</h1>
					<fieldset>
						<legend>Add a new Domain</legend>
						<xsl:if test="count(//View/errors/error) &gt; 0">
							<div style="display:block;margin-bottom:10px;margin-top:10px;color:red;font-weight:bold;">
								<xsl:for-each select="//View/errors/error">
									<xsl:value-of select="." /><br />
								</xsl:for-each>
							</div>
						</xsl:if>
						<form method="post" action="">
							<input type="hidden" name="reload" value="true" />
							
						</form>
					</fieldset>					
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>