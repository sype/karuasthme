<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="GenerateSiteMap">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Your basic sitemap</h1>
					
					<pre name="code" class="xml">
						<xsl:value-of select="//View/site_map" />
					</pre>
					<xsl:text disable-output-escaping="yes"><![CDATA[
					<script language="javascript">
						dp.SyntaxHighlighter.ClipboardSwf = 'Sls/Scripts/Javascript/Statics/clipboard.swf';
						dp.SyntaxHighlighter.HighlightAll('code');
					</script>]]></xsl:text>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>