<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Controllers">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Manage your Controllers &amp; Actions</h1>
					
					<h2>Existing controllers</h2>
					
					<h3>Statics Actions</h3>
					<a href="{//Links/Link[Name='ADDSTATICCONTROLLER']/Href}" title="Add a new Static Action"><img src="{concat($sls_url_img_core_icons, 'script_add.png')}" alt="Add a new Static Action" title="Add a new Static Action" style="margin-right:10px;" />Add a new Static Action</a>
					<xsl:if test="count(//View/statics/static) &gt; 0">
						<table>
							<xsl:for-each select="//View/statics/static">
								<tr>
									<td>
										<xsl:value-of select="name" />
									</td>
									<td>
										<a href="{concat(//Links/Link[Name='EDITSTATICCONTROLLER']/Href, '/Controller/', name, '.sls')}" title="View Details - Modify">
											<img src="{concat($sls_url_img_core_icons, 'magnifier.png')}" alt="View Details - Modify" title="View Details - Modify" />
										</a>
									</td>
									<td>
										<a href="{concat(//Links/Link[Name='DELSTATICCONTROLLER']/Href, '/Controller/', name, '.sls')}" title="Delete Controller">
											<img src="{concat($sls_url_img_core_icons, 'cross.png')}" alt="Delete Static Action" title="Delete Static Action" />
										</a>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
					<h3>Project Actions</h3>
					<a href="{//Links/Link[Name='ADDCONTROLLER']/Href}" title="Add a new Controller"><img src="{concat($sls_url_img_core_icons, 'script_add.png')}" alt="Add a new Controller" title="Add a new Controller" style="margin-right:10px;" />Add a new Controller</a>
					<xsl:if test="count(//View/controllers/controller) &gt; 0">
						<table>
							<tr>
								<td colspan="3"></td>
							</tr>
							<xsl:for-each select="//View/controllers/controller">
							
								<tr>
									<td>
										<xsl:value-of select="name" />
									</td>
									<td>
										<a href="{concat(//Links/Link[Name='EDITCONTROLLER']/Href, '/Controller/', name, '.sls')}" title="View Details - Modify">
											<img src="{concat($sls_url_img_core_icons, 'magnifier.png')}" alt="View Details - Modify" title="View Details - Modify" />
										</a>
									</td>
									<td>
										<xsl:if test="canBeDeleted = 'true'">
											<a href="{concat(//Links/Link[Name='DELCONTROLLER']/Href, '/Controller/', name, '.sls')}" title="Delete Controller">
												<img src="{concat($sls_url_img_core_icons, 'cross.png')}" alt="Delete Controller (will delete all actions refered)" title="Delete Controller (will delete all actions refered)" />
											</a>
										</xsl:if>
									</td>
								</tr>
								<xsl:if test="count(scontrollers/scontroller) &gt; 0">
									<xsl:variable name="controllerName" select="name" />
									<xsl:for-each select="scontrollers/scontroller">
										<tr>
											<td style="padding-left:30px;">
												<xsl:value-of select="name" />
											</td>
											<td>
												<a href="{concat(//Links/Link[Name='EDITACTION']/Href, '/Controller/', $controllerName, '/Action/', name, '.sls')}" title="View Details - Modify">
													<img src="{concat($sls_url_img_core_icons, 'magnifier.png')}" alt="View Details - Modify" title="View Details - Modify" />
												</a>
											</td>
											<td>
												<xsl:if test="canBeDeleted = 'true'">
													<a href="{concat(//Links/Link[Name='DELACTION']/Href, '/Controller/', $controllerName, '/Action/', name, '.sls')}" title="Delete Action">
														<img src="{concat($sls_url_img_core_icons, 'cross.png')}" alt="Delete Action" title="Delete Action" />
													</a>
												</xsl:if>
											</td>
										</tr>
									</xsl:for-each>
								</xsl:if>
								<tr>
									<td style="padding-left:30px;" colspan="3">
										<a href="{concat(//Links/Link[Name='ADDACTION']/Href, '/Controller/', name, '.sls')}" title=""><img src="{concat($sls_url_img_core_icons, 'script_add.png')}" style="margin-right:10px;" title="" alt="" />
											<xsl:value-of select="concat('Add a new Action in ', name)" />
										</a>
									</td>
								</tr>
								<tr height="10"></tr>
							</xsl:for-each>
						</table>
					</xsl:if>
					
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>