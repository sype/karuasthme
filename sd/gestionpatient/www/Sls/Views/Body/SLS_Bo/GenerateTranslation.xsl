<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="GenerateTranslation">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Generate a translation action into Bo</h1>
					<xsl:choose>
						<xsl:when test="count(//View/errors/error) &gt; 0">
							<div style="font-weight:bold;color:red">
								<xsl:for-each select="//View/errors/error">
									<xsl:value-of select="." /><br />
								</xsl:for-each>
								<xsl:if test="//View/error_type = 'rights'">
									<a href="{//View/url_add_controller}" title="Add your back-office controller">Add your back-office controller</a>
								</xsl:if>
							</div>
						</xsl:when>						
					</xsl:choose>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>