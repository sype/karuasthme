<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="ManageRights">
	<xsl:text disable-output-escaping="yes"><![CDATA[		
		<script type="text/javascript">
			function confirmDelete(link){if (confirm("Are you sure to delete this record ?"))window.location = link;else return false;}
		</script>
	]]></xsl:text>
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Manage your back-offices access</h1>
					<xsl:if test="count(//View/accounts/account) = 0">
						You don't have any back-office accounts.<br />
					</xsl:if>
					<xsl:if test="count(//View/accounts/account) &gt; 0">
						<table cellpadding="5" cellspacing="0" border="1">
							<tr style="background-color:#E9E9E9;color:#000;">
								<th>Login</th>
								<th>Password</th>
								<th>Rights</th>								
								<th>Modify</th>
								<th>Delete</th>
							</tr>
							<xsl:for-each select="//View/accounts/account">
								<tr>
									<td><xsl:value-of select="login" /></td>
									<td><xsl:value-of select="password" /></td>
									<td align="center"><xsl:value-of select="right" /></td>									
									<td align="center"><a href="{concat(//View/edit,'/name/',login)}" title="Modify"><img src="{concat($sls_url_img_core_icons,'edit16.png')}" title="Modify" alt="Modify" style="border:0" /></a></td>
									<td align="center"><a href="#" onclick="confirmDelete('{concat(//View/delete,'/name/',login)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete" alt="Delete" style="border:0" /></a></td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
					<xsl:if test="//View/admins_exist = 'false'">
						<span style="color:red;">Warning, no admins accounts have been configured to access on your customer back-office, we strongly recommend creating at least a root account to access your bo.</span><br />						
					</xsl:if>
					<a href="{//Links/Link[Name='ADD']/Href}" title="Manage your bo access" style="display:block;">Add a back-office access</a>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>