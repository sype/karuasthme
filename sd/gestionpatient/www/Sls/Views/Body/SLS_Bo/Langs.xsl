<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Langs">
	<xsl:text disable-output-escaping="yes"><![CDATA[		
		<script type="text/javascript">
			function confirmDelete(link){if (confirm("Are you sure to delete this record ?"))window.location = link;else return false;}
		</script>
	]]></xsl:text>
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Manage your Langs</h1>
					<xsl:if test="count(//View/langs/lang) &gt; 0">
						<table cellpadding="5" cellspacing="0" border="1">
							<tr style="background-color:#E9E9E9;color:#000;">
								<th>Language</th>
								<th>Iso</th>
								<th>Is Default</th>
								<th>Is Active</th>
								<th>Delete</th>
								<th>Set Default</th>
							</tr>
							<xsl:for-each select="//View/langs/lang">
								<tr>
									<td><xsl:value-of select="language" /></td>
									<td align="center"><xsl:value-of select="iso" /></td>
									<td align="center"><xsl:value-of select="default" /></td>
									<td align="center">
										<xsl:if test="active = 'true'">
											<xsl:if test="default = 'true'">
												<img src="{concat($sls_url_img_core_icons,'exclamation.png')}" title="Default lang cannot be disable" alt="Default lang cannot be disable" style="display:block;border:0;opacity:0.3;" />
											</xsl:if>
											<xsl:if test="default = 'false'">
												<a href="{concat(//Links/Link[Name='ENABLE']/Href, '/Enable/off/iso/', iso, '.sls')}"><img src="{concat($sls_url_img_core_icons,'exclamation.png')}" title="Disable this lang" alt="Disable this lang" style="border:0;" /></a>
											</xsl:if>
										</xsl:if>
										<xsl:if test="active = 'false'">
											<a href="{concat(//Links/Link[Name='ENABLE']/Href, '/Enable/on/iso/', iso, '.sls')}" ><img src="{concat($sls_url_img_core_icons,'accept.png')}" title="Enable this lang" alt="Enable this lang" style="border:0;display:block;" /></a>
										</xsl:if>
									</td>
									<td align="center">
										<xsl:if test="default = 'true'">
											<img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Can't delete default lang" alt="Can't delete default lang" style="display:block;border:0;opacity:0.3;" />
										</xsl:if>
										<xsl:if test="default = 'false'">
											<a href="#" onclick="confirmDelete('{concat(//View/delete,'/name/',iso)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete" alt="Delete" style="border:0" /></a>
										</xsl:if>
									</td>
									<td align="center">
										<xsl:if test="default = 'true'">
											<img src="{concat($sls_url_img_core_icons,'add16.png')}" title="Already default lang" alt="Already default lang" style="display:block;border:0;opacity:0.3;" />
										</xsl:if>
										<xsl:if test="default = 'false'">
											<a href="{concat(//View/default,'/name/',iso)}"><img src="{concat($sls_url_img_core_icons,'add16.png')}" title="Set as default lang" alt="Set as default lang" style="border:0" /></a>
										</xsl:if>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
					<a href="{//Links/Link[Name='ADD_LANG']/Href}" title="Add a new lang" style="display:block;">Add a new lang</a>					
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>