<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="AddTemplate">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Create a new template</h1>
					<form action="" method="post">
						<input type="hidden" name="reload" value="true" />
						<label for="tpl_name">Name :</label><input type="text" name="tpl_name" id="tpl_name" />
						<input type="submit" value="Add" />
					</form>
					<xsl:if test="count(//View/errors/error) &gt; 0">
						<span style="font-weight:bold;color:red;">
							<xsl:value-of select="//View/errors/error" />
						</span>
					</xsl:if>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>