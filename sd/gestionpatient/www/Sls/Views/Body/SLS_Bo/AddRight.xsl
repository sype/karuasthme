<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="AddRight">	
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Add a back-office access</h1>
					<form method="post" action="">
						<input type="hidden" name="reload" value="true" />
						<table>
							<tr>
								<td>Login</td>
								<td><input type="text" name="login" value="{//View/login}" /></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><input type="password" name="password" /></td>
							</tr>
							<tr>
								<td>Privilege</td>
								<td>
									<select name="privilege" id="privilege" onchange="checkPrivilege()">
										<option value="admin">
											<xsl:if test="//View/privilege = 'admin'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											Simple Admin
										</option>
										<option value="god">
											<xsl:if test="//View/privilege = 'god'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											Super Admin
										</option>
									</select>
								</td>
							</tr>
							<tr>
								<td>Rigth</td>
								<td>
									<input type="checkbox" name="add" id="rightAdd"><xsl:if test="//View/right_add = 'true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><label for="rightAdd">Add</label>&#160;
									<input type="checkbox" name="modify" id="rightModify"><xsl:if test="//View/right_modify = 'true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><label for="rightModify">Modify</label>&#160;
									<input type="checkbox" name="delete" id="rightDelete"><xsl:if test="//View/right_delete = 'true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><label for="rightDelete">Delete</label>
								</td>
							</tr>
							<tr>
								<td colspan="2" align="center"><input type="submit" value="Create" /></td>								
							</tr>
						</table>
					</form>
					<xsl:if test="count(//View/errors/error) &gt; 0">
						<div style="font-weight:bold;color:red;">
							<xsl:value-of select="//View/errors/error" />
						</div>
					</xsl:if>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>