<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Bo">
	<xsl:text disable-output-escaping="yes"><![CDATA[		
		<script type="text/javascript">
			function confirmDelete(link){if (confirm("Are you sure to delete this record ?"))window.location = link;else return false;}
		</script>
	]]></xsl:text>
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Manage your back-offices</h1>
					<xsl:if test="count(//View/bos/bo) = 0">
						You don't have any back-offices.<br />
					</xsl:if>
					<xsl:if test="count(//View/bos/bo) &gt; 0">
						<table cellpadding="5" cellspacing="0" border="1">
							<tr style="background-color:#E9E9E9;color:#000;">
								<th>Class</th>
								<th>Database</th>
								<th>Table</th>
								<th>Actions</th>								
								<th>Modify</th>
								<th>Delete</th>
							</tr>
							<xsl:for-each select="//View/bos/bo">
								<tr>
									<td><xsl:value-of select="class" /></td>
									<td><xsl:value-of select="db" /></td>
									<td><xsl:value-of select="table" /></td>
									<td align="center"><xsl:value-of select="nb_actions" /></td>									
									<td align="center"><a href="{concat(//View/edit,'/name/',db,'_',table)}" title="Modify"><img src="{concat($sls_url_img_core_icons,'edit16.png')}" title="Modify" alt="Modify" style="border:0" /></a></td>
									<td align="center"><a href="#" onclick="confirmDelete('{concat(//View/delete,'/name/',db,'_',table)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete" alt="Delete" style="border:0" /></a></td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:if>
					<a href="{//Links/Link[Name='GENERATE']/Href}" title="Generate your back-offices" style="display:block;">Generate Back-Offices</a>
					<xsl:if test="//View/admins_exist = 'false'">
						<span style="color:red;">Warning, no admins accounts have been configured to access on your customer back-office, we strongly recommend creating at least a root account to access your bo.</span><br />
						<a href="{//Links/Link[Name='MANAGE_RIGHTS']/Href}" title="Manage your bo access" style="display:block;">Manage your back-office access</a>
					</xsl:if><br />
					<h2>Specific Actions</h2>
					<xsl:if test="count(//View/actions/action[existed='true']) = 0">
						You don't have any specific actions.<br />
					</xsl:if>
					<xsl:if test="count(//View/actions/action[existed='true']) &gt; 0">
						<ul>
							<xsl:for-each select="//View/actions/action[existed='true']">
								<li><xsl:value-of select="name"	/></li>
							</xsl:for-each>
						</ul>
					</xsl:if>
					<xsl:if test="//View/actions/action[name='Translation']/existed = 'false'">
						<a href="{//Links/Link[Name='TRANSLATION']/Href}" title="Generate a translation action into Bo" style="display:block;">Generate Translation Action</a>
					</xsl:if>
					<xsl:if test="//View/actions/action[name='FileUpload']/existed = 'false'">
						<a href="{//Links/Link[Name='FILEUPLOAD']/Href}" title="Generate a file upload action into Bo" style="display:block;">Generate FileUpload Action</a>
					</xsl:if>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>