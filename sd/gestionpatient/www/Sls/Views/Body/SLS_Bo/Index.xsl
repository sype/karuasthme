<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Index">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Restricted Area</h1>
					<h2>Please fill your authentication informations</h2>					
					<fieldset>
						<legend>Authentication</legend>
						<xsl:if test="count(//View/errors) != 0">
							<div style="color:red;margin-bottom:20px;">
								<xsl:for-each select="//View/errors/error">
									<xsl:value-of select="." /><br />
								</xsl:for-each>
							</div>
						</xsl:if>
						<form action="{//Links/Link[Name='AUTHENTICATION']/Href}" enctype="multipart/form-data" method="post" id="authentication">
							<label for="login">Username :</label><input type="text" name="login" id="login" /><br />
							<label for="password">Password :</label><input type="password" name="password" id="password" /><br />
							<input type="hidden" name="reload" value="true" />
							<input type="submit" value="Authenticate" />
						</form>
					</fieldset>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>