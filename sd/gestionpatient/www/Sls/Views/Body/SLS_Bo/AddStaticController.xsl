<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="AddStaticController">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Manage your Controllers &amp; Actions</h1>
					<h2>Add a new Static Controller</h2>
					<form action="#" method="post" enctype="multipart/form-data">
						<table>
							<xsl:if test="count(//View/form) &gt; 0 and count(//View/errors/error) &gt; 0">
								<tr>
									<td colspan="2" style="padding-bottom:20px;color:red;">
										<xsl:for-each select="//View/errors/error">
											<xsl:value-of select="." /><br />
										</xsl:for-each>
									</td>
								</tr>
							</xsl:if>
							<xsl:if test="count(//View/form) &gt; 0 and count(//View/errors/error) = 0">
								<tr>
									<td colspan="2" style="padding-bottom:20px;color:green;">
										Your modifications have been saved
									</td>
								</tr>
							</xsl:if>
							<tr>
								<td>Static Controller Name :</td>
								<td>
									<input type="text" value="{//View/controller/name}" name="controllerName">
										<xsl:if test="count(//View/form) = 1 and //View/form/controllerName != ''">
											<xsl:attribute name="value">
												<xsl:value-of select="//View/form/controllerName" />
											</xsl:attribute>
										</xsl:if>
									</input>
								</td>
							</tr>
							<tr height="10"></tr>
							<tr>
								<td colspan="2"><input type="submit" value="Add a new Static Controller" /><input type="hidden" name="reload" value="true" /></td>
							</tr>
						</table>
					</form>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>