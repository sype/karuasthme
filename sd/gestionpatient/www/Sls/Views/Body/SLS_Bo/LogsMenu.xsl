<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="LogsMenu">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Logs</h1>
					<ul>
						<li><a href="{//View/logs_app}" title="Production App Logs">View Production Application Logs</a></li>
						<li><a href="{//View/logs_mail}" title="Mail Logs">View Mail Logs</a></li>
					</ul>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>