<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="MailSettings">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Edit Your Settings</h1>
					<h2>Mails</h2>
					<fieldset>
						<legend>Mails Settings</legend>
						<xsl:if test="count(//View/errors/error) &gt; 0">
							<div style="display:block;margin-bottom:10px;margin-top:10px;color:red;font-weight:bold;">
								<xsl:for-each select="//View/errors/error">
									<xsl:value-of select="." /><br />
								</xsl:for-each>
							</div>
						</xsl:if>
						<form action="" name="" enctype="multipart/form-data" method="post">
							<input type="hidden" name="ping" id="ping" value="false" />
							<input type="checkbox" name="export" id="export" />
							<label for="export">Export the configuration</label><br /><br /><br />
							
							<label for="host">SMTP Host :</label>
							<input type="text" name="host" id="host" value="{//View/current_values/host}" /><br />
							
							<label for="base">Host Port :</label>
							<input type="text" name="port" id="port" value="{//View/current_values/port}" /><br />
							
							<label for="user">Username :</label>
							<input type="text" name="user" id="user" value="{//View/current_values/user}" /><br />
							
							<label for="needpass">This SMTP doesn't need username :</label>
							<input type="checkbox" name="needuser" id="needuser">
								<xsl:if test="//View/current_values/user = ''">
									<xsl:attribute name="checked" select="'checked'" />
								</xsl:if>
							</input><br />
							
							<label for="pass">Password :</label>
							<input type="password" name="pass" id="pass" value="" /><br />
							
							<label for="needpass">This SMTP doesn't need password :</label>
							<input type="checkbox" name="needpass" id="needpass">
								<xsl:if test="//View/current_values/pass = ''">
									<xsl:attribute name="checked" select="'checked'" />
								</xsl:if>
							</input><br />
							
							<label for="pass">Default Domain :</label>
							<input type="text" name="domain" id="domain" value="{//View/current_values/domain}" onChange="document.getElementById('domainName1').value=this.value;document.getElementById('domainName2').value=this.value;document.getElementById('domainName3').value=this.value;" /><br />
							
							<label for="pass">Return :</label>
							<input type="text" name="nameReturn" id="nameReturn" value="{//View/current_values/nameReturn}" /> <input type="text" name="return" id="return" value="{//View/current_values/return}" />@<input type="text" value="{//View/current_values/domain}" id="domainName1" disabled="disabled" /><br />
							
							<label for="pass">Reply to :</label>
							<input type="text" name="nameReply" id="nameReply" value="{//View/current_values/nameReply}" /> <input type="text" name="reply" id="reply" value="{//View/current_values/reply}" />@<input type="text" value="{//View/current_values/domain}" id="domainName2" disabled="disabled" /><br />
							
							<label for="pass">Sender :</label>
							<input type="text" name="nameSender" id="nameSender" value="{//View/current_values/nameSender}" /> <input type="text" name="sender" id="sender" value="{//View/current_values/return}" />@<input type="text" value="{//View/current_values/domain}" id="domainName3" disabled="disabled" /><br />
							
							<input type="hidden" name="reload" value="true" />
							<input type="submit" value="Confirm Changes" />
							<input type="submit" value="Test Connection" onclick="document.getElementById('ping').value='true';" />
						</form>
						<xsl:if test="count(//View/ping) &gt; 0">																	
							<xsl:if test="//View/ping = 'true'">
								<div style="color:green;">Connection succeed</div>
							</xsl:if>
							<xsl:if test="//View/ping != 'true'">
								<div style="color:red;">Connection failed with message :<br /><xsl:value-of select="//View/ping" /></div>
							</xsl:if>							
						</xsl:if>
					</fieldset>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>