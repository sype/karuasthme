<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="ProdSettings">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Edit Your Settings</h1>
					<h2>Production Settings</h2>
					<fieldset>
						<legend>Production Settings</legend>
						<xsl:if test="count(//View/errors/error) &gt; 0">
							<div style="display:block;margin-bottom:10px;margin-top:10px;color:red;font-weight:bold;">
								<xsl:for-each select="//View/errors/error">
									<xsl:value-of select="." /><br />
								</xsl:for-each>
							</div>
						</xsl:if>
						<form action="" name="" enctype="multipart/form-data" method="post">
							Your application is on production ?
							<input type="radio" name="prod" value="1" id="prod-1">
								<xsl:if test="//View/current_values/prod = 1">	
									<xsl:attribute name="checked" select="'checked'" />
								</xsl:if>
							</input>
							<label for="prod-1">yes</label>
							<input type="radio" name="prod" value="0" id="prod-0">
								<xsl:if test="//View/current_values/prod = 0">	
									<xsl:attribute name="checked" select="'checked'" />
								</xsl:if>
							</input>
							<label for="prod-0">no</label>
							<br />
							<label for="prod">Cache is active ?</label>
							<input type="radio" name="cache" value="1" id="cache-1">
								<xsl:if test="//View/current_values/cache = 1">	
									<xsl:attribute name="checked" select="'checked'" />
								</xsl:if>
							</input>
							<label for="cache-1">yes</label>
							<input type="radio" name="cache" value="0" id="cache-0">
								<xsl:if test="//View/current_values/cache = 0">	
									<xsl:attribute name="checked" select="'checked'" />
								</xsl:if>
							</input>
							<label for="cache-1">no</label>
							<br /><br />
							<input type="hidden" name="reload" value="true" />
							<input type="submit" value="Confirm Changes" />
						</form>
					</fieldset>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>