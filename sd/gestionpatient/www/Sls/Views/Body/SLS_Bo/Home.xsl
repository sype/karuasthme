<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Home">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Welcome on your SillySmart DashBoard</h1>
					<h2>Actions</h2>
					<ul>
						<xsl:for-each select="//View/Actions/Action">
							<li><a href="{link}" title="{name}"><xsl:value-of select="name" /></a></li>
						</xsl:for-each>
					</ul>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>