<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="EditModel">	
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Edit the model `<xsl:value-of select="//View/model/class" />`<a href="#" onclick="confirmDelete('{concat(//View/delete,'/name/',//View/model/db,'_',//View/model/table)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete" alt="Delete" style="border:0" align="absmiddle" /></a></h1>
					<table cellspacing="0" cellpadding="5" border="1">
						<tr>
							<td style="background-color:#E9E9E9;color:#000;">Class</td>
							<td><xsl:value-of select="//View/model/class" /></td>
						</tr>
						<tr>
							<td style="background-color:#E9E9E9;color:#000;">Description</td>
							<td>
								<form action="{//View/descriptions}" method="post">
									<input type="hidden" name="__table" value="{concat(//View/model/db,'_',//View/model/table)}" />
									<input type="text" name="description" value="{//View/model/description}" maxlength="60" />
									<input type="submit" value="Update" />
								</form>
							</td>
						</tr>
						<tr>
							<td style="background-color:#E9E9E9;color:#000;">Database</td>
							<td><xsl:value-of select="//View/model/db" /></td>
						</tr>
						<tr>
							<td style="background-color:#E9E9E9;color:#000;">Table</td>
							<td><xsl:value-of select="//View/model/table" /></td>
						</tr>
						<tr>
							<td style="background-color:#E9E9E9;color:#000;">PK</td>
							<td><xsl:value-of select="//View/model/pk" /></td>
						</tr>
						<tr>
							<td style="background-color:#E9E9E9;color:#000;">Multilanguage</td>
							<td><xsl:value-of select="//View/model/multilanguage" /></td>
						</tr>
						<tr>
							<td style="background-color:#E9E9E9;color:#000;">Columns / <br />Descriptions</td>
							<td>
								<form action="{//View/descriptions}" method="post">
									<input type="hidden" name="__table" value="{concat(//View/model/db,'_',//View/model/table)}" />
									<table>									
										<xsl:for-each select="//View/model/columns/column">
											<tr>
												<td>
													<xsl:if test="name = //View/model/pk or (name = 'pk_lang' and //View/model/multilanguage = 'true')">
														<b><xsl:value-of select="name" /><xsl:if test="type != ''"><i> (Type <xsl:value-of select="type" />&#160;<a href="#" onclick="confirmDelete('{concat(//View/delete_type,'/table/',//View/model/db,'_',//View/model/table,'/column/',name)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete this Type" alt="Delete this Type" style="border:0" align="absmiddle" /></a>)</i></xsl:if>&#160;<xsl:if test="fk != ''"><i> (Fk <xsl:value-of select="fk" />&#160;<a href="#" onclick="confirmDelete('{concat(//View/delete_fk,'/tableFk/',//View/model/db,'_',//View/model/table,'/columnFk/',name,'/tablePk/',//View/model/db,'_',fk)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete this FK" alt="Delete this FK" style="border:0" align="absmiddle" /></a>)</i></xsl:if></b>
													</xsl:if>
													<xsl:if test="name != //View/model/pk and name != 'pk_lang'">
														<xsl:value-of select="name" /><xsl:if test="type != ''"><i> (Type <xsl:value-of select="type" />&#160;<a href="#" onclick="confirmDelete('{concat(//View/delete_type,'/table/',//View/model/db,'_',//View/model/table,'/column/',name)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete this Type" alt="Delete this Type" style="border:0" align="absmiddle" /></a>)</i></xsl:if>&#160;<xsl:if test="fk != ''"><i> (Fk <xsl:value-of select="fk" />&#160;<a href="#" onclick="confirmDelete('{concat(//View/delete_fk,'/tableFk/',//View/model/db,'_',//View/model/table,'/columnFk/',name,'/tablePk/',//View/model/db,'_',fk)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete this FK" alt="Delete this FK" style="border:0" align="absmiddle" /></a>)</i></xsl:if>
													</xsl:if>
												</td>
												<td>
													<input type="text" name="{concat('col_',name)}" value="{comment}" maxlength="60" />
												</td>
											</tr>
										</xsl:for-each>
										<tr>
											<td colspan="2" align="center"><input type="submit" value="Update descriptions" /></td>
										</tr>
									</table>
								</form>
							</td>
						</tr>
					</table>
					<a href="{//View/model/url_add_type}" title="Add a specific type on a column">Add a specific type on a column</a><br />
					<a href="{//View/model/url_add_fk}" title="Add a foreign key on a column">Add a foreign key on a column</a><br />
					<table style="margin-top:10px;">
						<tr>
							<td valign="bottom"><b><u>Source of the current model :</u></b></td>
							<td valign="bottom"><b><u>Source of the current table :</u></b></td>
						</tr>
						<tr>
							<td valign="top">								
								<pre name="code" class="php">
									<xsl:value-of select="//View/model/current_source" />
								</pre>								
							</td>
							<td valign="top">
								<xsl:if test="//View/model/current_table = -1">
									Sorry, this model is deprecated. Her mapped table doesn't exists anymore.<br />
									You should re-create this table or delete this model.
								</xsl:if>
								<xsl:if test="//View/model/current_table != -1">
									<pre name="code2" class="php">
										<xsl:value-of select="//View/model/current_table" />
									</pre>
								</xsl:if>
							</td>
						</tr>						
						<xsl:if test="//View/model/current_table != -1 and //View/model/current_source != //View/model/current_table">
							<tr>
								<td colspan="2" align="center">
									Your model isn't up to date, you should update it. <a href="{concat(//View/update,'/name/',//View/model/db,'_',//View/model/table)}" title="Update this model">Update this model</a>
								</td>
							</tr>
						</xsl:if>
						<xsl:if test="//View/model/current_table != -1 and //View/model/current_source = //View/model/current_table">
							<tr>
								<td colspan="2" align="center">
									Your model is up to date.
								</td>
							</tr>
						</xsl:if>
					</table>
					<xsl:text disable-output-escaping="yes"><![CDATA[
					<script language="javascript">
						dp.SyntaxHighlighter.ClipboardSwf = 'Sls/Scripts/Javascript/Statics/clipboard.swf';
						dp.SyntaxHighlighter.HighlightAll('code');
					</script>]]></xsl:text>
					<xsl:if test="//View/model/current_table != -1">
						<xsl:text disable-output-escaping="yes"><![CDATA[
							<script language="javascript">							
							dp.SyntaxHighlighter.HighlightAll('code2');
						</script>]]></xsl:text>						
					</xsl:if>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>