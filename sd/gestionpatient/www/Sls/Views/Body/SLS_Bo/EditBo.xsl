<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="EditBo">	
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Edit the back-office `<xsl:value-of select="//View/bo/class" />`<a href="#" onclick="confirmDelete('{concat(//View/delete,'/name/',//View/bo/class)}');return false;"><img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete" alt="Delete" style="border:0" align="absmiddle" /></a></h1>
					<xsl:for-each select="//View/bo/actions/action">
						<div style="display:block;float:left;width:600px;border-bottom:1px dotted black;clear:both;">
							<div style="display:block;float:left;font-weight:bold;width:580px;">
								Action <xsl:value-of select="name" />								
							</div>
							<div style="display:block;float:left;">
								<xsl:variable name="item" select="concat('delete_action_',php:functionString('strtolower',name))" />
								<a href="#">
									<xsl:attribute name="onclick">confirmDelete('<xsl:value-of select="//View/bo/urls/*[name()=$item]" />');return false;</xsl:attribute>									
									<img src="{concat($sls_url_img_core_icons,'delete16.png')}" title="Delete" alt="Delete" style="border:0" align="absmiddle" />
								</a>
							</div>
						</div>
						<xsl:if test="name='List'">
							<form action="{//View/bo/urls/customize_bo}" method="post" style="margin:0;padding:0;">
								<input type="hidden" name="class" value="{//View/bo/class}" />
								<div style="margin-left:80px;">
									Allowed Filters :
									<select name="filters[]" multiple="multiple" style="height:70px;margin-left:18px;">
										<xsl:for-each select="columns/column">
											<option value="{name}">
												<xsl:if test="selected_filters = 'true'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
												<xsl:value-of select="title" />
											</option>
										</xsl:for-each>
									</select> (columns on which you can filters)
								</div>
								<div style="margin-left:80px;">
									Allowed Columns :
									<select name="columns[]" multiple="multiple" style="height:70px;">
										<xsl:for-each select="columns/column">
											<option value="{name}">
												<xsl:if test="selected_columns = 'true'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
												<xsl:value-of select="title" />
											</option>
										</xsl:for-each>
									</select> (columns displayed in the list)
								</div>
								<div style="margin-left:80px;">
									Default group by :									
									<select name="group[]" multiple="multiple" style="margin-left:4px;" size="4">										
										<xsl:for-each select="groups/group">
											<option value="{name}">
												<xsl:if test="selected = 'true'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
												<xsl:value-of select="desc" />
											</option>
										</xsl:for-each>
									</select> (Default: any)
								</div>
								<div style="margin-left:80px;">
									Default order :
									<xsl:variable name="column" select="default_order/column" />
									<select name="column" style="margin-left:28px;">
										<option value=""></option>
										<xsl:for-each select="columns/column">
											<option value="{name}">
												<xsl:if test="name = $column"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
												<xsl:value-of select="title" />
											</option>
										</xsl:for-each>
									</select>
									<select name="order">
										<option value=""></option>
										<option value="asc">
											<xsl:if test="default_order/order = 'asc'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											ASC
										</option>
										<option value="desc">
											<xsl:if test="default_order/order = 'desc'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											DESC
										</option>
									</select> (PK ASC if empty)
								</div>
								<div style="margin-left:81px;">
									Default limit :
									<input type="text" name="start" value="{default_limit/start}" style="width:40px;margin-left:32px;" /> - 
									<select name="length" style="width:50px;">
										<option value="20">
											<xsl:if test="default_limit/length = 20"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											20
										</option>
										<option value="40">
											<xsl:if test="default_limit/length = 40"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											40
										</option>
										<option value="60">
											<xsl:if test="default_limit/length = 60"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											60
										</option>
										<option value="80">
											<xsl:if test="default_limit/length = 80"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											80
										</option>
										<option value="100">
											<xsl:if test="default_limit/length = 100"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
											100
										</option>
									</select> (Default: 0 - 20)
								</div>
								<div style="margin-left:80px;">
									Action Add :
									<input type="radio" name="action_add" id="action_add_true" style="margin-left:43px;" value="true">
										<xsl:if test="default_actions/action_add = 'true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
									</input>
									<label for="action_add_true">Yes</label>
									<input type="radio" name="action_add" id="action_add_false" style="margin-left:13px;" value="false">
										<xsl:if test="default_actions/action_add = 'false'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
									</input>
									<label for="action_add_false">No</label>
								</div>
								<div style="margin-left:80px;">
									Action Modify :
									<input type="radio" name="action_modify" id="action_modify_true" style="margin-left:23px;" value="true">
										<xsl:if test="default_actions/action_modify = 'true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
									</input>
									<label for="action_modify_true">Yes</label>
									<input type="radio" name="action_modify" id="action_modify_false" style="margin-left:13px;" value="false">
										<xsl:if test="default_actions/action_modify = 'false'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
									</input>
									<label for="action_modify_false">No</label>
								</div>
								<div style="margin-left:80px;">
									Action Delete :
									<input type="radio" name="action_delete" id="action_delete_true" style="margin-left:26px;" value="true">
										<xsl:if test="default_actions/action_delete = 'true'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
									</input>
									<label for="action_delete_true">Yes</label>
									<input type="radio" name="action_delete" id="action_delete_false" style="margin-left:13px;" value="false">
										<xsl:if test="default_actions/action_delete = 'false'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
									</input>
									<label for="action_delete_false">No</label>
								</div>
								<div style="margin-left:80px;">
									Natural Join :
									<xsl:variable name="join" select="joins/join/table" />
									<select name="join" style="margin-left:33px;width:100px;">
										<option value=""></option>
										<xsl:for-each select="//View/bo/models/model">
											<option value="{table}">
												<xsl:if test="table = $join"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
												<xsl:value-of select="table" />
											</option>
										</xsl:for-each>
									</select> (Default: any)
								</div>								
								<input type="submit" value="Update" style="margin-left:205px;" />
							</form>
						</xsl:if>
						<xsl:if test="name='Add'">
							<div style="clear:both;margin-left:80px;">								
								<form action="{//View/bo/urls/edit_action_add}" method="post" style="margin:0;padding:0;">
									Redirect on Action List after adding success ? 
									<input type="checkbox" name="redirect">
										<xsl:if test="redirect = 'true'">
											<xsl:attribute name="checked">checked</xsl:attribute>
										</xsl:if>
									</input>
									<input type="submit" value="Change" />
								</form>
							</div>
						</xsl:if>
					</xsl:for-each>					
					<div style="float:left;clear:both;margin-top:25px;">
						<h2>Add actions</h2>
						<xsl:if test="count(//View/bo/actions_absents/action_absent) = 0">
							No actions to add, all actions already exist.
						</xsl:if>
						<xsl:if test="count(//View/bo/actions_absents/action_absent) &gt; 0">
							<xsl:for-each select="//View/bo/actions_absents/action_absent">
								<a title="{concat('Add an action ',name)}" style="display:block;">
									<xsl:variable name="item" select="concat('add_action_',php:functionString('strtolower',name))" />
									<xsl:attribute name="href"><xsl:value-of select="//View/bo/urls/*[name()=$item]" /></xsl:attribute>
									<img src="{concat($sls_url_img_core_icons,'add16.png')}" style="border:0;" align="absmiddle" />&#160;Add an action '<xsl:value-of select="name" />'
								</a>
							</xsl:for-each>
						</xsl:if>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>