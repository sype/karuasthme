<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="AddAction">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Manage your Controllers &amp; Actions</h1>
					
					<h2>Add a new Action to the controller <xsl:value-of select="View/controller/name" /></h2>
					<form action="#" method="post" enctype="multipart/form-data">						
						<table>
							<xsl:if test="count(//View/form) &gt; 0 and count(//View/errors/error) &gt; 0">
								<tr>
									<td colspan="2" style="padding-bottom:20px;color:red;">
										<xsl:for-each select="//View/errors/error">
											<xsl:value-of select="." /><br />
										</xsl:for-each>
									</td>
								</tr>
							</xsl:if>
							<xsl:if test="count(//View/form) &gt; 0 and count(//View/errors/error) = 0">
								<tr>
									<td colspan="2" style="padding-bottom:20px;color:green;">
										Your modifications have been saved
									</td>
								</tr>
							</xsl:if>
							<tr>
								<td>Generic Action Name :</td>
								<td>
									<input type="text" value="{//View/action/name}" name="actionName">
										<xsl:if test="count(//View/form) = 1 and //View/form/actionName != ''">
											<xsl:attribute name="value">
												<xsl:value-of select="//View/form/actionName" />
											</xsl:attribute>
										</xsl:if>
									</input>
								</td>
							</tr>
							<tr height="10"></tr>
							<tr>
								<td colspan="2"><h2>Url Translations</h2></td>
							</tr>
							<xsl:for-each select="//View/controller/translations/translation">
								
								<tr>
									<td>
										<xsl:value-of select="concat('Translation in ', lang, ' :')" />
									</td>	 
									<td>
										<xsl:variable name="inputName" select="concat(lang, '-action')" />
										<input type="text" name="{$inputName}" value="{name}">
											<xsl:if test="count(//View/form) = 1 and //View/form/*[name()=$inputName] != ''">
												<xsl:attribute name="value">
													<xsl:value-of select="//View/form/*[name()=$inputName]" />
												</xsl:attribute>
											</xsl:if>
										</input>
									</td>
								</tr>
								
							</xsl:for-each>
							<tr height="20"></tr>
							<tr>
								<td colspan="2"><h2>Page Titles Translations</h2></td>
							</tr>
							<xsl:for-each select="//View/controller/translations/translation">
								<tr>
									<td>
										<xsl:value-of select="concat('Title Translation in ', lang, ' :')" />
									</td>	 
									<td>
										<xsl:variable name="inputName" select="concat(lang, '-title')" />
										<input type="text" name="{$inputName}" value="{title}">
											<xsl:if test="count(//View/form) = 1 and //View/form/*[name()=$inputName] != ''">
												<xsl:attribute name="value">
													<xsl:value-of select="//View/form/*[name()=$inputName]" />
												</xsl:attribute>
											</xsl:if>
										</input>
									</td>
								</tr>
							</xsl:for-each>
							<tr height="20"></tr>
							<tr>
								<td colspan="2"><h2>Descriptions Translations</h2></td>
							</tr>
							<xsl:for-each select="//View/controller/translations/translation">
								<tr>
									<td>
										<xsl:value-of select="concat('Description Translation in ', lang, ' :')" />
									</td>	 
									<td>
										<xsl:variable name="inputName" select="concat(lang, '-description')" />
										<input type="text" name="{$inputName}" value="{description}">
											<xsl:if test="count(//View/form) = 1 and //View/form/*[name()=$inputName] != ''">
												<xsl:attribute name="value">
													<xsl:value-of select="//View/form/*[name()=$inputName]" />
												</xsl:attribute>
											</xsl:if>
										</input>
									</td>
								</tr>
							</xsl:for-each>
							<tr height="20"></tr>
							<tr>
								<td colspan="2"><h2>Keywords Translations</h2></td>
							</tr>
							<xsl:for-each select="//View/controller/translations/translation">
								<tr>
									<td>
										<xsl:value-of select="concat('Keywords in ', lang, ' :')" />
									</td>	 
									<td>
										<xsl:variable name="inputName" select="concat(lang, '-keywords')" />
										<input type="text" name="{$inputName}" value="{keywords}">
											<xsl:if test="count(//View/form) = 1 and //View/form/*[name()=$inputName] != ''">
												<xsl:attribute name="value">
													<xsl:value-of select="//View/form/*[name()=$inputName]" />
												</xsl:attribute>
											</xsl:if>
										</input>
									</td>
								</tr>
							</xsl:for-each>
							<tr height="20"></tr>
							<tr>
								<td>Protocol :</td>
								<td>
									<select name="protocol" id="protocol">
										<option value="http">
											<xsl:if test="//View/protocol = 'http'">
												<xsl:attribute name="selected" select="'selected'" />
											</xsl:if>
											http
										</option>
										<option value="https">
											<xsl:if test="//View/protocol = 'https'">
												<xsl:attribute name="selected" select="'selected'" />
											</xsl:if>
											https
										</option>
									</select>
								</td>
							</tr>
							<tr height="10"></tr>
							<tr>
								<td>Template :</td>
								<td>
									<select name="template" id="template">
										<option value="-1">Default</option>
										<xsl:for-each select="//View/tpls/tpl[.!='__default']">
											<option value="{.}">
												<xsl:if test="//View/template = ."><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
												<xsl:value-of select="." />
											</option>
										</xsl:for-each>
									</select>
								</td>
							</tr>
							<tr height="10"></tr>
							<tr>
								<td>This action will need dynamic parameters :</td>
								<td>
									<input type="checkbox" name="dynamic" />
								</td>
							</tr>
							<tr height="20"></tr>
							<tr>
								<td>Choose Search Engine Behavior for this action :</td>
								<td>
									<select name="indexes">
										<option value="index, follow">Index the page and follow all links</option>
										<option value="noindex, nofollow">Doesn't index and doesn't follow links</option>
										<option value="index, nofollow">Index the page but doesn't follow links</option>
										<option value="noindex, follow">Doesn't index the page but follow links</option>
									</select>
								</td>
							</tr>
							<tr height="20"></tr>
							<tr>
								<td colspan="2"><input type="submit" value="Add a new Action" /><input type="hidden" name="reload" value="true" /></td>
							</tr>
						</table>
					</form>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>