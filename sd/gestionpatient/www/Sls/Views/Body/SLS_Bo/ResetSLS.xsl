<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="ResetSLS">
	<div id="header">
			<div id="logo"></div>
			<div id="baseline"></div>
		</div>
		<div id="main">
			<xsl:call-template name="makeMenu" />			
			<div id="rightSide">
				<div id="container">
					<h1>Reset the SillySmart Installation</h1>
					<h2>The reset will destroy all your site Data</h2>
					<p>You are ready to reset SillySmart Installation. The reset will destroy all your existing datas. Make sure you want to do this.</p>
					<form action="#" method="post" enctype="multipart/form-data">
						<xsl:if test="//View/stepPassword = 'no'">
							<input type="hidden" name="confirm" value="reset" />
							<input type="submit" value="Reset SillySmart Installation" />
						</xsl:if>
						<xsl:if test="//View/stepPassword = 'yes'">
							<label for="login">Username :</label> <input type="text" name="login" id="login" /><br />
							<label for="login">Password :</label> <input type="password" name="password" id="password" /><br />
							<input type="hidden" name="confirm" value="reset" />
							<input type="submit" value="Reset SillySmart Installation" />
						</xsl:if>
					</form>
				</div>	
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>