<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="HeaderAddType">
	<link rel="stylesheet" type="text/css" href="{concat($sls_url_css_core, 'Global.css')}" />
		<xsl:comment>[IF IE]&gt;
		&lt;link rel="stylesheet" type="text/css" href="<xsl:value-of select="concat($sls_url_css_core, 'GlobalIe.css')" />" /&gt;
		&lt;![endif]</xsl:comment>
		<xsl:comment>[IF lt IE 7]&gt;
		&lt;link rel="stylesheet" type="text/css" href="<xsl:value-of select="concat($sls_url_css_core, 'GlobalIe6.css')" />" /&gt;
		&lt;![endif]</xsl:comment>
		<script type="text/javascript">
			<xsl:text disable-output-escaping="yes"><![CDATA[
			function checkType()
			{
				if (document.getElementById('type').value == 'file')
				{
					document.getElementById('file').style.display = 'inline';
					document.getElementById('multilanguage').style.display = 'inline';
					checkFile();
					checkThumb();
				}
				else
				{
					document.getElementById('file').style.display = 'none';
					document.getElementById('multilanguage').style.display = 'none';
					document.getElementById('thumb_check').style.display = 'none';
					document.getElementById('thumbs').style.display = 'none';
				}
			}
			function checkFile()
			{
				if (document.getElementById('file').value == 'img')
				{
					document.getElementById('thumb_check').style.display = 'inline';
					checkThumb();
				}
				else
				{
					document.getElementById('thumb_check').style.display = 'none';
					document.getElementById('thumbs').style.display = 'none';	
				}
			}
			function checkThumb()
			{
				if (document.getElementById('file_thumb').checked)
				{
					document.getElementById('thumbs').style.display = 'block';
				}
				else
				{
					document.getElementById('thumbs').style.display = 'none';
				}
			}
			function addThumb(id)
			{
				document.getElementById('thumb'+id).style.display = 'block';
				resetLinksMore(id);
			}
			function resetLinksMore(id)
			{
				for(var i=0 ; i<id ; i++)
				{
					document.getElementById('more'+i).style.display = 'none';
				}
			}
			]]></xsl:text>
		</script>
	</xsl:template>
</xsl:stylesheet>