<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="HeaderCreatePlugin">
	<link rel="stylesheet" type="text/css" href="{concat($sls_url_css_core, 'Global.css')}" />
		<xsl:comment>[IF IE]&gt;
		&lt;link rel="stylesheet" type="text/css" href="<xsl:value-of select="concat($sls_url_css_core, 'GlobalIe.css')" />" /&gt;
		&lt;![endif]</xsl:comment>
		<xsl:comment>[IF lt IE 7]&gt;
		&lt;link rel="stylesheet" type="text/css" href="<xsl:value-of select="concat($sls_url_css_core, 'GlobalIe6.css')" />" /&gt;
		&lt;![endif]</xsl:comment>
		<script type="text/javascript" src="{concat($sls_url_js_core_statics, 'mootools.js')}"></script>
		<script type="text/javascript">
			window.addEvent('domready', function(){
				$$('a.delete_plugin').each(function(element, i){
					element.addEvent('click', function(e){
						if (!confirm("If you delete this plugin, all files and configurations will be deleted and if your plugin has not been already submitted to SillySmart community, you shoudn't recover your datas"))
							e.stop();
					});
				});
			});
		</script>
	</xsl:template>
</xsl:stylesheet>