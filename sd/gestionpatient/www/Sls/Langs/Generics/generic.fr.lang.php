<?
/**
 * Generic Sls Vars
 */
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_MONDAY'] = "lundi";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_TUESDAY'] = "mardi";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_WEDNESDAY'] = "mercredi";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_THURSDAY'] = "jeudi";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_FRIDAY'] = "vendredi";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_SATURDAY'] = "samedi";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_SUNDAY'] = "dimanche";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_JANUARY'] = "janvier";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_FEBRUARY'] = "février";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_MARCH'] = "mars";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_APRIL'] = "avril";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_MAY'] = "mai";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_JUNE'] = "juin";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_JULY'] = "juillet";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_AUGUST'] = "août";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_SEPTEMBER'] = "septembre";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_OCTOBER'] = "octobre";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_NOVEMBER'] = "novembre";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DECEMBER'] = "décembre";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DATE_PATTERN_TIME'] = "h:i:s";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DATE_PATTERN_FULL_TIME'] = "d-m-y h:i:s";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DATE_PATTERN_DATE'] = "d-m-y";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DATE_PATTERN_MONTH_LITTERAL'] = "d f y";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DATE_PATTERN_FULL_LITTERAL'] = "l d f y";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DATE_PATTERN_FULL_LITTERAL_TIME'] = "l d f y h:i:s";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_DATE_PATTERN_MONTH_LITTERAL_TIME'] = "d f y h:i:s";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_CONTENT'] = "mauvais contenu";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_EMPTY'] = "ne peut être vide";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_KEY'] = "ne respecte pas les contraintes d'intégrité";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_LENGTH'] = "ne respecte pas les contraintes de longueur";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_NULL'] = "ne peut être null";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_UNIQUE'] = "doit être unique";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_TYPE'] = "n'a pas le bon type";
$GLOBALS[$GLOBALS['PROJECT_NAME']]['JS']['SLS_E_SIZE'] = "excède la taille maximale autorisée";
?>