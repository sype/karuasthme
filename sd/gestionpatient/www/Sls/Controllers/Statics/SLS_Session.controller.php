<?php
/**
 * Controller SLS_Session - Specific controller of Session
 *
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package Sls.Controllers.Statics 
 * @since 1.0 
 */
class SLS_SessionController extends SLS_FrontStatic implements SLS_IStatic 
{
	private $_session;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @since 1.0
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Construct the XML
	 *
	 * @access public
	 * @since 1.0
	 */
	public function constructXML()
	{
		$params = $this->_generic->getObjectSession()->getParams();		
		
		$this->_xmlToolBox->startTag("session");
		$this->_xmlToolBox->startTag("params");
		foreach($params as $key => $value)
		{
			$this->_xmlToolBox->startTag("param");
			$this->_xmlToolBox->addFullTag("name",$key,true);
			$this->_xmlToolBox->addFullTag("value",$value,true);
			$this->_xmlToolBox->endTag("param");
		}
		$this->_xmlToolBox->endTag("params");
		$this->_xmlToolBox->endTag("session");
	}	
}
?>