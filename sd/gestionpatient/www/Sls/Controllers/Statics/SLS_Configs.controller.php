<?php
/**
 * Controller SLS_Configs - Append configuration files to final XML
 * 
 * @author Florian Collot 
 * @copyright SillySmart
 * @package Sls.Controllers.Statics 
 * @see Sls.Configs.Site.site.xml - Configs.Site.mail.xml - Configs.Site.db.xml
 * @since 1.0
 */
class SLS_ConfigsController extends SLS_FrontStatic implements SLS_IStatic 
{
	private $_httpRequest;
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @since 1.0
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Construct the XML
	 *
	 * @access private
	 * @since 1.0
	 */
	public function constructXML()
	{
		if ($this->_generic->getSiteConfig('isInstall'))
			$this->_xmlToolBox->addFullTag("Configs", "<site><domainName isSecure=\"false\" js=\"true\"><![CDATA[".$this->_generic->getSiteConfig('domainName')."]]></domainName><projectName isSecure=\"false\" js=\"true\"><![CDATA[".$this->_generic->getSiteConfig("projectName")."]]></projectName><protocol isSecure=\"false\" js=\"true\"><![CDATA[http]]></protocol></site><mails>".trim($this->_generic->getMailXml()->getTagsByAttribute('//mails/*', 'isSecure', 'false'))."</mails><paths>".trim($this->_generic->getPathsXml()->getTagsByAttribute('//paths/*', 'isSecure', 'false'))."</paths><db>".trim($this->_generic->getDbXml()->getTagsByAttribute('//db/*', 'isSecure', 'false'))."</db>", false);
		else{
			
			$this->_xmlToolBox->addFullTag("Configs", "<site><domainName isSecure=\"false\" js=\"true\"><![CDATA[".$this->_generic->getSiteConfig('domainName')."]]></domainName>".trim($this->_generic->getSiteXml()->getTagsByAttribute('//configs/*[name()!="domainName"]', 'isSecure', 'false'))."</site><mails>".trim($this->_generic->getMailXml()->getTagsByAttribute('//mails/*', 'isSecure', 'false'))."</mails><paths>".trim($this->_generic->getPathsXml()->getTagsByAttribute('//paths/*', 'isSecure', 'false'))."</paths><db>".trim($this->_generic->getDbXml()->getTagsByAttribute('//db/*', 'isSecure', 'false'))."</db>", false);
		}
	}
}
?>