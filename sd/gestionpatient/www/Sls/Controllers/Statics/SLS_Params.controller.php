<?php
/**
 * Controller SLS_Params - Specific controller of HTTP's params
 *
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package Sls.Controllers.Statics 
 * @since 1.0 
 */
class SLS_ParamsController extends SLS_FrontStatic implements SLS_IStatic 
{
	private $_httpRequest;	
	
	/**
	 * Constructor
	 *
	 * @access public
	 * @since 1.0
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Construct the XML
	 *
	 * @access public
	 * @since 1.0
	 */
	public function constructXML()
	{
		$this->_httpRequest = new SLS_HttpRequest();
		$params = $this->_httpRequest->getParams();
		
		$this->_xmlToolBox->startTag("Params");
		foreach($params as $key => $value)
		{
			$this->_xmlToolBox->startTag("param");
			$this->_xmlToolBox->addFullTag("name",$key,true);
			$this->_xmlToolBox->addFullTag("value",str_replace(array('<', '>'), array('&lt;', '&gt;'), $value),true);
			$this->_xmlToolBox->endTag("param");
		}
		$this->_xmlToolBox->startTag("param");
		$this->_xmlToolBox->addFullTag("name","genericmode",true);
		$this->_xmlToolBox->addFullTag("value",$this->_generic->getGenericControllerName(),true);
		$this->_xmlToolBox->endTag("param");
		$this->_xmlToolBox->startTag("param");
		$this->_xmlToolBox->addFullTag("name","genericsmode",true);
		$this->_xmlToolBox->addFullTag("value",$this->_generic->getGenericScontrollerName(),true);
		$this->_xmlToolBox->endTag("param");
		$this->_xmlToolBox->startTag("param");
		$this->_xmlToolBox->addFullTag("name","request_uri",true);
		$this->_xmlToolBox->addFullTag("value",str_replace("/","|",substr($_SERVER["REQUEST_URI"],1)),true);
		$this->_xmlToolBox->endTag("param");
		$this->_xmlToolBox->endTag("Params");		
	}
}
?>