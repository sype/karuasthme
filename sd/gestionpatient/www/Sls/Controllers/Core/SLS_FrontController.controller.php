<?
/**
 * Front Controller
 * 
 * @author Florian Collot
 * @author Laurent Bientz 
 * @copyright SillySmart
 * @package Sls.Controllers.Core 
 * @since 1.0
 */
class SLS_FrontController
{
	private static $_instance;
	private $_httpRequest;
	private $_lang;
	private $_controllerXML;
	private $_generic;
	private $_langController;
	private $_controller;
	private $_langScontroller;
	private $_scontroller;
	private $_runningController;
	private $_xml;
	private $_lastSide;
	private $_includePathControllers;
	private $_defaultClassName;
	
	/**
	 * Constructor
	 *
	 * @access private	 
	 * @since 1.0
	 */
	private function __construct()
	{
		$this->_generic = SLS_Generic::getInstance();		
		$this->_httpRequest = $this->_generic->getObjectHttpRequest();
		$this->_lang = $this->_generic->getObjectLang();		
		$this->_controllerXML = $this->_generic->getControllersXML();
		$this->parseUrl();		
		$this->_lastSide = $this->_generic->getObjectSession()->getParam('lastSide');
		$this->_includePathControllers = (!empty($this->_lastSide) && $this->_lastSide == 'sls') ? "coreActionsControllers" : "actionsControllers";
		$this->_defaultClassName = (!empty($this->_lastSide) && $this->_lastSide == 'sls') ? "SLS_Default" : "Default";
		(!empty($this->_lastSide) && $this->_lastSide == 'sls') ? $this->_lang = new SLS_Lang() : "";
	}

	/**
	 * Singleton's pattern
	 *
	 * @access public static	 
	 * @return SLS_FrontController $instance the self reference
	 * @since 1.0
	 */
	public static function getInstance()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new SLS_FrontController();
		}
		return self::$_instance;
	}

	/**
	 * URL's parsing to know the wanted couple Controller/Action
	 *
	 * @access public
	 * @since 1.0
	 */
	public function parseUrl() 
	{
		$this->_langController = $this->_httpRequest->getParam('mode');
		$this->_langScontroller = $this->_httpRequest->getParam('smode');
		
		if (empty($this->_langController) && $this->_generic->getSiteConfig('isInstall') == false) 
		{	
			$this->_controller = "Home";			
			$this->_generic->setGenericControllerName("Home");
			$this->_scontroller = "Index";			
			$this->_generic->setGenericScontrollerName("Index");
			$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='Home']/scontrollers/scontroller[@name='Index']/@id")));
		}
		elseif($this->_generic->getSiteConfig('isInstall') == true)
		{
			$this->_generic->setSide('sls');
			$controller = $this->_generic->getCoreConfig('installation/controller/name', 'sls');
			$scontroller = $this->_generic->getCoreConfig('installation/scontroller/name', 'sls');
			if (empty($controller) || empty($scontroller))
			{
				if (empty($this->_langController) && empty($this->_langScontroller))
				{
					$this->_controller = "SLS_Init";			
					$this->_generic->setGenericControllerName("SLS_Init");
					$this->_scontroller = "Index";			
					$this->_generic->setGenericScontrollerName("Index");
					$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='SLS_Init' and @side='sls']/scontrollers/scontroller[@name='Index']/@id")));					
				}
				else 
				{
					if ($this->_langController == "Initialization" || $this->_langController == "SLS_Init")
					{
						$this->_controller = "SLS_Init";
						$this->_generic->setGenericControllerName("SLS_Init");
						$controllerXml = $this->_generic->getControllersXML();
						$this->_scontroller = array_shift($controllerXml->getTags("//controllers/controller[@name='SLS_Init']/scontrollers/scontroller[scontrollerLangs/scontrollerLang='".$this->_langScontroller."']/@name"));			
						$this->_generic->setGenericScontrollerName($this->_scontroller);
						$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='SLS_Init' and @side='sls']/scontrollers/scontroller[@name='".$this->_scontroller."']/@id")));
					}
				}
			}
			else 
			{				
				$this->_controller = $controller;
				$this->_generic->setGenericControllerName($controller);
				$this->_scontroller = $scontroller;
				$this->_generic->setGenericScontrollerName($scontroller);
				$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$controller."']/scontrollers/scontroller[@name='".$scontroller."']/@id")));
			}			
		}
	}
	
	/**
	 * Dispatch the controller and the action within application's reloading	 
	 *
	 * @access public
	 * @param string $controller the generic name of the wanted controller
	 * @param string $action the generic name of the wanted action	 
	 * @param array $http set new params for $_GET and $_POST 
	 * <code>
	 * array(
	 * 		"POST" 	=> array("key" => "value"), 
	 * 		"GET" 	=> array("key" => "value")
	 * )
	 * </code>
	 * @since 1.0
	 */
	public function dispatch($controller,$action,$http=array("post"=>array(),"get"=>array()))
	{	
		// Set controller & action
		$generics = $this->_generic->getTranslatedController($controller, $action);
		$this->_generic->setGenericControllerName($controller);
		$this->_generic->setGenericScontrollerName($action);
		$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$controller."']/scontrollers/scontroller[@name='".$action."']/@id")));
		$this->_langController = $generics['controller'];
		$this->_langScontroller = $generics['scontroller'];
		$this->_controller = $controller;		
		$this->_scontroller = $action;	

		if (array_key_exists("POST", $http) && array_key_exists("GET", $http))
		{
			if (!empty($http['POST']))
				$_POST = $http['POST'];
			if (!empty($http['GET']))
				$_GET = $http['GET'];
		}
		$this->_generic->setBufferXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>",false);
		
		// We return into the mapping of the application		
		$this->loadController(); 
	}

	/**
	 * Recover rewrite params to find the controller' class and to call the action's function
	 *
	 * @access public
	 * @since 1.0
	 */
	public function loadController()
	{		
		// Recover Controller & Action
		$this->getControllerScontroller();
		
		// Check if current asked language is disabled		
		if ($this->_generic->getSiteConfig('isInstall') == 0 && $this->_generic->getSide() == "user")
		{			
			if (!$this->_lang->isEnabledLang($this->_lang->getLang()))		
				$this->_generic->switchLang($this->_generic->getSiteConfig("defaultLang"),$this->_generic->getGenericControllerName(),$this->_generic->getGenericScontrollerName());			
		}				
		// Load file language of the current action
		$this->_lang->loadActionLang($this->_generic->getGenericControllerName(),$this->_generic->getGenericScontrollerName());
		
		// Load statics controllers
		include_once($this->_generic->getPathConfig("coreControllers")."SLS_FrontStatic.controller.php");
		include_once($this->_generic->getPathConfig("coreControllers")."SLS_LoadStatics.controller.php");
		$staticsClasses = new SLS_LoadStaticsControllers();		
		$this->_generic->setBufferXML($staticsClasses->getXML(), true, "//root");		
		
		$path = ($this->_generic->getSide() == 'user') ? 'actionsControllers' : 'coreActionsControllers';
		
		// If the directory exists
		if (is_dir($this->_generic->getPathConfig($path).$this->_controller))
		{
			$this->loadScontroller($path);
			// Set Session Param to manage Errors
			if (!empty($this->_scontroller) && !empty($this->_controller) && $this->_controller != $this->_defaultClassName)
				$this->_generic->getObjectMemberSession()->setTrackingPage();	
		}
		// Else, include the Default Controller and call the actionError
		else
		{
			if (get_class($this->_runningController) != $this->_defaultClassName.'UrlError')
			{	
				include_once($this->_generic->getPathConfig($this->_includePathControllers).$this->_defaultClassName."/UrlError.controller.php");
				eval('$this->_runningController = new '.$this->_defaultClassName.'UrlError();');
			}			
			$this->_generic->setGenericControllerName($this->_defaultClassName);
			$this->_generic->setGenericScontrollerName("UrlError");
			$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_defaultClassName."']/scontrollers/scontroller[@name='UrlError']/@id")));
			$this->_runningController->action();
		}		
		
		// Get the XML from the current controller
		$this->_xml = $this->_runningController->getFinalXML();
	}

	/**
	 * Call the action's function
	 * 
	 * @access public
	 * @param string $path the controllers path
	 * @since 1.0
	 */
	public function loadScontroller($path)
	{		
		// If the file exist
		if (is_file($this->_generic->getPathConfig($path).$this->_generic->getGenericControllerName()."/".$this->_scontroller.".controller.php"))
		{			
			$controllerName = $this->_generic->getGenericControllerName().$this->_scontroller;
			if (is_file($this->_generic->getPathConfig($path)."__site.protected.php"))
				include_once($this->_generic->getPathConfig($path)."__site.protected.php");
			else 
				SLS_Tracing::addTrace(new Exception("A generic File is missing '__site.protected.php' for the current Action. Controller: '".$this->_generic->getGenericControllerName()."' ; Action : '".$this->_scontroller."'"));			
			
			if (is_file($this->_generic->getPathConfig($path).$this->_generic->getGenericControllerName()."/"."__".$this->_generic->getGenericControllerName().".protected.php"))
				include_once($this->_generic->getPathConfig($path).$this->_generic->getGenericControllerName()."/"."__".$this->_generic->getGenericControllerName().".protected.php");
			else
				SLS_Tracing::addTrace(new Exception("A generic File is missing '__".$this->_generic->getGenericControllerName().".protected.php' for the current Action. Controller: '".$this->_generic->getGenericControllerName()."' ; Action : '".$this->_scontroller."'"));
			
			include_once($this->_generic->getPathConfig($path).$this->_generic->getGenericControllerName()."/".$this->_scontroller.".controller.php");
			$this->_runningController = new $controllerName();
			$this->_runningController->action();
		}
		// Else, call the actionError of this specific controller
		else 
		{			
			if (is_file($this->_generic->getPathConfig($path)."__site.protected.php"))
				include_once($this->_generic->getPathConfig($path)."__site.protected.php");
			else 
				SLS_Tracing::addTrace(new Exception("A generic File is missing '__site.protected.php' for the current Action. Controller: '".$this->_defaultClassName."' ; Action : 'Error'"));
			
			if (is_file($this->_generic->getPathConfig($path).$this->_defaultClassName."/"."__".$this->_defaultClassName.".protected.php"))
				include_once($this->_generic->getPathConfig($path).$this->_defaultClassName."/"."__".$this->_defaultClassName.".protected.php");
			else
			{
				if ($this->_generic->getSide() != $this->_lastSide)
					$this->_generic->redirect($this->_langController."/".$this->_langScontroller);
				else
					SLS_Tracing::addTrace(new Exception("A generic File is missing '__".$this->_defaultClassName.".protected.php' for the current Action. Controller: '".$this->_defaultClassName."' ; Action : 'Error'"));
			}
				
			include_once($this->_generic->getPathConfig($path).$this->_defaultClassName."/Error.controller.php");
			$this->_generic->setGenericControllerName($this->_defaultClassName);
			$this->_generic->setGenericScontrollerName("Error");		
			$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_defaultClassName."']/scontrollers/scontroller[@name='Error']/@id")));	
			eval('$this->_runningController = new '.$this->_defaultClassName.'Error();');			
			$this->_runningController->action($this->_controller);
		}
	}

	/**
	 * Recover generic name of the controller class and it's action function according to what has been spent in any language
	 * 
	 * @access public
	 * @since 1.0
	 */
	public function getControllerScontroller() 
	{
		// If we haven't again found a generic controller and scontroller
		if ($this->_generic->getGenericControllerName() == "" && $this->_generic->getGenericScontrollerName() == "")
		{
			$actualLang = $this->_lang->getLang();
			$arrayLangs = $this->_lang->getSiteLangs();
			$tmpController = "";
			
			// Try to recover controller & action in the current language of the client
			$this->_controller = array_shift($this->_controllerXML->getTags("//controllers/controller[controllerLangs[controllerLang[@lang='".$actualLang."']='".$this->_langController."']]/@name"));
			$this->_scontroller = array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/scontrollers/scontroller[scontrollerLangs/scontrollerLang[@lang='".$actualLang."']='".$this->_langScontroller."']/@name"));
			
			// If it was not found in this language, seek in others
			if (empty($this->_controller) || empty($this->_scontroller))
			{				
				if (!in_array('en', $arrayLangs))
					array_push($arrayLangs, 'en');			
				
				foreach ($arrayLangs as $lang)
				{
					if ($actualLang == 'en' || $lang != $actualLang)
					{					
						$this->_controller = array_shift($this->_controllerXML->getTags("//controllers/controller[controllerLangs[controllerLang='".$this->_langController."']/controllerLang[@lang='".$lang."']]/@name"));
						
						if (!empty($this->_controller))
						{							
							$tmpController = $this->_controller; // Keep a copy of the controller found
							$this->_scontroller = array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/scontrollers/scontroller[scontrollerLangs/scontrollerLang[@lang='".$lang."']='".$this->_langScontroller."']/@name"));
							if (!empty($this->_scontroller))
							{
								$this->_generic->setGenericControllerName($this->_controller);
								$this->_generic->setGenericScontrollerName($this->_scontroller);	
								$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/scontrollers/scontroller[@name='".$this->_scontroller."']/@id")));								
								$this->_lang->setLang($lang);								
								break;
							}
						}
					}
				}				
			}
			
			// If controller is empty, try to re set-it with the copy
			if (empty($this->_controller))
				$this->_controller = $tmpController;		
			
			// If no language match these names, seek into generic names
			if (empty($this->_controller))
			{
				$this->_controller = array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_langController."']/@name"));
				// If controller has been found whereas we are into sls side, destroy the controller
				if (!empty($this->_controller) && array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/@side")) == 'sls')
					$this->_controller = "";
			}
			
			// If controller has been found
			if (!empty($this->_controller))
			{			
				// Seek the action
				if (empty($this->_scontroller) && array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/@side")) == 'user')
					$this->_scontroller = array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/scontrollers/scontroller[@name='".$this->_langScontroller."']/@name"));			
				// If the action hasn't been found, set it to "actionError"
				if (empty($this->_scontroller) && empty($this->_controller))			
					$this->_scontroller = "Error";					
			}
			
			// Set the generic names
			$this->_generic->setGenericControllerName($this->_controller);
			$this->_generic->setGenericScontrollerName($this->_scontroller);
			$this->_generic->setActionId(array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/scontrollers/scontroller[@name='".$this->_scontroller."']/@id")));
			
			// Set the template for this action
			$scontrollerTpl = array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/scontrollers/scontroller[@name='".$this->_scontroller."']/@tpl"));
			$controllerTpl 	= array_shift($this->_controllerXML->getTags("//controllers/controller[@name='".$this->_controller."']/@tpl"));
			if (!empty($scontrollerTpl))
				$this->_generic->setCurrentTpl($scontrollerTpl);
			else if (!empty($controllerTpl))
				$this->_generic->setCurrentTpl($controllerTpl);
		}
	}
	
	/**
	 * Get the XML of the current controller
	 *
	 * @access public
	 * @return string $xml the xml of the current controller
	 * @since 1.0
	 */
	public function getXML()
	{		
		return $this->_xml;
	}
}
?>