<?
/**
 * Controller SLS_LoadStatics - Include statics files
 *
 * @author Florian Collot 
 * @copyright SillySmart
 * @package SLSControllers.Core 
 * @since 1.0
 */
class SLS_LoadStaticsControllers
{
	private $_generic;
	private $_xmlToolBox;
	private $_tabControllers = array();
	private $_isCache;
	private $_cacheXML;
	
	/**
	 * Constructor
	 *
	 * @access public	 
	 * @since 1.0
	 */
	public function __construct() 
	{
		$this->_generic = SLS_Generic::getInstance();
		$this->_xmlToolBox = new SLS_XMLToolbox();
		$this->_xmlToolBox->startTag("Statics");
		$this->_xmlToolBox->addFullTag("Sls", "", false);
		if ($this->_generic->getSide() == "user")
			$this->_xmlToolBox->addFullTag("Site", "", false);
		$this->_xmlToolBox->endTag("Statics");
		
		$this->_isCache = $this->_generic->isCache();
		$this->_cacheXML = new SLS_XMLToolbox(false);
		if (!$this->_isCache)
			$this->_cacheXML->startTag("controllers");
		
		// Load Core Statics
		$this->recursiveStaticLoading($this->_generic->getPathConfig("coreStaticsControllers"), 'Sls');		
		if ($this->_generic->getSide() == "user")
			$this->recursiveStaticLoading($this->_generic->getPathConfig("staticsControllers"), 'Site');
		if (!$this->_isCache)
		{
			$this->_cacheXML->endTag("controllers");
			$this->_generic->saveCacheXML($this->_cacheXML->getXML('noHeader'));
		}
			
		return $this->getXML();
	}
	
	/**
	 * Read recursively path & sub-paths to include statics files
	 *
	 * @access private
	 * @param string $path the root path
	 * @param string $type the type ('core' || 'user')
	 * @since 1.0
	 */
	private function recursiveStaticLoading($path, $type)
	{
		if ($this->_isCache)
		{			
			$staticsHandle = file_get_contents($this->_generic->getPathConfig("configSecure")."cache.xml");
			$xmlCache = new SLS_XMLToolbox($staticsHandle);	
			$files = $xmlCache->getTags("//statics/controllers/".$type."/controller/file");
			$names = $xmlCache->getTags("//statics/controllers/".$type."/controller/name");
			for($i=0;$i<count($files);$i++)
			{
				
				include_once($this->_generic->getRoot().$files[$i]);
				$name = $names[$i];
				$classObj = new ${name}();				
				$this->_xmlToolBox->appendXMLNode("//Statics/".$type, $classObj->getXML());
			}
		}
		else 
		{			
			$searchedExt = array('php');
			$arrayResult = array();
			$arrayResult = $this->_generic->recursiveReadDir($path, $arrayResult, $searchedExt);
			
			$this->_cacheXML->startTag($type);
			for($i=0;$i<$count = count($arrayResult);$i++)
			{
				$this->_cacheXML->startTag("controller");
				include_once($arrayResult[$i]);
				$staticName = array_shift(explode(".", SLS_String::substrAfterLastDelimiter($arrayResult[$i], "/")))."Controller";				
				if (class_exists($staticName))
				{					
					$this->_cacheXML->addFullTag("file", $arrayResult[$i], true);
					$this->_cacheXML->addFullTag("name", $staticName, true);
					$classObj = new ${staticName}();					
					$this->_xmlToolBox->appendXMLNode("//Statics/".$type, $classObj->getXML());
				}
				$this->_cacheXML->endTag("controller");
			}
			$this->_cacheXML->endTag($type);			
		}
	}
	
	/**
	 * Get the XML of the controller
	 *
	 * @access public
	 * @return string the XML of the controller
	 * @since 1.0
	 */
	public function getXML()
	{
		return $this->_xmlToolBox->getXML('noHeader');
	}
}	
?>