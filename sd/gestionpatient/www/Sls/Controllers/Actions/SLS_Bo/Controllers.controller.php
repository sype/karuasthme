<?php
class SLS_BoControllers extends SLS_BoControllerProtected 
{	
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$langs = $this->_generic->getObjectLang()->getSiteLangs();
		$listing = true;
		$errors = array();
		
		// Get user controllers listing
		$controllersXML = $this->_generic->getControllersXML();
		$controllers = $controllersXML->getTags("//controllers/controller[@side='user']/@name");
		$xml->startTag("controllers");
		foreach($controllers as $controller)
		{
			$xml->startTag("controller");
			$xml->addFullTag("name",$controller,"true");
			$xml->addFullTag("canBeDeleted",($controller == 'Home' || $controller == 'Default') ? 'false' : 'true', true);
			$scontrollers = $controllersXML->getTags("//controllers/controller[@name='".$controller."']/scontrollers/scontroller/@name");
			$xml->startTag("scontrollers");
			foreach($scontrollers as $scontroller)
			{
				$xml->startTag("scontroller");
					$xml->addFullTag("name",$scontroller,true);
					if (($controller == 'Home' && ($scontroller == 'Index')) || ($controller == 'Default' && ($scontroller == 'UrlError' || $scontroller == 'BadRequestError' || $scontroller == 'AuthorizationError' || $scontroller == 'ForbiddenError' || $scontroller == 'InternalServerError')))
						$xml->addFullTag("canBeDeleted",'false',true);
					else 
						$xml->addFullTag("canBeDeleted",'true',true);
				$xml->endTag("scontroller");
			}
			$xml->endTag("scontrollers");	
			$xml->endTag("controller");
		}
		$xml->endTag("controllers");
		$xml->startTag("statics");
		$statics = $this->_generic->recursiveReadDir($this->_generic->getPathConfig("staticsControllers"), array(), array(0=>"php"));
		foreach ($statics as $static)
		{
			$xml->startTag("static");
				$xml->addFullTag("name", SLS_String::substrBeforeFirstDelimiter(SLS_String::substrAfterLastDelimiter($static, "/"), ".controller.php"),true);
			$xml->endTag("static");
		}
		$xml->endTag("statics");
		$xml->addFullTag('request', 'listing', true);
		$this->registerLink('ADDACTION', 'SLS_Bo', 'AddAction', false);
		$this->registerLink('ADDSTATICCONTROLLER', 'SLS_Bo', 'AddStaticController', false);
		$this->registerLink('EDITSTATICCONTROLLER', 'SLS_Bo', 'EditStaticController', false);
		$this->registerLink('DELSTATICCONTROLLER', 'SLS_Bo', 'DeleteStaticController', false);
		$this->registerLink('EDITACTION', 'SLS_Bo', 'EditAction', false);
		$this->registerLink('DELACTION', 'SLS_Bo', 'DeleteAction', false);
		$this->registerLink('ADDCONTROLLER', 'SLS_Bo', 'AddController', false);
		$this->registerLink('EDITCONTROLLER', 'SLS_Bo', 'EditController', false);
		$this->registerLink('DELCONTROLLER', 'SLS_Bo', 'DeleteController', false);
		$this->saveXML($xml);
	}
	
}
?>