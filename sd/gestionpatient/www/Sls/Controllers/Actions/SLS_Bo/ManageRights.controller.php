<?php
class SLS_BoManageRights extends SLS_BoControllerProtected 
{
	public function action() 
	{		
		// Objects
		$xml = $this->getXML();
		$user = $this->hasAuthorative();
		$xml = $this->makeMenu($xml);
		$this->_generic->registerLink('Add', 'SLS_Bo', 'AddRight');
		$xml->addFullTag("delete",$this->_generic->getFullPath("SLS_Bo","DeleteRight",array(),false));
		$xml->addFullTag("edit",$this->_generic->getFullPath("SLS_Bo","EditRight",array(),false));
		
		$pathsHandle = file_get_contents($this->_generic->getPathConfig("configSls")."/rights.xml");
		$xmlRights = new SLS_XMLToolbox($pathsHandle);
		$result = $xmlRights->getTagsAttributes("//sls_configs/entry",array("login","password","right"));
				
		$xml->startTag("accounts");
		for($i=0 ; $i<$count=count($result) ; $i++)
		{
			$xml->startTag("account");
			for($j=0 ; $j<$count2=count($result[$i]["attributes"]) ; $j++)
			{				
				if ($result[$i]["attributes"][$j]["key"] == "password")
					$xml->addFullTag($result[$i]["attributes"][$j]["key"],"******",true);
				else
					$xml->addFullTag($result[$i]["attributes"][$j]["key"],$result[$i]["attributes"][$j]["value"],true);
			}
			$xml->endTag("account");
		}
		$xml->endTag("accounts");
		
		$this->saveXML($xml);
	}
	
}
?>