<?php
class SLS_BoEditRight extends SLS_BoControllerProtected 
{
	
	public function action() 
	{		
		// Objects
		$xml = $this->getXML();
		$user = $this->hasAuthorative();
		$xml = $this->makeMenu($xml);
			
		$name = SLS_String::trimSlashesFromString($this->_http->getParam("name"));				
		$pathsHandle = file_get_contents($this->_generic->getPathConfig("configSls")."/rights.xml");
		$xmlRights = new SLS_XMLToolbox($pathsHandle);			
		$result = $xmlRights->getTags("//sls_configs/entry[@login='".($name)."']");
		
		if (empty($result))
		{
			$xml->startTag("errors");
			$xml->addFullTag("error","This account doesn't exists",true);
			$xml->endTag("errors");
		}
		else
		{
			$login 	= $name;
			$pwd 	= "";
			$right 	= array_shift($xmlRights->getTagsAttribute("//sls_configs/entry[@login='".($name)."']","right"));
			$right = $right["attribute"];
			$add 	= "";
			$modify = "";
			$delete	= "";
			for($i=0 ; $i<$count=strlen($right) ; $i++)
			{
				if ($right{$i} == "A")
					$add = "true";
				if ($right{$i} == "M")
					$modify = "true";
				if ($right{$i} == "D")
					$delete = "true";
			}
						
			if ($this->_http->getParam("reload") == "true")
			{
				$errors = array();
				
				$login 	= SLS_String::trimSlashesFromString($this->_http->getParam("login"));
				$pwd 	= SLS_String::trimSlashesFromString($this->_http->getParam("password"));
				$add 	= SLS_String::trimSlashesFromString($this->_http->getParam("add"));
				$modify = SLS_String::trimSlashesFromString($this->_http->getParam("modify"));
				$delete = SLS_String::trimSlashesFromString($this->_http->getParam("delete"));
								
				if ($login != $name)
				{
					$result = $xmlRights->getTags("//sls_configs/entry[@login='".($login)."']");			
					if (!empty($result))
					{		
						array_push($errors,"This account already exists",true);
					}
				}
				if (empty($login) || empty($pwd))
				{					
					array_push($errors,"You must fill the login and the password");					
				}
				
				if (empty($errors))
				{
					$right = "R";
					if (!empty($add))
						$right .= "A";
					if (!empty($modify))
						$right .= "M";
					if (!empty($delete))
						$right .= "D";
					
					$xmlRights->deleteTags('//sls_configs/entry[@login="'.($login).'"]');
					$xmlRights->saveXML($this->_generic->getPathConfig("configSls")."/rights.xml",$xmlRights->getXML());
					$pathsHandle = file_get_contents($this->_generic->getPathConfig("configSls")."/rights.xml");
					$xmlRights = new SLS_XMLToolbox($pathsHandle);
					$xmlNew = '<entry login="'.($login).'" password="'.sha1($pwd).'" right="'.$right.'" />';
					$xmlRights->appendXMLNode("//sls_configs",$xmlNew);
					$xmlRights->saveXML($this->_generic->getPathConfig("configSls")."/rights.xml",$xmlRights->getXML());
					$this->_generic->redirect("Manage/Rights");
				}
				else
				{
					$xml->startTag("errors");
					foreach($errors as $error)
						$xml->addFullTag("error",$error,true);
					$xml->endTag("errors");
				}
			}
						
			$xml->addFullTag("login",$login,true);
			$xml->addFullTag("right_add",(!empty($add)) ? "true" : "false",true);
			$xml->addFullTag("right_modify",(!empty($modify)) ? "true" : "false",true);
			$xml->addFullTag("right_delete",(!empty($delete)) ? "true" : "false",true);
			$xml->addFullTag("privilege",($right=="GOD") ? "god" : "admin",true);
		}
		$this->saveXML($xml);
	}
	
}
?>