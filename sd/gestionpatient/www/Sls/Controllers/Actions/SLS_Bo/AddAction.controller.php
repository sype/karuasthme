<?php
class SLS_BoAddAction extends SLS_BoControllerProtected 
{	
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$langs = $this->_generic->getObjectLang()->getSiteLangs();
		$listing = true;
		$errors = array();
		$protocol = $this->_generic->getProtocol();
		
		$controllersXML = $this->_generic->getControllersXML();
		
		$controller = SLS_String::trimSlashesFromString($this->_http->getParam("Controller"));
		$controllers = $controllersXML->getTags("//controllers/controller[@side='user' and @name='".$controller."']");
		if (count($controllers) == 1)
		{
			$controllerID = array_shift($controllersXML->getTags("//controllers/controller[@side='user' and @name='".$controller."']/@id"));
			$protocol = $this->_generic->getControllerProtocol($controllerID);
			$listing = false;
			$xml->addFullTag('request', 'AddAction', true);
			if ($this->_http->getParam('reload') == 'true')
			{
				// Get the form informations
				$newAction 				= SLS_String::stringToUrl(ucwords(SLS_String::trimSlashesFromString($this->_http->getParam("actionName"))), "", false);
				$needDynamic 			= SLS_String::trimSlashesFromString($this->_http->getParam("dynamic"));
				$searchEngine			= SLS_String::trimSlashesFromString($this->_http->getParam("indexes"));
				$postProtocol			= SLS_String::trimSlashesFromString($this->_http->getParam("protocol"));
				$tpl 					= SLS_String::trimSlashesFromString($this->_http->getParam('template'));
				if (empty($postProtocol))
					$postProtocol = "http";
				$postActionsLang		= array();
				$postTitlesLang			= array();
				$postDescriptionsLang	= array();
				$postKeywordsLang		= array();
				
				$actionExist 		= $controllersXML->getTags("//controllers/controller[@side='user' and @name='".$controller."']/scontrollers/scontroller[@name='".$newAction."']");
				if (count($actionExist) == 0)
				{
					if (empty($postProtocol) || ($postProtocol != 'http' && $postProtocol != 'https'))
						array_push($errors, "Protocol must be http or https");
					else 
						$protocol = $postProtocol;
					
					$siteLangs = $this->_generic->getObjectLang()->getSiteLangs();
					
					foreach ($siteLangs as $lang)
					{
						// Check Url
						$postLang = trim(SLS_String::stringToUrl(ucwords(SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-action"))), "", false));
						$translationExist = $controllersXML->getTags("//controllers/controller[@side='user' and @name='".$controller."']/scontrollers/scontroller/scontrollerLangs[scontrollerLang = '".$postLang."']");
						if (empty($postLang)) 
							array_push($errors, "You need to fill the ".$lang." url translations");
						elseif(count($translationExist) != 0)
							array_push($errors, "You URL translation in ".$lang." is already in use on another action in the same controller");
						else
							$postActionsLang[$lang] =  $postLang;
						
						// Get Titles	
						$postTitlesLang[$lang] = SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-title"));
						// Get Description
						$postDescriptionsLang[$lang] = SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-description"));
						// Get Keywords
						$postKeywordsLang[$lang] = SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-keywords"));
					}
					
					if (count($errors) == 0)
					{
						// If an error existing and the controller directory wasn't created
						if (!is_dir($this->_generic->getPathConfig("actionsControllers").$controller))
							mkdir($this->_generic->getPathConfig("actionsControllers").$controller);
						if (!is_file($this->_generic->getPathConfig("actionsControllers").$controller."/__".$controller.".protected.php"))
						{
							$strControllerProtected = '<?php'."\n".
							   '/**'."\n".
							   '* Class generic for the controller '.$newControllerName.''."\n".
							   '* Write here all your generic functions you need in your '.$newControllerName.' Actions'."\n".
							   '* @author SillySmart'."\n".
							   '* @copyright SillySmart'."\n".
							   '* @package Mvc.Controllers.'.$newControllerName.''."\n".
							   '* @see Mvc.Controllers.SiteProtected'."\n".
							   '* @see Sls.Controllers.Core.SLS_GenericController'."\n".
							   '* @since 1.0'."\n".
							   '*/'."\n".
							   'class ControllerProtected extends SiteProtected'."\n".
							   '{'."\n".
							   ''."\n".
							   '}'."\n".
							   '?>';   
						
							file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/__".$controller.".protected.php", $strControllerProtected);
						}
						
						// Create Controller File
						$strControllerAction = '<?php'."\n".
												'/**'."\n".
												'* Class '.$newAction.' into '.$controller.' Controller'."\n".
												'* @author SillySmart'."\n".
												'* @copyright SillySmart'."\n".
												'* @package Mvc.Controllers.'.$controller."\n".
												'* @see Mvc.Controllers.'.$controller.'.ControllerProtected'."\n".
												'* @see Mvc.Controllers.SiteProtected'."\n".
												'* @see Sls.Controllers.Core.SLS_GenericController'."\n".
												'* @since 1.0'."\n".
												'*'."\n".
												'*/'."\n".
												'class '.$controller.$newAction.' extends '.$controller.'ControllerProtected'."\n".
												'{'."\n".
												"\t".'public function action()'."\n".
												"\t".'{'."\n".
												"\t\t\n".
												"\t}\n".
												"}\n".
												'?>';
						file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/".$newAction.".controller.php", $strControllerAction);
						
						// Create Lang Files
						if (!is_dir($this->_generic->getPathConfig("langs")."Actions/".$controller))
							mkdir($this->_generic->getPathConfig("langs")."Actions/".$controller);
						$langsFiles = array();
						
						foreach ($siteLangs as $lang)
						{
							$strLang = '<?php'."\n".
												'/**'."\n".
												'* '.strtoupper($lang).' File for the action '.$newAction.' into '.$controller.' Controller'."\n".
												'* You can create all your sentences variables here. To create it, follow the exemple :'."\n".
												'* '."\t".'Access it with JS and XSL variable : $GLOBALS[$GLOBALS[\'PROJECT_NAME\']][\'JS\'][\'KEY_OF_YOUR_VARIABLE\'] = "value of your sentence in '.strtoupper($lang).'";'."\n".
												'* '."\t".'Access it with XSL variable only   : $GLOBALS[$GLOBALS[\'PROJECT_NAME\']][\'XSL\'][\'KEY_OF_YOUR_VARIABLE\'] = "value of your sentence in '.strtoupper($lang).'";'."\n".
												'*'."\n".
												'* '."\t".'You can customise the value \'KEY_OF_YOUR_VARIABLE\' and "value of your sentence in '.strtoupper($lang).'" '."\n".
												'* @author SillySmart'."\n".
												'* @copyright SillySmart'."\n".
												'* @package Langs.Actions.'.$controller."\n".
												'* @since 1.0'."\n".
												'*'."\n".
												'*/'."\n".
												'?>';
							file_put_contents($this->_generic->getPathConfig("langs")."Actions/".$controller."/".$newAction.".".strtolower($lang).".lang.php", $strLang);
						}
						
						// Create Views File
						if (!is_dir($this->_generic->getPathConfig("viewsBody").$controller))
							mkdir($this->_generic->getPathConfig("viewsBody").$controller);
						if (!is_dir($this->_generic->getPathConfig("viewsHeaders").$controller))
							mkdir($this->_generic->getPathConfig("viewsHeaders").$controller);
						
						$strBody = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
									"\t".'<xsl:template name="'.$newAction.'">'."\n".
									"\t\t".'<h1>Customize the body of this page in <i>'.$this->_generic->getPathConfig('viewsBody').$controller.'/'.$newAction.'.xsl</i></h1>'."\n".
									"\t\t".'<h2>And your headers in <i>'.$this->_generic->getPathConfig('viewsHeaders').$controller.'/'.$newAction.'.xsl</i></h2>'."\n".
									"\t".'</xsl:template>'."\n".
									'</xsl:stylesheet>';
						$strHeader = 	'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
										"\t".'<xsl:template name="Header'.$newAction.'">'."\n".
										"\t".'</xsl:template>'."\n".
										'</xsl:stylesheet>';
						file_put_contents($this->_generic->getPathConfig("viewsBody").$controller.'/'.$newAction.'.xsl', $strBody);
						file_put_contents($this->_generic->getPathConfig("viewsHeaders").$controller.'/'.$newAction.'.xsl', $strHeader);
						// End of files creation
						
						// XML Modifications
						$metasXML = $this->_generic->getCoreXML('metas');
						// Get the titles
						$actionId = $this->_generic->generateActionId();
						$xmlAction = "<scontroller name=\"".$newAction."\" needParam=\"";
						$xmlAction .= ($this->_http->getParam("dynamic") == "on") ? "1" : "0";
						$xmlAction .= "\" id=\"".$actionId."\" protocol=\"".$protocol."\"";
						if ($tpl != -1)
							$xmlAction .= " tpl=\"".$tpl."\"";
						$xmlAction .= "><scontrollerLangs>";
						$strMetas = "<action id=\"".$actionId."\" />";
						$metasXML->appendXMLNode("//sls_configs", $strMetas);
						foreach ($siteLangs as $lang)
						{
							$xmlAction .= "<scontrollerLang lang=\"".$lang."\"><![CDATA[".$postActionsLang[$lang]."]]></scontrollerLang>";
							// Metas
							$metas = "<title lang=\"".$lang."\"><![CDATA[".$postTitlesLang[$lang]."]]></title>";
							$metas .= "<description lang=\"".$lang."\"><![CDATA[".$postDescriptionsLang[$lang]."]]></description>";
							$metas .= "<keywords lang=\"".$lang."\"><![CDATA[".$postKeywordsLang[$lang]."]]></keywords>";
							$metasXML->appendXMLNode("//sls_configs/action[@id=\"".$actionId."\"]", $metas);
							
						}
						$xmlAction .= "</scontrollerLangs></scontroller>";
						if ($searchEngine != "index, follow" && $searchEngine != "noindex, follow" && $searchEngine != "noindex, nofollow" && $searchEngine != "index, nofollow")
							$searchEngine = "index, follow";
						$metasXML->appendXMLNode("//sls_configs/action[@id='".$actionId."']", "<robots><![CDATA[".$searchEngine."]]></robots>");
						$controllersXML->appendXMLNode("//controllers/controller[@side='user' and @name='".$controller."']/scontrollers", $xmlAction);
						file_put_contents($this->_generic->getPathConfig("configSecure")."controllers.xml", $controllersXML->getXML());
						file_put_contents($this->_generic->getPathConfig("configSls")."metas.xml", $metasXML->getXML());
						$controllers = $this->_generic->getTranslatedController("SLS_Bo", "Controllers");
						$this->_generic->redirect($controllers['controller']."/".$controllers['scontroller'].".sls");
					}
				
				}
				else 
					array_push($errors, "This generic name is already in use for this controller");
								
				if (!empty($errors))
				{
					$xml->startTag("errors");
						foreach ($errors as $error)
							$xml->addFullTag("error", $error, true);
					$xml->endTag("errors");
					$xml->startTag('form');
					$xml->addFullTag("controllerName", $postControllerName);
						foreach ($postActionsLang as $key=>$value)
							$xml->addFullTag($key."-controller", $value, true);
							
					$xml->endTag('form');
				}
			}
			
			// Build all tpls
			$tpls = $this->getAppTpls();
			$xml->startTag("tpls");
			foreach($tpls as $template)
				$xml->addFullTag("tpl",$template,true);
			$xml->endTag("tpls");
			
			$xml->startTag('controller');
			$xml->addFullTag("name", $controller, true);
				$xml->startTag('translations');
				foreach ($langs as $lang)
				{
					$xml->startTag('translation');
						$xml->addFullTag("lang", $lang, true);	
					$xml->endTag('translation');
				}
				$xml->endTag('translations');
			$xml->endTag('controller');
			$xml->addFullTag('request', 'addAction', true);
		}
		else {
			$this->_generic->dispatch('SLS_Bo', 'Controllers');
		}
		$xml->addFullTag('protocol', $protocol, true);
		$xml->addFullTag('template', $tpl, true);
		$this->saveXML($xml);
	}
	
}
?>