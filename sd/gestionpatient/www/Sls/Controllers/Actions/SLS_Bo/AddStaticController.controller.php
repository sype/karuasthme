<?php
class SLS_BoAddStaticController extends SLS_BoControllerProtected 
{	
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$errors = array();
		
		if ($this->_http->getParam('reload') == 'true')
		{
			$controller = SLS_String::trimSlashesFromString($this->_http->getParam('controllerName'));
			if (empty($controller))
				array_push($errors, "You must fill the static controller name");
			
			$controller = str_replace(" ", "", ucwords(trim(SLS_String::getAlphaString($controller))));
			
			$statics = $this->_generic->recursiveReadDir($this->_generic->getPathConfig("staticsControllers"), array(), array(0=>"php"));
			foreach ($statics as $static)
			{
				if (SLS_String::substrBeforeLastDelimiter(SLS_String::substrAfterLastDelimiter($static, "/"), ".controller.php") == $controller)
				{
					array_push($errors, "The name '".$controller."' is already in use for a static controller");
					break;
				}
			}
			
			if (empty($errors))
			{
				$strNewStatic = '<?php'."\n".
						   '/**'."\n".
						   '* Controller Static '.$controller.'Controller'."\n".
						   '*'."\n".
						   '* @author SillySmart'."\n".
						   '* @copyright SillySmart'."\n".
						   '* @package Mvc.Controllers.Statics.'.$controller.'Controller'."\n".
						   '* @see Sls.Controllers.Core.SLS_FrontStatic'."\n".
						   '* @since 1.0'."\n".
						   '*/'."\n".
						   'class '.$controller.'Controller extends SLS_FrontStatic implements SLS_IStatic '."\n".
						   '{'."\n".
						   ''."\n".
						   ''."\t".'public function __construct()'."\n".
						   ''."\t".'{'."\n".
						   ''."\t\t".'parent::__construct(true);'."\n".
						   ''."\t".'}'."\n".
						   ''."\n".
						   ''."\t".'public function constructXML()'."\n".
						   ''."\t".'{'."\n".
						   ''."\t\t".'// Write here all your instructions to make your Static configuraion with xml by $this->_xmlToolBox'."\n".
						   ''."\t".'}'."\n".
						   ''."\n".
						   '}'."\n".
						   '?>'; 
				file_put_contents($this->_generic->getPathConfig("staticsControllers").$controller.".controller.php", $strNewStatic);		  
				$this->_generic->dispatch('SLS_Bo', 'Controllers'); 
		
			}
			
			if (!empty($errors))
			{
				$xml->startTag("errors");
					foreach ($errors as $error)
						$xml->addFullTag("error", $error, true);
				$xml->endTag("errors");
				$xml->startTag('form');
				$xml->addFullTag("controllerName", $controller);
											
				$xml->endTag('form');
			}
		}
		
		$this->saveXML($xml);
	}
}
?>