<?php
class SLS_BoGlobalSettings extends SLS_BoControllerProtected 
{
	
	public function action()
	{
		$user 	= $this->hasAuthorative();
		$xml 	= $this->getXML();
		$xml	= $this->makeMenu($xml);
		$siteXML = $this->_generic->getSiteXML();
		
		$errors = array();
		
		// Get default values
		$defaultDomain 				= $this->_generic->getSiteConfig("domainName");
		$defaultProject				= $this->_generic->getSiteConfig("projectName"); 
		$defaultExtension			= $this->_generic->getSiteConfig("defaultExtension");
		$defaultCharset				= $this->_generic->getSiteConfig("defaultCharset"); 
		$defaultLang 				= $this->_generic->getSiteConfig("defaultLang");
		$defaultdomainSessionShare 	= $this->_generic->getSiteConfig("domainSession");
		
		$reload 			= $this->_http->getParam("reload");

		$charsetsXML = new SLS_XMLToolBox(file_get_contents($this->_generic->getPathConfig('configSls')."charset.xml"));
		$charsets = array_map('strtoupper', $charsetsXML->getTags('//sls_configs/charset/code'));
		
		$langs = $this->_generic->getSiteXML()->getTags('//configs/langs/name');
			
		if ($reload == "true")
		{		
			// Get New Parameters
			$exportConfig	= $this->_http->getParam('export');
			
			$postDomain 		= SLS_String::trimSlashesFromString($this->_http->getParam("domain", "post"));
			$postProject		= SLS_String::trimSlashesFromString($this->_http->getParam("project", "post"));
			$postExtension 		= SLS_String::trimSlashesFromString($this->_http->getParam("extension", "post"));
			$postCharset		= SLS_String::trimSlashesFromString($this->_http->getParam("charset", "post"));
			$postLang			= SLS_String::trimSlashesFromString($this->_http->getParam("lang", "post"));
			$domainSessionShare	= SLS_String::trimSlashesFromString($this->_http->getParam("domainSession", "post"));

			if ($this->_http->getParam("domainSessionActive") == "")
				$domainSessionShare = "";		
						
			if (empty($postDomain))
				array_push($errors, "The Domain name is required");
			if (empty($postProject))
				array_push($errors, "The project Name is required");
			if (empty($postExtension))
				array_push($errors, "The extension is required");
			if (!in_array($postCharset, $charsets))
				array_push($errors, "The Charset selected is incorrect");
			if (!in_array($postLang, $langs))
				array_push($errors, "The Default lang selected is incorrect");
			if ($this->_http->getParam("domainSessionActive") != "" && empty($domainSessionShare))
				array_push($errors,"You need to fill the domain pattern from which you want to share session");
			if (empty($errors))
			{			
				if ($defaultDomain != $postDomain)
					 $siteXML->setTag("//configs/domainName/domain[default='1']", $postDomain, true);
				if ($defaultProject != $postProject)
					 $siteXML->setTag("//configs/projectName", $postProject, true);
				if ($defaultExtension != $postExtension)
					 $siteXML->setTag("//configs/defaultExtension", $postExtension, true);
				if ($defaultCharset != $postCharset)
					 $siteXML->setTag("//configs/defaultCharset", $postCharset, true);
				if ($defaultLang != $postLang)
					 $siteXML->setTag("//configs/defaultLang", $postLang, true);
				if ($defaultdomainSessionShare != $domainSessionShare)
					$siteXML->setTag("//configs/domainSession", $domainSessionShare, true);
				if ($exportConfig == "on")
				{
					$date = gmdate('D, d M Y H:i:s');
					header("Content-Type: text/xml"); 
					header('Content-Disposition: attachment; filename=site.xml');
					header('Last-Modified: '.$date. ' GMT');
					header('Expires: ' .$date);
					// For This Fuck'in Browser
					if(preg_match('/msie|(microsoft internet explorer)/i', $_SERVER['HTTP_USER_AGENT']))
					{
						header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
						header('Pragma: public');
					}
					else
						header('Pragma: no-cache');
					
					print($siteXML->getXML());
					exit; 
				}
				else 
				{
					file_put_contents($this->_generic->getPathConfig("configSecure")."site.xml", $siteXML->getXML());
				}
			}
			else 
			{
				$xml->startTag('errors');
				foreach ($errors as $error)
				{
					$xml->addFullTag('error', $error);
				}
				$xml->endTag('errors');
			}
	
		}
		$this->_generic->eraseCache('Site');
		$xml->startTag("charsets");
		foreach ($charsets as $charset)
			$xml->addFullTag('charset', $charset, true);
		$xml->endTag("charsets");
		
		$xml->startTag("langs");
			foreach ($langs as $lang)
			$xml->addFullTag('lang', $lang, true);
		$xml->endTag("langs");
		
		$xml->startTag("current_values");
			$xml->addFullTag("domain", $this->_generic->getSiteConfig("domainName"), true);
			$xml->addFullTag("project", $this->_generic->getSiteConfig("projectName"), true);
			$xml->addFullTag("extension", $this->_generic->getSiteConfig("defaultExtension"), true);
			$xml->addFullTag("charset", $this->_generic->getSiteConfig("defaultCharset"), true);
			$xml->addFullTag("lang", $this->_generic->getSiteConfig("defaultLang"), true);
			$xml->addFullTag("domain_session", (is_null($this->_generic->getSiteConfig("domainSession"))) ? "" : $this->_generic->getSiteConfig("domainSession"), true);
		$xml->endTag("current_values");
		
		$this->saveXML($xml);		
	}
	
}
?>