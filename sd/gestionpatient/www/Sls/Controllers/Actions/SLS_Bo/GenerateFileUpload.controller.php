<?php
/**
* Class GenerateFileUpload into Bo Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Bo
* @see Mvc.Controllers.Bo.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class SLS_BoGenerateFileUpload extends SLS_BoControllerProtected
{
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$controllersXML = $this->_generic->getControllersXML();
		$controller = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/@name"));		
		$action = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='FileUpload']"));		
		
		// Check if bo controller already exist
		if (empty($controller))
		{
			$xml->startTag("errors");
			$xml->addFullTag("error","Back-office controller could not be found. Please follow the following link to create it",true);
			$xml->endTag("errors");
			$xml->addFullTag("error_type","rights",true);
		}
		// If action already exist
		else if (!empty($action))
		{
			$xml->startTag("errors");
			$xml->addFullTag("error","Translation action already exists",true);
			$xml->endTag("errors");			
		}
		// Else, let's generate action
		else 
		{
			$langs = $this->_generic->getObjectLang()->getSiteLangs();
		
			$params = array(0 => array("key" 	=> "reload",
					  				   "value" 	=> "true"),
					  		1 => array("key" 	=> "Controller",
					  				   "value" 	=> $controller),
						 	2 => array("key" 	=> "actionName",
					  				   "value" 	=> "FileUpload"),
					  		3 => array("key"	=> "token",
					  					"value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
					  		4 => array("key"	=> "indexes",
					  				   "value"	=> "noindex, nofollow")
					  			)
						    );
			foreach($langs as $lang)
			{
				$tmpParam = array("key" 	=> $lang."-action",
								  "value" 	=> "FileUpload");
				$tmpTitle = array("key" 	=> $lang."-title",
								  "value" 	=> "FileUpload");
				array_push($params,$tmpParam);
				array_push($params,$tmpTitle);
			}
			file_get_contents($this->_generic->getFullPath("SLS_Bo",
														  "AddAction",
														  $params,
														  true));
			$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controller."/FileUpload".".controller.php");
			
			
			$action = 	'        // Check account rights'."\n".
				        '        $right = SLS_BoRights::isAuthorized("Add");'."\n".
				        '        if ($right == -1)'."\n".
				        '            $this->_generic->redirect("Bo/Connection.sls");'."\n\n".						
						'        $xml = $this->getXML();'."\n".
				        '        $boActions = SLS_BoRights::getDistinctActions();'."\n".
				        '        $xml->startTag("bo_actions");'."\n".
				        '        for($i=0 ; $i<$count=count($boActions) ; $i++)'."\n".
				        '        {'."\n".
				        '            $xml->startTag("bo_action");'."\n".
				        '            foreach($boActions[$i] as $key => $value)'."\n".
				        '                $xml->addFullTag($key,$value,true);'."\n".
				        '            $xml->endTag("bo_action");'."\n".
				        '        }'."\n".
				        '        $xml->endTag("bo_actions");'."\n\n".				        
				        '        if ($this->_http->getParam("reload") == "true")'."\n".
				        '        {'."\n".
				        '        	$file = $this->_http->getParam("file");'."\n".
				        '        	move_uploaded_file($file["tmp_name"],$this->_generic->getPathConfig("files").$file["name"]);'."\n".
				        '        }'."\n\n".				                
				        '        $handle = opendir($this->_generic->getPathConfig("files"));'."\n".
				        '        $files = array();'."\n".
				        '        while (false !== ($file = readdir($handle)))'."\n".
				        '        {'."\n".
				        '            if (!SLS_String::startsWith($file,".") && is_file($this->_generic->getPathConfig("files").$file))'."\n".
				        '            {'."\n".
				        '                $files[$file] = array("filename" 	=> $file,'."\n".
				        '                                      "path" 		=> "http://".$this->_generic->getSiteConfig("domainName")."/".$this->_generic->getPathConfig("files").$file,'."\n".
				        '                                      "ext" 		=> strtoupper(SLS_String::substrAfterLastDelimiter($file,".")),'."\n".
				        '                                      "raw" 		=> rawurlencode($file));'."\n".
				        '            }'."\n".
				        '        }'."\n".
				        '        sort($files);'."\n\n".				        
				        '        $xml->startTag("files");'."\n".
				        '        foreach($files as $key => $value)'."\n".
				        '        {'."\n".
				        '            $xml->startTag("file");'."\n".
				        '            $xml->addFullTag("filename",$files[$key]["filename"],true);'."\n".
				        '            $xml->addFullTag("path",$files[$key]["path"],true);'."\n".
				        '            $xml->addFullTag("ext",$files[$key]["ext"],true);'."\n".
				        '            $xml->addFullTag("raw",$files[$key]["raw"],true);'."\n".
				        '            $xml->endTag("file");'."\n".
				        '        }'."\n".
				        '        $xml->endTag("files");'."\n\n".
				        '        $xml->addFullTag("delete",$this->_generic->getFullPath("Bo","DeleteFile",array(),false),true);'."\n\n".				        
				        '        $this->saveXML($xml);'."\n";
				      
			$source = substr($source,0,strpos($source,"action()")+8)."\n".
							  '    {'."\n".
							  $action.
							  '    }'."\n".
							  '}'."\n".
							  '?>';
			file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/FileUpload.controller.php",$source);
			
			$params = array(0 => array("key" 	=> "reload",
					  				   "value" 	=> "true"),
					  		1 => array("key" 	=> "Controller",
					  				   "value" 	=> $controller),
						 	2 => array("key" 	=> "actionName",
					  				   "value" 	=> "DeleteFile"),
					  		3 => array("key"	=> "token",
					  					"value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
					  		4 => array("key"	=> "indexes",
					  				   "value"	=> "noindex, nofollow")
					  			)
						    );
			foreach($langs as $lang)
			{
				$tmpParam = array("key" 	=> $lang."-action",
								  "value" 	=> "DeleteFile");
				$tmpTitle = array("key" 	=> $lang."-title",
								  "value" 	=> "DeleteFile");
				array_push($params,$tmpParam);
				array_push($params,$tmpTitle);
			}
			file_get_contents($this->_generic->getFullPath("SLS_Bo",
														  "AddAction",
														  $params,
														  true));
			$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controller."/DeleteFile".".controller.php");
			
			
			$action = 	'        // Check account rights'."\n".
				        '        $right = SLS_BoRights::isAuthorized("Add");'."\n".
				        '        if ($right == -1)'."\n".
				        '            $this->_generic->redirect("Bo/Connection.sls");'."\n\n".						
						'        $file = rawurldecode($this->_http->getParam("File"));'."\n\n".						
						'        if (file_exists($this->_generic->getPathConfig("files").$file))'."\n".
						'            unlink($this->_generic->getPathConfig("files").$file);'."\n\n".							
						'        $this->forward("Bo","FileUpload");'."\n";
				      
			$source = substr($source,0,strpos($source,"action()")+8)."\n".
							  '    {'."\n".
							  $action.
							  '    }'."\n".
							  '}'."\n".
							  '?>';
			file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/DeleteFile.controller.php",$source);
			
			$sourceXsl = 	'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
							'<xsl:template name="FileUpload">'."\n".
							'    <xsl:call-template name="BoMenu" />'."\n".
							'    <xsl:text disable-output-escaping="yes"><![CDATA['."\n".
							'    <script type="text/javascript">'."\n".
							'        function confirmDelete(link){if (confirm("Etes vous certain de vouloir supprimer ce(s) enregistrement(s) ?"))window.location = link;else return false;}'."\n".
							'        var page = \'listing\';'."\n".
							'    </script>'."\n".
							'    ]]></xsl:text>'."\n".
							'    <div class="bo_content_center">'."\n".
							'        <h1 class="main">Resultats pour : <span class="light">fichiers</span></h1>'."\n".
							'    	 <div class="grey_wrapper">'."\n".
							'    	     <fieldset class="top_border">'."\n".
							'    	         <legend><img src="{concat($sls_url_img_core_buttons,\'add.png\')}" alt="Add" align="absmiddle" />&#160;Ajouter un nouvel enregistrement</legend>'."\n".
							'    			 <div class="row_center">'."\n".
							'    			     <form method="post" action="" enctype="multipart/form-data">'."\n".
							'    			         <input type="hidden" name="reload" value="true" />'."\n".
							'    					 <input type="file" name="file" class="input_filter" />'."\n".
							'    					 <input type="submit" value="Envoyer" />'."\n".
							'    				 </form>'."\n".
							'    		     </div>'."\n".
							'    	    </fieldset>'."\n".
							'    	 </div>'."\n".
							'    	 <xsl:if test="count(//View/files/file) = 0">'."\n".
							'          <ul class="error" style="display:block;float:left;margin-top:20px;">'."\n".
							'    	         Vous n\'avez encore envoyé aucun fichiers.'."\n".
							'          </ul>'."\n".
							'    	 </xsl:if>'."\n".
							'    	 <xsl:if test="count(//View/files/file) &gt; 0">'."\n".
							'    	     <table class="listing" cellpadding="0" cellspacing="0">'."\n".
							'    		     <tr class="summary">'."\n".
							'    			     <th>Nom du fichier</th>'."\n".
							'    				 <th>Type de fichier</th>'."\n".
							'    				 <th>Supprimer</th>'."\n".
							'    			 </tr>'."\n".
							'    			 <xsl:for-each select="//View/files/file">'."\n".
							'    			     <xsl:variable name="trClass">'."\n".
							'    				     <xsl:if test="position() mod 2  = 0">dark</xsl:if>'."\n".
							'    					 <xsl:if test="position() mod 2 != 0">clear</xsl:if>'."\n".
							'    				 </xsl:variable>'."\n".
							'    				 <tr class="{$trClass}">'."\n".
							'    				     <td><a href="{path}" title="{filename}" target="_blank"><xsl:value-of select="filename" /></a></td>'."\n".
							'    					 <td><xsl:value-of select="ext" /></td>'."\n".
							'    					 <td><a onclick="javascript:confirmDelete(\'{concat(//View/delete,\'/File/\',raw,\'.sls\')}\')" href="#" title="Supprimer"><img src="{concat($sls_url_img_core_icons,\'delete16.png\')}" style="border:0;" align="absmiddle" /></a></td>'."\n".
							'    				 </tr>'."\n".
							'    			 </xsl:for-each>'."\n".
							'    		 </table>'."\n".
							'    	 </xsl:if>'."\n".
							'    </div>'."\n".
							'</xsl:template>'."\n".
							'</xsl:stylesheet>';
			file_put_contents($this->_generic->getPathConfig("viewsBody").$controller."/FileUpload.xsl",$sourceXsl);
			$this->_generic->goDirectTo("SLS_Bo","Bo");
		}
		
		$xml->addFullTag("url_add_controller",$this->_generic->getFullPath("SLS_Bo","AddController",array(0=>array("key"=>"isBo","value"=>"true"))),true);
		$this->saveXML($xml);
	}
}
?>