<?php
class SLS_BoAddRight extends SLS_BoControllerProtected 
{
	
	public function action() 
	{		
		// Objects
		$xml = $this->getXML();
		$user = $this->hasAuthorative();
		$xml = $this->makeMenu($xml);
		
		if ($this->_http->getParam("reload") == "true")
		{
			$login 		= SLS_String::trimSlashesFromString($this->_http->getParam("login"));
			$pwd 		= SLS_String::trimSlashesFromString($this->_http->getParam("password"));
			$privilege 	= SLS_String::trimSlashesFromString($this->_http->getParam("privilege"));
			$add 		= SLS_String::trimSlashesFromString($this->_http->getParam("add"));
			$modify 	= SLS_String::trimSlashesFromString($this->_http->getParam("modify"));
			$delete 	= SLS_String::trimSlashesFromString($this->_http->getParam("delete"));
			
			$pathsHandle = file_get_contents($this->_generic->getPathConfig("configSls")."/rights.xml");
			$xmlRights = new SLS_XMLToolbox($pathsHandle);
			$result = $xmlRights->getTags("//sls_configs/entry[@login='".($login)."']");
			
			if (!empty($result))
			{
				$xml->startTag("errors");
				$xml->addFullTag("error","This account already exists",true);
				$xml->endTag("errors");
			}
			else
			{	
				$right = "R";
				
				if (empty($login) || empty($pwd))
				{
					$xml->startTag("errors");
					$xml->addFullTag("error","You must fill the login and the password",true);
					$xml->endTag("errors");
					$xml->addFullTag("login",$login,true);
					$xml->addFullTag("right_add",(!empty($add)) ? "true" : "false",true);
					$xml->addFullTag("right_modify",(!empty($modify)) ? "true" : "false",true);
					$xml->addFullTag("right_delete",(!empty($delete)) ? "true" : "false",true);
				}
				else
				{
					if (!empty($add))
						$right .= "A";
					if (!empty($modify))
						$right .= "M";
					if (!empty($delete))
						$right .= "D";
						
					if ($privilege == "god")
						$right = "GOD";
						
					$xmlNew = '<entry login="'.($login).'" password="'.sha1($pwd).'" right="'.$right.'" />';
					$xmlRights->appendXMLNode("//sls_configs",$xmlNew);
					$xmlRights->saveXML($this->_generic->getPathConfig("configSls")."/rights.xml",$xmlRights->getXML());
					$this->_generic->redirect("Manage/Rights");
				}
			}
		}
		
		$this->saveXML($xml);
	}
	
}
?>