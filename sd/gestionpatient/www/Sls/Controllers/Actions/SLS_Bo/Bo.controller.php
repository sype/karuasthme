<?php
class SLS_BoBo extends SLS_BoControllerProtected 
{
	
	public function action() 
	{		
		// Objects
		$xml = $this->getXML();
		$user = $this->hasAuthorative();
		$xml = $this->makeMenu($xml);
		$this->_generic->registerLink('Generate', 'SLS_Bo', 'GenerateBo');
		$this->_generic->registerLink('Translation', 'SLS_Bo', 'GenerateTranslation');
		$this->_generic->registerLink('FileUpload', 'SLS_Bo', 'GenerateFileUpload');
		$this->_generic->registerLink('Manage_Rights', 'SLS_Bo', 'ManageRights');
		$xml->addFullTag("delete",$this->_generic->getFullPath("SLS_Bo","DeleteBo",array(),false));
		$xml->addFullTag("edit",$this->_generic->getFullPath("SLS_Bo","EditBo",array(),false));
		$bos = array();
		
		// Search for user back-office
		$controllersXML = $this->_generic->getControllersXML();
		$controller = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/@name"));
		if (!empty($controller))
		{
			$models = $this->getAllModels();			
			for($i=0 ; $i<$count=count($models) ; $i++)
			{
				$model 	= SLS_String::substrAfterFirstDelimiter($models[$i],".");
				$db 	= SLS_String::substrBeforeFirstDelimiter($models[$i],".");
				if ($this->boActionExist($model,$db))
				{
					$this->_generic->useModel($model,$db,"user");
					$class = ucfirst($db)."_".SLS_String::tableToClass($model);
					$object = new $class();
					$bos[$class] = array("db" 			=> $db,
									 	 "className" 	=> $class,
									 	 "tableName" 	=> $object->getTable(),											 	
									 	 "nb_actions" 	=> count($this->getActionsBo($model,$db)));
				}
			}	
		}		
		
		asort($bos,SORT_REGULAR);
		
		$xml->startTag("bos");
		foreach($bos as $bo)
		{
			$xml->startTag("bo");
			$xml->addFullTag("db",strtolower($bo["db"]),true);
			$xml->addFullTag("class",$bo["className"],true);
			$xml->addFullTag("table",$bo["tableName"],true);			
			$xml->addFullTag("nb_actions",$bo["nb_actions"],true);
			$xml->endTag("bo");
		}		
		$xml->endTag("bos");		
		
		$pathsHandle = file_get_contents($this->_generic->getPathConfig("configSls")."/rights.xml");
		$xmlRights = new SLS_XMLToolbox($pathsHandle);
		$result = $xmlRights->getTags("//sls_configs/entry");
		$xml->addFullTag("admins_exist",(!empty($result)) ? "true" : "false",true);
		
		$xml->startTag("actions");
		$action = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='Translation']"));
		$action2 = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='FileUpload']"));
		$xml->startTag("action");
		$xml->addFullTag("name","Translation",true);
		$xml->addFullTag("existed",(!empty($action)) ? "true" : "false",true);
		$xml->endTag("action");
		$xml->startTag("action");
		$xml->addFullTag("name","FileUpload",true);
		$xml->addFullTag("existed",(!empty($action2)) ? "true" : "false",true);
		$xml->endTag("action");
		$xml->endTag("actions");
		
		$this->saveXML($xml);
	}
	
}
?>