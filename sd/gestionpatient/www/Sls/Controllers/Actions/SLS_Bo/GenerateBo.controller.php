<?php
/**
* Class GenerateBo into Bo Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Bo
* @see Mvc.Controllers.Bo.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class SLS_BoGenerateBo extends SLS_BoControllerProtected
{
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$controllersXML = $this->_generic->getControllersXML();
		$controller = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/@name"));
		
		// Check if bo controller already exist
		if (empty($controller))
		{
			$xml->startTag("errors");
			$xml->addFullTag("error","Back-office controller could not be found. Please follow the following link to create it",true);
			$xml->endTag("errors");
		}
		// Else, let's choose the models
		else 
		{
			// If reload
			if ($this->_http->getParam("reload") == "true")
			{
				$modelsWanted = $this->_http->getParam("models");
				$langs = $this->_generic->getObjectLang()->getSiteLangs();
				
				// Foreach models choose, generate files
				foreach($modelsWanted as $model)
				{
					$db = Sls_String::substrBeforeFirstDelimiter($model,".");
					$table = Sls_String::substrAfterFirstDelimiter($model,".");
					
					// Create action list file
					$this->createActionBoList($controller,$table,$db);
										
					// Create action add file
					$this->createActionBoAdd($controller,$table,$db);
					
					// Create action modify file
					$this->createActionBoModify($controller,$table,$db);
					
					// Create action delete file
					$this->createActionBoDelete($controller,$table,$db);					
				}
			}
			
			$sql = SLS_Sql::getInstance();
			$models = $this->getAllModels();			
			$dbs = $sql->getDbs();
			sort($dbs,SORT_REGULAR);
						
			$xml->startTag("dbs");
			foreach($dbs as $db)
			{
				sort($models,SORT_REGULAR);
				
				$xml->startTag("db");
				$xml->addFullTag("name",$db,true);
				$xml->startTag("models");
				for($i=0 ; $i<$count=count($models) ; $i++)
				{
					if (SLS_String::startsWith($models[$i],$db))
					{
						$xml->startTag("model");
						$xml->addFullTag("name",SLS_String::substrAfterFirstDelimiter($models[$i],"."),true);
						$xml->addFullTag("existed",($this->boActionExist(SLS_String::substrAfterFirstDelimiter($models[$i],"."),SLS_String::substrBeforeFirstDelimiter($models[$i],"."))) ? "true" : "false",true);
						$xml->endTag("model");
					}
				}
				$xml->endTag("models");
				$xml->endTag("db");
			}
			$xml->endTag("dbs");
		}
		
		$xml->addFullTag("url_add_controller",$this->_generic->getFullPath("SLS_Bo","AddController",array(0=>array("key"=>"isBo","value"=>"true"))),true);
		$this->saveXML($xml);
	}
}
?>