<?php
class SLS_BoDeleteBo extends SLS_BoControllerProtected 
{
	
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
				
		$param = $this->_http->getParam("name");
		$model = SLS_String::substrAfterFirstDelimiter($param,"_");
		$alias = SLS_String::substrBeforeFirstDelimiter($param,"_");
		
		if ($this->_http->getParam("type") == "")	
			$actionsTypes = array("list","add","modify","delete");
		else
			$actionsTypes = array($this->_http->getParam("type"));
		foreach($actionsTypes as $actionType)
			$this->deleteActionBo($model,$actionType,$alias);
		
		$controllers = $this->_generic->getTranslatedController("SLS_Bo","Bo");
		$this->_generic->redirect($controllers["controller"]."/".$controllers["scontroller"]);
	}
	
}
?>