<?php
class SLS_BoAddTemplate extends SLS_BoControllerProtected 
{	
	public function action()
	{
		$user 	= $this->hasAuthorative();
		$xml 	= $this->getXML();
		$xml 	= $this->makeMenu($xml);		
		$errors = array();		
		$tpls 	= $this->getAppTpls();
		
		if ($this->_http->getParam("reload") == "true")
		{
			$tpl = SLS_String::trimSlashesFromString(SLS_String::stringToUrl($this->_http->getParam("tpl_name"),"_"));
			
			if (empty($tpl))
				array_push($errors,"You must choose a name for your template");
			else if (in_array($tpl,$tpls))
				array_push($errors,"This template name already exists, please choose another one");
				
			if (empty($errors))
			{
				$str =  '<!--'."\n".
					 	'   - Global template for your application'."\n".
					 	'   - Don\'t change anything between marked delimiter |||dtd:tagName|||'."\n".
					 	'   - Beyond you can add additional headers or/and xhtml structure'."\n".
						'-->'."\n".
						'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
						'    <xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes" encoding="|||sls:getCharset|||" />'."\n\n".	
						'    <!-- Variable Builder -->'."\n".
						'    |||sls:buildUrlVars|||'."\n".
						'    <!-- /Variable Builder -->'."\n\n".	
						'    <!-- Generic include -->'."\n".
						'    |||sls:includeActionFileBody|||'."\n".
						'    |||sls:includeActionFileHeader|||'."\n".
						'    |||sls:includeStaticsFiles|||'."\n".
						'    <!-- /Generic include -->'."\n\n".	
						'    <xsl:template match="root">'."\n".
						'        <html xml:lang="|||sls:getLanguage|||" lang="|||sls:getLanguage|||">'."\n".
						'            <head>'."\n\n".			
						'                <!-- Generic headers loading -->'."\n".
						'                |||sls:loadCoreHeaders|||'."\n".
						'                |||sls:loadUserHeaders|||'."\n".
						'                |||sls:loadActionFileHeader|||'."\n".
						'                <!-- /Generic headers loading -->'."\n\n".			
						'            </head>'."\n".
						'            <body>'."\n\n".
						'                <!-- Generic bodies loading -->'."\n".
						'                |||sls:loadCoreBody|||'."\n".
						'                |||sls:loadUserBody|||'."\n".
						'                |||sls:loadActionFileBody|||'."\n".
						'                <!-- /Generic bodies loading -->'."\n\n".			
						'            </body>'."\n".
						'        </html>'."\n".
						'    </xsl:template>'."\n".
						'</xsl:stylesheet>';
				file_put_contents($this->_generic->getPathConfig("viewsTemplates").$tpl.".xsl",$str);
				$this->_generic->goDirectTo("SLS_Bo","Templates");
			}
			else
			{
				$xml->startTag("errors");
				foreach($errors as $error)
					$xml->addFullTag("error",$error,true);
				$xml->endTag("errors");
			}
		}
		
		
		$this->saveXML($xml);
	}	
}
?>