<?php
class SLS_BoProdSettings extends SLS_BoControllerProtected 
{
	
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		
		$errors = array();
		
		// Get default values
		$defaultIsProd 		= $this->_generic->getSiteConfig("isProd");
		$defaultActiveCache = $this->_generic->getSiteConfig("activeCache"); 
		$reload 			= $this->_http->getParam("reload");
				
		if ($reload == "true")
		{
			// Get New Parameters
			$postIsProd 		= $this->_http->getParam("prod", "post");
			$postActiveCache 	= $this->_http->getParam("cache", "post");
			$siteXML = $this->_generic->getSiteXML();
			
			if ($postActiveCache != 0 && $postActiveCache != 1)
				array_push($errors, "Incorrect value for Cache");
			if ($postIsProd != 0 && $postIsProd != 1)
				array_push($errors, "Incorrect value for Production mode");
			if (empty($errors))
			{
				if ($defaultActiveCache != $postActiveCache)
					 $siteXML->setTag("//configs/activeCache", $postActiveCache, true);
				if ($defaultIsProd != $postIsProd)
					 $siteXML->setTag("//configs/isProd", $postIsProd, true);
					 
				if (($defaultActiveCache != $postActiveCache) || ($defaultIsProd != $postIsProd))
					file_put_contents($this->_generic->getPathConfig("configSecure")."site.xml", $siteXML->getXML());
			}
			else 
			{
				$xml->startTag("errors");
					foreach ($errors as $error)
						$xml->addFullTag("error", $error, true);
				$xml->endTag("errors");
			}
		}
		$this->_generic->eraseCache('Site');
		$xml->startTag("current_values");
			$xml->addFullTag("prod", $this->_generic->getSiteConfig("isProd"), true);
			$xml->addFullTag("cache", $this->_generic->getSiteConfig("activeCache"), true);
		$xml->endTag("current_values");
		
		$this->saveXML($xml);		
	}
	
}
?>