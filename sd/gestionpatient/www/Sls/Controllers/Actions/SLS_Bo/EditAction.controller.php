<?php
class SLS_BoEditAction extends SLS_BoControllerProtected 
{	
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$langs = $this->_generic->getObjectLang()->getSiteLangs();
		$errors = array();
		
		$controllersXML = $this->_generic->getControllersXML();
		$metasXML = $this->_generic->getCoreXML('metas');
		$controller = $this->_http->getParam('Controller');
		$action = $this->_http->getParam('Action');
				
		$actionExist = $controllersXML->getTags("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$action."']");
		$actionId = array_shift($controllersXML->getTags("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$action."']/@id"));
		$protocol = $this->_generic->getActionProtocol($actionId);
		if (count($actionExist) == 1)
		{
			
			$reload = $this->_http->getParam("reload");
			if ($reload == 'true')
			{
				$oldAction  		= SLS_String::trimSlashesFromString($this->_http->getParam("genericName"));
				$newAction 			= SLS_String::stringToUrl(ucwords(SLS_String::trimSlashesFromString($this->_http->getParam("actionName"))), "", false);
				$needDynamic 		= SLS_String::trimSlashesFromString($this->_http->getParam("dynamic"));
				$searchEngine 		= SLS_String::trimSlashesFromString($this->_http->getParam("indexes"));
				$postProtocol		= SLS_String::trimSlashesFromString($this->_http->getParam("protocol"));
				$tpl 				= SLS_String::trimSlashesFromString($this->_http->getParam('template'));
				
				$postActionsLang	= array();
				$postTitlesLang		= array();
				$postDescriptionsLang = array();
				$postKeywordsLang	= array();
				
				
				// Save Form informations
				$xml->startTag("form");
					$xml->addFullTag("actionName", $newAction, true);
					foreach ($langs as $lang)
					{
						$postLang = trim(SLS_String::stringToUrl(ucwords(SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-action"))), "", false));
						$postOldLang = SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-oldAction"));
						if ($postLang != $oldAction)
						{
							$translationExist = $controllersXML->getTags("//controllers/controller[@side='user' and @name='".$controller."']/scontrollers/scontroller[@name != '".$oldAction."']/scontrollerLangs[scontrollerLang = '".$postLang."']");
						
							if (empty($postLang)) 
								array_push($errors, "You need to fill the ".$lang." url translations");
							elseif(count($translationExist) != 0)
								array_push($errors, "You URL translation in ".$lang." is already in use on another action in the same controller");
							else
								$postActionsLang[$lang] =  $postLang;
						}
						
						// Get Titles	
						$postTitlesLang[$lang] = SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-title"));
						$postDescriptionsLang[$lang] = SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-description"));
						$postKeywordsLang[$lang] = SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-keywords"));
						$xml->addFullTag($lang."-action", $postLang, true);
						$xml->addFullTag($lang."-title", $postTitlesLang[$lang], true);
						$xml->addFullTag($lang."-description", $postDescriptionsLang[$lang], true);
						$xml->addFullTag($lang."-keywords", $postKeywordsLang[$lang], true);
					}
				$xml->endTag("form");
				
				if (empty($postProtocol) || ($postProtocol != 'http' && $postProtocol != 'https'))
						array_push($errors, "Protocol must be http or https");
					else 
						$protocol = $postProtocol;
				
				if ((($controller != 'Home' && $controller != 'Default') || (($controller == 'Home' && $action != 'Index') || ($controller == 'Default' && ($action != 'UrlError' && $action != 'BadRequestError' && $action != 'AuthorizationError' && $action != 'ForbiddenError' && $action != 'InternalServerError')))) && ($oldAction != $newAction)) 
				{
					$newNameExist = $controllersXML->getTags("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$newAction."']");
					if (count($newNameExist) != 0)
						array_push($errors, "The generic action name is already in use in this controller");
						
				}
				else 
					$newAction = $oldAction;
					
				if ($tpl == -1)
					$controllersXML->deleteTagAttribute("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$action."']", "tpl");
				else
					$controllersXML->setTagAttributes("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$action."']", array('tpl' => $tpl));
				file_put_contents($this->_generic->getPathConfig('configSecure')."controllers.xml", $controllersXML->getXML());

				$dynamic = (($controller != 'Home' && $controller != 'Default') || (($controller == 'Home' && $action != 'Index') || ($controller == 'Default' && ($action != 'UrlError' && $action != 'BadRequestError' && $action != 'AuthorizationError' && $action != 'ForbiddenError' && $action != 'InternalServerError')))) ? ($needDynamic == 'on') ? "1" : "0" : array_shift($controllersXML->getTags("//controllers/controller[@name='".$controller."' and side='user']/scontrollers/scontroller[@name='".$oldAction."']/@needParam"));
				// If no errors
				if (empty($errors))
				{
					
					$controllersXML->setTagAttributes("//controllers/controller/scontrollers/scontroller[@id='".$actionId."']", array("name"=>$newAction,"needParam"=>$dynamic,'protocol'=>$protocol));
							
					foreach ($langs as $lang)
					{
						if (array_key_exists($lang, $postActionsLang))
							$controllersXML->setTag("//controllers/controller/scontrollers/scontroller[@id='".$actionId."']/scontrollerLangs/scontrollerLang[@lang='".$lang."']", $postActionsLang[$lang], true);
						else 
							$controllersXML->setTag("//controllers/controller/scontrollers/scontroller[@id='".$actionId."']/scontrollerLangs/scontrollerLang[@lang='".$lang."']", SLS_String::trimSlashesFromString($this->_http->getParam(strtolower($lang)."-oldAction")), true);
					}
					
					// If generic name is not the same, we modify all files
					if ($oldAction != $newAction)
					{
						// Controller
						$contentController = file_get_contents($this->_generic->getPathConfig('actionsControllers').$controller."/".$oldAction.".controller.php");
						$contentController = str_replace(array(0=>" ".$controller.$oldAction." ", 1=>" ".$oldAction." "), array(0=>" ".$controller.$newAction." ", 1=>" ".$newAction." "), $contentController);
						file_put_contents($this->_generic->getPathConfig('actionsControllers').$controller."/".$newAction.".controller.php", $contentController);
						unlink($this->_generic->getPathConfig('actionsControllers').$controller."/".$oldAction.".controller.php");
						
						//Langs
						foreach ($langs as $lang)
						{
							$contentLang = file_get_contents($this->_generic->getPathConfig('actionLangs').$controller."/".$oldAction.".".$lang.".lang.php");
							$contentLang = str_replace(array(0=>" ".$controller.$oldAction." ", 1=>" ".$oldAction." "), array(0=>" ".$controller.$newAction." ", 1=>" ".$newAction." "), $contentLang);
							file_put_contents($this->_generic->getPathConfig('actionLangs').$controller."/".$newAction.".".$lang.".lang.php", $contentLang);
							unlink($this->_generic->getPathConfig('actionLangs').$controller."/".$oldAction.".".$lang.".lang.php");
						}
						
						// Views
							// Body
							$contentBody = file_get_contents($this->_generic->getPathConfig('viewsBody').$controller."/".$oldAction.".xsl");
							$contentBody = str_replace(array(0=>"name=\"".$oldAction."\">", 1=>$oldAction.".xsl"), array(0=>"name=\"".$newAction."\">", 1=>$newAction.".xsl"), $contentBody);
							file_put_contents($this->_generic->getPathConfig('viewsBody').$controller."/".$newAction.".xsl", $contentBody);
							unlink($this->_generic->getPathConfig('viewsBody').$controller."/".$oldAction.".xsl");
							
							// Headers
							$contentHeader = file_get_contents($this->_generic->getPathConfig('viewsHeaders').$controller."/".$oldAction.".xsl");
							$contentHeader = str_replace(array(0=>"name=\"Header".$oldAction."\">"), array(0=>"name=\"Header".$newAction."\">"), $contentHeader);
							file_put_contents($this->_generic->getPathConfig('viewsHeaders').$controller."/".$newAction.".xsl", $contentHeader);
							unlink($this->_generic->getPathConfig('viewsHeaders').$controller."/".$oldAction.".xsl");
					}
					
					// We now update the XML
					
					foreach ($langs as $lang)
					{
						// Metas
						$metasXML->setTag("//sls_configs/action[@id='".$actionId."']/title[@lang='".$lang."']", $postTitlesLang[$lang], true);
						$metasXML->setTag("//sls_configs/action[@id='".$actionId."']/description[@lang='".$lang."']", $postDescriptionsLang[$lang], true);
						$metasXML->setTag("//sls_configs/action[@id='".$actionId."']/keywords[@lang='".$lang."']", $postKeywordsLang[$lang], true);
					}
					if ($searchEngine != "index, follow" && $searchEngine != "noindex, follow" && $searchEngine != "noindex, nofollow" && $searchEngine != "index, nofollow")
						$searchEngine = "index, follow";
					$metasXML->setTag("//sls_configs/action[@id='".$actionId."']/robots", $searchEngine, true);
					
					
					file_put_contents($this->_generic->getPathConfig("configSecure")."controllers.xml", $controllersXML->getXML());
					file_put_contents($this->_generic->getPathConfig("configSls")."metas.xml", $metasXML->getXML());
					
				}
				
				if (count($errors) != 0)
				{
					$xml->startTag("errors");
					foreach ($errors as $error)
						$xml->addFullTag("error", $error, true);
					$xml->endTag("errors");
				}
			}
			$tplResult = array_shift($controllersXML->getTagsAttribute("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$action."']","tpl"));
			$tpl = $tplResult["attribute"];
			
			$xml->startTag("action");
				$xml->addFullTag("name", $action, true);
				$xml->addFullTag("dynamic", (array_shift($controllersXML->getTags("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$action."']/@needParam")) == '1') ? 'true' : 'false', true);
				$xml->addFullTag("indexes", array_shift($metasXML->getTags("//sls_configs/action[@id='".$actionId."']/robots")), true);
				$xml->addFullTag("canBeModified", (($controller != 'Home' && $controller != 'Default') || (($controller == 'Home' && $action != 'Index') || ($controller == 'Default' && ($action != 'UrlError' && $action != 'BadRequestError' && $action != 'AuthorizationError' && $action != 'ForbiddenError' && $action != 'InternalServerError')))) ? 'true' : 'false', true);
				$xml->startTag("translations");
				foreach ($langs as $lang)
				{
					$xml->startTag("translation");
					$xml->addFullTag("lang", $lang, true);
					$xml->addFullTag("name", array_shift($controllersXML->getTags("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$action."']/scontrollerLangs/scontrollerLang[@lang='".$lang."']")) ,true);
					$xml->addFullTag("title", array_shift($metasXML->getTags("//sls_configs/action[@id='".$actionId."']/title[@lang='".$lang."']")),true);
					$xml->addFullTag("description", array_shift($metasXML->getTags("//sls_configs/action[@id='".$actionId."']/description[@lang='".$lang."']")),true);
					$xml->addFullTag("keywords", array_shift($metasXML->getTags("//sls_configs/action[@id='".$actionId."']/keywords[@lang='".$lang."']")),true);
					$xml->endTag("translation");
				}
				$xml->endTag("translations");
			$xml->endTag("action");
			$xml->startTag("controller");
			$xml->addFullTag("name", $controller, true);
			$xml->endTag("controller");
			
			// Build all tpls
			$tpls = $this->getAppTpls();
			$xml->startTag("tpls");
			foreach($tpls as $template)
				$xml->addFullTag("tpl",$template,true);
			$xml->endTag("tpls");
			
			$xml->addFullTag('request', 'modifyAction', true);
		}
		else 
		{
			$this->_generic->dispatch('SLS_Bo', 'Controllers');
		}
		$xml->addFullTag('protocol', $protocol, true);
		$xml->addFullTag('template', $tpl, true);
		$this->saveXML($xml);
	}
	
}
?>