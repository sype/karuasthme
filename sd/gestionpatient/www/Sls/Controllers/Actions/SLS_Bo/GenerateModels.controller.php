<?php
class SLS_BoGenerateModels extends SLS_BoControllerProtected 
{
	
	public function action()
	{
		$user = $this->hasAuthorative();
		$errors = array();
		$sql = SLS_Sql::getInstance();
		$xml = $this->getXML();
		
		$arrayConvertTypes = array(
			'varchar'	=>	'string',
			'tinyint'	=>	'int',
			'text'		=>	'string',
			'date'		=>	'string',
			'smallint'	=>	'int',
			'mediumint'	=>	'int',
			'int'		=>	'int',
			'bigint'	=>	'int',
			'float'		=>	'float | int',
			'double'	=>	'float | int',
			'decimal'	=>	'float',
			'datetime'	=>	'string',
			'timestamp'	=>	'int',
			'time'		=>	'string | int',
			'year'		=>	'int',
			'char'		=>	'string',
			'tinyblob'	=>	'string',
			'tinytext'	=>	'string',
			'blob'		=>	'string',
			'mediumblob'=>	'string',
			'mediumtext'=>	'string',
			'longblob'	=>	'string',
			'longtext'	=>	'string',
			'enum'		=>	'string',
			'set'		=>	'string',
			'bool'		=>	'int',
			'binary'	=>	'string',
			'varbinary'	=>	'string'
		);
		
		
		$xml = $this->makeMenu($xml);

		// Get all models
		$models = array();
		$handle = opendir($this->_generic->getPathConfig("models"));
		while (false !== ($file = readdir($handle))) 			
			if (!is_dir($this->_generic->getPathConfig("models")."/".$file) && substr($file, 0, 1) != ".") 
			{
				$modelExploded = explode(".",$file);
				array_push($models,strtolower($modelExploded[0]).".".$modelExploded[1]);
			}
		
		// If reload
		if ($this->_http->getParam("reload")=="true")
		{
			// Get the tables dude wants to generate
			$tablesG = ($this->_http->getParam("tables")=="") ? array() : $this->_http->getParam("tables");
			
			// Foreach tables, generate model
			foreach($tablesG as $tableG)
			{
				$db = Sls_String::substrBeforeFirstDelimiter($tableG,".");
				$table = Sls_String::substrAfterFirstDelimiter($tableG,".");
				
				// Change db if it's required
				if ($sql->getCurrentDb() != $db)
					$sql->changeDb($db);
				
				// If table exists
				if ($sql->tableExists($table))
				{					
					// Create Model
					$pathsHandle = file_get_contents($this->_generic->getPathConfig("configSls")."/types.xml");
					$xmlType = new SLS_XMLToolbox($pathsHandle);
					$pathsHandle = file_get_contents($this->_generic->getPathConfig("configSls")."/fks.xml");
					$xmlFk = new SLS_XMLToolbox($pathsHandle);
					$columns = $sql->showColumns($table);					
					$tableName = $table;
					$currentTable = array("table"=>$db.".".$tableName,"errors"=>array());
					$fieldsOk = true;
					$className = ucfirst($db)."_".SLS_String::tableToClass($tableName);
					$fileName  = ucfirst($db).".".SLS_String::tableToClass($table).".model.php";
					$primaryKey = "";
					$multiLanguage = 'false';
					
					$contentM = '<?php'."\n".
							   '/**'."\n".
							   ' * Object '.$className.''."\n".
							   ' * @author SillySmart'."\n".
							   ' * @copyright SillySmart'."\n".
							   ' * @package Mvc.Models.Objects'."\n".
							   ' * @see Sls.Models.Core.SLS_FrontModel'."\n".
							   ' * @since 1.0'."\n".
							   ' */'."\n".
							   'class '.$className.' extends SLS_FrontModel'."\n".
							   '{'."\n".
							   '    /**'."\n".
	 					       '     * Class variables'."\n".
	 						   '	 */'."\n";				   
	 				$pkFound = false;
					for($i=0 ; $i<$count=count($columns) ; $i++)
					{
						if (!$pkFound && $columns[$i]->Key == "PRI")
						{
							$primaryKey = $columns[$i]->Field;
							$pkFound = true;
						}
						if ($columns[$i]->Field == "pk_lang" && $columns[$i]->Key == "PRI")
							$multiLanguage = 'true';
						
						// Check forbidden chars
						if (SLS_String::removePhpChars($columns[$i]->Field) != $columns[$i]->Field)
						{
							$error = array("column"=>$columns[$i]->Field,"column_clean"=>SLS_String::removePhpChars($columns[$i]->Field));
							array_push($currentTable["errors"],$error);
							$fieldsOk = false;
						}
						$contentM .= '    protected $__'.$columns[$i]->Field.';'."\n";
					}
					
					// If all ok with special chars for the current model
					if ($fieldsOk)
					{
						$contentM .= '    protected $_table = "'.$tableName.'";'."\n".
									 '    protected $_db = "'.$db.'";'."\n".
									 '    protected $_primaryKey = "'.$primaryKey.'";'."\n".
									 '    public $_typeErrors = array();'."\n".
									 ''."\n".
									 '    /**'."\n".
								     '     * Constructor '.$className.''."\n".
								     '     * @author SillySmart'."\n".
								     '     * @copyright SillySmart'."\n".
								     '     * @param bool $mutlilanguage true if multilanguage content, else false'."\n".
								     '     */'."\n".
									 '    public function __construct($multilanguage='.$multiLanguage.')'."\n".
									 '    {'."\n".
									 '        parent::__construct($multilanguage);'."\n".
									 '    }'."\n\n";
						
						for($i=0 ; $i<$count=count($columns) ; $i++)
						{
							if ($columns[$i]->Key != "PRI")
							{
								$column = $columns[$i]->Field;
								$columnType = (strpos($columns[$i]->Type, "(")) ? SLS_String::substrBeforeFirstDelimiter(strtolower($columns[$i]->Type), "(") : $columns[$i]->Type;
								$functionName = "set".SLS_String::fullTrim(ucwords(SLS_String::stringToUrl(str_replace("_"," ",$column)," ",false)),"");
								
								$contentM .= '    /**'."\n".
										     '     * Set the value of '.$column.''."\n".
										     '     * Errors can be catched with the public variable $this->_typeErrors[\''.$column.'\']'."\n".
										     '     * @author SillySmart'."\n".
										     '     * @copyright SillySmart'."\n".
										     '     * @param '.$arrayConvertTypes[$columnType].' $value'."\n".
										     '     * @return bool true if updated, false if not'."\n".
										     '     */'."\n".
											 '    public function '.$functionName.'($value';
								
								if ($columns[$i]->Default !== null)
									$contentM .= '="'.SLS_String::addSlashesToString($columns[$i]->Default,false).'"';
									
								$contentM .= ')'."\n";
								$contentM .= '    {'."\n";
								
								// Recover Fk
								$res = $xmlFk->getTagsByAttributes("//sls_configs/entry",array("tableFk","columnFk"),array($db."_".$tableName,$column));
								if (!empty($res))
								{
									if ($columns[$i]->Null == "YES")
									{
										$contentM .= '        if (empty($value))'."\n".
													 '        {'."\n".
													 '            $this->__set(\''.$column.'\', $value);'."\n".
													 '            return true;'."\n".
													 '        }'."\n";
									}								
									$tableTm = substr($res,(strpos($res,'tablePk="')+9),(strpos($res,'"/>')-(strpos($res,'tablePk="')+9)));							
									$tablePk = SLS_String::substrAfterFirstDelimiter($tableTm,"_");
									$dbPk 	 = SLS_String::substrBeforeFirstDelimiter($tableTm,"_");
									$contentM .= '        $this->_generic->useModel("'.SLS_String::tableToClass($tablePk).'","'.$dbPk.'");'."\n".
												 '        $object = new '.ucfirst($dbPk).'_'.SLS_String::tableToClass($tablePk).'();'."\n".
												 '        if ($object->getModel($value) === false)'."\n".										 
												 '        {'."\n".
												 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_KEY";'."\n".
												 '            return false;'."\n".
												 '        }'."\n".										 
												 '        $this->__set(\''.$column.'\', $value);'."\n".
												 '        return true;'."\n".
												'    }'."\n\n";
								}
								
								// If not a fk
								else
								{
									$result = $xmlType->getTags("//sls_configs/entry[@table='".$db."_".$tableName."' and @column='".$column."']");
									if (!empty($result))
									{
										$type = "";
										$array = array('password','email','ip','url','file_all','file_img');
										for($j=0 ; $j<count($array) ; $j++)
										{
											$result = $xmlType->getTags("//sls_configs/entry[@table='".$db."_".$tableName."' and @column='".$column."' and @type='".$array[$j]."']");
											if (!empty($result))
											{
												$type = $array[$j];
												switch($type)
												{
													case "password":
														$contentM .= '        $value = sha1($value);'."\n";
														break;											
													case "email":
														$contentM .= '        if (!SLS_String::validateEmail($value))'."\n";
														$contentM .= '        {'."\n".
															 		 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
															 		 '            return false;'."\n".
															 		 '        }'."\n\n";
														break;
													case "url":
														$contentM .= '        if (!SLS_String::isValidUrl($value))'."\n";
														$contentM .= '        {'."\n".
															 		 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
															 		 '            return false;'."\n".
															 		 '        }'."\n\n";
														break;
													case "ip":
														$contentM .= '        if (empty($value))'."\n".
															 		 '            $value = $_SERVER["REMOTE_ADDR"];'."\n\n";
														$contentM .= '        if (!SLS_String::isIp($value))'."\n";
														$contentM .= '        {'."\n".
															 		 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
															 		 '            return false;'."\n".
															 		 '        }'."\n\n";
														break;
													case "file_all":
														$contentM .= '        if (is_array($value))'."\n".
																	 '        {'."\n";
														if ($columns[$i]->Null == "YES")
															$contentM .= '            if ($value["error"] == 4)'."\n".
																		 '            {'."\n".
																		 '                $this->__set(\''.$column.'\',(empty($this->{__.$column})) ? "" : $this->{__.$column});'."\n".
																		 '                return true;'."\n".
																		 '            }'."\n";
														$contentM .= '            if ($value["error"] == 1 || $value["error"] == 2)'."\n".
																	 '            {'."\n".
																	 '                $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_SIZE";'."\n".
																	 '                return false;'."\n".
																	 '            }'."\n".
																	 '            else'."\n".
																	 '            {'."\n".
																	 '                try {'."\n".
																	 '                    $token = substr(md5(time().substr(sha1(microtime()),0,rand(5,12))),mt_rand(1,20),10);'."\n".
																	 '                    $fileName = SLS_String::substrBeforeLastDelimiter($value["name"],".")."_".$token.".".SLS_String::substrAfterLastDelimiter($value["name"],".");'."\n".
																	 '                    rename($value["tmp_name"],$this->_generic->getPathConfig("files").$fileName);'."\n".
																	 '                    $value = $fileName;'."\n".
																	 '                }'."\n".
																	 '                catch (Exception $e) {'."\n".
																	 '                    $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
																	 '                    return false;'."\n".
																	 '                }'."\n".
																	 '            }'."\n".
																	 '        }'."\n\n";
														break;
													case "file_img":
														$contentM .= '        if (is_array($value))'."\n".
																	 '        {'."\n";
														if ($columns[$i]->Null == "YES")
															$contentM .= '            if ($value["error"] == 4)'."\n".
																		 '            {'."\n".
																		 '                $this->__set(\''.$column.'\',(empty($this->{__.$column})) ? "" : $this->{__.$column});'."\n".
																		 '                return true;'."\n".
																		 '            }'."\n";
														$contentM .= '            if ($value["error"] == 1 || $value["error"] == 2)'."\n".
																	 '            {'."\n".
																	 '                $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_SIZE";'."\n".
																	 '                return false;'."\n".
																	 '            }'."\n".
																	 '            else'."\n".
																	 '            {'."\n".
																	 '                try {'."\n".
																	 '                    $tmpName = SLS_String::substrBeforeLastDelimiter($value["tmp_name"],"/")."/".$value["name"];'."\n".
																	 '                    $token = substr(md5(time().substr(sha1(microtime()),0,rand(5,12))),mt_rand(1,20),10);'."\n".
																	 '                    $fileName = SLS_String::substrBeforeLastDelimiter($value["name"],".")."_".$token;'."\n".
																	 '                    @rename($value["tmp_name"],$tmpName);'."\n".
														 			 '                    $extension = pathinfo($tmpName, PATHINFO_EXTENSION);'."\n\n".
																	 '                    // Check img'."\n".
																	 '                    $img = new SLS_Image($tmpName);'."\n".
																	 '                    if ($img->getParam("existed"))'."\n".
																	 '                    {'."\n".
																	 '                        $img->transform("","",$this->_generic->getPathConfig("files").$fileName.".".$extension,$extension);'."\n\n".													 
																	 '                        // Check thumbs'."\n".
																	 '                        $xmlType = new SLS_XMLToolbox(file_get_contents($this->_generic->getPathConfig("configSls")."/types.xml"));'."\n".
																	 '                        $result = array_shift($xmlType->getTagsAttribute("//sls_configs/entry[@table=\'".$this->getDatabase()."_".$this->getTable()."\' and @column=\'".'.$column.'."\' and @type=\'file_img\']","thumbs"));'."\n".
																	 '                        $thumbs = unserialize(str_replace("||#||",\'"\',$result["attribute"]));'."\n".
																	 '                        if (!empty($thumbs))'."\n".
																	 '                        {'."\n".
																	 '                            for($i=0 ; $i<$count=count($thumbs) ; $i++)'."\n".
																	 '                            {'."\n".
																	 '                                $img->transform($thumbs[$i]["width"],$thumbs[$i]["height"],$this->_generic->getPathConfig("files").$fileName.$thumbs[$i]["suffix"].".".$extension,$extension);'."\n".
																	 '                            }'."\n".
																	 '                        }'."\n".
																	 '                    }'."\n".
																	 '                    else'."\n".
																	 '                    {'."\n".
																	 '                        $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
																	 '                        return false;'."\n".
																	 '                    }'."\n".
																	 '                    $value = $fileName.".".$extension;'."\n".
																	 '                }'."\n".
																	 '                catch (Exception $e) {'."\n".
																	 '                    $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
																	 '                    return false;'."\n".
																	 '                }'."\n".
																	 '            }'."\n".
																	 '        }'."\n\n";
														break;
												}
												break;
											}
										}
									}
									 			 
									// Not Nullable
									if ($columns[$i]->Null == "NO")
									{
										$contentM .= '        if ($value === "")'."\n".
													 '        {'."\n".
													 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_EMPTY";'."\n".
													 '            return false;'."\n".
													 '        }'."\n\n";
									
										$contentM .= '        if (is_null($value))'."\n".
													 '        {'."\n".
													 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_NULL";'."\n".
													 '            return false;'."\n".
													 '        }'."\n\n";
									}
									
									// Check change
									$contentM .= '        if ($this->__'.$column.' == $value)'."\n".								 
									 			 '            return true;'."\n\n";
									
									// Unique
									if ($columns[$i]->Key == "UNI")
									{
										$contentM .= '        if (!$this->isUnique(\''.SLS_String::addSlashes($column, 'QUOTES').'\',$value))'."\n".
													 '        {'."\n".
													 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_UNIQUE";'."\n".
													 '            return false;'."\n".
													 '        }'."\n\n";
									}
									
									if (($columnType == "float" || $columnType == "double" || $columnType == "decimal") && (strpos($columns[$i]->Type, "(")))
									{
										$length = SLS_String::substrBeforeFirstDelimiter(SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ')'), '('), ",");
										$contentM .= '        $decimal = (strpos($value, \',\')) ? str_replace(\',\', \'.\', $value) : (!strpos($value, \'.\')) ? $value.\'.0\' : $value;'."\n".									
													 '        if (!is_float($decimal))'."\n".
													 '        {'."\n".
													 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
													 '            return false;'."\n".
													 '        }'."\n\n".
													 '        if ((strlen($decimal)-1) > '.$length.')'."\n".
													 '        {'."\n".
													 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_LENGTH";'."\n".
													 '            return false;'."\n".
													 '        }'."\n\n";
									}
									else if ($columnType == "enum" || $columnType == "set")
									{
										
										$values = SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, "')"), "('");
										
										$contentM .= '        $values = explode("\',\'", "'.str_replace("''", "\'", $values).'");'."\n".
													 '        if (!in_array($value, $values))'."\n".
													 '        {'."\n".
													 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_CONTENT";'."\n".
													 '            return false;'."\n".
													 '        }'."\n\n";
													 
									}
									else 
									{										
										if (strpos($columns[$i]->Type, "("))
										{
											$length = SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($columns[$i]->Type, ")"), "(");
											$contentM .= '        if (strlen($value) > '.$length.')'."\n".
														 '        {'."\n".
														 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_LENGTH";'."\n".
														 '            return false;'."\n".
														 '        }'."\n\n";
										}
										if(SLS_String::endsWith($columnType, "int"))
										{
											$contentM .= '        if (!is_numeric($value))'."\n".
														 '        {'."\n".
														 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
														 '            return false;'."\n".
														 '        }'."\n\n";
										}
										else if ($columnType == "date" || $columnType == "datetime" || $columnType == "timestamp")
										{
											switch ($columnType)
											{
												case "date":
													$contentM .= '        if (!SLS_Date::isDate($value))'."\n";
													break;
												case "datetime":
													$contentM .= '        if (!SLS_Date::isDateTime($value))'."\n";
													break;
												case "timestamp":
													$contentM .= '        if ((!SLS_Date::isTimestamp($value) && $value != "CURRENT_TIMESTAMP"))'."\n";
													break;
											}
											$contentM .= '        {'."\n".
														 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
														 '            return false;'."\n".
														 '        }'."\n\n";
										}
										else if ($columnType == "time" || $columnType == "year")
										{
											switch ($columnType)
											{
												case "time":
													$contentM .= '        if (strpos(\':\', $value) && substr_count($value, \':\') != 2)'."\n".
					 											 '        {'."\n".
					 											 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
																 '            return false;'."\n".
																 '        }'."\n\n".
																 '        $check = explode(\':\', $value);'."\n".	
																 '		  if (count($check) == 1 && !is_numeric($check[0]))'."\n".	
																 '        {'."\n".
					 											 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
																 '            return false;'."\n".
																 '        }'."\n".
																 '		  else if ((count($check) > 1) && (!is_numeric($check[0]) || (!is_numeric($check[1]) || strlen($check[1]) > 2) || (!is_numeric($check[2]) || strlen($check[2]) > 2)))'."\n".	
																 '        {'."\n".
					 											 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
																 '            return false;'."\n".
																 '        }'."\n\n";
													break;
												case "year":
													$contentM .= '        if (!mktime("", "", "", "", "", $value))'."\n".
					 											 '        {'."\n".
					 											 '            $this->_typeErrors[\''.SLS_String::addSlashes($column, 'QUOTES').'\'] = "E_TYPE";'."\n".
																 '            return false;'."\n".
																 '        }'."\n\n";
													break;
											}
										}
										
									}
									$contentM .= 	'        $this->__set(\''.$column.'\', $value);'."\n".
													'        return true;'."\n".
													'    }'."\n\n";
								}
							}						
						}
									  
						$contentM .= '}'."\n".
									 '?>';						
						$status = touch($this->_generic->getPathConfig("models")."/".$fileName);
						if ($status)					
							file_put_contents($this->_generic->getPathConfig("models").$fileName,$contentM);
						
						// Create SQL
						$fileNameS = ucfirst($db).".".SLS_String::tableToClass($table).".sql.php";
						$contentS = '<?php'."\n".
								   '/**'."\n".
								   '* Object '.$className.'Sql'."\n".
								   '* @author SillySmart'."\n".
								   '* @copyright SillySmart'."\n".
								   '* @package Mvc.Models.Objects'."\n".
								   '* @see Sls.Models.Core.SLS_FrontModel'."\n".
								   '* @since 1.0'."\n".
								   '*/'."\n".
								   'class '.$className.'Sql extends SLS_FrontModelSql'."\n".
								   '{'."\n".
								   ''."\n".
								   '}'."\n".
								   '?>';
						if ($status)
							$status2 = touch($this->_generic->getPathConfig("modelsSql")."/".$fileNameS);
						if ($status2)					
							file_put_contents($this->_generic->getPathConfig("modelsSql")."/".$fileNameS,$contentS);					 
					}
					else					
						array_push($errors,$currentTable);					
				}
			}
			// If no errors
			if (empty($errors))
			{
				$controllers = $this->_generic->getTranslatedController("SLS_Bo","Models");
				$this->_generic->redirect($controllers['controller']."/".$controllers['scontroller']);
			}
			else
			{	
				// Get all models
				$models = array();
				$handle = opendir($this->_generic->getPathConfig("models"));
				while (false !== ($file = readdir($handle))) 			
					if (!is_dir($this->_generic->getPathConfig("models")."/".$file) && substr($file, 0, 1) != ".") 
					{
						$modelExploded = explode(".",$file);
						array_push($models,strtolower($modelExploded[0]).".".$modelExploded[1]);
					}
					
				// Form errors
				$xml->startTag("errors");
				for($i=0 ; $i<$count=count($errors) ; $i++)
				{
					$xml->startTag("error");
					$xml->addFullTag("table",SLS_String::substrAfterFirstDelimiter($errors[$i]["table"],"."),true);
					$xml->addFullTag("db",SLS_String::substrBeforeFirstDelimiter($errors[$i]["table"],"."),true);
					$xml->startTag("columns");
					for($j=0 ; $j<$count2=count($errors[$i]["errors"]) ; $j++)
					{
						$xml->startTag("column");
						$xml->addFullTag("old",$errors[$i]["errors"][$j]["column"],true);
						$xml->addFullTag("new",$errors[$i]["errors"][$j]["column_clean"],true);
						$xml->endTag("column");
					}
					$xml->endTag("columns");
					$xml->endTag("error");
				}
				$xml->endTag("errors");				
			}
		}
		
		// Foreach db
		$dbs = $sql->getDbs();
		$allDbs = array();	
		
		foreach($dbs as $db)
		{
			$allDbs[$db] = array();
			
			// Change db
			$sql->changeDb($db);
			
			// Get all tables
			$tables = $sql->showTables();						
			for($i=0 ; $i<$count=count($tables) ; $i++)
			{
				$allDbs[$db][$tables[$i]->Name] = array("name" 	=> $tables[$i]->Name,
														"existed" => (in_array($db.".".SLS_String::tableToClass($tables[$i]->Name),$models)) ? 'true' : 'false');				
			}
		}
		
		asort($allDbs,SORT_REGULAR);
		
		$xml->startTag("dbs");
		foreach($allDbs as $key => $db)
		{
			asort($db,SORT_REGULAR);
			
			$xml->startTag("db");
			$xml->addFullTag("name",$key,true);
			$xml->startTag("tables");
			foreach($db as $tableCur)
			{
				$xml->startTag("table");				
				$xml->addFullTag("name",$tableCur["name"]);
				$xml->addFullTag("existed",$tableCur["existed"]);
				$xml->endTag("table");
			}
			$xml->endTag("tables");
			$xml->endTag("db");
		}
		$xml->endTag("dbs");
			
		$this->saveXML($xml);
	}
	
}
?>