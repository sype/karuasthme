<?
/**
 * Protected functions for SLS_Bo Controller
 *
 */
class SLS_BoControllerProtected extends SiteProtected 
{
	
	/**
	 * Generate the main Menu
	 *
	 * @param XmlToolBox $xml
	 * @return XmlToolBox
	 */
	protected function makeMenu($xml)
	{
		$xml->startTag('Actions');
			$xml->startTag("Action");
				$xml->addFullTag("name", "Dashboard", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Home'), true);
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Manage Controllers & Actions", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Controllers'), true);
				$xml->startTag('sub');
					$xml->addFullTag("name", "Add a new Action Controller", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'AddController'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "Add a new Static Controller", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'AddStaticController'), true);
				$xml->endTag('sub');
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Manage Templates", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Templates'), true);
				$xml->startTag('sub');
					$xml->addFullTag("name", "Add Template", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'AddTemplate'), true);
				$xml->endTag('sub');
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Manage Models", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Models'), true);
				$xml->startTag('sub');
					$xml->addFullTag("name", "Generate Models", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'GenerateModels'), true);
				$xml->endTag('sub');
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Manage Langs", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Langs'), true);
				$xml->startTag('sub');
					$xml->addFullTag("name", "Add Lang", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'AddLang'), true);
				$xml->endTag('sub');
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "SillySmart Settings", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Settings'), true);
				$xml->startTag('sub');
					$xml->addFullTag("name", "Global Settings", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'GlobalSettings'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "Personnal Settings", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'ProjectSettings'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "SMTP Settings", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'MailSettings'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "Database Settings", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'DataBaseSettings'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "Production on/off", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'ProdSettings'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "Ajax Settings", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'JSSettings'), true);
				$xml->endTag('sub');
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Application Back-office", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Bo'), true);
				$xml->startTag('sub');
					$xml->addFullTag("name", "Generate yours BO", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'GenerateBo'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "Manage your back-office rights", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'ManageRights'), true);
				$xml->endTag('sub');
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Manage Plugins", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Plugins'), true);
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Generate Sitemap", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'GenerateSiteMap'), true);
			$xml->endTag("Action");			
			$xml->startTag("Action");
				$xml->addFullTag("name", "View Logs", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'LogsMenu'), true);
				$xml->startTag('sub');
					$xml->addFullTag("name", "Production Logs", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Logs'), true);
				$xml->endTag('sub');
				$xml->startTag('sub');
					$xml->addFullTag("name", "Mail Logs", true);
					$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'LogsMail'), true);
				$xml->endTag('sub');
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Update SillySmart", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'Updates'), true);
			$xml->endTag("Action");
			$xml->startTag("Action");
				$xml->addFullTag("name", "Reset SillySmart", true);
				$xml->addFullTag("link", $this->_generic->getFullPath('SLS_Bo', 'ResetSLS'), true);
			$xml->endTag("Action");
		$xml->endTag('Actions');
		return $xml;
	}
	
	/**
	 * Check if the request is done by a authentificated people
	 *
	 * @return array or false
	 */
	protected function hasAuthorative()
	{
		$session = $this->_generic->getObjectSession();
		$controllers = $this->_generic->getControllersXML();
		$arrayUser = array();	
		$token = $this->_http->getParam('token');
		
		$sessionToken = substr(substr(sha1($this->_generic->getSiteConfig("privateKey")),12,31).substr(sha1($this->_generic->getSiteConfig("privateKey")),4,11),6);
		
		if ($session->getParam('SLS_SESSION_VALID_'.$sessionToken) == 'true' || ($_SERVER["REMOTE_ADDR"] == $_SERVER["SERVER_ADDR"] && $token == sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3))))
		{
			$arrayUser['user'] = $session->getParam('SLS_SESSION_USER_'.$sessionToken);
			$arrayUser['pass'] = $session->getParam('SLS_SESSION_PASS_'.$sessionToken);
			$arrayUser['level'] = $session->getParam('SLS_SESSION_LEVEL_'.$sessionToken);
			return $arrayUser;
		}
		else 
		{
			$this->_generic->redirect(array_shift($controllers->getTags("//controllers/controller[@name='SLS_Bo']/controllerLangs/controllerLang"))."/".array_shift($controllers->getTags("//controllers/controller[@name='SLS_Bo']/scontrollers/scontroller[@name='Index']/scontrollerLangs/scontrollerLang")));			
		}
	}
	
	/**
	 * Return all the user models
	 *
	 * @return array list of all models
	 */
	protected function getAllModels()
	{
		// Get all models
		$models = array();
		$handle = opendir($this->_generic->getPathConfig("models"));
		while (false !== ($file = readdir($handle)))					
			if (!is_dir($this->_generic->getPathConfig("models").$file) && substr($file, 0, 1) != ".") 			
			{
				$modelExploded = explode(".",$file);
				array_push($models,strtolower($modelExploded[0]).".".$modelExploded[1]);
			}
		return $models;
	}
	
	/**
	 * If action bo already exist
	 *
	 * @param string $model the model to check
	 * @param string $alias the alias of the db
	 * @return bool true if already exist, else false
	 */
	protected function boActionExist($model,$alias)
	{		
		$controllersXML = $this->_generic->getControllersXML();
		$test = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='List".ucfirst($alias)."_".SLS_String::tableToClass($model)."']"));		
		return (empty($test)) ? false : true;
	}
	
	/**
	 * Get all bo actions for one model
	 *
	 * @param string $model the model to check
	 * @param string $alias the alias of the db
	 * @return array $actions the list of bo actions
	 */
	protected function getActionsBo($model,$alias)
	{
		$actions = array();
		$controllersXML = $this->_generic->getControllersXML();
		
		$list = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='List".ucfirst($alias)."_".SLS_String::tableToClass($model)."']"));
		if (!empty($list))
			array_push($actions,"List".ucfirst($alias)."_".SLS_String::tableToClass($model));		
		$add = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='Add".ucfirst($alias)."_".SLS_String::tableToClass($model)."']"));
		if (!empty($add))
			array_push($actions,"Add".ucfirst($alias)."_".SLS_String::tableToClass($model));
		$edit = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='Modify".ucfirst($alias)."_".SLS_String::tableToClass($model)."']"));
		if (!empty($edit))
			array_push($actions,"Modify".ucfirst($alias)."_".SLS_String::tableToClass($model));
		$del = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='Delete".ucfirst($alias)."_".SLS_String::tableToClass($model)."']"));
		if (!empty($del))
			array_push($actions,"Delete".ucfirst($alias)."_".SLS_String::tableToClass($model));
		return $actions;
	}	
	
	/**
	 * Create a new action list in bo
	 *
	 * @param string $controller generic controller name
	 * @param string $model the model you want to create bo	 
	 * @param string $alias the alias of the db
	 * @param array $columns the columns list you want to display
	 * @param array $filters the columns list you want to filters
	 * @param array $group the column on which you want to group by
	 * @param array $order the column & direction of order
	 * @param array $limit the bounds limit
	 * @param string $join the natural join table
	 * @param array $actions list of actions you want to map with action list (default: all)	 
	 */
	protected function createActionBoList($controller,$model,$alias,$columns=array(),$filters=array(),$group=array(),$order=array(),$limit=array(),$join="",$actions=array("add"=>true,"modify"=>true,"delete"=>true))
	{		
		$langs = $this->_generic->getObjectLang()->getSiteLangs();
		
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controller),
					 	2 => array("key" 	=> "actionName",
				  				   "value" 	=> "List".ucfirst($alias)."_".SLS_String::tableToClass($model)),
	  				   	3 => array("key" 	=> "token",
				  				   "value" 	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
				  		4 => array("key"	=> "indexes",
				  				   "value"	=> "noindex, nofollow")				  		
				  				   )
					    );
		foreach($langs as $lang)
		{
			$tmpParam = array("key" 	=> $lang."-action",
							  "value" 	=> "List".ucfirst($alias)."_".SLS_String::tableToClass($model));
			$tmpTitle = array("key" 	=> $lang."-title",
							  "value" 	=> $model."_listing");
			array_push($params,$tmpParam);
			array_push($params,$tmpTitle);
		}
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "AddAction",
													  $params,
													  true));
		$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controller."/List".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php");
		
		$this->_generic->useModel($model,$alias,"user");
		$class = ucfirst($alias)."_".SLS_String::tableToClass($model);
		$object = new $class();
		$properties = $object->getParams();
		
		// Columns to display
		if (empty($columns))
		{
			$columnsD = 'array(';
			$i=0;
			foreach($properties as $key => $value)
			{
				$tmp = array("column" => $key, "description" => "");
				if ($i>0)
					$columnsD .= '                              ';
				$columnsD .= ''.$i.' => array("column" => "'.$key.'", "description" => "")';
				if ($i<(count($properties)-1))
					$columnsD .= ','."\n";
				$i++;
			}
			$columnsD .= ');';
		}
		else
		{
			$columnsD = 'array(';
			$i=0;
			foreach($columns as $key)
			{
				$tmp = array("column" => $key, "description" => "");
				if ($i>0)
					$columnsD .= '                              ';
				$columnsD .= ''.$i.' => array("column" => "'.$key.'", "description" => "")';
				if ($i<(count($columns)-1))
					$columnsD .= ','."\n";
				$i++;
			}
			$columnsD .= ');';
		}
		// /Columns to display
		
		// Columns to filter
		if (empty($filters))
		{
			$columnsF = 'array(';
			$i=0;
			foreach($properties as $key => $value)
			{
				$tmp = array("column" => $key, "description" => "");
				if ($i>0)
					$columnsF .= '                              ';
				$columnsF .= ''.$i.' => array("column" => "'.$key.'", "description" => "")';
				if ($i<(count($properties)-1))
					$columnsF .= ','."\n";
				$i++;
			}
			$columnsF .= ');';
		}
		else
		{
			$columnsF = 'array(';
			$i=0;
			foreach($filters as $key)
			{
				$tmp = array("column" => $key, "description" => "");
				if ($i>0)
					$columnsF .= '                              ';
				$columnsF .= ''.$i.' => array("column" => "'.$key.'", "description" => "")';
				if ($i<(count($filters)-1))
					$columnsF .= ','."\n";
				$i++;
			}
			$columnsF .= ');';
		}
		// /Columns to filter	
		
		$controllersXML = $this->_generic->getControllersXML();
		$controllerBo = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/@name"));
		
		$action = 	'        // Params'."\n".
					'        $arrayFilters = '.$columnsF.''."\n".
					'        $arrayColumns = '.$columnsD.''."\n".
					'        // /Params'."\n".
					'        '."\n".
					'        // Check account rights'."\n".
					'        $right = SLS_BoRights::isAuthorized("List");'."\n".
					'        if ($right == -1)'."\n".
					'            $this->_generic->redirect("'.$controllerBo.'/Connection.sls");'."\n".
					'        // /Check account rights'."\n".
					'        '."\n".
					'        $boListing = new SLS_BoListing($this,"'.$model.'","'.$alias.'");'."\n".
					'        $boListing->setCurrentUrl("'.$controller.'","List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'");'."\n".
					'        $boListing->setAllowedFilters($arrayFilters);'."\n".
					'        $boListing->setColumnsToShow($arrayColumns);'."\n";
		
		// Order
		if (!empty($order))
		{
			$action .= '        $boListing->setDefaultOrder("'.$order["column"].'","'.$order["order"].'");'."\n";
		}
		// /Order
		
		// Group by
		if (!empty($group))
		{
			$strGroup = "array(";
			$first = true;
			foreach($group as $col)
			{
				if (!$first)
					$strGroup .= ", ";
				$strGroup .= "'".$col."'";
				$first = false;
			}
			$strGroup .= ")";
			$action .= '        $boListing->setDefaultGroupBy('.$strGroup.');'."\n";
		}
		// /Group by
		
		// Limit
		if (!empty($limit))
		{
			$action .= '        $boListing->setDefaultLimit("'.$limit["start"].'","'.$limit["length"].'");'."\n";
		}
		// /Limit
		
		// Join
		if (!empty($join))
		{
			$action .= '        $boListing->setJoin(array("'.$join.'"));'."\n";			
		}
		// /Join
		
		// Actions
		if ($actions["delete"])
		{			
			$action .=  '        if (SLS_BoRights::boActionExist("'.$alias.'","'.$model.'","Delete") && SLS_BoRights::isAuthorized("Delete"))'."\n";
			$action .=	'            $boListing->addActionDelete("'.$controller.'","Delete'.ucfirst($alias)."_".SLS_String::tableToClass($model).'");'."\n";
		}
		if ($actions["modify"])
		{
			$action .=  '        if (SLS_BoRights::boActionExist("'.$alias.'","'.$model.'","Modify") && SLS_BoRights::isAuthorized("Modify"))'."\n";
			$action .=	'            $boListing->addActionModify("'.$controller.'","Modify'.ucfirst($alias)."_".SLS_String::tableToClass($model).'");'."\n";
		}
		if ($actions["add"])
		{
			$action .=  '        if (SLS_BoRights::boActionExist("'.$alias.'","'.$model.'","Add") && SLS_BoRights::isAuthorized("Add"))'."\n";
			$action .=	'            $boListing->addActionAdd("'.$controller.'","Add'.ucfirst($alias)."_".SLS_String::tableToClass($model).'");'."\n";
		}
		// /Actions
				
		$action .=	'        $boListing->constructList();'."\n".
					'        $xml = $this->getXML();'."\n".
					'        $boActions = SLS_BoRights::getDistinctActions();'."\n".
        		    '        $xml->startTag("bo_actions");'."\n".
        		    '        for($i=0 ; $i<$count=count($boActions) ; $i++)'."\n".
        		    '        {'."\n".
        		    '            $xml->startTag("bo_action");'."\n".
        		    '            foreach($boActions[$i] as $key => $value)'."\n".
        		    '                $xml->addFullTag($key,$value,true);'."\n".
        		    '            $xml->endTag("bo_action");'."\n".
        		    '        }'."\n".
        		    '        $xml->endTag("bo_actions");'."\n\n".
        		    '        // Export'."\n".
			        '        $http = $this->_generic->getObjectHttpRequest();'."\n".
			        '        $controllersXML = $this->_generic->getControllersXML();'."\n".
					'        $controller = array_shift($controllersXML->getTags("//controllers/controller[@isBo=\'true\']/@name"));'."\n".
					'        $controllers = $this->_generic->getTranslatedController($controller,"Export");'."\n".
					'        $column = ($http->getParam("column") == "") ? "|sls_empty|" : $http->getParam("column");'."\n".
					'        $order = ($http->getParam("order") == "") ? "|sls_empty|" : $http->getParam("order");'."\n".
					'        $group_filter = ($http->getParam("group_filter") == "") ? "|sls_empty|" : $http->getParam("group_filter");'."\n".		
			        '        $queryString = $controllers["protocol"]."://".$this->_generic->getSiteConfig("domainName")."/".$controllers["controller"]."/".$controllers["scontroller"]."/column/".$column."/order/".$order;'."\n".
			        '        foreach($http->getParams() as $key => $value)'."\n".
					'        	if (SLS_String::startsWith($key,"filter_"))'."\n".
					'        	{'."\n".
					'        		if (empty($value))'."\n".
					'        			$value = "|sls_empty|";'."\n".
					'        		$queryString .= "/".$key."/".$value;'."\n".
					'        	}'."\n".
					'        for($i=0 ; $i<$count=count($arrayColumns) ; $i++)'."\n".
					'        	$queryString .= "/Export_Column_".$i."/".$arrayColumns[$i]["column"];'."\n".
					'        $queryString .= "/Export_Model/'.$object->getTable().'/Export_Db/'.$alias.'";'."\n";
		if (!empty($join))
			$action .= '        $queryString .= "/Export_Join/'.$join.'";'."\n";			
		$action	.=	'        $xml->addFullTag("url_export",$queryString,true);'."\n".
			        '        // /Export'."\n\n".
			        '        // Referrer'."\n".
        			'        $http = $this->_generic->getObjectHttpRequest();'."\n".
					'        $params = $http->getParams();'."\n".
					'        $strPost = "";'."\n".
					'        foreach($params as $key => $value)'."\n".
					'        	if ($key != "mode" && $key != "smode" && !empty($value))'."\n".
					'        		$strPost .= $key."/".$value."/";'."\n".
					'        $this->_generic->getObjectSession()->setParam("SLS_PREVIOUS_POST_List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'",$strPost);'."\n".
			        '        // /Referrer'."\n\n".
        		    '        $this->saveXML($xml);'."\n";
       	
		$source = substr($source,0,strpos($source,"action()")+8)."\n".
				  '    {'."\n".
				  $action.
				  '    }'."\n".
				  '}'."\n".
				  '?>';
		file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/List".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php",$source);
		$sourceXsl = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
					 '    <xsl:template name="List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'">'."\n".
					 '        <xsl:call-template name="BoMenu" />'."\n".
					 '        <xsl:call-template name="BoListing" />'."\n".
					 '    </xsl:template>'."\n".
					 '</xsl:stylesheet>';
		file_put_contents($this->_generic->getPathConfig("viewsBody").$controller."/List".ucfirst($alias)."_".SLS_String::tableToClass($model).".xsl",$sourceXsl);
	}
	
	/**
	 * Create a new action add in bo
	 *
	 * @param string $controller generic controller name
	 * @param string $model the model you want to create bo
	 * @param string $alias the alias of the db
	 */
	protected function createActionBoAdd($controller,$model,$alias)
	{
		$langs = $this->_generic->getObjectLang()->getSiteLangs();
		
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controller),
					 	2 => array("key" 	=> "actionName",
				  				   "value" 	=> "Add".ucfirst($alias)."_".SLS_String::tableToClass($model)),
				  		3 => array("key"	=> "token",
				  					"value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
				  		4 => array("key"	=> "indexes",
				  				   "value"	=> "noindex, nofollow")
				  			)
					    );
		foreach($langs as $lang)
		{
			$tmpParam = array("key" 	=> $lang."-action",
							  "value" 	=> "Add".ucfirst($alias)."_".SLS_String::tableToClass($model));
			$tmpTitle = array("key" 	=> $lang."-title",
							  "value" 	=> $model."_adding");
			array_push($params,$tmpParam);
			array_push($params,$tmpTitle);
		}
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "AddAction",
													  $params,
													  true));
		$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Add".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php");
		
		
		$action = 	'        $xml = $this->getXML();'."\n".
					'        $boActions = SLS_BoRights::getDistinctActions();'."\n".
        		    '        $xml->startTag("bo_actions");'."\n".
        		    '        for($i=0 ; $i<$count=count($boActions) ; $i++)'."\n".
        		    '        {'."\n".
        		    '            $xml->startTag("bo_action");'."\n".
        		    '            foreach($boActions[$i] as $key => $value)'."\n".
        		    '                $xml->addFullTag($key,$value,true);'."\n".
        		    '            $xml->endTag("bo_action");'."\n".
        		    '        }'."\n".
        		    '        $xml->endTag("bo_actions");'."\n".
					'        '."\n".
					'        // Check account rights'."\n".
					'        $right = SLS_BoRights::isAuthorized("Add");'."\n".
					'        if ($right == -1)'."\n".
					'            $this->_generic->redirect("'.$controller.'/Connection.sls");'."\n".
					'        else if ($right == 0)'."\n".					
					'            $this->_generic->redirect("'.$controller.'/List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'.sls");'."\n".					
					'        // /Check account rights'."\n".
					'        '."\n".
					'        $bo = new SLS_BoAdd($this,"'.$model.'","'.$alias.'",$xml);'."\n".					
					'        $bo->setUrlToRedirect($this->_generic->getFullPath("'.$controller.'","List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'"));'."\n".
					'        $this->saveXML($bo->constructAdd());'."\n";
		$source = substr($source,0,strpos($source,"action()")+8)."\n".
				  '    {'."\n".
				  $action.
				  '    }'."\n".
				  '}'."\n".
				  '?>';
		file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Add".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php",$source);
		$sourceXsl = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
					 '    <xsl:template name="Add'.ucfirst($alias)."_".SLS_String::tableToClass($model).'">'."\n".
					 '        <xsl:call-template name="BoMenu" />'."\n".
					 '        <xsl:call-template name="BoAdd" />'."\n".
					 '    </xsl:template>'."\n".
					 '</xsl:stylesheet>';
		file_put_contents($this->_generic->getPathConfig("viewsBody").$controller."/Add".ucfirst($alias)."_".SLS_String::tableToClass($model).".xsl",$sourceXsl);
	}
	
	/**
	 * Create a new action modify in bo
	 *
	 * @param string $controller generic controller name
	 * @param string $model the model you want to create bo	
	 * @param string $alias the alias of the db 
	 */
	protected function createActionBoModify($controller,$model,$alias)
	{
		$langs = $this->_generic->getObjectLang()->getSiteLangs();
		
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controller),
					 	2 => array("key" 	=> "actionName",
				  				   "value" 	=> "Modify".ucfirst($alias)."_".SLS_String::tableToClass($model)),
				  		3 => array("key"	=> "token",
				  					"value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
				  		4 => array("key"	=> "indexes",
				  				   "value"	=> "noindex, nofollow")
				  			)
					    );
		foreach($langs as $lang)
		{
			$tmpParam = array("key" 	=> $lang."-action",
							  "value" 	=> "Modify".ucfirst($alias)."_".SLS_String::tableToClass($model));
			$tmpTitle = array("key" 	=> $lang."-title",
							  "value" 	=> $model."_modifying");
			array_push($params,$tmpParam);
			array_push($params,$tmpTitle);
		}
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "AddAction",
													  $params,
													  true));
		$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Modify".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php");
		
		
		$action = 	'        $xml = $this->getXML();'."\n".
					'        $boActions = SLS_BoRights::getDistinctActions();'."\n".
        		    '        $xml->startTag("bo_actions");'."\n".
        		    '        for($i=0 ; $i<$count=count($boActions) ; $i++)'."\n".
        		    '        {'."\n".
        		    '            $xml->startTag("bo_action");'."\n".
        		    '            foreach($boActions[$i] as $key => $value)'."\n".
        		    '                $xml->addFullTag($key,$value,true);'."\n".
        		    '            $xml->endTag("bo_action");'."\n".
        		    '        }'."\n".
        		    '        $xml->endTag("bo_actions");'."\n\n".
					'        // Check account rights'."\n".
					'        $right = SLS_BoRights::isAuthorized("Modify");'."\n".
					'        if ($right == -1)'."\n".
					'            $this->_generic->redirect("'.$controller.'/Connection.sls");'."\n".
					'        else if ($right == 0)'."\n".					
					'            $this->_generic->redirect("'.$controller.'/List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'.sls");'."\n".					
					'        // /Check account rights'."\n".
					'        '."\n".
					'        $bo = new SLS_BoEdit($this,"'.$model.'","'.$alias.'",$xml);'."\n".
					'        $bo->setUrlToRedirect("'.$controller.'","List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'");'."\n".
					'        $this->saveXML($bo->constructEdit());'."\n";
		$source = substr($source,0,strpos($source,"action()")+8)."\n".
				  '    {'."\n".
				  $action.
				  '    }'."\n".
				  '}'."\n".
				  '?>';
		file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Modify".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php",$source);
		$sourceXsl = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
					 '    <xsl:template name="Modify'.ucfirst($alias)."_".SLS_String::tableToClass($model).'">'."\n".
					 '        <xsl:call-template name="BoMenu" />'."\n".
					 '        <xsl:call-template name="BoEdit" />'."\n".
					 '    </xsl:template>'."\n".
					 '</xsl:stylesheet>';
		file_put_contents($this->_generic->getPathConfig("viewsBody").$controller."/Modify".ucfirst($alias)."_".SLS_String::tableToClass($model).".xsl",$sourceXsl);
	}
	
	/**
	 * Create a new action delete in bo
	 *
	 * @param string $controller generic controller name
	 * @param string $model the model you want to create bo	 
	 * @param string $alias the alias of the db
	 */
	protected function createActionBoDelete($controller,$model,$alias)
	{
		$langs = $this->_generic->getObjectLang()->getSiteLangs();
		
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controller),
					 	2 => array("key" 	=> "actionName",
				  				   "value" 	=> "Delete".ucfirst($alias)."_".SLS_String::tableToClass($model)),
				  		3 => array("key"	=> "token",
				  				   "value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
				  		4 => array("key"	=> "indexes",
				  				   "value"	=> "noindex, nofollow")
				  			)
					    );
		foreach($langs as $lang)
		{
			$tmpParam = array("key" 	=> $lang."-action",
							  "value" 	=> "Delete".ucfirst($alias)."_".SLS_String::tableToClass($model));
			$tmpTitle = array("key" 	=> $lang."-title",
							  "value" 	=> $model."_deleting");
			array_push($params,$tmpParam);
			array_push($params,$tmpTitle);
		}
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "AddAction",
													  $params,
													  true));
		$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Delete".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php");
		
		
		$action = 	'        // Check account rights'."\n".
					'        $right = SLS_BoRights::isAuthorized("Delete");'."\n".
					'        if ($right == -1)'."\n".
					'            $this->_generic->redirect("'.$controller.'/Connection.sls");'."\n".
					'        else if ($right == 0)'."\n".					
					'            $this->_generic->redirect("'.$controller.'/List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'.sls");'."\n".					
					'        // /Check account rights'."\n".
					'        '."\n".
					'        $bo = new SLS_BoDelete($this,"'.$model.'","'.$alias.'");'."\n".
					'        $bo->setUrlToRedirect("'.$controller.'","List'.ucfirst($alias)."_".SLS_String::tableToClass($model).'");'."\n".
					'        $this->saveXML($bo->constructDelete());'."\n";
		$source = substr($source,0,strpos($source,"action()")+8)."\n".
				  '    {'."\n".
				  $action.
				  '    }'."\n".
				  '}'."\n".
				  '?>';
		file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Delete".ucfirst($alias)."_".SLS_String::tableToClass($model).".controller.php",$source);
	}
	
	/**
	 * Create templates for Bo controller
	 *
	 */
	protected function createBoTemplates()
	{
		$tpl1= '<!--'."\n".
			   '  - Global template for your application'."\n".
			   '  - Don\'t change anything between marked delimiter |||dtd:tagName|||'."\n".
			   '  - Beyond you can add additional headers or/and xhtml structure'."\n".
			   '-->'."\n".
			   '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
			   '    <xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes" encoding="|||sls:getCharset|||" />'."\n\n".
			   '    <!-- Variable Builder -->'."\n".
			   '    |||sls:buildUrlVars|||'."\n".
			   '    <!-- /Variable Builder -->'."\n\n".			
			   '    <!-- Generic include -->'."\n".
			   '    |||sls:includeActionFileBody|||'."\n".
			   '    |||sls:includeActionFileHeader|||'."\n".
			   '    |||sls:includeStaticsFiles|||'."\n".
			   '    <!-- /Generic include -->'."\n\n".			
			   '    <xsl:template match="root">'."\n".
		       '        <html xml:lang="|||sls:getLanguage|||" lang="|||sls:getLanguage|||">'."\n".
		       '            <head>'."\n\n".			
		       '                <!-- Generic headers loading -->'."\n".
		       '                <xsl:call-template name="GenerateMetas" />'."\n".
		       '                |||sls:loadUserHeaders|||'."\n".
		       '                |||sls:loadActionFileHeader|||'."\n".
		       '                <!-- /Generic headers loading -->'."\n\n".			                
		       '                <link rel="stylesheet" type="text/css" href="{concat($sls_url_css_core,\'ssBo.css\')}" />'."\n".
		       '                <script type="text/javascript" src="{concat($sls_url_js_core_statics, \'SLS_Statics_compressed.js\')}"></script>'."\n".
		       '                <script type="text/javascript" src="{concat($sls_url_js_core_dyn, \'Selects.js\')}"></script>'."\n".
		       '                <script type="text/javascript" src="{concat($sls_url_js_core_dyn, \'slsBo.js\')}"></script>'."\n\n".			                
		       '            </head>'."\n".
		       '            <body>'."\n\n".
		       '                <div id="bo_header">'."\n".
			   '                    <div class="left">'."\n".
			   '                        <img src="{concat($sls_url_img_core,\'Logos/listing_logo.png\')}" alt="SillySmart BackOffice" title="SillySmart BackOffice" class="logo" />'."\n".
			   '                    </div>'."\n".
			   '                    <div class="right">'."\n".
			   '                        <a href="{//View/bo_actions/bo_action[label=\'Disconnection\']/url}" title="Logout" class="logout">Sign Out</a>'."\n".
			   '                        <xsl:variable name="login"><xsl:choose><xsl:when test="//Statics/Sls/session/params/param[name=\'SLS_BO_USER\']/value != \'\'"><xsl:value-of select="//Statics/Sls/session/params/param[name=\'SLS_BO_USER\']/value" /></xsl:when><xsl:otherwise>SillySmart</xsl:otherwise></xsl:choose></xsl:variable>'."\n".
			   '                        <span class="logged">you are logged in as <strong><xsl:value-of select="$login" /></strong> - </span>'."\n".
			   '                    </div>'."\n".
			   '                </div>'."\n".
		       '                <div id="bo_wrapper">'."\n".
			   '                    <div id="bo_float_wrapper">'."\n".
			   '                        <div id="content_wrapper">'."\n".
			   '                            <h1 class="main">Back-Office <span class="light"><xsl:value-of select="//Statics/Sls/Configs/site/projectName" /></span></h1>'."\n\n".
			   '                            <!-- Generic bodies loading -->'."\n".
			   '                            |||sls:loadCoreBody|||'."\n".
			   '                            |||sls:loadUserBody|||'."\n".
			   '                            |||sls:loadActionFileBody|||'."\n".
			   '                            <!-- /Generic bodies loading -->'."\n\n".
		       '                        </div>'."\n".
			   '                    </div>'."\n".
			   '                </div>'."\n".
		       '            </body>'."\n".
		       '        </html>'."\n".
			   '    </xsl:template>'."\n".
			   '</xsl:stylesheet>';
		file_put_contents($this->_generic->getPathConfig("viewsTemplates")."bo.xsl",$tpl1);
		
		$tpl2= '<!--'."\n".
			   '  - Global template for your application'."\n".
			   '  - Don\'t change anything between marked delimiter |||dtd:tagName|||'."\n".
			   '  - Beyond you can add additional headers or/and xhtml structure'."\n".
			   '-->'."\n".
			   '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
			   '    <xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes" encoding="|||sls:getCharset|||" />'."\n\n".
			   '    <!-- Variable Builder -->'."\n".
			   '    |||sls:buildUrlVars|||'."\n".
			   '    <!-- /Variable Builder -->'."\n\n".			
			   '    <!-- Generic include -->'."\n".
			   '    |||sls:includeActionFileBody|||'."\n".
			   '    |||sls:includeActionFileHeader|||'."\n".
			   '    |||sls:includeStaticsFiles|||'."\n".
			   '    <!-- /Generic include -->'."\n\n".			
			   '    <xsl:template match="root">'."\n".
		       '        <html xml:lang="|||sls:getLanguage|||" lang="|||sls:getLanguage|||">'."\n".
		       '            <head>'."\n\n".			
		       '                <!-- Generic headers loading -->'."\n".
		       '                <xsl:call-template name="GenerateMetas" />'."\n".
		       '                |||sls:loadCoreHeaders|||'."\n".
		       '                |||sls:loadUserHeaders|||'."\n".
		       '                |||sls:loadActionFileHeader|||'."\n".
		       '                <!-- /Generic headers loading -->'."\n\n".			                
		       '                <link rel="stylesheet" type="text/css" href="{concat($sls_url_css_core,\'ssBoLogin.css\')}" />'."\n".
		       '                <script type="text/javascript" src="{concat($sls_url_js_core_statics, \'SLS_Statics_compressed.js\')}"></script>'."\n".
		       '                <script type="text/javascript" src="{concat($sls_url_js_core_dyn, \'slsBo.js\')}"></script>'."\n\n".			                
		       '            </head>'."\n".
		       '            <body>'."\n\n".		       
			   '                <!-- Generic bodies loading -->'."\n".
			   '                |||sls:loadCoreBody|||'."\n".
			   '                |||sls:loadUserBody|||'."\n".
			   '                |||sls:loadActionFileBody|||'."\n".
			   '                <!-- /Generic bodies loading -->'."\n\n".
		       '            </body>'."\n".
		       '        </html>'."\n".
			   '    </xsl:template>'."\n".
			   '</xsl:stylesheet>';
		file_put_contents($this->_generic->getPathConfig("viewsTemplates")."bo_login.xsl",$tpl2);
	}
	
	/**
	 * Create bo action connection
	 *
	 * @param string $controllerBo generic controller name (Bo)
	 */
	protected function createActionBoConnection($controllerBo="")
	{
		$langs = $this->_generic->getObjectLang()->getSiteLangs();		
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controllerBo),
					 	2 => array("key" 	=> "actionName",
				  				   "value" 	=> "Connection"),
				  		3 => array("key"	=> "token",
				  				   "value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
				  		4 => array("key"	=> "indexes",
				  				   "value"	=> "noindex, nofollow")
				  			)
					    );
		foreach($langs as $lang)
		{
			$tmpParam = array("key" 	=> $lang."-action",
							  "value" 	=> "Connection");
			$tmpTitle = array("key" 	=> $lang."-title",
							  "value" 	=> "Connection");
			array_push($params,$tmpParam);
			array_push($params,$tmpTitle);
		}
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "AddAction",
													  $params,
													  true));
		
		$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controllerBo."/Connection.controller.php");
				
		$action = '        $xml = $this->getXML();'."\n".
				  '        $boActions = SLS_BoRights::getDistinctActions();'."\n".
        		  '        $xml->startTag("bo_actions");'."\n".
        		  '        for($i=0 ; $i<$count=count($boActions) ; $i++)'."\n".
        		  '        {'."\n".
        		  '            $xml->startTag("bo_action");'."\n".
        		  '            foreach($boActions[$i] as $key => $value)'."\n".
        		  '                $xml->addFullTag($key,$value,true);'."\n".
        		  '            $xml->endTag("bo_action");'."\n".
        		  '        }'."\n".
        		  '        $xml->endTag("bo_actions");'."\n".
				  '        if ($this->_http->getParam("reload") == "true")'."\n".
				  '        {'."\n".
				  '            $login 	 = $this->_http->getParam("login");'."\n".
				  '            $password = $this->_http->getParam("password");'."\n".
				  '            if (SLS_BoRights::connect($login,$password))'."\n".								  
				  '                $this->_generic->redirect("'.$controllerBo.'/Connection.sls");'."\n".								  
				  '            else'."\n".
				  '            {'."\n".
				  '                $xml->startTag("errors");'."\n".
				  '                $xml->addFullTag("error","Access denied",true);'."\n".
				  '                $xml->endTag("errors");'."\n".
				  '            }'."\n".
				  '        }'."\n".
				  '        if (SLS_BoRights::isLogged())'."\n".
				  '        {'."\n".
				  '            $xml->addFullTag("isLogged","true",true);'."\n".
				  '            if (count($boActions) > 1)'."\n".
				  '                $this->_generic->redirect($boActions[0]["url"]);'."\n".
				  '        }'.
				  '        $this->saveXML($xml);'."\n";
		$source = substr($source,0,strpos($source,"action()")+8)."\n".
				  '    {'."\n".
				  $action.
				  '    }'."\n".
				  '}'."\n".
				  '?>';
		
		file_put_contents($this->_generic->getPathConfig("actionsControllers").$controllerBo."/Connection.controller.php",$source);
				
		$sourceXsl = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
					 '    <xsl:template name="Connection">'."\n".
					 '        <script type="text/javascript">'."\n".
					 '            page = \'login\';'."\n".
				     '        </script>'."\n".
		             '        <div id="login_box_wrapper">'."\n".
					 '            <img src="{concat($sls_url_img_core,\'Logos/login_logo.png\')}" alt="SillySmart BackOffice" title="SillySmart BackOffice" class="logo"/>'."\n".
					 '            <fieldset class="login_form">'."\n".
					 '                <div class="top"></div>'."\n".
					 '                <div class="content">'."\n".
					 '                    <xsl:if test="count(//View/errors/error) &gt; 0">'."\n".
					 '                        <ul class="error">'."\n".
					 '                            <xsl:for-each select="//View/errors/error">'."\n".
					 '                                <li><xsl:value-of select="." /></li>'."\n".
					 '                            </xsl:for-each>'."\n".
					 '                        </ul>'."\n".
					 '                    </xsl:if>'."\n".
					 '                    <form action="#" method="post">'."\n".
					 '                        <input type="hidden" name="reload" value="true" />'."\n".
					 '                        <div class="row">'."\n".
					 '                            <label for="username">Login</label>'."\n".
					 '                            <div class="login_input">'."\n".
					 '                                <input type="text" name="login" id="username" />'."\n".
					 '                            </div>'."\n".
					 '                        </div>'."\n".
					 '                       <div class="row">'."\n".
					 '                           <label for="pass">Password</label>'."\n".
					 '                           <div class="login_input">'."\n".
					 '                               <input type="password" name="password" id="pass" />'."\n".
					 '                           </div>'."\n".
					 '                       </div>'."\n".
					 '                       <div class="row">'."\n".
					 '                           <div class="login_submit">'."\n".
					 '                               <div class="left"></div>'."\n".
					 '                               <div class="middle">'."\n".
					 '                                   <input type="submit" value="Connection" />'."\n".
					 '                               </div>'."\n".
					 '                               <div class="right"></div>'."\n".
					 '                           </div>'."\n".
					 '                       </div>'."\n".
					 '                    </form>'."\n".
					 '                </div>'."\n".
					 '                <div class="foot"></div>'."\n".
					 '            </fieldset>'."\n".
				     '        </div>'."\n".
					 '    </xsl:template>'."\n".
					 '</xsl:stylesheet>';
		file_put_contents($this->_generic->getPathConfig("viewsBody").$controllerBo."/Connection.xsl",$sourceXsl);
	}
	
	/**
	 * Create bo action export
	 *
	 * @param string $controllerBo generic controller name (Bo)
	 */
	protected function createActionBoExport($controllerBo="")
	{
		$langs = $this->_generic->getObjectLang()->getSiteLangs();		
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controllerBo),
					 	2 => array("key" 	=> "actionName",
				  				   "value" 	=> "Export"),
				  		3 => array("key"	=> "token",
				  				   "value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
				  		4 => array("key"	=> "indexes",
				  				   "value"	=> "noindex, nofollow")
				  			)
					    );
		foreach($langs as $lang)
		{
			$tmpParam = array("key" 	=> $lang."-action",
							  "value" 	=> "Export");
			$tmpTitle = array("key" 	=> $lang."-title",
							  "value" 	=> "Export");
			array_push($params,$tmpParam);
			array_push($params,$tmpTitle);
		}
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "AddAction",
													  $params,
													  true));
		
		$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controllerBo."/Export.controller.php");
		
		$action =   '        $http = $this->_generic->getObjectHttpRequest();'."\n".		
					'        $columns = array();'."\n".
					'        $clause = array();'."\n".
					'        $join = array();'."\n".									
					'        // Model + Db alias + Join'."\n".
					'        $model = $http->getParam("Export_Model");'."\n".
					'        $db = $http->getParam("Export_Db");'."\n".
					'        $nJoin = $http->getParam("Export_Join");'."\n".					
					'        if (!empty($nJoin))'."\n".
					'        	array_push($join,$nJoin);'."\n".					
					'        $nbColumns = count(SLS_Sql::getInstance()->getColumnsName($model));'."\n".					
					'        // Columns'."\n".
					'        foreach($http->getParams() as $key => $value)'."\n".
					'        	if (SLS_String::startsWith($key,"Export_Column_"))'."\n".
					'        		array_push($columns,$value);'."\n".							
					'        // Pagination + Order'."\n".
					'        $column = ($http->getParam("column") == "|sls_empty|") ? "" : $http->getParam("column");'."\n".
					'        $order = ($http->getParam("order") == "|sls_empty|") ? "" : $http->getParam("order");'."\n".							
					'        // Filters'."\n".
					'        for($i=0 ; $i<$count=count($nbColumns) ; $i++)'."\n".
					'        {'."\n".
					'        	if ($http->getParam("filter_exact_".($i+1)) == "null" || '."\n".
					'        		$http->getParam("filter_exact_".($i+1)) == "notnull" ||'."\n".
					'        		$http->getParam("filter_value_".($i+1)) != "")'."\n".
					'        	{'."\n".
					'        		if ($http->getParam("filter_value_".($i+1)) != "|sls_empty|")'."\n".
					'        		{'."\n".
					'        			$arrayTmp = array("column" 	=> $http->getParam("filter_name_".($i+1)),'."\n".
					'        							  "value"	=> $http->getParam("filter_value_".($i+1)),'."\n".
					'        							  "mode"	=> $http->getParam("filter_exact_".($i+1)));'."\n".
					'        			array_push($clause,$arrayTmp);'."\n".
					'        		}'."\n".
					'        	}'."\n".
					'        }'."\n".							
					'        if ($this->_generic->useModel($model,$db))'."\n".
					'        {'."\n".
					'        	if (!empty($column) && !empty($order))'."\n".
					'        		$order = array(0=>array("column"=>$column,"order"=>$order));'."\n".
					'        	else'."\n".
					'        		$order = array();'."\n".						
					'        	$className = ucfirst($db)."_".SLS_String::tableToClass($model);'."\n".
					'        	$object = new $className();'."\n".
					'        	$entities = $object->searchModels("",$join,$clause,array(),$order);'."\n".						
					'        	$type = $http->getParam("Export_Type");'."\n".						
					'        	switch($type)'."\n".
					'        	{'."\n".
					'        		case "excel":'."\n".
					'        			$title = $object->getTableComment($model,$db)." Listing";'."\n".
					'        			header("Content-Type: application/vnd.ms-excel");'."\n".
					'        			header("Content-Disposition: attachment; filename=\"".$title."\".xls");'."\n".
					'        			$src = \'<html><head><meta http-equiv="Content-Type" content="text/html; charset='.$this->_generic->getSiteConfig('defaultCharset').'" /></head><body style="font-family:\\\'Franklin Gothic Book\\\', Verdana, Helvetica, Arial;"><table>\';'."\n".
					'        			$src .= \'<tr>\';'."\n".
					'        			for($k=0 ; $k<$count3=count($columns) ; $k++)'."\n".
					'        			{'."\n".
					'        				$description = $object->getColumnComment($columns[$k],$model,$db);'."\n".
					'        				if ($description == $columns[$k] && !empty($join))'."\n".
					'        					$description = $object->getColumnComment($columns[$k],$join[0],$db);'."\n".
					'        				$src .= \'<td style="text-align:center;background-color:#CAC3ED;color:#100F16;border:1px solid gray;font-weight:bold;font-size:14px;">\'.$description.\'</td>\';'."\n".
					'        			}'."\n".
					'        			$src .= \'</tr>\';'."\n".
					'        			for($i=0 ; $i<$count=count($entities) ; $i++)'."\n".
					'        			{'."\n".
					'        				$src .= \'<tr>\';'."\n".
					'        				for($j=0 ; $j<$count2 = count($columns) ; $j++)'."\n".
					'        					$src .= \'<td style="border:1px solid gray">\'.$entities[$i]->$columns[$j].\'</td>\';'."\n".
					'        				$src .= \'</tr>\';'."\n".
					'        			}'."\n".
					'        			$src .= \'</table></body></html>\';'."\n".
					'        			echo $src;'."\n".
					'        			die();'."\n".
					'        			break;'."\n".
					'        		case "html":'."\n".
					'        			$title = $object->getTableComment($model,$db)." Listing";'."\n".
					'        			header("Content-Type: text/html");'."\n".
					'        			header("Content-Disposition: attachment; filename=\"".$title."\".html");'."\n".
					'        			$src = \'<html><head><meta http-equiv="Content-Type" content="text/html; charset='.$this->_generic->getSiteConfig('defaultCharset').'" /></head><body style="font-family:\\\'Franklin Gothic Book\\\', Verdana, Helvetica, Arial;"><table cellpadding="5" cellspacing="0">\';'."\n".
					'        			$src .= \'<tr>\';'."\n".
					'        			for($k=0 ; $k<$count3=count($columns) ; $k++)'."\n".
					'        			{'."\n".
					'        				$description = $object->getColumnComment($columns[$k],$model,$db);'."\n".
					'        				if ($description == $columns[$k] && !empty($join))'."\n".
					'        					$description = $object->getColumnComment($columns[$k],$join[0],$db);'."\n".
					'        				$src .= \'<td style="text-align:center;background-color:#CAC3ED;color:#100F16;border:1px solid gray;font-weight:bold;font-size:14px;">\'.$description.\'</td>\';'."\n".
					'        			}'."\n".
					'        			$src .= \'</tr>\';'."\n".
					'        			for($i=0 ; $i<$count=count($entities) ; $i++)'."\n".
					'        			{'."\n".
					'        				$src .= \'<tr>\';'."\n".
					'        				for($j=0 ; $j<$count2 = count($columns) ; $j++)'."\n".
					'        					$src .= \'<td style="border:1px solid gray">\'.$entities[$i]->$columns[$j].\'</td>\';'."\n".
					'        				$src .= \'</tr>\';'."\n".
					'        			}'."\n".
					'        			$src .= \'</table></body></html>\';'."\n".
					'        			echo $src; '."\n".
					'        			die();'."\n".
					'        			break;'."\n".
					'        		case "csv":'."\n".
					'        			$title = $object->getTableComment($model,$db)." Listing";'."\n".
					'        			header("Content-Type: text/csv");'."\n".
					'        			header("Content-Disposition: attachment; filename=\"".$title."\".csv");'."\n".
					'        			$src = \'\';'."\n".
					'        			for($k=0 ; $k<$count3=count($columns) ; $k++)'."\n".
					'        			{'."\n".
					'        				$description = $object->getColumnComment($columns[$k],$model,$db);'."\n".
					'        				if ($description == $columns[$k] && !empty($join))'."\n".
					'        					$description = $object->getColumnComment($columns[$k],$join[0],$db);'."\n".
					'        				$src .= str_replace(",","\,",$description);'."\n".
					'        				if ($k < ($count3-1))'."\n".
					'        					$src .= \',\';'."\n".
					'        			}'."\n".
					'        			$src .= "\n";'."\n".
					'        			for($i=0 ; $i<$count=count($entities) ; $i++)'."\n".
					'        			{'."\n".
					'        				for($j=0 ; $j<$count2=count($columns) ; $j++)'."\n".
					'        				{'."\n".
					'        					$src .= str_replace(",","\,",$entities[$i]->$columns[$j]);'."\n".
					'        					if ($j < ($count2-1))'."\n".
					'        						$src .= \',\';'."\n".
					'        				}'."\n".
					'        				$src .= "\n";'."\n".
					'        			}'."\n".
					'        			echo $src;'."\n".
					'        			die();'."\n".
					'        			break;'."\n".
					'        		case "txt":'."\n".
					'        			$title = $object->getTableComment($model,$db)." Listing";'."\n".
					'        			header("Content-Type: text/tab-separated-value");'."\n".
					'        			header("Content-Disposition: attachment; filename=\"".$title."\".tsv");'."\n".
					'        			$src = \'\';'."\n".
					'        			for($k=0 ; $k<$count3=count($columns) ; $k++)'."\n".
					'        			{'."\n".
					'        				$description = $object->getColumnComment($columns[$k],$model,$db);'."\n".
					'        				if ($description == $columns[$k] && !empty($join))'."\n".
					'        					$description = $object->getColumnComment($columns[$k],$join[0],$db);'."\n".
					'        				$src .= $description;'."\n".
					'        				if ($k < ($count3-1))'."\n".
					'        					$src .= "\t";'."\n".
					'        			}'."\n".
					'        			$src .= "\n";'."\n".
					'        			for($i=0 ; $i<$count=count($entities) ; $i++)'."\n".
					'        			{'."\n".
					'        				for($j=0 ; $j<$count2=count($columns) ; $j++)'."\n".
					'        				{'."\n".
					'        					$src .= $entities[$i]->$columns[$j];'."\n".
					'        					if ($j < ($count2-1))'."\n".
					'        						$src .= "\t";'."\n".
					'        				}'."\n".
					'        				$src .= "\n";'."\n".
					'        			}'."\n".
					'        			echo $src;'."\n". 
					'        			die();'."\n".
					'        			break;'."\n".
					'        	}'."\n".
					'        }'."\n";
		$source = substr($source,0,strpos($source,"action()")+8)."\n".
				  '    {'."\n".
				  $action.
				  '    }'."\n".
				  '}'."\n".
				  '?>';
		
		file_put_contents($this->_generic->getPathConfig("actionsControllers").$controllerBo."/Export.controller.php",$source);
	}
	
	/**
	 * Create bo action disconnection
	 *
	 * @param string $controllerBo generic controller name (Bo)
	 */
	protected function createActionBoDisconnection($controllerBo="")
	{
		$langs = $this->_generic->getObjectLang()->getSiteLangs();		
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controllerBo),
					 	2 => array("key" 	=> "actionName",
				  				   "value" 	=> "Disconnection"),
				  		3 => array("key"	=> "token",
				  				   "value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
				  		4 => array("key"	=> "indexes",
				  				   "value"	=> "noindex, nofollow")
				  			)
					    );
		foreach($langs as $lang)
		{
			$tmpParam = array("key" 	=> $lang."-action",
							  "value" 	=> "Disconnection");
			$tmpTitle = array("key" 	=> $lang."-title",
							  "value" 	=> "Disconnection");
			array_push($params,$tmpParam);
			array_push($params,$tmpTitle);
		}
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "AddAction",
													  $params,
													  true));
													  
		$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controllerBo."/Disconnection.controller.php");
		
		$action = '        SLS_BoRights::disconnect();'."\n".
        		  '        $this->_generic->redirect("'.$controllerBo.'/Connection.sls");'."\n";
		$source = substr($source,0,strpos($source,"action()")+8)."\n".
				  '    {'."\n".
				  $action.
				  '    }'."\n".
				  '}'."\n".
				  '?>';
		
		file_put_contents($this->_generic->getPathConfig("actionsControllers").$controllerBo."/Disconnection.controller.php",$source);
	}
	
	/**
	 * Delete a action bo
	 *
	 * @param string $model the model described the bo
	 * @param string $actionType the action you want to delete ('list'|'add'|'modify'|'delete')
	 * @param string $alias the alias of the db
	 * @return bool true if ok, else false
	 */
	protected function deleteActionBo($model,$actionType="list",$alias)
	{		
		$controllersXML = $this->_generic->getControllersXML();
		$controller = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/@name"));
		$actionTypes = array("list","add","modify","delete");
		if (!in_array($actionType,$actionTypes) || empty($controller))
			return false;
		$params = array(0 => array("key" 	=> "reload",
				  				   "value" 	=> "true"),
				  		1 => array("key" 	=> "Controller",
				  				   "value" 	=> $controller),
					 	2 => array("key" 	=> "Action",
				  				   "value" 	=> ucfirst($actionType).ucfirst($alias)."_".ucfirst($model)),
				  		3 => array(
				  					"key"	=> "token",
				  					"value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3))
				  			)
					    );		
		file_get_contents($this->_generic->getFullPath("SLS_Bo",
													  "DeleteAction",
													  $params,
													  true));
		return true;
	}
	
	/**
	 * Just copy all actions langs files from given controllers
	 *
	 * @param array $controllers a list of all controllers in which we want to copy actions
	 * @param string $lang the iso code of the new lang
	 */
	protected function copyActionsLang($controllers=array(),$iso)
	{
		if (!empty($controllers) && !empty($iso))
		{
			$actions = array();
			
			foreach($controllers as $controller)
			{
				$handle = opendir($this->_generic->getPathConfig("actionLangs").$controller);
				while (false !== ($file = readdir($handle)))
					if (!is_dir($this->_generic->getPathConfig("actionLangs").$controller."/".$file) && substr($file, 0, 1) != ".")
						if (!in_array(SLS_String::substrBeforeFirstDelimiter($file,"."),$actions))
							array_push($actions,SLS_String::substrBeforeFirstDelimiter($file,"."));
				
				foreach($actions as $action)				
					file_put_contents($this->_generic->getPathConfig("actionLangs").$controller."/".$action.".".$iso.".lang.php",file_get_contents($this->_generic->getPathConfig("actionLangs").$controller."/".$action.".".$this->_generic->getSiteConfig("defaultLang").".lang.php"));
				$actions = array();
			}
		}
	}
	
	/**
	 * Delete all actions lang files from given lang
	 *
	 * @param string $root the path to list
	 * @param string $iso the lang
	 */
	protected function deleteActionsLang($root,$iso)
	{
		$handle = opendir($root);
		while (false !== ($file = readdir($handle)))
		{
			// Recursive call			
			if (is_dir($root."/".$file)  && substr($file, 0, 1) != ".")				
				$this->deleteActionsLang($root."/".$file,$iso);
			// Check if file in correct lang, if yes, delete it !
			if (!is_dir($root."/".$file) && substr($file, 0, 1) != ".")				
				if (SLS_String::endsWith($file,".".$iso.".lang.php"))
					@unlink($root."/".$file);
		}
	}
	
	/**
	 * Get all the application templates
	 *
	 * @return array
	 */
	protected function getAppTpls()
	{
		$tpls = array();		
	
		$handle = opendir($this->_generic->getPathConfig("viewsTemplates"));
		while($file = readdir($handle))
		{
			if (is_file($this->_generic->getPathConfig("viewsTemplates").$file) && substr($file, 0, 1) != ".")
			{
				$fileName 	= SLS_String::substrBeforeLastDelimiter($file,".");
				$extension 	= SLS_String::substrAfterLastDelimiter($file,".");
				
				if ($extension == "xsl")				
					array_push($tpls,$fileName);				
			}
		}
		closedir($handle);
		
		return $tpls;
	}
}
?>