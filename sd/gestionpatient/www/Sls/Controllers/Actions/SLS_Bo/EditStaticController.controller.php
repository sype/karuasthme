<?php
class SLS_BoEditStaticController extends SLS_BoControllerProtected 
{	
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$errors = array();
		
		$controller = SLS_String::trimSlashesFromString($this->_http->getParam('Controller'));
				
		if (is_file($this->_generic->getPathConfig("staticsControllers").$controller.".controller.php"))
		{
			if ($this->_http->getParam('reload') == 'true')
			{
				$newController = SLS_String::trimSlashesFromString($this->_http->getParam('controllerName'));
				$oldController = SLS_String::trimSlashesFromString($this->_http->getParam('oldName'));
				
				if (empty($newController))
					array_push($errors, "You must fill the static controller name");
				
				$newController = str_replace(" ", "", ucwords(trim(SLS_String::getAlphaString($newController))));
				
				if ($oldController == $newController)
					$this->_generic->dispatch('SLS_Bo', 'Controllers');
				
				$statics = $this->_generic->recursiveReadDir($this->_generic->getPathConfig("staticsControllers"), array(), array(0=>"php"));
				foreach ($statics as $static)
				{
					if (SLS_String::substrBeforeLastDelimiter(SLS_String::substrAfterLastDelimiter($static, "/"), ".controller.php") == $newController)
					{
						array_push($errors, "The name '".$newController."' is already in use for a static controller");
						break;
					}
				}
				
				if (empty($errors))
				{
					$strController = file_get_contents($this->_generic->getPathConfig("staticsControllers").$oldController.".controller.php");
					$strController = str_replace($oldController."Controller", $newController."Controller", $strController);
					file_put_contents($this->_generic->getPathConfig("staticsControllers").$newController.".controller.php", $strController);
					unlink($this->_generic->getPathConfig("staticsControllers").$oldController.".controller.php");
					$this->_generic->dispatch("SLS_Bo", "Controllers");
				}		
				if (!empty($errors))
				{
					$xml->startTag("errors");
						foreach ($errors as $error)
							$xml->addFullTag("error", $error, true);
					$xml->endTag("errors");
				}
				$xml->startTag('form');
					$xml->addFullTag("controllerName", $postControllerName);
						
				$xml->endTag('form');
			}
			$xml->startTag('controller');
				$xml->addFullTag("name", $controller, true);
			$xml->endTag('controller');
		}
		else 
		{
			$this->_generic->dispatch('SLS_Bo', 'Controllers');
		}
		
		$this->saveXML($xml);
	}
	
}
?>