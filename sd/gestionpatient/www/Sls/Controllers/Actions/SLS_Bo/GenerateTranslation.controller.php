<?php
/**
* Class GenerateTranslation into Bo Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Bo
* @see Mvc.Controllers.Bo.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class SLS_BoGenerateTranslation extends SLS_BoControllerProtected
{
	public function action()
	{
		$user = $this->hasAuthorative();
		$xml = $this->getXML();
		$xml = $this->makeMenu($xml);
		$controllersXML = $this->_generic->getControllersXML();
		$controller = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/@name"));		
		$action = array_shift($controllersXML->getTags("//controllers/controller[@isBo='true']/scontrollers/scontroller[@name='Translation']"));		
		
		// Check if bo controller already exist
		if (empty($controller))
		{
			$xml->startTag("errors");
			$xml->addFullTag("error","Back-office controller could not be found. Please follow the following link to create it",true);
			$xml->endTag("errors");
			$xml->addFullTag("error_type","rights",true);
		}
		// If action already exist
		else if (!empty($action))
		{
			$xml->startTag("errors");
			$xml->addFullTag("error","Translation action already exists",true);
			$xml->endTag("errors");			
		}
		// Else, let's generate action
		else 
		{
			$langs = $this->_generic->getObjectLang()->getSiteLangs();
		
			$params = array(0 => array("key" 	=> "reload",
					  				   "value" 	=> "true"),
					  		1 => array("key" 	=> "Controller",
					  				   "value" 	=> $controller),
						 	2 => array("key" 	=> "actionName",
					  				   "value" 	=> "Translation"),
					  		3 => array("key"	=> "token",
					  					"value"	=> sha1(substr($this->_generic->getSiteConfig("privateKey"), 0, 3).substr($this->_generic->getSiteConfig("privateKey"), strlen($this->_generic->getSiteConfig("privateKey"))-3)),
					  		4 => array("key"	=> "indexes",
					  				   "value"	=> "noindex, nofollow")
					  			)
						    );
			foreach($langs as $lang)
			{
				$tmpParam = array("key" 	=> $lang."-action",
								  "value" 	=> "Translation");
				$tmpTitle = array("key" 	=> $lang."-title",
								  "value" 	=> "Translation");
				array_push($params,$tmpParam);
				array_push($params,$tmpTitle);
			}
			file_get_contents($this->_generic->getFullPath("SLS_Bo",
														  "AddAction",
														  $params,
														  true));
			$source = file_get_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Translation".".controller.php");
			
			
			$action = 	'        $xml = $this->getXML();'."\n".
				        '        $boActions = SLS_BoRights::getDistinctActions();'."\n".
				        '        $xml->startTag("bo_actions");'."\n".
				        '        for($i=0 ; $i<$count=count($boActions) ; $i++)'."\n".
				        '        {'."\n".
				        '            $xml->startTag("bo_action");'."\n".
				        '            foreach($boActions[$i] as $key => $value)'."\n".
				        '                $xml->addFullTag($key,$value,true);'."\n".
				        '            $xml->endTag("bo_action");'."\n".
				        '        }'."\n".
				        '        $xml->endTag("bo_actions");'."\n\n".
				        '        // Check account rights'."\n".
				        '        $right = SLS_BoRights::isAuthorized("Modify");'."\n".
				        '        if ($right == -1)'."\n".
				        '            $this->_generic->redirect("'.$controller.'/Connection.sls");'."\n".
				        '        else if ($right == 0)'."\n".
				        '            $this->_generic->redirect("'.$controller.'/Connection.sls");'."\n".
				        '        // /Check account rights'."\n\n".				        
				        '        set_time_limit(0);'."\n".
				        '        $xmlController = $this->_generic->getControllersXML();'."\n\n".				        
				        '        // Langs'."\n".
						'        $langObject = $this->_generic->getObjectLang();'."\n".
						'        $langs = $langObject->getSiteLangs();'."\n\n".				        
						'        $xml->startTag("langs");'."\n\n".						
				        '        // Check reload'."\n".
				        '        if ($this->_http->getParam("reload_translate") == "true")'."\n".
				        '        {'."\n".
				        '            // Get the target'."\n".
				        '            $target_id = $this->_http->getParam("target_id");'."\n".
				        '            $xml->addFullTag("target_id",$target_id,true);'."\n\n".				        	
				        '            // Init source file'."\n".
				        '            $fileName = "";'."\n\n".				        	
				        '            // If target_id map to site'."\n".
				        '            if ($target_id == "site")'."\n".
				        '                $fileName = $this->_generic->getPathConfig("genericLangs")."site";'."\n".
				        '            // Else if map to controller'."\n".
				        '            else if (SLS_String::startsWith($target_id,"c_"))'."\n".
				        '            {'."\n".
				        '                // Try to get the controller'."\n".
				        '                $result = array_shift($xmlController->getTagsAttributes("//controllers/controller[@id=\'".$target_id."\']",array("name")));'."\n".
				        '                if (!empty($result["attributes"][0]["value"]))'."\n".
				        '                    $fileName = $this->_generic->getPathConfig("actionLangs").$result["attributes"][0]["value"]."/__".$result["attributes"][0]["value"];'."\n".
				        '            }'."\n".
				        '            // Else if map to action'."\n".
				        '            else if (SLS_String::startsWith($target_id,"a_"))'."\n".
				        '            {'."\n".
				        '                // Try to get the controller'."\n".
				        '                $result = array_shift($xmlController->getTagsAttributes("//controllers/controller[scontrollers/scontroller[@id=\'".$target_id."\']]",array("name")));'."\n".
				        '                if (!empty($result["attributes"][0]["value"]))'."\n".
				        '                {'."\n".
				        '                    $controllerName = $result["attributes"][0]["value"];'."\n".
				        '                    // Try to get the action'."\n".
				        '                    $result = array_shift($xmlController->getTagsAttributes("//controllers/controller/scontrollers/scontroller[@id=\'".$target_id."\']",array("name")));'."\n".
				        '                    if (!empty($result["attributes"][0]["value"]))'."\n".
				        '                        $fileName = $this->_generic->getPathConfig("actionLangs").$controllerName."/".$result["attributes"][0]["value"];'."\n".
				        '                }'."\n".
				        '            }'."\n\n".				        	
				        '        	// If we\'ve found a source file'."\n".
				        '        	if (!empty($fileName))'."\n".
				        '        	{'."\n".
				        '                $properties = array();'."\n".
				        '                $params = $this->_http->getParams();'."\n".
				        '                foreach($params as $key => $value)'."\n".
				        '                    $params[$key] = SLS_String::trimSlashesFromString($value);'."\n\n".				        		
				        '                foreach($langs as $lang)'."\n".
				        '                    $properties[$lang] = array();'."\n\n".				        		
				        '                foreach($params as $key => $value)'."\n".
				        '                    if (SLS_String::startsWith($key,"LG_"))'."\n".
				        '                        foreach($langs as $lang)'."\n".
				        '                            if (SLS_String::startsWith($key,"LG_".$lang."_"))'."\n".
				        '                                $properties[$lang][SLS_String::substrAfterFirstDelimiter($key,"LG_".$lang."_")] = $value;'."\n".
				        '                $this->setLangProperties($fileName,$properties,$langs);'."\n".
				        '        	}'."\n".
				        '        }'."\n\n".						
						'        // Load Generics Langs'."\n".
						'        $xml->startTag("site");'."\n".
						'        $properties = $this->getLangProperties($this->_generic->getPathConfig("genericLangs")."site",$langs);'."\n".
						'        $xml->startTag("properties");'."\n".
						'        foreach($properties as $key => $value)'."\n".
						'        {'."\n".
						'            $xml->startTag("property");'."\n".
						'            $xml->addFullTag("name",$key,true);'."\n".
						'            $xml->startTag("values");'."\n".
						'            foreach($langs as $lang)'."\n".
						'            {'."\n".
						'                $xml->startTag("value");'."\n".
						'                $xml->addFullTag("name",str_replace(\'\"\',\'"\',$value[$lang]),true);'."\n".
						'                $xml->addFullTag("lang",$lang,true);'."\n".
						'                $xml->addFullTag("type",(strlen(str_replace(\'\"\',\'"\',$value[$lang]))>50) ? "textarea" : "input",true);'."\n".
						'                $xml->endTag("value");'."\n".
						'            }'."\n".
						'            $xml->endTag("values");'."\n".
						'            $xml->endTag("property");'."\n".
						'        }'."\n".
						'        $xml->endTag("properties");'."\n".
						'        $xml->endTag("site");'."\n\n".						
						'        // Load Controller Lang'."\n".
						'        $controllers = $xmlController->getTagsAttributes("//controllers/controller[@side=\'user\' and count(@isBo) = 0]",array("name","id"));'."\n".
						'        $xml->startTag("controllers");'."\n".
						'        for($i= 0 ; $i<$count=count($controllers) ; $i++)'."\n".
						'        {'."\n".
						'            $xml->startTag("controller");'."\n".
						'            $xml->addFullTag("name",$controllers[$i]["attributes"][0]["value"],true);'."\n".
						'            $xml->addFullTag("id",$controllers[$i]["attributes"][1]["value"],true);'."\n".
						'            $properties = $this->getLangProperties($this->_generic->getPathConfig("actionLangs").$controllers[$i]["attributes"][0]["value"]."/__".$controllers[$i]["attributes"][0]["value"],$langs);'."\n".
						'            $xml->startTag("properties");'."\n".
						'            foreach($properties as $key => $value)'."\n".
						'            {'."\n".
						'                $xml->startTag("property");'."\n".
						'                $xml->addFullTag("name",$key,true);'."\n".
						'                $xml->startTag("values");'."\n".
						'                foreach($langs as $lang)'."\n".
						'                {'."\n".
						'                    $xml->startTag("value");'."\n".
						'                    $xml->addFullTag("name",str_replace(\'\"\',\'"\',$value[$lang]),true);'."\n".
						'                    $xml->addFullTag("lang",$lang,true);'."\n".
						'                    $xml->addFullTag("type",(strlen(str_replace(\'\"\',\'"\',$value[$lang]))>50) ? "textarea" : "input",true);'."\n".
						'                    $xml->endTag("value");'."\n".
						'                }'."\n".
						'                $xml->endTag("values");'."\n".
						'                $xml->endTag("property");'."\n".
						'            }'."\n".
						'            $xml->endTag("properties");'."\n\n".							
						'            $actions = $xmlController->getTagsAttributes("//controllers/controller[@name=\'".$controllers[$i]["attributes"][0]["value"]."\']/scontrollers/scontroller",array("name","id"));'."\n".
						'            $xml->startTag("actions");'."\n".
						'            for($j= 0 ; $j<$countJ=count($actions) ; $j++)'."\n".
						'            {'."\n".
						'                $xml->startTag("action");'."\n".
						'                $xml->addFullTag("name",$actions[$j]["attributes"][0]["value"],true);'."\n".
						'                $xml->addFullTag("id",$actions[$j]["attributes"][1]["value"],true);'."\n".
						'                $properties = $this->getLangProperties($this->_generic->getPathConfig("actionLangs").$controllers[$i]["attributes"][0]["value"]."/".$actions[$j]["attributes"][0]["value"],$langs);'."\n".
						'                $xml->startTag("properties");'."\n".
						'                foreach($properties as $key => $value)'."\n".
						'                {'."\n".
						'                    $xml->startTag("property");'."\n".
						'                    $xml->addFullTag("name",$key,true);'."\n".
						'                    $xml->startTag("values");'."\n".
						'                    foreach($langs as $lang)'."\n".
						'                    {'."\n".
						'                        $xml->startTag("value");'."\n".
						'                        $xml->addFullTag("name",str_replace(\'\"\',\'"\',$value[$lang]),true);'."\n".
						'                        $xml->addFullTag("lang",$lang,true);'."\n".
						'                        $xml->addFullTag("type",(strlen(str_replace(\'\"\',\'"\',$value[$lang]))>50) ? "textarea" : "input",true);'."\n".
						'                        $xml->endTag("value");'."\n".
						'                    }'."\n".
						'                    $xml->endTag("values");'."\n".
						'                    $xml->endTag("property");'."\n".
						'                }'."\n".
						'                $xml->endTag("properties");'."\n".
						'                $xml->endTag("action");'."\n".
						'            }'."\n".
						'            $xml->endTag("actions");'."\n\n".							
						'            $xml->endTag("controller");'."\n".
						'        }'."\n".
						'        $xml->endTag("controllers");'."\n\n".						
						'        $xml->endTag("langs");'."\n".				        
				        '        $this->saveXML($xml);';
			
			$next =		'    public function getLangProperties($file,$langs)'."\n".	
						'    {'."\n".	
						'        $properties = array();'."\n\n".								
						'        foreach($langs as $lang)'."\n".	
						'        {'."\n".	
						'            $fileN = $file.".".$lang.".lang.php";'."\n".
						'            $properties_{$lang} = array();'."\n\n".								
						'            if (file_exists($fileN))'."\n".
						'            {'."\n".
						'                $fileExploded = explode("\n",file_get_contents($fileN));'."\n".
						'                array_map("trim",$fileExploded);'."\n\n".									
						'                foreach($fileExploded as $line)'."\n".
						'                {'."\n".
						'                    $line = trim($line);'."\n".
						'                    if (SLS_String::startsWith($line,\'$GLOBALS[$GLOBALS[\\\'PROJECT_NAME\\\']]\'))'."\n".
						'                    {'."\n".
						'                        $type = (SLS_String::startsWith($line,\'$GLOBALS[$GLOBALS[\\\'PROJECT_NAME\\\']][\\\'XSL\\\'][\\\'\')) ? "XSL" : "JS";'."\n".
						'                        $end = SLS_String::substrAfterFirstDelimiter($line,\'$GLOBALS[$GLOBALS[\\\'PROJECT_NAME\\\']][\\\'\'.$type.\'\\\'][\\\'\');'."\n\n".											
						'                        $key = SLS_String::substrBeforeFirstDelimiter($end,\'\\\']\');'."\n".
						'                        $value = SLS_String::substrAfterFirstDelimiter(SLS_String::substrBeforeLastDelimiter($end,\'";\'),\'"\');'."\n".
						'                        $properties_{$lang}[$key] = $value;'."\n".
						'                    }'."\n".
						'                }'."\n".
						'            }'."\n".
						'        }'."\n\n".							
						'        foreach($langs as $lang)'."\n".
						'            foreach($properties_{$lang} as $key => $value)'."\n".
						'                $properties[$key][$lang] = $value;'."\n\n".							
						'        return $properties;'."\n".
						'    }'."\n\n".						
						'    public function setLangProperties($file,$properties,$langs)'."\n".
						'    {'."\n".
						'        foreach($langs as $lang)'."\n".
						'        {'."\n".
						'            $fileN = $file.".".$lang.".lang.php";'."\n\n".											
						'            if (file_exists($fileN))'."\n".
						'            {'."\n".
						'                $newFile = "";'."\n".
						'                $fileExploded = explode("\n",file_get_contents($fileN));'."\n".
						'                array_map("trim",$fileExploded);'."\n".
						'                $endFound = false;'."\n\n".									
						'                foreach($fileExploded as $line)'."\n".
						'                {'."\n".
						'                    if (!$endFound)'."\n".
						'                    {'."\n".
						'                        $line = trim($line);'."\n\n".											
						'                        if (SLS_String::startsWith($line,\'?>\'))'."\n".
						'                            $endFound = true;'."\n\n".											
						'                        if (SLS_String::startsWith($line,\'$GLOBALS[$GLOBALS[\\\'PROJECT_NAME\\\']]\'))'."\n".
						'                        {'."\n".
						'                            $type = (SLS_String::startsWith($line,\'$GLOBALS[$GLOBALS[\\\'PROJECT_NAME\\\']][\\\'XSL\\\'][\\\'\')) ? "XSL" : "JS";'."\n".
						'                            $end = SLS_String::substrAfterFirstDelimiter($line,\'$GLOBALS[$GLOBALS[\\\'PROJECT_NAME\\\']][\\\'\'.$type.\'\\\'][\\\'\');'."\n".
						'                            $key = SLS_String::substrBeforeFirstDelimiter($end,\'\\\']\');'."\n\n".												
						'                            if (!empty($properties[$lang][$key]))'."\n".
						'                                $newFile .= \'$GLOBALS[$GLOBALS[\\\'PROJECT_NAME\\\']][\\\'\'.$type.\'\\\'][\\\'\'.$key.\'\\\'] = "\'.str_replace(\'"\',\'\"\',$properties[$lang][$key]).\'";\';'."\n".
						'                            else'."\n".
						'                                $newFile .= $line;'."\n".
						'                        }'."\n".
						'                        else'."\n".
						'                            $newFile .= $line;'."\n\n".																
						'                        if (!$endFound)'."\n".
						'                            $newFile .= "\n";'."\n".
						'                    }'."\n".
						'                }'."\n".
						'    			file_put_contents($fileN,$newFile);'."\n".
						'            }'."\n".
						'        }'."\n".
						'    }'."\n";  
				      
			$source = substr($source,0,strpos($source,"action()")+8)."\n".
					  '    {'."\n".
					  $action."\n".
					  '    }'."\n\n".
					  $next.
					  '}'."\n".
					  '?>';
			file_put_contents($this->_generic->getPathConfig("actionsControllers").$controller."/Translation.controller.php",$source);			
			
			$sourceXsl = 	'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">'."\n".
						 	'    <xsl:template name="Translation">'."\n".
						 	'        <xsl:call-template name="BoMenu" />'."\n".
							'        <style>'."\n".
							'        	input[type="text"], table, textarea {'."\n".
							'        		width:100%;'."\n".
							'        	}'."\n".
							'        </style>'."\n".
							'        <h1>Project Translations</h1>'."\n\n".							
							'        <!-- Global Translations (site.tld.lang.php) -->'."\n".
							'        <xsl:if test="count(//View/langs/site/properties/property) &gt; 0">'."\n".
							'        	<h3>Global Translations</h3>'."\n".
							'        	<form action="" method="post">'."\n".
							'        		<input type="hidden" name="reload_translate" value="true" />'."\n".
							'        		<input type="hidden" name="target_id" value="site" />'."\n".
							'        		<table class="bo_table_visible" style="margin-top:0" id="site">'."\n".
							'        			<tr class="summary">'."\n".
							'        				<th style="width:300px;">Label</th>'."\n".
							'        				<xsl:for-each select="//Statics/Sls/Configs/site/langs/name" >'."\n".
							'        					<th>Language \'<xsl:value-of select="." />\'</th>'."\n".
							'        				</xsl:for-each>'."\n".
							'        			</tr>'."\n".
							'        			<xsl:for-each select="//View/langs/site/properties/property">'."\n".
							'        				<tr>'."\n".
							'        					<td><xsl:value-of select="name" /></td>'."\n".
							'        					<xsl:variable name="position" select="position()" />'."\n".
							'        					<xsl:for-each select="//Statics/Sls/Configs/site/langs/name" >'."\n".
							'        						<xsl:variable name="lang" select="." />'."\n".
							'        						<td>'."\n".
							'        							<xsl:if test="//View/langs/site/properties/property[$position]/values/value[lang=$lang]/type = \'input\'">'."\n".
							'        								<input type="text" name="{concat(\'LG_\',$lang,\'_\',//View/langs/site/properties/property[$position]/name)}" value="{//View/langs/site/properties/property[$position]/values/value[lang=$lang]/name}" />'."\n".
							'        							</xsl:if>'."\n".
							'        							<xsl:if test="//View/langs/site/properties/property[$position]/values/value[lang=$lang]/type = \'textarea\'">'."\n".
							'        								<textarea rows="5" name="{concat(\'LG_\',$lang,\'_\',//View/langs/site/properties/property[$position]/name)}">'."\n".
							'        									<xsl:value-of select="//View/langs/site/properties/property[$position]/values/value[lang=$lang]/name" />'."\n".
							'        								</textarea>'."\n".
							'        							</xsl:if>'."\n".
							'        						</td>'."\n".
							'        					</xsl:for-each>'."\n".
							'        				</tr>'."\n".
							'        			</xsl:for-each>'."\n".
							'        		</table>'."\n".
							'        		<input type="submit" value="Update" />'."\n".
							'        	</form>'."\n".
							'        </xsl:if>'."\n".
							'        <!-- /Global Translations (site.tld.lang.php) -->'."\n\n".							
							'        <!-- Controllers Translations -->'."\n".
							'        <h3>Controllers Translations</h3>'."\n".
							'        <xsl:for-each select="//View/langs/controllers/controller">'."\n".
							'        	<xsl:variable name="positionC" select="position()" />'."\n".
							'        	<xsl:if test="count(properties/property) &gt; 0 or count(actions/action) &gt; 0">'."\n".
							'        		<h5>Controller \'<xsl:value-of select="name" />\'</h5>'."\n".
							'        			<xsl:if test="count(properties/property) = 0">'."\n".
							'        				This Controller haven\'t yet any language properties.'."\n".
							'        			</xsl:if>'."\n".
							'        			<xsl:if test="count(properties/property) &gt; 0">'."\n".
							'        				<form action="" method="post">'."\n".
							'        					<input type="hidden" name="reload_translate" value="true" />'."\n".
							'        					<input type="hidden" name="target_id" value="{id}" />'."\n".
							'        					<table class="bo_table_visible" style="margin-top:0" id="{id}">'."\n".
							'        						<tr class="summary">'."\n".
							'        							<th style="width:300px;">Label</th>'."\n".
							'        							<xsl:for-each select="//Statics/Sls/Configs/site/langs/name" >'."\n".
							'        								<th>Language \'<xsl:value-of select="." />\'</th>'."\n".
							'        							</xsl:for-each>'."\n".
							'        						</tr>'."\n".
							'        						<xsl:for-each select="properties/property">'."\n".
							'        							<tr>'."\n".
							'        								<td><xsl:value-of select="name" /></td>'."\n".
							'        								<xsl:variable name="position" select="position()" />'."\n".
							'        								<xsl:for-each select="//Statics/Sls/Configs/site/langs/name" >'."\n".
							'        									<xsl:variable name="lang" select="." />'."\n".
							'        									<td>'."\n".
							'        										<xsl:if test="//View/langs/controllers/controller[$positionC]/properties/property[$position]/values/value[lang=$lang]/type = \'input\'">'."\n".
							'        											<input type="text" name="{concat(\'LG_\',$lang,\'_\',//View/langs/controllers/controller[$positionC]/properties/property[$position]/name)}" value="{//View/langs/controllers/controller[$positionC]/properties/property[$position]/values/value[lang=$lang]/name}" />'."\n".
							'        										</xsl:if>'."\n".
							'        										<xsl:if test="//View/langs/controllers/controller[$positionC]/properties/property[$position]/values/value[lang=$lang]/type = \'textarea\'">'."\n".
							'        											<textarea rows="5" name="{concat(\'LG_\',$lang,\'_\',//View/langs/controllers/controller[$positionC]/properties/property[$position]/name)}">'."\n".
							'        												<xsl:value-of select="//View/langs/controllers/controller[$positionC]/properties/property[$position]/values/value[lang=$lang]/name" />'."\n".
							'        											</textarea>'."\n".
							'        										</xsl:if>'."\n".
							'        									</td>'."\n".
							'        								</xsl:for-each>'."\n".
							'        							</tr>'."\n".
							'        						</xsl:for-each>'."\n".
							'        					</table>'."\n".
							'        					<input type="submit" value="Update" />'."\n".
							'        				</form>'."\n".
							'        			</xsl:if>'."\n\n".										
							'        			<!-- Actions Translations -->'."\n".
							'        			<xsl:if test="count(actions/action) = 0">'."\n".
							'        				This Controller haven\'t yet any actions.'."\n".
							'        			</xsl:if>'."\n".
							'        			<xsl:if test="count(actions/action) &gt; 0">'."\n".
							'        				<div style="margin-left:40px;">'."\n".
							'        					<h5>Actions</h5>'."\n".
							'        					<xsl:for-each select="actions/action">'."\n".
							'        						<xsl:variable name="positionA" select="position()" />'."\n".
							'        						<fieldset style="display:block;">'."\n".
							'        							<legend>Action \'<xsl:value-of select="name" />\'</legend>'."\n".
							'        							<xsl:if test="count(properties/property) = 0">'."\n".
							'        								This Action haven\'t yet any language properties.'."\n".
							'        							</xsl:if>'."\n".
							'        							<xsl:if test="count(properties/property) &gt; 0">'."\n".
							'        								<form action="" method="post">'."\n".
							'        									<input type="hidden" name="reload_translate" value="true" />'."\n".
							'        									<input type="hidden" name="target_id" value="{id}" />'."\n".
							'        									<table class="bo_table_visible" style="margin-top:0" id="{id}">'."\n".
							'        										<tr class="summary">'."\n".
							'        											<th style="width:300px;">Label</th>'."\n".
							'        											<xsl:for-each select="//Statics/Sls/Configs/site/langs/name" >'."\n".
							'        												<th>Language \'<xsl:value-of select="." />\'</th>'."\n".
							'        											</xsl:for-each>'."\n".
							'        										</tr>'."\n".
							'        										<xsl:for-each select="properties/property">'."\n".
							'        											<tr>'."\n".
							'        												<td><xsl:value-of select="name" /></td>'."\n".
							'        												<xsl:variable name="position" select="position()" />'."\n".
							'        												<xsl:for-each select="//Statics/Sls/Configs/site/langs/name" >'."\n".
							'        													<xsl:variable name="lang" select="." />'."\n".
							'        													<td>'."\n".
							'        														<xsl:if test="//View/langs/controllers/controller[$positionC]/actions/action[$positionA]/properties/property[$position]/values/value[lang=$lang]/type = \'input\'">'."\n".
							'        															<input type="text" name="{concat(\'LG_\',$lang,\'_\',//View/langs/controllers/controller[$positionC]/actions/action[$positionA]/properties/property[$position]/name)}" value="{//View/langs/controllers/controller[$positionC]/actions/action[$positionA]/properties/property[$position]/values/value[lang=$lang]/name}" />'."\n".
							'        														</xsl:if>'."\n".
							'        														<xsl:if test="//View/langs/controllers/controller[$positionC]/actions/action[$positionA]/properties/property[$position]/values/value[lang=$lang]/type = \'textarea\'">'."\n".
							'        															<textarea rows="5" name="{concat(\'LG_\',$lang,\'_\',//View/langs/controllers/controller[$positionC]/actions/action[$positionA]/properties/property[$position]/name)}">'."\n".
							'        																<xsl:value-of select="//View/langs/controllers/controller[$positionC]/actions/action[$positionA]/properties/property[$position]/values/value[lang=$lang]/name" />'."\n".
							'        															</textarea>'."\n".
							'        														</xsl:if>'."\n".
							'        													</td>'."\n".
							'        												</xsl:for-each>'."\n".
							'        											</tr>'."\n".
							'        										</xsl:for-each>'."\n".
							'        									</table>'."\n".
							'        									<input type="submit" value="Update" />'."\n".
							'        								</form>'."\n".
							'        							</xsl:if>'."\n".
							'        						</fieldset>'."\n".
							'        					</xsl:for-each>'."\n".
							'        				</div>'."\n".
							'        			</xsl:if>'."\n".
							'        			<!-- /Actions Translations -->'."\n\n".										
							'        	</xsl:if>'."\n".
							'        </xsl:for-each>'."\n".
							'        <!-- /Controllers Translations -->'."\n\n".							
							'        <!-- Focus -->'."\n".
							'        <xsl:if test="count(//View/langs/target_id) &gt; 0">'."\n".
							'        	<script type="text/javascript">'."\n".
							'        		window.location = \'#<xsl:value-of select="//View/langs/target_id" />\';'."\n".
							'        	</script>'."\n".
							'        </xsl:if>'."\n".
							'        <!-- /Focus -->'."\n".
							 '    </xsl:template>'."\n".
							 '</xsl:stylesheet>';
			file_put_contents($this->_generic->getPathConfig("viewsBody").$controller."/Translation.xsl",$sourceXsl);
			$this->_generic->goDirectTo("SLS_Bo","Bo");
		}
		
		$xml->addFullTag("url_add_controller",$this->_generic->getFullPath("SLS_Bo","AddController",array(0=>array("key"=>"isBo","value"=>"true"))),true);
		$this->saveXML($xml);
	}
}
?>