window.addEvent('domready', function() {
	
	/**
     * Set default options, overrideable from later calls.
     */
    SqueezeBox.initialize({
        size: {x: 800, y: 600}
    });
 
    /**
     * Assign SqueezeBox to all links with rel="boxed" attribute, the class then reads the "href".
     */
    SqueezeBox.assign($$('a[rel=boxed]'), {
    	ajaxOptions: {
			method: 'get' // we use GET for requesting plain HTML (you can skip it, it is the default value)
		}
    });
	
	$('patient_input_sch').addEvent('focus', function(){
		this.set('value', '');
	});
	
	$('patient_input_sch').addEvent('blur', function(e){
		this.set('value', 'Recherche...');
	});
	
	$('patient_input_sch').addEvent('keypress', function(e){
		if( e.key == 'enter')
		{
			$('filter').submit();
		}
	});
	
	$('nbToDisplay').addEvent('keypress', function(e){
		if( e.key == 'enter')
		{
			$('filter').submit();
		}
	});
	
	if($('pages'))
	{
		$('pages').addEvent('change', function(){
			$('filter').submit();
		});
	}
	
	$('patient_input_sch').addEvent('keyup', function(){
		
		//On verifie si l'utilisateur à tapé quelque chose
		if(trim($('patient_input_sch').get('value')) != '')
		{
        	searchPatient($('patient_input_sch').get('value'));
        }else
    	{
    		$('result_item').innerHTML = "";
    	}
    });
	
	//Auto complétion
	var searchPatient= function (keyword){
		var reqKeyword = keyword;
		//On split par les espaces
		var tabKey = keyword.split(' ');
		//On construit notre variable à transmettre
		var schWord = "";
		for(i=0; i<tabKey.length; i++)
		{
			if(tabKey[i] != '')
			{
				schWord += tabKey[i] + "|";
			}
		}
		schWord = schWord.substring(0, schWord.length - 1);
	    var url = slsBuild.site.protocol + "://" + slsBuild.site.domainName + '/Ajax/SearchPatient.sls';
	    if(keyword !== ''){
	        var request = new Request.JSON({
	            'url' : url,
	            onRequest : function(){
	            	$('loading_img').innerHTML = '<img src = "' + slsBuild.site.protocol + '://' + slsBuild.site.domainName + '/Public/Style/Img/Loader/loading.gif" alt="Chargement" title="Chargement"/>';
	            },
	            onComplete : function(resp){
	            	//On verifie si le mot cherché correspond au mot en cours
	            	$('loading_img').innerHTML = '&#160;';
	            	if(reqKeyword == $('patient_input_sch').get('value').replace(' ', '')){
		            	$('result_item').innerHTML = "";
		            	xhr = eval(resp);
		                var patients = xhr;
		                if(patients)
	                	{
	                       for (i=0; i<patients.length; i++)
	                        {
	                            var li = new Element('li', {
	                            	'html' : patients[i].nom+' '+patients[i].prenom
	                            });
	                            var addLink = new Element('a', {
	                            	'href' : slsBuild.site.protocol + "://" + slsBuild.site.domainName + '/Patient/Suivi/Patient/' + patients[i].id + '.sls',
	                            	'title' : 'Effectuer un suivi',
	                                events: {
	                                    click: function(e){
	                                    	SqueezeBox.open(this.get('href'));
	                                    	e.stop();
	                                    }
	                                }
	                            });
	                            
	                            var addLinkImg = new Element('img', {
	                            	'src' : slsBuild.site.protocol + "://" + slsBuild.site.domainName + '/Public/Style/Img/Buttons/add.gif',
	                            	'alt' : 'Effectuer un suivi',
	                            	'title' : 'Effectuer un suivi'
	                            }).inject(addLink);
	                            
	                            var viewLink = new Element('a', {
	                            	'href' : slsBuild.site.protocol + "://" + slsBuild.site.domainName + '/Patient/ListSuivi/Patient/' + patients[i].id + '.sls',
	                            	'title' : 'Liste de suivi'
	                            });
	                            
	                            var viewLinkImg = new Element('img', {
	                            	'src' : slsBuild.site.protocol + "://" + slsBuild.site.domainName + '/Public/Style/Img/Buttons/view.gif',
	                            	'alt' : 'Liste de suivi',
	                            	'title' : 'Liste de suivi'
	                            }).inject(viewLink);
	                            li.innerHTML += '&nbsp;';
	                            addLink.inject(li);
	                            li.innerHTML += '&nbsp;';
	                            viewLink.inject(li);
	                            
	                            $('result_item').insertBefore(li, $('result_item').firstChild);
	                        }
	                	}
	            	}
	            }
	        }).send('keyword='+schWord);
	    }
	};
	
	function trim (myString)
	{
		return myString.replace(/^\s+/g,'').replace(/\s+$/g,'')
	}
});

function order(champs)
{
	if($('order').value == champs)
	{
		if($('orderSens').value == 'ASC')
		{
			$('orderSens').value = 'DESC';
		}else
		{
			$('orderSens').value = 'ASC';
		}
	}else
	{
		$('order').value = champs;
		$('orderSens').value = 'ASC';
	}
	$('filter').submit();
}