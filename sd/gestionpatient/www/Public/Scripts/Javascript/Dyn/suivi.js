//Change le curseur en fonction de type
function cursor(type){
	switch(type){
		case 1:
		document.body.style.cursor='pointer';
		break;
		
		case 0:
		document.body.style.cursor='default';
		break;
	}
}

//Change la page en fonction de state:page en cours et butt:boutton appuyé
function change(state, butt){
	if(state==0 && butt==0){
		document.location.href = slsBuild.site.protocol + "://" + slsBuild.site.domainName + '/Patient/Index.sls'
	}else if(state==0 && butt==1){
		document.getElementById('title').innerHTML = "Test de controle de l'asthme";
		document.getElementById('subTitle').innerHTML = "découvrez si l'asthme de votre patient est controlé ou non";
		document.getElementById('divButtonRight').innerHTML = "terminé";
		document.getElementById('divButtonLeft').innerHTML = "suivi patient";
		document.getElementById('questionPartA').style.display = 'none';
		document.getElementById('questionPartB').style.display = 'block'
		document.getElementById('state').value = 1;
	}else if(state==1 && butt==0){
		document.getElementById('divButtonRight').innerHTML = "test de contrôle";
		document.getElementById('divButtonLeft').innerHTML = "annuler";
		document.getElementById('title').innerHTML = "Questionnaire de suivi du patient asthmatique";
		document.getElementById('subTitle').innerHTML ="Par le médecin traitant";
		document.getElementById('questionPartB').style.display = 'none';
		document.getElementById('questionPartA').style.display = 'block';
		document.getElementById('state').value = 0;
	}else if(state==1 && butt==1){
		var st1 = false;
		var st2  = false;
		var i = 1;
		//Vérifie si toutes les questions ont été traitées
		for(i = 1;i<7;i++){
			if(!document.getElementById('questA_'+i).checked && !document.getElementById('questA_'+i+'b').checked){
				st1 = true;
			}
		}
		for(i = 1;i<6;i++){
			if(document.getElementById('questB_'+i).value == 0){
				st2 = true;
			}
		}
		if(st1 || st2){
			alert('Veuillez vous assurer d\'avoir répondu à toutes les questions avant de valider');
		}else{
			if(box.innerHTML == 'Non controlé')
			{
				alert("L'asthme de votre patient n'est pas controlé !");
			}
			document.suivi.submit();
		}
	}
	
}

//Gère le questionnaire partie B
//state:position de la souris, id:id de la div, part:question en cours, no: valeur de la réponse
function quest(state, id, part, no){
	switch(state){
		case 1:
		cursor(1);
		document.getElementById(id).style.backgroundColor = bgColor(no);
		break;
		case 0:
		cursor(0);
		document.getElementById(id).style.backgroundColor = "";
		break;
		case 2:
		old = 'divQuest'+part+'_';
		if(document.getElementById('quest'+part)){
			old+=document.getElementById('quest'+part).value;
		}
		if(document.getElementById(old)){
			document.getElementById(old).style.backgroundColor = "";
		}
		document.getElementById(id).style.backgroundColor = bgColor(no);
		document.getElementById('quest'+part).value = no;
		document.getElementById('pointQuest'+part).innerHTML = no;
		sum = 0;
		for(var i=1;i<6;i++){
			sum += parseInt(document.getElementById('questB_'+i).value);
		}
		document.getElementById('total').innerHTML = sum;
		asthmeState(sum);
		break;
	}
}

//Permet de changer le background de la div en fonction de no
function bgColor(no){
	switch(no){
		case 1:
		color = "#ff0000";
		break;
		case 2:
		color = "#FF6600";
		break;
		case 3:
		color = "#FFCC00";
		break;
		case 4:
		color = "#FFFF33";
		break;
		case 5:
		color = "#00CC00";
		break;
	}
	return color;
}

//fonction qui determine le type d'asthme
function asthmeState(val){
	box = document.getElementById('asthmeState');
	box.style.padding = '2px';
	if(val>0 && val<15){
		box.style.color = 'red';
		box.innerHTML = 'Non controlé';
	}
	if(val>=15 && val<20){
		box.style.color = '#FF6600';
		box.innerHTML = 'Perfectible';
	}
	if(val>=20 && val<=25){
		box.style.color = 'green';
		box.innerHTML = 'Controlé';
	}
}