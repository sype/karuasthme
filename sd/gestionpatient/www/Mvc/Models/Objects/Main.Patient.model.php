<?php
/**
 * Object Main_Patient
 * @author SillySmart
 * @copyright SillySmart
 * @package Mvc.Models.Objects
 * @see Sls.Models.Core.SLS_FrontModel
 * @since 1.0
 */
class Main_Patient extends SLS_FrontModel
{
    /**
     * Class variables
	 */
    protected $__idPatient;
    protected $__nomPatient;
    protected $__prenomPatient;
    protected $__idMed;
    protected $__idGesPat;
    protected $__toDoSuivi;
    protected $__noSecuPat;
    protected $__civilitePat;
    protected $__ruePat;
    protected $__cpPat;
    protected $__villePat;
    protected $__dateNais;
    protected $__telPat;
    protected $__portPat;
    protected $__idIncl;
    protected $__DateFinEducation;
    protected $__DateProchainRappel;
    protected $__Reseau;
    protected $_table = "patient";
    protected $_db = "main";
    protected $_primaryKey = "idPatient";
    public $_typeErrors = array();

    /**
     * Constructor Main_Patient
     * @author SillySmart
     * @copyright SillySmart
     * @param bool $mutlilanguage true if multilanguage content, else false
     */
    public function __construct($multilanguage=false)
    {
        parent::__construct($multilanguage);
    }

    /**
     * Set the value of nomPatient
     * Errors can be catched with the public variable $this->_typeErrors['nomPatient']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setNomPatient($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['nomPatient'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['nomPatient'] = "E_NULL";
            return false;
        }

        if ($this->__nomPatient == $value)
            return true;

        if (strlen($value) > 128)
        {
            $this->_typeErrors['nomPatient'] = "E_LENGTH";
            return false;
        }

        $this->__set('nomPatient', $value);
        return true;
    }

    /**
     * Set the value of prenomPatient
     * Errors can be catched with the public variable $this->_typeErrors['prenomPatient']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setPrenomPatient($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['prenomPatient'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['prenomPatient'] = "E_NULL";
            return false;
        }

        if ($this->__prenomPatient == $value)
            return true;

        if (strlen($value) > 128)
        {
            $this->_typeErrors['prenomPatient'] = "E_LENGTH";
            return false;
        }

        $this->__set('prenomPatient', $value);
        return true;
    }

    /**
     * Set the value of idMed
     * Errors can be catched with the public variable $this->_typeErrors['idMed']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setIdMed($value="")
    {
        $this->_generic->useModel("Medecin","main");
        $object = new Main_Medecin();
        if ($object->getModel($value) === false)
        {
            $this->_typeErrors['idMed'] = "E_KEY";
            return false;
        }
        $this->__set('idMed', $value);
        return true;
    }

    /**
     * Set the value of idGesPat
     * Errors can be catched with the public variable $this->_typeErrors['idGesPat']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setIdGesPat($value)
    {
        if ($this->__idGesPat == $value)
            return true;

        if (strlen($value) > 5)
        {
            $this->_typeErrors['idGesPat'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['idGesPat'] = "E_TYPE";
            return false;
        }

        $this->__set('idGesPat', $value);
        return true;
    }

    /**
     * Set the value of toDoSuivi
     * Errors can be catched with the public variable $this->_typeErrors['toDoSuivi']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setToDoSuivi($value)
    {
        if ($this->__toDoSuivi == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['toDoSuivi'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['toDoSuivi'] = "E_TYPE";
            return false;
        }

        $this->__set('toDoSuivi', $value);
        return true;
    }

    /**
     * Set the value of noSecuPat
     * Errors can be catched with the public variable $this->_typeErrors['noSecuPat']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setNoSecuPat($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['noSecuPat'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['noSecuPat'] = "E_NULL";
            return false;
        }

        if ($this->__noSecuPat == $value)
            return true;

        if (strlen($value) > 30)
        {
            $this->_typeErrors['noSecuPat'] = "E_LENGTH";
            return false;
        }

        $this->__set('noSecuPat', $value);
        return true;
    }

    /**
     * Set the value of civilitePat
     * Errors can be catched with the public variable $this->_typeErrors['civilitePat']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setCivilitePat($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['civilitePat'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['civilitePat'] = "E_NULL";
            return false;
        }

        if ($this->__civilitePat == $value)
            return true;

        if (strlen($value) > 6)
        {
            $this->_typeErrors['civilitePat'] = "E_LENGTH";
            return false;
        }

        $this->__set('civilitePat', $value);
        return true;
    }

    /**
     * Set the value of ruePat
     * Errors can be catched with the public variable $this->_typeErrors['ruePat']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setRuePat($value)
    {
        if ($this->__ruePat == $value)
            return true;

        if (strlen($value) > 256)
        {
            $this->_typeErrors['ruePat'] = "E_LENGTH";
            return false;
        }

        $this->__set('ruePat', $value);
        return true;
    }

    /**
     * Set the value of cpPat
     * Errors can be catched with the public variable $this->_typeErrors['cpPat']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setCpPat($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['cpPat'] = "E_EMPTY";
            return false;
        }
		
        if ($value === 0)
        {
            $this->_typeErrors['cpPat'] = "E_NULL";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['cpPat'] = "E_NULL";
            return false;
        }

        if ($this->__cpPat == $value)
            return true;

        if (strlen($value) > 6)
        {
            $this->_typeErrors['cpPat'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['cpPat'] = "E_TYPE";
            return false;
        }

        $this->__set('cpPat', $value);
        return true;
    }

    /**
     * Set the value of villePat
     * Errors can be catched with the public variable $this->_typeErrors['villePat']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setVillePat($value)
    {
        if ($value === "vide")
        {
            $this->_typeErrors['villePat'] = "E_NULL";
            return false;
        }
        if ($this->__villePat == $value)
            return true;

        if (strlen($value) > 128)
        {
            $this->_typeErrors['villePat'] = "E_LENGTH";
            return false;
        }

        $this->__set('villePat', $value);
        return true;
    }

    /**
     * Set the value of dateNais
     * Errors can be catched with the public variable $this->_typeErrors['dateNais']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setDateNais($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['dateNais'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['dateNais'] = "E_NULL";
            return false;
        }

        if ($this->__dateNais == $value)
            return true;

        if (!SLS_Date::isDateTime($value))
        {
            $this->_typeErrors['dateNais'] = "E_TYPE";
            return false;
        }

        $this->__set('dateNais', $value);
        return true;
    }

    /**
     * Set the value of telPat
     * Errors can be catched with the public variable $this->_typeErrors['telPat']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setTelPat($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['telPat'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['telPat'] = "E_NULL";
            return false;
        }

        if ($this->__telPat == $value)
            return true;

        if (strlen($value) > 20)
        {
            $this->_typeErrors['telPat'] = "E_LENGTH";
            return false;
        }

        $this->__set('telPat', $value);
        return true;
    }

    /**
     * Set the value of portPat
     * Errors can be catched with the public variable $this->_typeErrors['portPat']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setPortPat($value)
    {
        if ($this->__portPat == $value)
            return true;

        if (strlen($value) > 20)
        {
            $this->_typeErrors['portPat'] = "E_LENGTH";
            return false;
        }

        $this->__set('portPat', $value);
        return true;
    }

    /**
     * Set the value of idIncl
     * Errors can be catched with the public variable $this->_typeErrors['idIncl']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setIdIncl($value)
    {
        if (empty($value))
        {
            $this->__set('idIncl', $value);
            return true;
        }
        $this->_generic->useModel("Inclusion","main");
        $object = new Main_Inclusion();
        if ($object->getModel($value) === false)
        {
            $this->_typeErrors['idIncl'] = "E_KEY";
            return false;
        }
        $this->__set('idIncl', $value);
        return true;
    }

    /**
     * Set the value of DateFinEducation
     * Errors can be catched with the public variable $this->_typeErrors['DateFinEducation']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setDateFinEducation($value)
    {
        if ($this->__DateFinEducation == $value)
            return true;

        if (!SLS_Date::isDateTime($value))
        {
            $this->_typeErrors['DateFinEducation'] = "E_TYPE";
            return false;
        }

        $this->__set('DateFinEducation', $value);
        return true;
    }

    /**
     * Set the value of DateProchainRappel
     * Errors can be catched with the public variable $this->_typeErrors['DateProchainRappel']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setDateProchainRappel($value)
    {
        if ($this->__DateProchainRappel == $value)
            return true;

        if (strlen($value) > 11)
        {
            $this->_typeErrors['DateProchainRappel'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['DateProchainRappel'] = "E_TYPE";
            return false;
        }

        $this->__set('DateProchainRappel', $value);
        return true;
    }

    /**
     * Set the value of Reseau
     * Errors can be catched with the public variable $this->_typeErrors['Reseau']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setReseau($value="PaP")
    {
        if ($value === "")
        {
            $this->_typeErrors['Reseau'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['Reseau'] = "E_NULL";
            return false;
        }

        if ($this->__Reseau == $value)
            return true;

        if (strlen($value) > 5)
        {
            $this->_typeErrors['Reseau'] = "E_LENGTH";
            return false;
        }

        $this->__set('Reseau', $value);
        return true;
    }
	
	public function searchPatient($keyword, $user_id, $limit = 5)  
    {  
        return $this->_sql->searchPatient($keyword, $user_id, $limit);  
    }

}
?>