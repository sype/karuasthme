<?php
/**
 * Object Main_Configuration
 * @author SillySmart
 * @copyright SillySmart
 * @package Mvc.Models.Objects
 * @see Sls.Models.Core.SLS_FrontModel
 * @since 1.0
 */
class Main_Configuration extends SLS_FrontModel
{
    /**
     * Class variables
	 */
    protected $__config_id;
    protected $__LastSynchroDate;
    protected $__Reseau;
    protected $_table = "configuration";
    protected $_db = "main";
    protected $_primaryKey = "config_id";
    public $_typeErrors = array();

    /**
     * Constructor Main_Configuration
     * @author SillySmart
     * @copyright SillySmart
     * @param bool $mutlilanguage true if multilanguage content, else false
     */
    public function __construct($multilanguage=false)
    {
        parent::__construct($multilanguage);
    }

    /**
     * Set the value of LastSynchroDate
     * Errors can be catched with the public variable $this->_typeErrors['LastSynchroDate']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setLastSynchroDate($value)
    {
        if ($value === "")
        {
            $this->_typeErrors['LastSynchroDate'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['LastSynchroDate'] = "E_NULL";
            return false;
        }

        if ($this->__LastSynchroDate == $value)
            return true;

        if (!SLS_Date::isDateTime($value))
        {
            $this->_typeErrors['LastSynchroDate'] = "E_TYPE";
            return false;
        }

        $this->__set('LastSynchroDate', $value);
        return true;
    }

    /**
     * Set the value of Reseau
     * Errors can be catched with the public variable $this->_typeErrors['Reseau']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setReseau($value="PaP")
    {
        if ($value === "")
        {
            $this->_typeErrors['Reseau'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['Reseau'] = "E_NULL";
            return false;
        }

        if ($this->__Reseau == $value)
            return true;

        if (strlen($value) > 5)
        {
            $this->_typeErrors['Reseau'] = "E_LENGTH";
            return false;
        }

        $this->__set('Reseau', $value);
        return true;
    }

}
?>