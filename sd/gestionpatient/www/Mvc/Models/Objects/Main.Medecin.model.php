<?php
/**
 * Object Main_Medecin
 * @author SillySmart
 * @copyright SillySmart
 * @package Mvc.Models.Objects
 * @see Sls.Models.Core.SLS_FrontModel
 * @since 1.0
 */
class Main_Medecin extends SLS_FrontModel
{
    /**
     * Class variables
	 */
    protected $__idMed;
    protected $__nomMed;
    protected $__prenomMed;
    protected $__pwdMed;
    protected $__token;
    protected $__mailMed;
    protected $__nbConnect;
    protected $_table = "medecin";
    protected $_db = "main";
    protected $_primaryKey = "idMed";
    public $_typeErrors = array();

    /**
     * Constructor Main_Medecin
     * @author SillySmart
     * @copyright SillySmart
     * @param bool $mutlilanguage true if multilanguage content, else false
     */
    public function __construct($multilanguage=false)
    {
        parent::__construct($multilanguage);
    }

    /**
     * Set the value of nomMed
     * Errors can be catched with the public variable $this->_typeErrors['nomMed']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setNomMed($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['nomMed'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['nomMed'] = "E_NULL";
            return false;
        }

        if ($this->__nomMed == $value)
            return true;

        if (strlen($value) > 128)
        {
            $this->_typeErrors['nomMed'] = "E_LENGTH";
            return false;
        }

        $this->__set('nomMed', $value);
        return true;
    }

    /**
     * Set the value of prenomMed
     * Errors can be catched with the public variable $this->_typeErrors['prenomMed']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setPrenomMed($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['prenomMed'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['prenomMed'] = "E_NULL";
            return false;
        }

        if ($this->__prenomMed == $value)
            return true;

        if (strlen($value) > 128)
        {
            $this->_typeErrors['prenomMed'] = "E_LENGTH";
            return false;
        }

        $this->__set('prenomMed', $value);
        return true;
    }

    /**
     * Set the value of pwdMed
     * Errors can be catched with the public variable $this->_typeErrors['pwdMed']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setPwdMed($value)
    {
        if ($this->__pwdMed == $value)
            return true;

        if (strlen($value) > 40)
        {
            $this->_typeErrors['pwdMed'] = "E_LENGTH";
            return false;
        }

        $this->__set('pwdMed', $value);
        return true;
    }

    /**
     * Set the value of token
     * Errors can be catched with the public variable $this->_typeErrors['token']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setToken($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['token'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['token'] = "E_NULL";
            return false;
        }

        if ($this->__token == $value)
            return true;

        if (strlen($value) > 40)
        {
            $this->_typeErrors['token'] = "E_LENGTH";
            return false;
        }

        $this->__set('token', $value);
        return true;
    }

    /**
     * Set the value of mailMed
     * Errors can be catched with the public variable $this->_typeErrors['mailMed']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setMailMed($value="")
    {
        if (!SLS_String::validateEmail($value))
        {
            $this->_typeErrors['mailMed'] = "E_TYPE";
            return false;
        }

        if ($value === "")
        {
            $this->_typeErrors['mailMed'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['mailMed'] = "E_NULL";
            return false;
        }

        if ($this->__mailMed == $value)
            return true;

        if (!$this->isUnique('mailMed',$value))
        {
            $this->_typeErrors['mailMed'] = "E_UNIQUE";
            return false;
        }

        if (strlen($value) > 128)
        {
            $this->_typeErrors['mailMed'] = "E_LENGTH";
            return false;
        }

        $this->__set('mailMed', $value);
        return true;
    }

    /**
     * Set the value of nbConnect
     * Errors can be catched with the public variable $this->_typeErrors['nbConnect']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setNbConnect($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['nbConnect'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['nbConnect'] = "E_NULL";
            return false;
        }

        if ($this->__nbConnect == $value)
            return true;

        if (strlen($value) > 10)
        {
            $this->_typeErrors['nbConnect'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['nbConnect'] = "E_TYPE";
            return false;
        }

        $this->__set('nbConnect', $value);
        return true;
    }
	
	public function newMedecin($id, $nom, $prenom, $pwd, $mail)
	{
		return $this->_sql->newMedecin($id, $nom, $prenom, $pwd, $mail);  
	}

}
?>