<?php
/**
 * Object Main_Inclusion
 * @author SillySmart
 * @copyright SillySmart
 * @package Mvc.Models.Objects
 * @see Sls.Models.Core.SLS_FrontModel
 * @since 1.0
 */
class Main_Inclusion extends SLS_FrontModel
{
    /**
     * Class variables
	 */
    protected $__idIncl;
    protected $__severitePat;
    protected $__nbGene;
    protected $__traitCrise;
    protected $__traitFond;
    protected $__traitAutre;
    protected $__consulPrec;
    protected $__hospPrec;
    protected $__consultNoProg;
    protected $__nbStopTrav;
    protected $__dureeStopTrav;
    protected $__nbStopSchool;
    protected $__dureeStopSchool;
    protected $__dateIncl;
    protected $_table = "inclusion";
    protected $_db = "main";
    protected $_primaryKey = "idIncl";
    public $_typeErrors = array();

    /**
     * Constructor Main_Inclusion
     * @author SillySmart
     * @copyright SillySmart
     * @param bool $mutlilanguage true if multilanguage content, else false
     */
    public function __construct($multilanguage=false)
    {
        parent::__construct($multilanguage);
    }

    /**
     * Set the value of severitePat
     * Errors can be catched with the public variable $this->_typeErrors['severitePat']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setSeveritePat($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['severitePat'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['severitePat'] = "E_NULL";
            return false;
        }

        if ($this->__severitePat == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['severitePat'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['severitePat'] = "E_TYPE";
            return false;
        }

        $this->__set('severitePat', $value);
        return true;
    }

    /**
     * Set the value of nbGene
     * Errors can be catched with the public variable $this->_typeErrors['nbGene']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setNbGene($value="0")
    {
        if ($this->__nbGene == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['nbGene'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['nbGene'] = "E_TYPE";
            return false;
        }

        $this->__set('nbGene', $value);
        return true;
    }

    /**
     * Set the value of traitCrise
     * Errors can be catched with the public variable $this->_typeErrors['traitCrise']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setTraitCrise($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['traitCrise'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['traitCrise'] = "E_NULL";
            return false;
        }

        if ($this->__traitCrise == $value)
            return true;

        if (strlen($value) > 256)
        {
            $this->_typeErrors['traitCrise'] = "E_LENGTH";
            return false;
        }

        $this->__set('traitCrise', $value);
        return true;
    }

    /**
     * Set the value of traitFond
     * Errors can be catched with the public variable $this->_typeErrors['traitFond']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setTraitFond($value="Non précisé")
    {
        if ($this->__traitFond == $value)
            return true;

        if (strlen($value) > 256)
        {
            $this->_typeErrors['traitFond'] = "E_LENGTH";
            return false;
        }

        $this->__set('traitFond', $value);
        return true;
    }

    /**
     * Set the value of traitAutre
     * Errors can be catched with the public variable $this->_typeErrors['traitAutre']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setTraitAutre($value="Non précisé")
    {
        if ($this->__traitAutre == $value)
            return true;

        if (strlen($value) > 256)
        {
            $this->_typeErrors['traitAutre'] = "E_LENGTH";
            return false;
        }

        $this->__set('traitAutre', $value);
        return true;
    }

    /**
     * Set the value of consulPrec
     * Errors can be catched with the public variable $this->_typeErrors['consulPrec']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setConsulPrec($value="0")
    {
        if ($this->__consulPrec == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['consulPrec'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['consulPrec'] = "E_TYPE";
            return false;
        }

        $this->__set('consulPrec', $value);
        return true;
    }

    /**
     * Set the value of hospPrec
     * Errors can be catched with the public variable $this->_typeErrors['hospPrec']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setHospPrec($value="0")
    {
        if ($this->__hospPrec == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['hospPrec'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['hospPrec'] = "E_TYPE";
            return false;
        }

        $this->__set('hospPrec', $value);
        return true;
    }

    /**
     * Set the value of consultNoProg
     * Errors can be catched with the public variable $this->_typeErrors['consultNoProg']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setConsultNoProg($value="0")
    {
        if ($this->__consultNoProg == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['consultNoProg'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['consultNoProg'] = "E_TYPE";
            return false;
        }

        $this->__set('consultNoProg', $value);
        return true;
    }

    /**
     * Set the value of nbStopTrav
     * Errors can be catched with the public variable $this->_typeErrors['nbStopTrav']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setNbStopTrav($value="0")
    {
        if ($this->__nbStopTrav == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['nbStopTrav'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['nbStopTrav'] = "E_TYPE";
            return false;
        }

        $this->__set('nbStopTrav', $value);
        return true;
    }

    /**
     * Set the value of dureeStopTrav
     * Errors can be catched with the public variable $this->_typeErrors['dureeStopTrav']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setDureeStopTrav($value="0")
    {
        if ($this->__dureeStopTrav == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['dureeStopTrav'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['dureeStopTrav'] = "E_TYPE";
            return false;
        }

        $this->__set('dureeStopTrav', $value);
        return true;
    }

    /**
     * Set the value of nbStopSchool
     * Errors can be catched with the public variable $this->_typeErrors['nbStopSchool']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setNbStopSchool($value="0")
    {
        if ($this->__nbStopSchool == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['nbStopSchool'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['nbStopSchool'] = "E_TYPE";
            return false;
        }

        $this->__set('nbStopSchool', $value);
        return true;
    }

    /**
     * Set the value of dureeStopSchool
     * Errors can be catched with the public variable $this->_typeErrors['dureeStopSchool']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setDureeStopSchool($value="0")
    {
        if ($this->__dureeStopSchool == $value)
            return true;

        if (strlen($value) > 3)
        {
            $this->_typeErrors['dureeStopSchool'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['dureeStopSchool'] = "E_TYPE";
            return false;
        }

        $this->__set('dureeStopSchool', $value);
        return true;
    }

    /**
     * Set the value of dateIncl
     * Errors can be catched with the public variable $this->_typeErrors['dateIncl']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setDateIncl($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['dateIncl'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['dateIncl'] = "E_NULL";
            return false;
        }

        if ($this->__dateIncl == $value)
            return true;

        if (!SLS_Date::isDateTime($value))
        {
            $this->_typeErrors['dateIncl'] = "E_TYPE";
            return false;
        }

        $this->__set('dateIncl', $value);
        return true;
    }

}
?>