<?php
/**
 * Object Main_Fiches
 * @author SillySmart
 * @copyright SillySmart
 * @package Mvc.Models.Objects
 * @see Sls.Models.Core.SLS_FrontModel
 * @since 1.0
 */
class Main_Fiches extends SLS_FrontModel
{
    /**
     * Class variables
	 */
    protected $__idFiche;
    protected $__questA_1;
    protected $__questA_2;
    protected $__questA_3;
    protected $__questA_4;
    protected $__questA_5;
    protected $__questA_6;
    protected $__questB_1;
    protected $__questB_2;
    protected $__questB_3;
    protected $__questB_4;
    protected $__questB_5;
    protected $__date;
    protected $__idPatient;
    protected $_table = "fiches";
    protected $_db = "main";
    protected $_primaryKey = "idFiche";
    public $_typeErrors = array();

    /**
     * Constructor Main_Fiches
     * @author SillySmart
     * @copyright SillySmart
     * @param bool $mutlilanguage true if multilanguage content, else false
     */
    public function __construct($multilanguage=false)
    {
        parent::__construct($multilanguage);
    }

    /**
     * Set the value of questA_1
     * Errors can be catched with the public variable $this->_typeErrors['questA_1']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestA1($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questA_1'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questA_1'] = "E_NULL";
            return false;
        }

        if ($this->__questA_1 == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['questA_1'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questA_1'] = "E_TYPE";
            return false;
        }

        $this->__set('questA_1', $value);
        return true;
    }

    /**
     * Set the value of questA_2
     * Errors can be catched with the public variable $this->_typeErrors['questA_2']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestA2($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questA_2'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questA_2'] = "E_NULL";
            return false;
        }

        if ($this->__questA_2 == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['questA_2'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questA_2'] = "E_TYPE";
            return false;
        }

        $this->__set('questA_2', $value);
        return true;
    }

    /**
     * Set the value of questA_3
     * Errors can be catched with the public variable $this->_typeErrors['questA_3']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestA3($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questA_3'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questA_3'] = "E_NULL";
            return false;
        }

        if ($this->__questA_3 == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['questA_3'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questA_3'] = "E_TYPE";
            return false;
        }

        $this->__set('questA_3', $value);
        return true;
    }

    /**
     * Set the value of questA_4
     * Errors can be catched with the public variable $this->_typeErrors['questA_4']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestA4($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questA_4'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questA_4'] = "E_NULL";
            return false;
        }

        if ($this->__questA_4 == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['questA_4'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questA_4'] = "E_TYPE";
            return false;
        }

        $this->__set('questA_4', $value);
        return true;
    }

    /**
     * Set the value of questA_5
     * Errors can be catched with the public variable $this->_typeErrors['questA_5']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestA5($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questA_5'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questA_5'] = "E_NULL";
            return false;
        }

        if ($this->__questA_5 == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['questA_5'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questA_5'] = "E_TYPE";
            return false;
        }

        $this->__set('questA_5', $value);
        return true;
    }

    /**
     * Set the value of questA_6
     * Errors can be catched with the public variable $this->_typeErrors['questA_6']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestA6($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questA_6'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questA_6'] = "E_NULL";
            return false;
        }

        if ($this->__questA_6 == $value)
            return true;

        if (strlen($value) > 1)
        {
            $this->_typeErrors['questA_6'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questA_6'] = "E_TYPE";
            return false;
        }

        $this->__set('questA_6', $value);
        return true;
    }

    /**
     * Set the value of questB_1
     * Errors can be catched with the public variable $this->_typeErrors['questB_1']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestB1($value)
    {
        if ($this->__questB_1 == $value)
            return true;

        if (strlen($value) > 2)
        {
            $this->_typeErrors['questB_1'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questB_1'] = "E_TYPE";
            return false;
        }

        $this->__set('questB_1', $value);
        return true;
    }

    /**
     * Set the value of questB_2
     * Errors can be catched with the public variable $this->_typeErrors['questB_2']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestB2($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questB_2'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questB_2'] = "E_NULL";
            return false;
        }

        if ($this->__questB_2 == $value)
            return true;

        if (strlen($value) > 2)
        {
            $this->_typeErrors['questB_2'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questB_2'] = "E_TYPE";
            return false;
        }

        $this->__set('questB_2', $value);
        return true;
    }

    /**
     * Set the value of questB_3
     * Errors can be catched with the public variable $this->_typeErrors['questB_3']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestB3($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questB_3'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questB_3'] = "E_NULL";
            return false;
        }

        if ($this->__questB_3 == $value)
            return true;

        if (strlen($value) > 2)
        {
            $this->_typeErrors['questB_3'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questB_3'] = "E_TYPE";
            return false;
        }

        $this->__set('questB_3', $value);
        return true;
    }

    /**
     * Set the value of questB_4
     * Errors can be catched with the public variable $this->_typeErrors['questB_4']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestB4($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questB_4'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questB_4'] = "E_NULL";
            return false;
        }

        if ($this->__questB_4 == $value)
            return true;

        if (strlen($value) > 2)
        {
            $this->_typeErrors['questB_4'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questB_4'] = "E_TYPE";
            return false;
        }

        $this->__set('questB_4', $value);
        return true;
    }

    /**
     * Set the value of questB_5
     * Errors can be catched with the public variable $this->_typeErrors['questB_5']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setQuestB5($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['questB_5'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['questB_5'] = "E_NULL";
            return false;
        }

        if ($this->__questB_5 == $value)
            return true;

        if (strlen($value) > 2)
        {
            $this->_typeErrors['questB_5'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['questB_5'] = "E_TYPE";
            return false;
        }

        $this->__set('questB_5', $value);
        return true;
    }

    /**
     * Set the value of date
     * Errors can be catched with the public variable $this->_typeErrors['date']
     * @author SillySmart
     * @copyright SillySmart
     * @param string $value
     * @return bool true if updated, false if not
     */
    public function setDate($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['date'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['date'] = "E_NULL";
            return false;
        }

        if ($this->__date == $value)
            return true;

        if (!SLS_Date::isDateTime($value))
        {
            $this->_typeErrors['date'] = "E_TYPE";
            return false;
        }

        $this->__set('date', $value);
        return true;
    }

    /**
     * Set the value of idPatient
     * Errors can be catched with the public variable $this->_typeErrors['idPatient']
     * @author SillySmart
     * @copyright SillySmart
     * @param int $value
     * @return bool true if updated, false if not
     */
    public function setIdPatient($value="")
    {
        if ($value === "")
        {
            $this->_typeErrors['idPatient'] = "E_EMPTY";
            return false;
        }

        if (is_null($value))
        {
            $this->_typeErrors['idPatient'] = "E_NULL";
            return false;
        }

        if ($this->__idPatient == $value)
            return true;

        if (strlen($value) > 5)
        {
            $this->_typeErrors['idPatient'] = "E_LENGTH";
            return false;
        }

        if (!is_numeric($value))
        {
            $this->_typeErrors['idPatient'] = "E_TYPE";
            return false;
        }

        $this->__set('idPatient', $value);
        return true;
    }

}
?>