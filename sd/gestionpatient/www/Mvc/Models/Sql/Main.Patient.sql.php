<?php
/**
* Object Main_PatientSql
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Models.Objects
* @see Sls.Models.Core.SLS_FrontModel
* @since 1.0
*/
class Main_PatientSql extends SLS_FrontModelSql
{
	/**
     * Search patient for autocompletion
     * @param Array $keyword
     */
    public function searchPatient($keyword, $user_id, $limit = 5)
    {
    	$orReq = "";
    	if(is_array($keyword))
    	for($i=0;$i<count($keyword);$i++)
    	{
    		$orReq .= "
    					nomPatient LIKE ".$this->_db->quote("%".$keyword[$i]."%")."
	    			OR
	    				prenomPatient LIKE ".$this->_db->quote("%".$keyword[$i]."%")."
	    			OR
	    				noSecuPat LIKE ".$this->_db->quote("%".$keyword[$i]."%")."";
    		if(!$i == count($keyword) -1)
    		{
    			$orReq .= " OR";
    		}
    	}else
    	{
    		$orReq .= "
    					nomPatient LIKE ".$this->_db->quote("%".$keyword."%")."
	    			OR
	    				prenomPatient LIKE ".$this->_db->quote("%".$keyword."%")."
	    			OR
	    				noSecuPat LIKE ".$this->_db->quote("%".$keyword."%")."";
    	}
    	$sql =  "SELECT
    				*
    			FROM
    				patient
    			WHERE
    				idMed = ".$this->_db->quote($user_id)."
    			AND
    			(
	    		 ".$orReq."
    			)
    			ORDER BY
    				prenomPatient DESC
    			LIMIT ".$limit;
    	return $this->_db->select($sql); 
    }					 	
}
?>