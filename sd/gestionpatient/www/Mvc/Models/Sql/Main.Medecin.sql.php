<?php
/**
* Object Main_MedecinSql
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Models.Objects
* @see Sls.Models.Core.SLS_FrontModel
* @since 1.0
*/
class Main_MedecinSql extends SLS_FrontModelSql
{
	/**
    * Insert a new medecin
    */
    public function newMedecin($id, $nom, $prenom, $pwd, $mail)
    {
    	$sql = "INSERT INTO medecin(idMed, nomMed, prenomMed, pwdMed, mailMed) VALUE(".$this->_db->quote($id).", ".$this->_db->quote($nom).", ".$this->_db->quote($prenom).", ".$this->_db->quote($pwd).", ".$this->_db->quote($mail).");";
    	return $this->_db->insert($sql); 
    }
}
?>