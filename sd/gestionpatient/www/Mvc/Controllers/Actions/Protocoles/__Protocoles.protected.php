<?php
/**
* Class generic for the controller Protocoles
* Write here all your generic functions you need in your Protocoles Actions
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Protocoles
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*/
class ProtocolesControllerProtected extends SiteProtected
{

}
?>