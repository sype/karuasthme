<?php
/**
* Class Index into Protocoles Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Protocoles
* @see Mvc.Controllers.Protocoles.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class ProtocolesIndex extends ProtocolesControllerProtected
{
	public function action()
	{
		if($this->isLogged())
		{
			$xml = $this->getXML();
			$xml->addFullTag("protocoles_part", "selected");
			$this->saveXML($xml);
			
			if($this->_http->getParam("Pages") && $page = explode("-", $this->_http->getParam("Pages")))
			{
				require_once($this->_generic->getPathConfig("scripts")."Class/Fpdf/fpdf.php");
				require_once($this->_generic->getPathConfig("scripts")."Class/Fpdf/fpdi.php");
				$pdf=new FPDI();
				$pdf->setSourceFile($this->_generic->getPathConfig("files")."guide.pdf");
				for($i = $page[0];$i<$page[1] + 1;$i++)
				{
					$pdf->addPage();
					$tplidx = $pdf->importPage($i);
					$pdf->useTemplate($tplidx);
				}
				$pdf->Output('newpdf', 'I');
			}
		}else 
			$this->forward("User", "Login");
	}
}
?>