<?php
/**
 * Class Generic for the entire WebSite
 * Write here all your functions you need in your website.
 * 
 * @author SillySmart
 * @copyright SillySmart
 * @package Mvc.Controllers
 * @see Sls.Controllers.Core.SLS_GenericController
 * @since 1.0
 */
class SiteProtected extends SLS_GenericController
{
	/**
	 * Check if current user is logged
	 * 
	 * @return bool true if logged, else false
	 */
	public function isLogged()
	{
		$session = $this->_generic->getObjectSession();
		return ($session->getParam("is_logged") == "true") ? true : false;
	}
	
	/**
	 * Force login for a given user_id
	 * 
	 * @param int $user_id PK User
	 */
	public function forceLogin($user_id)
	{
		$this->_generic->useModel("Medecin");
		$user = new Main_Medecin();
		$session = $this->_generic->getObjectSession();

		if ($user->getModel($user_id) == true)
		{
			$session->setParam("is_logged","true");
			$session->setParam("user_id",$user->__get("idMed"));
			$session->setParam("user_name",ucfirst(strtolower($user->__get("nomMed"))));
			$session->setParam("user_firstname",ucfirst(strtolower($user->__get("prenomMed"))));                
		}
	}
}
?>