<?php
/**
* Class ListeNouveauQuestionnaire into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewayListeNouveauQuestionnaire extends GatewayControllerProtected
{
	public function action()
	{
		header("Content-Type: text/xml;charset=UTF8;");
		echo "<?xml version='1.0'?>";
		echo "<liste>";
		$date = date("Y-m-d", strtotime("-1 day", strtotime($this->_http->getParam("date"))));
		$this->useModel("fiches");
		$fiche = new Main_Fiches();
		$fiches = $fiche->searchModels("fiches", array(0=>array("table"=>"patient","column"=>"idPatient")), array(0=>array("column"=>"date","value"=>$date,"mode"=>"gt"), 1=>array("column"=>"Reseau","value"=>$this->_http->getParam("reseau"),"mode"=>"equal")));
		if(!empty($fiches))
		{
			for($i=0;$i<count($fiches);$i++):
			?>
				<questionnaire>
			     <idPatient><?php echo $fiches[$i]->idGesPat; ?></idPatient>
			     <idFiche><?php echo $fiches[$i]->idFiche; ?></idFiche>
			     <nomPatient><?php echo $fiches[$i]->nomPatient; ?></nomPatient>
			     <prenomPatient><?php echo $fiches[$i]->prenomPatient; ?></prenomPatient>
			     <Date><?php echo date("Y-m-d",strtotime($fiches[$i]->date)); ?></Date>
			   </questionnaire>
			<?php
			endfor;
		}
		echo "</liste>";
		die();
	}
}
?>