<?php
/**
* Class EmailRappel into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewayEmailRappel extends GatewayControllerProtected
{
	public function action()
	{
		header("Content-Type: text/xml;charset=UTF8;");
		echo "<?xml version='1.0'?>";
		$EmailType = 0;
		
		if($this->_http->getParam("EmailType"))
		{
			$EmailType = $this->_http->getParam("EmailType");
		}
		echo "<rapport>";
		echo "<etat>";
		if($EmailType == 0)
		{
			echo "0";
		}else
		{
			$this->useModel("Patient");
			$patient = new Main_Patient();
			$emailSended = 0;
			$DateProchainRappel = date("Y-m-d",strtotime(date("Y-m-d") . " -".($EmailType -1)." month"));
			$DateProchainRappellimit = date("Y-m-d",strtotime(date("Y-m-d") . " -".($EmailType + 2)." month"));
			//echo "DateFinEducation inférieur à ".$DateProchainRappel." ";
			//echo "DateFinEducation suppérieur à ".$DateProchainRappellimit." ";
			$patients = $patient->searchModels("patient",array("medecin"), array(0=>array("column"=>"DateFinEducation","value"=>$DateProchainRappel,"mode"=>"lt"),1=>array("column"=>"DateFinEducation","value"=>$DateProchainRappellimit,"mode"=>"gt"), 2=>array("column"=>"DateProchainRappel","value"=>$EmailType,"mode"=>"equal")));
			if(empty($patients))
			{
				echo "0";
			}else
			{
			$j = 0;
				for($i=0;$i<count($patients);$i++)
				{   if($EmailType <= 9)
				    {
						$mailMed = $patients[$i]->mailMed;
						$nomMed = $patients[$i]->nomMed;
						$prenomPatient = $patients[$i]->prenomPatient;
						$nomPatient = $patients[$i]->nomPatient;
						$idPatient = $patients[$i]->idPatient;
						$civilitePatient = $patients[$i]->civilitePat;
						$DateFinEducation = $patients[$i]->DateFinEducation;
						$telPat = $patients[$i]->telPat;
						$portPat = $patients[$i]->portPat;
						$toDoSuivi = $patients[$i]->toDoSuivi;
						
						/*if($toDoSuivi == 1)
						{*/
							$msgHtml = "Bonjour Docteur ".strtoupper($nomMed).",<br/><br/>Un suivi patient planifié à $EmailType mois arrive bientot à echeance.<br/><br/>";
							$msgHtml .= "   Veuillez trouver ci dessous les informations relative au patient concerné par le suivi ainsi qu'un moyen de le contacter : <br/>";
							$msgHtml .= "             - Patient : <b> $civilitePatient $prenomPatient $nomPatient </b> <br/>";
							$msgHtml .= "             - A terminé son éducation le : <b>" . date("d/m/Y",strtotime($DateFinEducation)) . "</b> <br/>";
							$msgHtml .= "             - Téléphone : <b>$telPat </b> <br/>";
							if($portPat)
								$msg .= "             - GSM : <b>$portPat </b> <br/>";
							$msgHtml .= "<br/>Vous pouvez vous connecter sur votre espace professionnel en cliquant sur le lien ci-dessous ou en le copiant dans votre navigateur :<br/><br/>";
							$msgHtml .= "<a href=\"".$this->_generic->getFullPath("Home","Index")."\">".$this->_generic->getFullPath("Home","Index")."</a>";
							$msgHtml .= "<br/><br/>Merci de votre collaboration.";
							$msgHtml .= "<br/><br/>Cordialement,";
							$msgHtml .= "<br/><br/>Karu Asthme.";
												
							$email = new SLS_Email(SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("host"),$this->_generic->getSiteConfig("privateKey")),
												   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("port"),$this->_generic->getSiteConfig("privateKey")),
												   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("username"),$this->_generic->getSiteConfig("privateKey")),
												   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("password"),$this->_generic->getSiteConfig("privateKey")));
							$email->openConnection();
							$email->setCharset("UTF-8");
							$email->addRecipient($mailMed,"To");
							//$email->addRecipient("crochemont@pisystems.fr","To"); 
							$email->setSender($this->_generic->getMailConfig('defaultSender').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameSender'));
							$email->setReply($this->_generic->getMailConfig('defaultReply').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReply'));
							$email->setReturn($this->_generic->getMailConfig('defaultReturn').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReturn'));
							$email->setSubject(SLS_String::makeEmailSubject('Karu Asthme - Suivi du Patient ' . $prenomPatient . ' ' . $nomPatient));											
							$email->setHtml(mb_convert_encoding(str_replace(array("\n","\t"),array("",""),$msgHtml),"UTF-8","UTF-8"));
							$email->setPlain(strip_tags(SLS_String::br2nl($msgHtml)));
							$email->compileMail();
							$state = $email->send();
							$emailSended = $emailSended + 1;
						/*}
						else
						{
							$j++;
						}*/
					}
					$patient->getModel($idPatient);
					
					if($EmailType <= 9)
					{	
						$DateProchainRappel = $EmailType + 3;
						$patient->setDateProchainRappel($DateProchainRappel);		
					}else
					{
						//il n'y a plus de rappel apres 9 mois
						$patient->setDateProchainRappel(NULL);
						$patient->setToDoSuivi(0);						
					}
					$patient->save();
				}
				//echo count($patients)."patients dont ";
				//echo $j." ne correspondant pas aux critères ";
				//echo $emailSended." mail envoyer ";
				echo $emailSended;
			}
		}
		echo "</etat>";
		echo "</rapport>";
		die();
	}
}
?>