<?php
/**
* Class FichePatient into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewayFichePatient extends GatewayControllerProtected
{
	public function action()
	{
		$id = $this->_http->getParam("idPatient");
		$this->useModel("Patient");
		$patient = new Main_Patient();
		header("Content-Type: text/xml;charset=UTF8;");
		echo "<?xml version='1.0'?>";
		echo "<liste>";
		$patients = array_shift($patient->searchModels("patient",array("inclusion"), array(0=>array("column"=>"idPatient","value"=>$id,"mode"=>"equal"))));
		if(!empty($patients)):
		?>
			<patient>
			   <noSecu><?php echo $patients->noSecuPat; ?></noSecu>
			   <civilite><?php echo $patients->civilitePat; ?></civilite>
			   <nom><?php echo $patients->nomPatient; ?></nom>
			   <prenom><?php echo $patients->prenomPatient; ?></prenom>
			   <rue><?php echo $patients->ruePat; ?></rue>
			   <cp><?php echo $patients->cpPat; ?></cp>
			   <ville><?php echo $patients->villePat; ?></ville>
			   <dateNais><?php echo $patients->dateNais; ?></dateNais>
			   <tel><?php echo $patients->telPat; ?></tel>
			   <port><?php echo $patients->portPat; ?></port>
			   <severite><?php echo $patients->severitePat; ?></severite>
			   <nbGene><?php echo $patients->nbGene; ?></nbGene>
			   <traitCrise><?php echo str_replace($tab, $tab2, $patients->traitCrise); ?></traitCrise>
			   <traitFond><?php echo str_replace($tab, $tab2, $patients->traitFond); ?></traitFond>
			   <traitAutre><?php echo str_replace($tab, $tab2, $patients->traitAutre); ?></traitAutre>
			   <consulPrec><?php echo $patients->consulPrec; ?></consulPrec>
			   <hospPrec><?php echo $patients->hospPrec; ?></hospPrec>
			   <idMed><?php echo $patients->idMed; ?></idMed>
			   <consultNoProg><?php echo $patients->consultNoProg; ?></consultNoProg>
			   <nbStopTrav><?php echo $patients->nbStopTrav; ?></nbStopTrav>
			   <dureeStopTrav><?php echo $patients->dureeStopTrav; ?></dureeStopTrav>
			   <nbStopSchool><?php echo $patients->nbStopSchool; ?></nbStopSchool>
			   <dureeStopSchool><?php echo $patients->dureeStopSchool; ?></dureeStopSchool>
		   </patient>
		   <?php
		endif;
		echo "</liste>";
		die();
	}
}
?>