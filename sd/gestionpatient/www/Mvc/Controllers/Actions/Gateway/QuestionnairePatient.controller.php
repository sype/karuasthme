<?php
/**
* Class QuestionnairePatient into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewayQuestionnairePatient extends GatewayControllerProtected
{
	public function action()
	{
		require_once($this->_generic->getPathConfig("scripts")."Class/Fpdf/fpdf.php");
		require_once($this->_generic->getPathConfig("scripts")."Class/Fpdf/fpdi.php");
		$this->useModel("Patient");
		$patient = new Main_Patient();
		$patients = array_shift($patient->searchModels("patient",array("fiches", "medecin"), array(0=>array("column"=>"idFiche","value"=>$this->_http->getParam("id"),"mode"=>"equal"))));
		if(!empty($patients))
		{
			$pdf=new FPDI();
           $pdf->AddPage();
		   $pdf->Image($this->_generic->getPathConfig("scripts")."Class/Fpdf/pdf/question.jpg", 0, 0); 
		   $pdf->SetFont('Arial','B',10);
		   $pdf->Text(5, 5, utf8_decode("Médecin : ".$patients->prenomMed." ".strtoupper($patients->nomMed)));
		   $pdf->Text(5, 10, utf8_decode("Patient : ".$patients->prenomPatient." ".strtoupper($patients->nomPatient)));
		   $pdf->Text(5, 15, "Date : ".$patients->date);
		   $pdf->SetFont('Arial','B',12);
		   $pdf->SetTextColor(220,50,50);
		   if($patients->questA_1){
		       $pdf->Text(122, 85, "X");
		   }else{
		         $pdf->Text(167, 85, "X");
		   }
		   if($patients->questA_2){
		       $pdf->Text(122, 98, "X");
		   }else{
		         $pdf->Text(167, 98, "X");
		   }
		   if($patients->questA_3){
		       $pdf->Text(123, 111, "X");
		   }else{
		         $pdf->Text(167, 111, "X");
		   }
		   if($patients->questA_4){
		       $pdf->Text(170, 121, "X");
		   }else{
		         $pdf->Text(187, 121, "X");
		   }
		   if($patients->questA_5){
		       $pdf->Text(151, 140, "X");
		   }else{
		         $pdf->Text(183, 140, "X");
		   }
		   if($patients->questA_6){
		       $pdf->Text(97, 212, "X");
		   }else{
		         $pdf->Text(138, 212, "X");
		   }
           $pdf->AddPage();
		   $pdf->Image($this->_generic->getPathConfig("scripts")."Class/Fpdf/pdf/controle.jpg", 0, 0); 
		   $pdf->SetFont('Arial','B',25);
		   $pdf->Text(179, 106, $patients->questB_1);
		   $pdf->Text(179, 133, $patients->questB_2);
		   $pdf->Text(179, 171, $patients->questB_3);
		   $pdf->Text(179, 203, $patients->questB_4);
		   $pdf->Text(179, 230, $patients->questB_5);
		   $total = $patients->questB_1+$patients->questB_2+$patients->questB_3+$patients->questB_4+$patients->questB_5;
		   //Détection type asthme
			if($total>0 && $total<15){
				$pdf->SetTextColor(255,0,0);
				$type = "non controlé";
			}					
			if($total>=15 && $total<20){
				$pdf->SetTextColor(255,102,0);
				$type = "perfectible";
			}
			if($total>=20 && $total<=25){
				$pdf->SetTextColor(0,128,0);
				$type = "controlé";
			}
		   $pdf->Text(179, 251, $total);
		   $pdf->Text(25, 251, "Asthme ".utf8_decode($type));
		   $pdf->Output('newpdf', 'I');
		}
		die();
	}
}
?>