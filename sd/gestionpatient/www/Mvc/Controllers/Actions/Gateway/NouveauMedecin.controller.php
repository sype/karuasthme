<?php
/**
* Class NouveauMedecin into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewayNouveauMedecin extends GatewayControllerProtected
{
	public function action()
	{
		$this->useModel("Medecin");
		$user = new Main_Medecin();
		header("Content-Type: text/xml;charset=ISO-8859-1;");
		echo "<?xml version='1.0'?>";
		echo "<rapport><etat>";
		if($this->_http->getParam("id") && $this->_http->getParam("nom") && $this->_http->getParam("prenom") && $this->_http->getParam("mail"))
		{
			if(SLS_String::validateEmail($this->_http->getParam("mail")))
			{
				
				$id = $this->_http->getParam("id");
				$nom = $this->_http->getParam("nom");
				$prenom = $this->_http->getParam("prenom");
				$mail = $this->_http->getParam("mail");
				$pwdMed = $this->passwd();
				
				if($user->newMedecin($id, $nom, $prenom, sha1($pwdMed), $mail) != NULL)
				{
					$msgHtml = "Bonjour ".ucfirst($prenom)." ".strtoupper($nom).",<br/><br/>Votre inscription à \"Karu Asthme - Gestion Patient\" a été effectuée, ci-dessous vos identifiants de connexion :<br/><br/>";
					$msgHtml .= "Email : ".$mail."<br/>"."Mot de passe : ".$pwdMed."<br/><br/>";
					$msgHtml .= "Vous pouvez vous connecter dès maintenant en cliquant sur le lien ci-dessous ou en le copiant dans votre navigateur :<br/><br/>";
					$msgHtml .= "<a href=\"".$this->_generic->getFullPath("Home","Index")."\">".$this->_generic->getFullPath("Home","Index")."</a>";
					$msgHtml .= "<br/><br/>Nous vous conseillons vivement de modifier votre mot de passe dans le menu \"Mon compte\" de l'application.";
					$msgHtml .= "<br/><br/>Cordialement,";
					$msgHtml .= "<br/><br/>Karu Asthme.";
					$email = new SLS_Email(SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("host"),$this->_generic->getSiteConfig("privateKey")),
										   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("port"),$this->_generic->getSiteConfig("privateKey")),
										   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("username"),$this->_generic->getSiteConfig("privateKey")),
										   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("password"),$this->_generic->getSiteConfig("privateKey")));
					$email->openConnection();
					$email->setCharset("UTF-8");
					$email->addRecipient($mail,"To");
					$email->setSender($this->_generic->getMailConfig('defaultSender').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameSender'));
					$email->setReply($this->_generic->getMailConfig('defaultReply').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReply'));
					$email->setReturn($this->_generic->getMailConfig('defaultReturn').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReturn'));
					$email->setSubject(SLS_String::makeEmailSubject('Inscription Karu Asthme - Gestion Patient'));											
					$email->setHtml(mb_convert_encoding(str_replace(array("\n","\t"),array("",""),$msgHtml),"UTF-8","UTF-8"));
					$email->setPlain(strip_tags(SLS_String::br2nl($msgHtml)));
					$email->compileMail();
					$state = $email->send();
					echo 1;
				}else
				{
					echo 0;
				}
			}else
			{
				echo 0;
			}
		}else
		{
			echo 0;
		}
		echo "</etat></rapport>";
		die();
	}
}
?>