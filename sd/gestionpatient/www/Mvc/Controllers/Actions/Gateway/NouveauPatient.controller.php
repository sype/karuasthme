<?php
/**
* Class NouveauPatient into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewayNouveauPatient extends GatewayControllerProtected
{
	public function action()
	{
		header("Content-Type: text/xml;charset=ISO-8859-1;");
		echo "<?xml version='1.0'?>";
		$errors = 0;
		$champ_erreur = "";
		$this->useModel("Patient");
		$patient = new Main_Patient();
		$result = $patient->searchModels("patient",array(), array(0=>array("column"=>"idGesPat","value"=>$this->_http->getParam("idGesPat"),"mode"=>"equal")));
		if(!empty($result))
		{
			$errors++;
			$champ_erreur .= "idGesPat_existant ";
			/*for($i=0;$i<count($result);$i++)
			{
				if(!$result[$i]->setNoSecuPat($this->_http->getParam("noSecu")))
				{
					$errors++;
					$champ_erreur .= "noSecu ";
				}
				
				if(!$result[$i]->setNomPatient($this->_http->getParam("nom")))
				{
					$errors++;
					$champ_erreur .= "nom ";
				}
				
				if(!$result[$i]->setPrenomPatient($this->_http->getParam("prenom")))
				{
					$errors++;
					$champ_erreur .= "prenom ";
				}
				
				if(!$result[$i]->setIdMed($this->_http->getParam("idMed")))
				{
					$errors++;
					$champ_erreur .= "idMed ";
				}
				
				if(!$result[$i]->setCivilitePat($this->_http->getParam("civilite")))
				{
					$errors++;
					$champ_erreur .= "civilite ";
				}
				
				if(!$result[$i]->setRuePat($this->_http->getParam("rue")))
				{
					$errors++;
					$champ_erreur .= "rue ";
				}
				
				if(!$result[$i]->setCpPat($this->_http->getParam("cp")))
				{
					$errors++;
					$champ_erreur .= "cp ";
				}
				
				if(!$result[$i]->setVillePat($this->_http->getParam("ville")))
				{
					$errors++;
					$champ_erreur .= "ville ";
				}
				
				if(!$result[$i]->setTelPat($this->_http->getParam("telPat")))
				{
					$errors++;
					$champ_erreur .= "telPat ";
				}
				
				if(!$result[$i]->setPortPat($this->_http->getParam("gsmPat")))
				{
					$errors++;
					$champ_erreur .= "gsmPat ";
				}
				
				if(!$result[$i]->setDateNais(SLS_Date::dateToDateTime($this->_http->getParam("dateNais"))))
				{
					$errors++;
					$champ_erreur .= "dateNais ";
				}
				
				if(!$result[$i]->setDateFinEducation(SLS_Date::dateToDateTime($this->_http->getParam("dateFin"))))
				{
					$errors++;
					$champ_erreur .= "dateFin ";
				}
				
				if(!$result[$i]->setIdGesPat($this->_http->getParam("idGesPat")))
				{
					$errors++;
					$champ_erreur .= "idGesPat ";
				}
				
				if(!$result[$i]->setReseau($this->_http->getParam("Reseau")))
				{
					$errors++;
					$champ_erreur .= "Reseau ";
				}
			$result[$i]->save();
			}*/
		}
		else
		{
			if(!$patient->setNoSecuPat($this->_http->getParam("noSecu")))
			{
				$errors++;
				$champ_erreur .= "noSecu ";
			}
			
			if(!$patient->setNomPatient($this->_http->getParam("nom")))
			{
				$errors++;
				$champ_erreur .= "nom ";
			}
			
			if(!$patient->setPrenomPatient($this->_http->getParam("prenom")))
			{
				$errors++;
				$champ_erreur .= "prenom ";
			}
			
			if(!$patient->setIdMed($this->_http->getParam("idMed")))
			{
				$errors++;
				$champ_erreur .= "idMed ";
			}
			
			if(!$patient->setCivilitePat($this->_http->getParam("civilite")))
			{
				$errors++;
				$champ_erreur .= "civilite ";
			}
			
			if(!$patient->setRuePat($this->_http->getParam("rue")))
			{
				$errors++;
				$champ_erreur .= "rue ";
			}
			
			if(!$patient->setCpPat($this->_http->getParam("cp")))
			{
				$errors++;
				$champ_erreur .= "cp ";
			}
			
			if(!$patient->setVillePat($this->_http->getParam("ville")))
			{
				$errors++;
				$champ_erreur .= "ville ";
			}
			
			if(!$patient->setTelPat($this->_http->getParam("telPat")))
			{
				$errors++;
				$champ_erreur .= "telPat ";
			}
			
			if(!$patient->setPortPat($this->_http->getParam("gsmPat")))
			{
				$errors++;
				$champ_erreur .= "gsmPat ";
			}
			
			if(!$patient->setDateNais(SLS_Date::dateToDateTime($this->_http->getParam("dateNais"))))
			{
				$errors++;
				$champ_erreur .= "dateNais ";
			}
			
			if(!$patient->setDateFinEducation(SLS_Date::dateToDateTime($this->_http->getParam("dateFin"))))
			{
				$errors++;
				$champ_erreur .= "dateFin ";
			}
			
			if(!$patient->setIdGesPat($this->_http->getParam("idGesPat")))
			{
				$errors++;
				$champ_erreur .= "idGesPat ";
			}
			
			if(!$patient->setReseau($this->_http->getParam("Reseau")))
			{
				$errors++;
				$champ_erreur .= "Reseau ";
			}
		}
		echo "<rapport>";
		//echo $champ_erreur;
		//echo "<br><br>mail = $mail";
		if($errors == 0){
			$patient->create();
			$patient->save();
			
			$this->useModel("Medecin");
			$user = new Main_Medecin();
			$result = $user->searchModels("Medecin",array(), array(0=>array("column"=>"idMed","value"=>$this->_http->getParam("idMed"),"mode"=>"equal")));
			if($this->_http->getParam("civilite") == "vide"){$msgHtml = "Bonjour, <br/> ".$this->_http->getParam("prenom")." ".$this->_http->getParam("nom")." � fini son �ducation � \"Karu Asthme - Gestion Patient\" le ".substr($this->_http->getParam("dateFin"),8)."/".substr($this->_http->getParam("dateFin"),5,2)."/".substr($this->_http->getParam("dateFin"),0,4).".<br/><br/>";}
			else{$msgHtml = "Bonjour, <br/>".$this->_http->getParam("civilite")." ".$this->_http->getParam("prenom")." ".$this->_http->getParam("nom")." � fini son �ducation � \"Karu Asthme - Gestion Patient\" le ".substr($this->_http->getParam("dateFin"),8)."/".substr($this->_http->getParam("dateFin"),5,2)."/".substr($this->_http->getParam("dateFin"),0,4).".<br/><br/>";}
			$msgHtml .= "Vous pouvez vous connecter d�s maintenant en cliquant sur le lien ci-dessous ou en le copiant dans votre navigateur afin d'en assur� le suivi:<br/><br/>";
			$msgHtml .= "<a href=\"".$this->_generic->getFullPath("Home","Index")."\">".$this->_generic->getFullPath("Home","Index")."</a>";
			$msgHtml .= "<br/><br/>Cordialement,";
			$msgHtml .= "<br/><br/>Karu Asthme.";
			//echo $msgHtml;
			$email = new SLS_Email(SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("host"),$this->_generic->getSiteConfig("privateKey")),
								   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("port"),$this->_generic->getSiteConfig("privateKey")),
								   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("username"),$this->_generic->getSiteConfig("privateKey")),
								   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("password"),$this->_generic->getSiteConfig("privateKey")));
			$email->openConnection();
			$email->setCharset("UTF-8");
			if(!empty($result))
			{
				$email->addRecipient($result[0]->mailMed,"To");
			}
			else
			{
				$email->addRecipient("crochemont@pisystems.fr","To");
			}
			$email->setSender($this->_generic->getMailConfig('defaultSender').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameSender'));
			$email->setReply($this->_generic->getMailConfig('defaultReply').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReply'));
			$email->setReturn($this->_generic->getMailConfig('defaultReturn').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReturn'));
			$email->setSubject(SLS_String::makeEmailSubject('Email Fin Education - Gestion Patient'));											
			$email->setHtml(mb_convert_encoding(str_replace(array("\n","\t"),array("",""),$msgHtml),"UTF-8","UTF-8"));
			$email->setPlain(strip_tags(SLS_String::br2nl($msgHtml)));
			$email->compileMail();
			$state = $email->send();
					
			//echo "<id>".$patient->__get("idPatient")."</id>";
			echo $patient->__get("idPatient");
		}else
		{
			echo 0;
			//echo 0-$errors;
			//echo "_".$champ_erreur;
		}
		echo "</rapport>";
		die();
	}
}
?>