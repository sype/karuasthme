<?php
/**
* Class ListeNouveauPatient into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewayListeNouveauPatient extends GatewayControllerProtected
{
	public function action()
	{
		$theReseau = "PaP";
		$theTime = date("Y-m-d");
		$this->useModel("configuration");
		$this->useModel("Patient");
		$this->useModel("Inclusion");
		$patient = new Main_Patient();
		$inclusion = new Main_Inclusion();
		$configuration = new Main_Configuration();
		if($this->_http->getParam("reseau")){
			$theReseau = $this->_http->getParam("reseau");
		}
		/*if($this->_http->getParam("time")){
			$theTime = $this->_http->getParam("time");
		}*/
		$reseau = array_shift($configuration->searchModels("configuration",array(), array(0=>array("column"=>"Reseau","value"=>$theReseau,"mode"=>"equal"))));
		$LastSynchroDate = "";
		if(!empty($reseau)):
			$LastSynchroDate = $reseau->LastSynchroDate;
			$patients = $patient->searchModels("patient",array(), array(0=>array("column"=>"Reseau","value"=>$theReseau,"mode"=>"equal"), 1=>array("column"=>"idGesPat","value"=>0,"mode"=>"equal")));
			header("Content-Type: text/xml;charset=UTF8;");
			echo "<?xml version='1.0'?>";
			?>
			<liste lastSynchroDate="<?php echo $LastSynchroDate ?>" <?php/*nb=" echo count($patients) ?>"*/?>>
			<?php
			for($i=0;$i<count($patients);$i++):
			//
			$inclusions = $inclusion->searchModels("inclusion",array(), array(0=>array("column"=>"idIncl","value"=>$patients[$i]->idIncl,"mode"=>"equal"), 1=>array("column"=>"dateIncl","value"=>$LastSynchroDate,"mode"=>"gt")));
			if(!empty($inclusions)):
				//
				?>
					<patient>
					 <id><?php echo $patients[$i]->idPatient; ?></id>
					 </patient>
				<?php
				//
			endif;
			//
			?>
	           
			<?php
			endfor;
			?>
			</liste>
			<?php 
			
			
			
			
		
		
		
		endif;
		die(); 
	}
}
?>