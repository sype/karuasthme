<?php
/**
* Class generic for the controller Gateway
* Write here all your generic functions you need in your Gateway Actions
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*/
class GatewayControllerProtected extends SiteProtected
{
	public function passwd() { 
		$tpass=array(); 
		$id=0; 
		$taille=7; 
		// récupération des chiffres et lettre 
		for($i=48;$i<58;$i++) $tpass[$id++]=chr($i); 
		for($i=65;$i<91;$i++) $tpass[$id++]=chr($i); 
		for($i=97;$i<123;$i++) $tpass[$id++]=chr($i); 
		$passwd=""; 
		for($i=0;$i<6;$i++) { 
			$passwd.=$tpass[rand(0,$id-1)]; 
		} 
			return $passwd; 
	}
}
?>