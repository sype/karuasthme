<?php
/**
* Class SetSynchroDate into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewaySetSynchroDate extends GatewayControllerProtected
{
	public function action()
	{
		$this->useModel("Configuration");
		$config = new Main_Configuration();
		
		header("Content-Type: text/xml;charset=UTF8;");
		echo "<?xml version='1.0'?>";
		echo "<rapport>";
		$result = array_shift($config->searchModels("configuration",array(), array(0=>array("column"=>"Reseau","value"=>$this->_http->getParam("reseau"),"mode"=>"equal"))));
		if(!empty($result))
		{
			if($config->getModel($result->config_id))
			{
				$config->__set("LastSynchroDate", date("Y-m-j H:i:s"));
				$config->save();
				echo 1;
			}else
			{
				echo 0;
			}
		}else
		{
			echo 0;
		}
		echo "</rapport>";
		die();
	}
}
?>