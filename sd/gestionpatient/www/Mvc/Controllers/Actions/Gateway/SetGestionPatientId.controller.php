<?php
/**
* Class SetGestionPatientId into Gateway Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Gateway
* @see Mvc.Controllers.Gateway.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class GatewaySetGestionPatientId extends GatewayControllerProtected
{
	public function action()
	{
		$this->useModel("Patient");
		$patient = new Main_Patient();
		
		header("Content-Type: text/xml;charset=ISO-8859-1;");
		echo "<?xml version='1.0'?>";
		echo "<rapport><etat>";
		$results = $patient->searchModels("patient",array(), array(0=>array("column"=>"idGesPat","value"=>$this->_http->getParam("idGesPat"),"mode"=>"equal")));
		if(empty($results))
		{
			if($patient->getModel($this->_http->getParam("id")))
			{
				if($patient->setIdGesPat($this->_http->getParam("idGesPat")))
				{
					$patient->save();
					echo 1;
				}else
				{
					echo 0;
				}
			}else
			{
				echo 0;
			} 
		}else
		{
			echo 0;
		}
		echo "</etat></rapport>";
		die();
	}
}
?>