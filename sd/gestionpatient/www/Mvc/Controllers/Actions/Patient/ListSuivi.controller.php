<?php
/**
* Class ListSuivi into Patient Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Patient
* @see Mvc.Controllers.Patient.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class PatientListSuivi extends PatientControllerProtected
{
	public function action()
	{
		$xml = $this->getXML();
		if($this->isLogged())
		{
			$xml->addFullTag("patients_part", "selected");
			$this->useModel("Fiches");
			$this->useModel("Patient");
			$suivi = new Main_Fiches();
			$patient = new Main_Patient();
			$session = $this->_generic->getObjectSession();
			
			//Get patient
			$suivis = $suivi->searchModels("fiches",array("patient", "medecin"), array(0=>array("column"=>"idPatient","value"=>$this->_http->getParam("Patient"),"mode"=>"equal"), 1=>array("column"=>"idMed","value"=>$session->getParam("user_id"),"mode"=>"equal")), array(), array(0=> array("column"=>"date", "order"=>"desc" )));
			if($patient->getModel($this->_http->getParam("Patient")))
			{
				$xml->addFullTag("id", $suivis[$i]->idPat);
				$xml->addFullTag("name", $patient->__get("nomPatient"));
				$xml->addFullTag("firstname", $patient->__get("prenomPatient"));
				if(!empty($suivis))
				{
					$xml->startTag("suivis");
					for($i = 0;$i<count($suivis);$i++)
					{
						$xml->startTag("suivi");
						$xml->addFullTag("date", $suivis[$i]->date);
						$xml->addFullTag("questA_1", ($suivis[$i]->questA_1 == 1) ? "oui":"non");
						$xml->addFullTag("questA_2", ($suivis[$i]->questA_2 == 1) ? "oui":"non");
						$xml->addFullTag("questA_3", ($suivis[$i]->questA_3 == 1) ? "oui":"non");
						$xml->addFullTag("questA_4", ($suivis[$i]->questA_4 == 1) ? "oui":"non");
						$xml->addFullTag("questA_5", ($suivis[$i]->questA_5 == 1) ? "oui":"non");
						$xml->addFullTag("questA_6", ($suivis[$i]->questA_6 == 1) ? "oui":"non");
						$xml->addFullTag("total_test", $suivis[$i]->questB_1 + $suivis[$i]->questB_2 + $suivis[$i]->questB_3 + $suivis[$i]->questB_4 + $suivis[$i]->questB_5);
						$xml->endTag("suivi");
					}
					$xml->endTag("suivis");
				}else
				{
					$xml->startTag("errors");
					$xml->addFullTag("error","Pas de suivi pour ce patient",true);
					$xml->endTag("errors");
				}
			}
		}
		else
			$this->forward("User","Login");	
		$this->saveXML($xml);
	}
}
?>