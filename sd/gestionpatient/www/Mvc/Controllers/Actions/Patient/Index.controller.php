<?php
/**
* Class Index into Patient Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Patient
* @see Mvc.Controllers.Patient.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class PatientIndex extends PatientControllerProtected
{
	public function action()
	{
		$xml = $this->getXML();
		if($this->isLogged())
		{
			$this->useModel("Patient");
			$patient = new Main_Patient();
			$session = $this->_generic->getObjectSession();
			
			if($this->_http->getParam("refresh") == "true")
			{
				//Nombre de patient par page
				if(is_numeric($this->_http->getParam("nbToDisplay")) && round($this->_http->getParam("nbToDisplay"), 0) != 0) 
				{
					//Au cas où ils mettent un nombre decimal
					$nbToDisplay = round($this->_http->getParam("nbToDisplay"), 0);
				}else{
					$nbToDisplay = 25;
				}
				$selectedPage = $this->_http->getParam("pages");
				
				if($this->_http->getParam("order") != "" && $this->_http->getParam("orderSens") != "") 
				{
					$order = $this->_http->getParam("order");
					$orderSens = $this->_http->getParam("orderSens");
					$orderReq =  array(0=> array ("column"=>$order, "order"=>$orderSens));
				}else
				{
					$order = "";
					$orderSens = "";
					$orderReq = array();
				}
				
				if($this->_http->getParam("patient_input_sch") != "" && $this->_http->getParam("patient_input_sch") != "Recherche...")
				{
					$patients = $patient->searchPatient($this->_http->getParam("patient_input_sch"), $session->getParam("user_id"), $nbToDisplay);
				}else
				{
					$patients = $patient->searchModels("patient",array(), array(0=>array("column"=>"idMed","value"=>$session->getParam("user_id"),"mode"=>"equal")), array(), $orderReq, array("start" => $selectedPage, "length" => $nbToDisplay));
				}
			}else
			{
				$nbToDisplay = 25;
				$selectedPage = 0;
				$order = "";
				$orderSens = "";
				$orderReq = array();
				$patients = $patient->searchModels("patient",array(), array(0=>array("column"=>"idMed","value"=>$session->getParam("user_id"),"mode"=>"equal")), array(), $orderReq, array("start" => $selectedPage, "length" => $nbToDisplay));
			}
			
			//Load var
			$xml->addFullTag("patients_part", "selected");
			$this->useModel("Fiches");
			$suivi = new Main_Fiches();
			
			//Get patient
			$nbPatient = count($patient->searchModels("patient",array(), array(0=>array("column"=>"idMed","value"=>$session->getParam("user_id"),"mode"=>"equal")), array(), array()));
			if($nbPatient >  0)
			{
				$xml->addFullTag("nbPatient", $nbPatient);
			}
			$xml->startTag("patients");
			for($i = 0;$i<count($patients);$i++)
			{
				$xml->startTag("patient");
				$xml->addFullTag("id", $patients[$i]->idPatient);
				$xml->addFullTag("name", $patients[$i]->nomPatient);
				$xml->addFullTag("firstname", $patients[$i]->prenomPatient);
				$xml->addFullTag("no_secu", $patients[$i]->noSecuPat);
				$xml->addFullTag("nb_suivi", count($suivi->searchModels("fiches",array(), array(0=>array("column"=>"idPatient","value"=>$patients[$i]->idPatient,"mode"=>"equal")))));
				$xml->addFullTag("addsuivi_url", $this->_generic->getFullPath("Patient","Suivi",array(0=>array("key"=>"Patient","value"=>$patients[$i]->idPatient))),true);
				$xml->addFullTag("listsuivi_url", $this->_generic->getFullPath("Patient","ListSuivi",array(0=>array("key"=>"Patient","value"=>$patients[$i]->idPatient))),true);
				$xml->addFullTag("inclus", ($patients[$i]->idGesPat == NULL || $patients[$i]->idGesPat == 0)?"non inclus":"inclus");
				$xml->endTag("patient");
			}
			$xml->endTag("patients");
			$xml->startTag("pages");
			if(is_double($nbPatient/$nbToDisplay))
			{
				$cmpt = round($nbPatient/$nbToDisplay, 0) + 1;
			}else
			{
				$cmpt = round($nbPatient/$nbToDisplay, 0);
			}
			for($i = 0;$i<$cmpt;$i++)
			{
				$xml->startTag("page");
				$xml->addFullTag("value", $nbToDisplay * $i);
				$xml->addFullTag("display", $i + 1);
				$xml->endTag("page");
				
			}
			$xml->endTag("pages");
			$xml->addFullTag("selectedPage", $selectedPage);
			$xml->addFullTag("nbToDisplay", $nbToDisplay);
			$xml->addFullTag("order", $order);
			$xml->addFullTag("orderSens", $orderSens);
		}
		else
			$this->forward("User","Login");	
		$this->saveXML($xml);
	}
}
?>