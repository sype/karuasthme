<?php
/**
* Class Suivi into Patient Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Patient
* @see Mvc.Controllers.Patient.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class PatientSuivi extends PatientControllerProtected
{
	public function action()
	{
		if($this->isLogged())
		{
			$this->useModel("Patient");
			$this->useModel("Medecin");
			$session = $this->_generic->getObjectSession();
			
			$patient = new Main_Patient();
			$user = new Main_Medecin();
			
			$xml = $this->getXML();
			if($this->_http->getParam("refresh") == "true")
			{
				//Verification existance patient et du medecin
				if($patient->getModel($this->_http->getParam("Patient")) && $user->getModel($session->getParam("user_id")))
				{
					//Verification patient appartient au medecin
					if($patient->__get("idMed") == $user->__get("idMed"))
					{
						$this->useModel("Fiches");
						$suivi = new Main_Fiches();
						
						//Réponse au questionnaire
						$suivi->setQuestA1($this->_http->getParam("questA_1"));
						$suivi->setQuestA2($this->_http->getParam("questA_2"));
						$suivi->setQuestA3($this->_http->getParam("questA_3"));
						$suivi->setQuestA4($this->_http->getParam("questA_4"));
						$suivi->setQuestA5($this->_http->getParam("questA_5"));
						$suivi->setQuestA6($this->_http->getParam("questA_6"));
						
						//Note au test de controle de l'asthme
						$suivi->setQuestB1($this->_http->getParam("questB_1"));
						$suivi->setQuestB2($this->_http->getParam("questB_2"));
						$suivi->setQuestB3($this->_http->getParam("questB_3"));
						$suivi->setQuestB4($this->_http->getParam("questB_4"));
						$suivi->setQuestB5($this->_http->getParam("questB_5"));
						$suivi->setDate(date("Y-m-d H:i:s"));
						
						$suivi->setIdPatient($patient->__get("idPatient"));
						
						$suivi->create();
						$suivi->save();
						
						$this->forward("Patient","Index");
					}
				}
			}else
			{
				//Verification existance patient et du medecin
				if($patient->getModel($this->_http->getParam("Patient")) && $user->getModel($session->getParam("user_id")))
				{
					//Verification patient appartient au medecin
					if($patient->__get("idMed") == $user->__get("idMed"))
					{
						$xml->addFullTag("patient_name", $patient->__get("prenomPatient")." ".$patient->__get("nomPatient"));
						$xml->addFullTag("patient_id", $patient->__get("idPatient"));
						$xml->addFullTag("post_url", $this->_generic->getFullPath("Patient","Suivi",array(0=>array("key"=>"Patient","value"=>$patient->__get("idPatient")))),true);
					}else
					{
						$this->forward("Patient","Index");	
					}
				}else
				{
					$this->forward("Patient","Index");	
				}
			}
			$this->saveXML($xml);
		}else 
			$this->forward("User", "Login");
	}
}
?>