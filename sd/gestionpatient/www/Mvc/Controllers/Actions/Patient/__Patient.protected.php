<?php
/**
* Class generic for the controller Patient
* Write here all your generic functions you need in your Patient Actions
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Patient
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*/
class PatientControllerProtected extends SiteProtected
{

}
?>