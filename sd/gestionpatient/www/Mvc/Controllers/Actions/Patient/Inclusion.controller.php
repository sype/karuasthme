<?php
/**
* Class Inclusion into Patient Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Patient
* @see Mvc.Controllers.Patient.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class PatientInclusion extends PatientControllerProtected
{
	public function action()
	{
		$xml = $this->getXML();
		if($this->isLogged())
		{
			//Load var
			$xml->addFullTag("inclusion_part", "selected");
			if($this->_http->getParam("refresh") == "true")
			{
				$this->useModel("Patient");
				$patient = new Main_Patient();
				$this->useModel("Inclusion");
				$inclusion = new Main_Inclusion();
				$session = $this->_generic->getObjectSession();
				
				$errors = array();
				$succes = array();
				
				//Patient
				if(!$patient->setNoSecuPat($this->_http->getParam("noSecu")))
				{
					array_push($errors, "Erreur numéro de sécurité social");
				}
				
				if(!$patient->setCivilitePat($this->_http->getParam("civilite")))
				{
					array_push($errors, "Erreur civilité");
				}
				
				if(!$patient->setReseau($this->_http->getParam("reseau")))
				{
					array_push($errors, "Erreur réseau");
				}
				
				if(!$patient->setNomPatient($this->_http->getParam("nom")))
				{
					array_push($errors, "Erreur nom patient");
				}
				
				if(!$patient->setPrenomPatient($this->_http->getParam("prenom")))
				{
					array_push($errors, "Erreur prenom patient");
				}
				
				if($this->_http->getParam("rue")){
					if(!$patient->setRuePat($this->_http->getParam("rue")))
					{
						array_push($errors, "Erreur rue");
					}
				}
				
				if($this->_http->getParam("ville")){
					if(!$patient->setVillePat($this->_http->getParam("ville")))
					{
						array_push($errors, "Erreur ville");
					}
				}
				
				if(!$patient->setCpPat($this->_http->getParam("cp")))
				{
					array_push($errors, "Erreur code postal");
				}
				
				$birthdate = explode("/", $this->_http->getParam("birthdate"));
				if(!$patient->setDateNais(SLS_Date::dateToDateTime($birthdate[2]."-".$birthdate[1]."-".$birthdate[0])))
				{
					array_push($errors, "Erreur date de naissance");
				}
				
				
				if(!$patient->setTelPat($this->_http->getParam("telephone")))
				{
					array_push($errors, "Erreur téléphone");
				}
				
				$patient->setPortPat($this->_http->getParam("portable"));
				
				$patient->setIdMed($session->getParam("user_id"));
				$patient->setToDoSuivi(1);
				
				//Fiche d'inclusion
				if(!$inclusion->setSeveritePat($this->_http->getParam("severite")))
				{
					array_push($errors, "Erreur Sévérité");
				}
				
				if(!$inclusion->setNbGene($this->_http->getParam("nbgene")))
				{
					array_push($errors, "Erreur nombre de gène");
				}
				
				if(!$inclusion->setTraitCrise($this->_http->getParam("traitCrise")))
				{
					array_push($errors, "Erreur traitement de crise");
				}
				
				if(!$inclusion->setTraitFond($this->_http->getParam("traitFond")))
				{
					array_push($errors, "Erreur traitement de fond");
				}
				
				if(!$inclusion->setTraitAutre($this->_http->getParam("traitAutres")))
				{
					array_push($errors, "Erreur traitement autre");
				}
				
				if(!$inclusion->setConsulPrec($this->_http->getParam("consulPrec")))
				{
					array_push($errors, "Erreur consulPrec");
				}
				
				if(!$inclusion->setHospPrec($this->_http->getParam("hospPrec")))
				{
					array_push($errors, "Erreur hospPrec");
				}
				
				if(!$inclusion->setConsultNoProg($this->_http->getParam("consultNoProg")))
				{
					array_push($errors, "Erreur consultNoProg");
				}
				
				if(!$inclusion->setNbStopTrav($this->_http->getParam("nbStopTrav")))
				{
					array_push($errors, "Erreur nbStopTrav");
				}
				
				if(!$inclusion->setDureeStopTrav($this->_http->getParam("dureeStopTrav")))
				{
					array_push($errors, "Erreur dureeStopTrav");
				}
				
				if(!$inclusion->setNbStopSchool($this->_http->getParam("nbStopSchool")))
				{
					array_push($errors, "Erreur nbStopSchool");
				}
				
				if(!$inclusion->setDureeStopSchool($this->_http->getParam("dureeStopSchool")))
				{
					array_push($errors, "Erreur dureeStopSchool");
				}
				
				$result = $patient->searchModels("patient",array(), array(0=>array("column"=>"noSecuPat","value"=>$this->_http->getParam("noSecu"),"mode"=>"equal"), 1=>array("column"=>"dateNais","value"=>$birthdate[2]."-".$birthdate[1]."-".$birthdate[0],"mode"=>"equal"), 2=>array("column"=>"nomPatient","value"=>$this->_http->getParam("nom"),"mode"=>"equal"), 3=>array("column"=>"prenomPatient","value"=>$this->_http->getParam("prenom"),"mode"=>"equal")));
				if(!empty($result))
				{
					array_push($errors, "Ce patient existe déjà");
				}
				
				$inclusion->setDateIncl(date("Y-m-d H:i:s"));
				
				if(empty($errors))
				{
					$inclusion->create();
					$inclusion->save();
					
					$patient->setIdIncl($inclusion->__get('idIncl'));
					$patient->create();
					$patient->save();
					
					array_push($succes, "Demande d'inclusion envoyée");
					$xml->startTag("succes");
					for ($i = 0;$i<count($succes);$i++)
					{
						$xml->addFullTag("succe", $succes[$i]);
					}
					$xml->endTag("succes");
				}else
				{
					$xml->startTag("errors");
					for ($i = 0;$i<count($errors);$i++)
					{
						$xml->addFullTag("error", $errors[$i]);
					}
					$xml->endTag("errors");
					
					//Resend var
					
					//Patient
					$xml->addFullTag("noSecu", $this->_http->getParam("noSecu"));
					
					$xml->addFullTag("reseau", $this->_http->getParam("reseau"));
					
					$xml->addFullTag("civilite", $this->_http->getParam("civilite"));
					
					$xml->addFullTag("nom", $this->_http->getParam("nom"));
					
					$xml->addFullTag("prenom", $this->_http->getParam("prenom"));
					
					$xml->addFullTag("rue", $this->_http->getParam("rue"));
					
					$xml->addFullTag("ville", $this->_http->getParam("ville"));
					
					$xml->addFullTag("cp", $this->_http->getParam("cp"));
					
					$xml->addFullTag("birthdate", $this->_http->getParam("birthdate"));
					
					$xml->addFullTag("telephone", $this->_http->getParam("telephone"));
					
					//Fiche d'inclusion
					$xml->addFullTag("severite", $this->_http->getParam("severite"));
					
					$xml->addFullTag("nbgene", $this->_http->getParam("nbgene"));
					
					$xml->addFullTag("traitCrise", $this->_http->getParam("traitCrise"));
					
					$xml->addFullTag("traitFond", $this->_http->getParam("traitFond"));
					
					$xml->addFullTag("traitAutres", $this->_http->getParam("traitAutres"));
					
					$xml->addFullTag("consulPrec", $this->_http->getParam("consulPrec"));
					
					$xml->addFullTag("hospPrec", $this->_http->getParam("hospPrec"));
					
					$xml->addFullTag("consultNoProg", $this->_http->getParam("consultNoProg"));
					
					$xml->addFullTag("nbStopTrav", $this->_http->getParam("nbStopTrav"));
					
					$xml->addFullTag("dureeStopTrav", $this->_http->getParam("dureeStopTrav"));
					
					$xml->addFullTag("nbStopSchool", $this->_http->getParam("nbStopSchool"));
					
					$xml->addFullTag("dureeStopSchool", $this->_http->getParam("dureeStopSchool"));
					
				}
			}
		}else
		{
			$this->forward("User", "Login");
		}
		$this->saveXML($xml);
	}
}
?>