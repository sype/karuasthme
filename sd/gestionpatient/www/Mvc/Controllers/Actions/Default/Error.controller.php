<?php
/**
 * Action Error Controller Default
 * 
 * @author SillySmart
 * @copyright SillySmart
 * @package Mvc.Controllers.Default
 * @see Mvc.Controllers.Default.ControllerProtected
 * @see Mvc.Controllers.SiteProtected
 * @see Sls.Controllers.Core.SLS_GenericController
 * @since 1.0
 */
class DefaultError extends DefaultControllerProtected 
{	
	/**
	 * Error of action for an existing controller
	 *
	 * @access public
	 * @param string $controller the current controller
	 */
	public function action($controller)
	{
		// We recover actions of the same controller who doesn't need a dynamic param
		$actions = $this->_generic->getActionsNoParams($controller);
		$this->setPageTitle(ucwords($this->_generic->getSiteConfig("projectName"))." - Error - Unknown Action");
		$this->_xmlToolBox = new SLS_XMLToolbox(false);		
		$this->_xmlToolBox->startTag("actions");		
		foreach($actions as $value)
		{
			$currentPage = array_shift($this->_generic->getControllersXML()->getTagsAttribute("//controllers/controller[@name='".$controller."' and @side='user']/scontrollers/scontroller[@name='".$value."']/scontrollerLangs/scontrollerLang[@lang='".$this->_generic->getObjectLang()->getLang()."']","title"));			
			$controllers = $this->_generic->getTranslatedController($controller,$value);
			$this->_xmlToolBox->startTag("action");
			$this->_xmlToolBox->addFullTag("label",(empty($currentPage["attribute"])) ? $value : $currentPage["attribute"],true);
			$this->_xmlToolBox->addFullTag("href",$controllers['protocol']."://".$this->_generic->getSiteConfig("domainName")."/".$controllers['controller']."/".$controllers['scontroller']."/Index.html",true);
			$this->_xmlToolBox->endTag("action");
		}
		$this->_xmlToolBox->endTag("actions");		
		$xmlTmp = $this->_xmlToolBox->getXML();	
		$this->_xmlToolBox = new SLS_XMLToolbox($this->_xml);
		$this->_xmlToolBox->appendXMLNode("//root",$xmlTmp);
		$this->_xml = $this->_xmlToolBox->getXML();		
	}	
}
?>