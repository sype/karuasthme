<?php
/**
* Class SearchPatient into Ajax Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Ajax
* @see Mvc.Controllers.Ajax.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class AjaxSearchPatient extends AjaxControllerProtected
{
	public function action()
	{
		if($this->isLogged())
		{
		$keyword = explode("|", $this->_http->getParam('keyword'));
		$this->_generic->useModel("Patient");
		$patient = new Main_Patient();
		$session = $this->_generic->getObjectSession();
		
		$patients = $patient->searchPatient($keyword, $session->getParam("user_id"));
		$result = array();
		for($i = 0; $i<count($patients); $i++)
		{
                    array_push($result, array("id"=>$patients[$i]->idPatient, "nom"=>$patients[$i]->nomPatient, "prenom"=>$patients[$i]->prenomPatient));
		}
		echo json_encode($result);
		die();
		}
	}
}
?>