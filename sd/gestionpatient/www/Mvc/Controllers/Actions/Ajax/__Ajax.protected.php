<?php
/**
* Class generic for the controller Ajax
* Write here all your generic functions you need in your Ajax Actions
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Ajax
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*/
class AjaxControllerProtected extends SiteProtected
{

}
?>