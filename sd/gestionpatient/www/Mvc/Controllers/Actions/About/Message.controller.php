<?php
/**
* Class Message into About Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.About
* @see Mvc.Controllers.About.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class AboutMessage extends AboutControllerProtected
{
	public function action()
	{
		$xml = $this->getXML();
		$xml->addFullTag("contact_part", "selected");
		if($this->_http->getParam("refresh") == "true")
		{
			$this->useModel("Medecin");
			$medecin = new Main_Medecin();
			$session = $this->_generic->getObjectSession();
			$medecin->getModel($session->getParam("user_id"));
			$errors = array();
			$succes = array();
			
			if($this->_http->getParam("message") != "")
			{
				$msgHtml = $this->_http->getParam("message");
											
				$email = new SLS_Email(SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("host"),$this->_generic->getSiteConfig("privateKey")),
									   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("port"),$this->_generic->getSiteConfig("privateKey")),
									   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("username"),$this->_generic->getSiteConfig("privateKey")),
									   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("password"),$this->_generic->getSiteConfig("privateKey")));
				$email->openConnection();
				$email->setCharset("UTF-8");
				$email->addRecipient($this->_generic->getMailConfig('defaultReturn').'@'.$this->_generic->getMailConfig('defaultDomain'),"To");
				$email->setSender($medecin->__get("mailMed"), $medecin->__get("nomMed")." ".$medecin->__get("prenomMed"));
				$email->setReply($medecin->__get("mailMed"), $medecin->__get("nomMed")." ".$medecin->__get("prenomMed"));
				$email->setReturn($medecin->__get("mailMed"), $medecin->__get("nomMed")." ".$medecin->__get("prenomMed"));
				$email->setSubject(SLS_String::makeEmailSubject('Gestion Patient - Nouveau message de ' .$medecin->__get("nomMed")." ".$medecin->__get("prenomMed")));											
				$email->setHtml(mb_convert_encoding(str_replace(array("\n","\t"),array("",""),$msgHtml),"UTF-8","UTF-8"));
				$email->setPlain(strip_tags(SLS_String::br2nl($msgHtml)));
				$email->compileMail();
				$state = $email->send();
				
				if($state == null)
				{
					array_push($succes, "Message envoyé");		
				}else
				{
					array_push($errors, "Votre message n'a pu être envoyé");
				}
			}else
			{
				array_push($errors, "Veuillez saisir un message");
			}
			
			if(empty($errors))
			{
				$xml->startTag("succes");
				$xml->addFullTag("succe", $succes[0]);
				$xml->endTag("succes");
			}else
			{
				$xml->startTag("errors");
				$xml->addFullTag("error", $errors[0]);
				$xml->endTag("errors");
			}
		}
		$this->saveXML($xml);
	}
}
?>