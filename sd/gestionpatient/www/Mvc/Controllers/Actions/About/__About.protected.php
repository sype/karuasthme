<?php
/**
* Class generic for the controller About
* Write here all your generic functions you need in your About Actions
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.About
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*/
class AboutControllerProtected extends SiteProtected
{

}
?>