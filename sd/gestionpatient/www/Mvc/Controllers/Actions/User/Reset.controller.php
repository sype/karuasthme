<?php
/**
* Class Reset into User Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.User
* @see Mvc.Controllers.User.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class UserReset extends UserControllerProtected
{
	public function action()
	{
		$this->useModel("Medecin");
		$user = new Main_Medecin();
		if($this->_http->getParam("refresh") == "true")
		{
			$xml = $this->getXML();
			$errors = array();
			$succes = array();
			
			if($this->_http->getParam("email") != null)
			{
				$mail = $this->_http->getParam("email");
				if($user->isUnique("mailMed", $mail))
				{
					array_push($errors, "Adresse email inconue");
				}else
				{
					$result = array_shift($user->searchModels("medecin",array(),array(0=>array("column"=>"mailMed","value"=>$mail,"mode"=>"equal"))));
					$token = sha1($result->pwdMed);
					$user->getModel($result->idMed);
					$user->setToken($token);
					$user->save();
					
					// Notify
					$msgHtml = "Bonjour ".$user->__get("prenomMed")." ".$user->__get("nomMed").",<br/><br/>Une demande de réinitialisation du mot de passe de votre compte sur \"Gestion Patient\" a été faite. Pour réinitialiser votre mot de passe, Cliquez sur le lien ci-dessous ou copiez le dans votre navigateur :<br/><br/>";
					$msgHtml .= "<a href=\"".$this->_generic->getFullPath("User","UpdMdp",array(0=>array("key"=>"Token","value"=>$token)))."\" title=\"Réinitialisation\">".$this->_generic->getFullPath("User","UpdMdp",array(0=>array("key"=>"Token","value"=>$token)))."</a>";
					$msgHtml .= "<br/><br/>Cordialement,";
					$msgHtml .= "<br/><br/>Karu Asthme.";					
					$email = new SLS_Email(SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("host"),$this->_generic->getSiteConfig("privateKey")),
										   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("port"),$this->_generic->getSiteConfig("privateKey")),
										   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("username"),$this->_generic->getSiteConfig("privateKey")),
										   SLS_Security::getInstance()->decrypte($this->_generic->getMailConfig("password"),$this->_generic->getSiteConfig("privateKey")));
					$email->openConnection();
					$email->setCharset("UTF-8");
					$email->addRecipient($mail,"To");
					$email->setSender($this->_generic->getMailConfig('defaultSender').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameSender'));
					$email->setReply($this->_generic->getMailConfig('defaultReply').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReply'));
					$email->setReturn($this->_generic->getMailConfig('defaultReturn').'@'.$this->_generic->getMailConfig('defaultDomain'), $this->_generic->getMailConfig('defaultNameReturn'));
					$email->setSubject(SLS_String::makeEmailSubject('Réinitialisation du mot de passe'));											
					$email->setHtml(mb_convert_encoding(str_replace(array("\n","\t"),array("",""),$msgHtml),"UTF-8","UTF-8"));
					$email->setPlain(strip_tags(SLS_String::br2nl($msgHtml)));
					$email->compileMail();
					$state = $email->send();
					if($state != null)
					{
						array_push($errors, $state);
					}else
					{
						array_push($succes, "Un email a été envoyé à l'adresse indiquée");
					}
					$email->closeConnection();
				}
			}else
			{
				array_push($errors, "Veuillez entrer une adresse email");
			}
			
			if(empty($errors))
			{
				$xml->startTag("succes");
				for ($i = 0;$i<count($succes);$i++)
				{
					$xml->addFullTag("succe", $succes[$i]);
				}
				$xml->endTag("succes");
			}else
			{
				$xml->startTag("errors");
				for ($i = 0;$i<count($errors);$i++)
				{
					$xml->addFullTag("error", $errors[$i]);
				}
				$xml->endTag("errors");
			}
			$this->saveXML($xml);
		}
	}
}
?>