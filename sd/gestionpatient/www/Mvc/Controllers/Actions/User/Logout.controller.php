<?php
/**
* Class Logout into User Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.User
* @see Mvc.Controllers.User.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class UserLogout extends UserControllerProtected
{
	public function action()
	{
		// Objects
		$session = $this->_generic->getObjectSession();
		
		// Unset specific keys
		$session->delParam("is_logged");
		$session->delParam("user_id");
		$session->delParam("user_name");
		$session->delParam("user_firstname");
		
		// Forward on home
		$this->forward("Home","Index");
	}
}
?>