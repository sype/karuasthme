<?php
/**
* Class Setting into User Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.User
* @see Mvc.Controllers.User.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class UserSetting extends UserControllerProtected
{
	public function action()
	{
		if($this->isLogged())
		{
			$this->useModel("Medecin");
			$user = new Main_Medecin();
			$session = $this->_generic->getObjectSession();
			$xml = $this->getXML();
			if($user->getModel($session->getParam("user_id")))
			{
				if($this->_http->getParam("refresh") == "true")
				{
					$errors = array();
					$succes = array();
					if($this->_http->getParam("password")!="" && $this->_http->getParam("passwordconf")!="")
					{
						if($this->_http->getParam("password") == $this->_http->getParam("passwordconf"))
						{
							if(sha1($this->_http->getParam("oldPwd")) == $user->__get("pwdMed"))
							{
								if($user->setPwdMed(sha1($this->_http->getParam("password"))))
								{
									array_push($succes, "Mot de passe changé");
								}else
								{
									array_push($errors, "Erreur mot de passe");
								}
							}else
							{
								array_push($errors, "Ancien mot de passe incorrect");
							}
						}else
						{
							array_push($errors, "Mot de passe différents");
						}
					}
					
					if(!$user->setMailMed($this->_http->getParam("email")))
					{
						array_push($errors, "Erreur email");
					}
					
					if(empty($errors))
					{
						$user->save();
						$xml->startTag("succes");
						$xml->addFullTag("succe", "Mise à jour effectuée");
						$xml->endTag("succes");
					}else
					{
						$xml->startTag("errors");
						for($i=0;$i<count($errors);$i++)
						{
							$xml->addFullTag("error", $errors[$i]);
						}
						$xml->endTag("errors");
					}
				}
			}
			$xml->addFullTag("email", $user->__get("mailMed"));
			$this->saveXML($xml);
		}else
			$this->forward("User", "Login");
	}
}
?>