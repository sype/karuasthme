<?php
/**
* Class UpdMdp into User Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.User
* @see Mvc.Controllers.User.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class UserUpdMdp extends UserControllerProtected
{
	public function action()
	{
		$errors = array();
		$succes = array();
		$xml = $this->getXML();
		
		if($this->_http->getParam("Token") != NULL)
		{
			$xml->addFullTag("Token", $this->_http->getParam("Token"));
			$this->useModel("Medecin");
			$user = new Main_Medecin();
			$result = array_shift($user->searchModels("medecin",array(),array(0=>array("column"=>"token","value"=>$this->_http->getParam("Token"),"mode"=>"equal"))));
			if(!empty($result))
			{
				if($this->_http->getParam("refresh") == "true")
				{
					$user->getModel($result->idMed);
					if($this->_http->getParam("password") != "")
					{
						if($this->_http->getParam("password_conf") != "")
						{
							if($this->_http->getParam("password_conf") == $this->_http->getParam("password"))
							{
								$user->__set("pwdMed", sha1($this->_http->getParam("password")));
								$user->__set("token", NULL);
								if($user->save())
								{
									array_push($succes, "Mot de passe changé");	
								}
								else
								{
									array_push($succes, "Une erreur c'est produite");	
								}
							}else
							{
								array_push($errors, "Les mots de passe sont différents");
							}
						}else
						{
							array_push($errors, "Veuillez confirmer le mot de passe");
						}
					}else
					{
						array_push($errors, "Veuillez entrer un mot de passe");
					}
				}
			}else
			{
				array_push($errors, "Ce lien est périmé");
			}
		}else
		{
			array_push($errors, "Erreur Token invalide");
		}
		
		if(empty($errors))
			{
				$xml->startTag("succes");
				for ($i = 0;$i<count($succes);$i++)
				{
					$xml->addFullTag("succe", $succes[$i]);
				}
				$xml->endTag("succes");
			}else
			{
				$xml->startTag("errors");
				for ($i = 0;$i<count($errors);$i++)
				{
					$xml->addFullTag("error", $errors[$i]);
				}
				$xml->endTag("errors");
			}
		
		$this->saveXml($xml);
	}
}
?>