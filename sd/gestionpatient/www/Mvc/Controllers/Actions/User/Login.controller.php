<?php
/**
* Class Login into User Controller
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.User
* @see Mvc.Controllers.User.ControllerProtected
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*
*/
class UserLogin extends UserControllerProtected
{
	public function action()
	{
		// If login
		$xml = $this->getXML();
		if ($this->_http->getParam("refresh") == "true")
		{
			$user_mail 	= SLS_String::trimSlashesFromString($this->_http->getParam("email"));
			$user_pwd 	= SLS_String::trimSlashesFromString($this->_http->getParam("user_pwd"));
			$this->useModel("Medecin");
			$user = new Main_Medecin();
			
			$result = array_shift($user->searchModels("medecin",array(),array(0=>array("column"=>"mailMed","value"=>$user_mail,"mode"=>"equal"),1=>array("column"=>"pwdMed","value"=>sha1($user_pwd),"mode"=>"equal"))));
			// If unknown user
			if (empty($result))
			{
				// Format user
				$xml->startTag("login");
				$xml->addFullTag("user_mail",$user_mail,true);
				$xml->endTag("login");
				
				// Format errors
				$xml->startTag("errors");
				$xml->addFullTag("error","Combinaison Email/mot de passe incorrecte",true);
				$xml->endTag("errors");
			}else
			{
				// Force login
				$this->forceLogin($result->idMed);
				$result2 = array_shift($user->searchModels("medecin",array(), array(0=>array("column"=>"mailMed","value"=>$user_mail,"mode"=>"equal"))));
				if(!empty($result2))
				{
				//echo'<SCRIPT language=javascript>alert(" IdMed = '.$result->idMed.' ; .n idmed2 = '.$result2->idMed.'")</SCRIPT>';
					if($user->getModel($result2->idMed))
					{
					//echo'<SCRIPT language=javascript>alert(" nbConnect = '.$result2->nbConnect.'")</SCRIPT>';
						$user->__set("nbConnect", $result2->nbConnect + 1);
						$user->save();
					//echo'<SCRIPT language=javascript>alert(" nbConnect = '.$result2->nbConnect.'")</SCRIPT>';
					}
				}
				// Redirect
				if (!empty($redirect))
				{
					$cXML = $this->_generic->getControllersXML();
					$smod = array_shift($cXML->getTags("//controllers/controller/scontrollers/scontroller[@id='".$redirect."']/@name"));
					$mod = array_shift($cXML->getTags("//controllers/controller[scontrollers/scontroller[@id='".$redirect."']]/@name"));
					if (!empty($mod) && !empty($smod))
					{
						$more = array();
						if (!empty($redirectMore))
						{
							$redirectMoreE = explode("|",$redirectMore);										
							for($i=0 ; $i<$count=count($redirectMoreE) ; $i+=2)
							{
								if (!empty($redirectMoreE[$i]) && !empty($redirectMoreE[$i+1]))
									array_push($more,array("key"=>$redirectMoreE[$i],"value"=>$redirectMoreE[$i+1]));
							}
						}
						$this->forward($mod,$smod,$more);
					}
					else
						$this->forward("Home","Index");	
				}							
				else
					$this->forward("Home","Index");	
			}
		}
		$this->saveXML($xml);
	}
}
?>