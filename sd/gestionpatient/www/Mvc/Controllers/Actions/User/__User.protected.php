<?php
/**
* Class generic for the controller User
* Write here all your generic functions you need in your User Actions
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.User
* @see Mvc.Controllers.SiteProtected
* @see Sls.Controllers.Core.SLS_GenericController
* @since 1.0
*/
class UserControllerProtected extends SiteProtected
{

}
?>