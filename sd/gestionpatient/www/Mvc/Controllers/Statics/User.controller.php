<?php
/**
* Controller Static UserController
*
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Statics.UserController
* @see Sls.Controllers.Core.SLS_FrontStatic
* @since 1.0
*/
class UserController extends SLS_FrontStatic implements SLS_IStatic 
{

	public function __construct()
	{
		parent::__construct(true);
	}

	public function constructXML()
	{
		// Objects
		$this->_generic->useModel("Medecin");
		$user = new Main_Medecin();
		$session = $this->_generic->getObjectSession();
		$user_id = $session->getParam("user_id");
				
		// If logged
		if ($user->getModel($user_id) === true)
		{
			$this->_xmlToolBox->addFullTag("is_logged","true",true);
			$this->_xmlToolBox->addFullTag("nom", $user->__get("nomMed"),true);
			$this->_xmlToolBox->addFullTag("prenom", $user->__get("prenomMed"),true);
			$this->_xmlToolBox->addFullTag("nbconnect", $user->__get("nbConnect"),true);
		}
		else
			$this->_xmlToolBox->addFullTag("is_logged","false",true);
	}

}
?>