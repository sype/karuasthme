<?php
/**
* Controller Static MenuController
*
* @author SillySmart
* @copyright SillySmart
* @package Mvc.Controllers.Statics.MenuController
* @see Sls.Controllers.Core.SLS_FrontStatic
* @since 1.0
*/
class MenuController extends SLS_FrontStatic implements SLS_IStatic 
{

	public function __construct()
	{
		parent::__construct(true);
	}

	public function constructXML()
	{
		// Write here all your instructions to make your Static configuraion with xml by $this->_xmlToolBox
		$this->_xmlToolBox->addFullTag("patientIndex_url",$this->_generic->getFullPath("Patient","Index"));
		$this->_xmlToolBox->addFullTag("patientInclusion_url",$this->_generic->getFullPath("Patient","Inclusion"));
		$this->_xmlToolBox->addFullTag("patientSuivi_url",$this->_generic->getFullPath("Patient","Suivi"));
		$this->_xmlToolBox->addFullTag("patientListSuivi_url",$this->_generic->getFullPath("Patient","ListSuivi"));
		$this->_xmlToolBox->addFullTag("protocolesIndex_url",$this->_generic->getFullPath("Protocoles","Index"));
		$this->_xmlToolBox->addFullTag("protocolesGetProtocoles_url",$this->_generic->getFullPath("Protocoles","GetProtocoles"));
		$this->_xmlToolBox->addFullTag("userSetting_url",$this->_generic->getFullPath("User","Setting"));
		$this->_xmlToolBox->addFullTag("userLogin_url",$this->_generic->getFullPath("User","Login"));
		$this->_xmlToolBox->addFullTag("userLogout_url",$this->_generic->getFullPath("User","Logout"));
		$this->_xmlToolBox->addFullTag("updatemdp_url", $this->_generic->getFullPath("User","Reset"));
		$this->_xmlToolBox->addFullTag("contact_url", $this->_generic->getFullPath("About","Message"));
	}

}
?>