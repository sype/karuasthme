<!--
  - Global template for your application
  - Don't change anything between marked delimiter |||dtd:tagName|||
  - Beyond you can add additional headers or/and xhtml structure
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes" encoding="|||sls:getCharset|||" />

    <!-- Variable Builder -->
    |||sls:buildUrlVars|||
    <!-- /Variable Builder -->

    <!-- Generic include -->
    |||sls:includeActionFileBody|||
    |||sls:includeActionFileHeader|||
    |||sls:includeStaticsFiles|||
    <!-- /Generic include -->

    <xsl:template match="root">
        <html xml:lang="|||sls:getLanguage|||" lang="|||sls:getLanguage|||">
            <head>

                <!-- Generic headers loading -->
                <xsl:call-template name="GenerateMetas" />
                |||sls:loadUserHeaders|||
                |||sls:loadActionFileHeader|||
                <!-- /Generic headers loading -->

                <link rel="stylesheet" type="text/css" href="{concat($sls_url_css_core,'ssBo.css')}" />
                <script type="text/javascript" src="{concat($sls_url_js_core_statics, 'SLS_Statics_compressed.js')}"></script>
                <script type="text/javascript" src="{concat($sls_url_js_core_dyn, 'Selects.js')}"></script>
                <script type="text/javascript" src="{concat($sls_url_js_core_dyn, 'slsBo.js')}"></script>

            </head>
            <body>

                <div id="bo_header">
                    <div class="left">
                        <img src="{concat($sls_url_img_core,'Logos/listing_logo.png')}" alt="SillySmart BackOffice" title="SillySmart BackOffice" class="logo" />
                    </div>
                    <div class="right">
                        <a href="{//View/bo_actions/bo_action[label='Disconnection']/url}" title="Logout" class="logout">Sign Out</a>
                        <xsl:variable name="login"><xsl:choose><xsl:when test="//Statics/Sls/session/params/param[name='SLS_BO_USER']/value != ''"><xsl:value-of select="//Statics/Sls/session/params/param[name='SLS_BO_USER']/value" /></xsl:when><xsl:otherwise>SillySmart</xsl:otherwise></xsl:choose></xsl:variable>
                        <span class="logged">you are logged in as <strong><xsl:value-of select="$login" /></strong> - </span>
                    </div>
                </div>
                <div id="bo_wrapper">
                    <div id="bo_float_wrapper">
                        <div id="content_wrapper">
                            <h1 class="main">Back-Office <span class="light"><xsl:value-of select="//Statics/Sls/Configs/site/projectName" /></span></h1>

                            <!-- Generic bodies loading -->
                            |||sls:loadCoreBody|||
                            |||sls:loadUserBody|||
                            |||sls:loadActionFileBody|||
                            <!-- /Generic bodies loading -->

                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>