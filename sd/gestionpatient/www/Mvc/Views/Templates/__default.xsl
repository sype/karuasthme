<!-- 
 	- Global template for your application 	
 	- Don't change anything between marked delimiter |||dtd:tagName|||
 	- Beyond you can add additional headers or/and xhtml structure
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" omit-xml-declaration="yes" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes" encoding="|||sls:getCharset|||" />
	
	<!-- Variable Builder -->
	|||sls:buildUrlVars|||
	<!-- /Variable Builder -->
	
	<!-- Generic include -->
	|||sls:includeActionFileBody|||
	|||sls:includeActionFileHeader|||
	|||sls:includeStaticsFiles|||
	<!-- /Generic include -->
	
	<xsl:template match="root">
		<html xml:lang="|||sls:getLanguage|||" lang="|||sls:getLanguage|||">
			<head>
			<link type="text/css" rel="stylesheet" href="{concat($sls_url_css,'global.css')}"/>
			<link type="text/css" rel="stylesheet" href="{concat($sls_url_css,'SqueezeBox.css')}" />
			<link type="text/css" rel="stylesheet" href="{concat($sls_url_css,'suivi.css')}" />
				<!-- Generic headers loading -->
				|||sls:loadCoreHeaders|||
				|||sls:loadUserHeaders|||
				|||sls:loadActionFileHeader|||
				<!-- /Generic headers loading -->
			
			</head>
			<body>
			<div class = "header_wrapper">
				<div class="top_menu">
					<xsl:if test="//Statics/Site/User/is_logged = 'true'">
						<ul id="top_menu">
							<li>
								Utilisateur : <xsl:value-of select="//Statics/Site/User/prenom"></xsl:value-of>&#160;<xsl:value-of select="//Statics/Site/User/nom"></xsl:value-of>
							</li>
							<li>
								<a href="{//Statics/Site/Menu/userSetting_url}" title="Mon compte" >Mon compte</a>
							</li>
							<li>
								<a href="{//Statics/Site/Menu/userLogout_url}" title="Déconnexion">Déconnexion</a>
							</li>
							<div class="clear"></div>
						</ul>	
					</xsl:if>
				</div>
				<h1>Gestion Patient 2.0</h1>
            </div>
            <div class = "left_menu" >
            <ul class = "left_menu_item">
	            <li class = "{//View/patients_part}">
	            	<a href="{//Statics/Site/Menu/patientIndex_url}" title="Patients" >Patients</a>
	            </li>
	            <li class = "{//View/inclusion_part}">
	            	<a href="{//Statics/Site/Menu/patientInclusion_url}" title="Inclusion">Inclusion</a>
	            </li>
				<li  class = "{//View/protocoles_part}">
	            	<a href="{//Statics/Site/Menu/protocolesIndex_url}" title="Protocoles">Protocoles</a>
	            </li>
	            <li  class = "{//View/contact_part}">
	            	<a href="{//Statics/Site/Menu/contact_url}" title="Contact">Contact</a>
	            </li>
            </ul>
            </div>
            <div class = "content_wrapper" >
            	<div class = "content_sub_wrapper" id="content_sub_wrapper">
					<!-- Generic bodies loading -->
					|||sls:loadCoreBody|||
					|||sls:loadUserBody|||
					|||sls:loadActionFileBody|||
					<!-- /Generic bodies loading -->
				</div>				
			</div>
			</body>		
		</html>
	</xsl:template>
</xsl:stylesheet>