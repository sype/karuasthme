<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="HeaderSuivi">
			<link type="text/css" rel="stylesheet" href="{concat($sls_url_css,'suivi2.css')}" />
			<script src="{concat($sls_url_js_dyn,'suivi.js')}" type="text/javascript"></script>
	</xsl:template>
</xsl:stylesheet>