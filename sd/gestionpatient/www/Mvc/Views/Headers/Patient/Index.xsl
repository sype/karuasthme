<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="HeaderIndex">
		<link type="text/css" rel="stylesheet" href="{concat($sls_url_css,'patient.css')}" />
		<script src="{concat($sls_url_js_dyn,'patient.js')}" type="text/javascript"></script>
		<script src="{concat($sls_url_js_dyn,'suivi.js')}" type="text/javascript"></script>
	</xsl:template>
</xsl:stylesheet>