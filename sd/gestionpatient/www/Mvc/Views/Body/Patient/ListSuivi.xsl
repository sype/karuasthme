<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="ListSuivi">
		<h1>Liste de suivi pour <xsl:value-of select="//View/firstname" />&#160;<xsl:value-of select="//View/name" /></h1>
		<xsl:if test="count(//View/errors/error) > 0" >
			<div class = "errors">
				<ul>
					<xsl:for-each select="//View/errors/error">
						<li>
							<xsl:value-of select="." />
						</li>
					</xsl:for-each>
				</ul>
			</div>
		</xsl:if>
		<xsl:for-each select="//View/suivis/suivi">
			<div class="suivi">
				<p>Examen du <xsl:value-of select="date" /></p>
				<h3>Questionnaire de suivi</h3>
				<p>Vider les poumons :  <xsl:value-of select="questA_1" /></p>
				<p>Inhaler :  <xsl:value-of select="questA_2" /></p>
				<p>Bloquer :  <xsl:value-of select="questA_3" /></p>
				<p>Différencier son traitement de fond de son traitement de secours :  <xsl:value-of select="questA_4" /></p>
				<p>Gérer une exacerbation PAR LES PLANS D'ACTION :  <xsl:value-of select="questA_5" /></p>
				<p>Maitriser son peakflow :  <xsl:value-of select="questA_6" /></p>
				<p>Test de contrôle de l'asthme : <xsl:value-of select="total_test" /></p>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>