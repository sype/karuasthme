<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Index">
		<div class = "setting_menu">
			<ul class = "tools_menu">
				<form method="post" id="filter" action="">
				<li class="menu_item">
	                <div id="input_fld" class="input_fld">
	                    <input type = "text" name="patient_input_sch" id="patient_input_sch"  value="Recherche..." autocomplete="off"/>
	                </div>
	               <div id="patients_result" class="patients_result" >
	                   <ul id="result_item" class="result_item">
	                   </ul>
	               </div>
				</li>
                <li  class="menu_item">
                	<div id="loading_img">
                		&#160;
                	</div>
                </li>
					<li class="menu_item">
						<xsl:if test="count(//View/pages/page) &gt; 1">
							<label for="pages">
								Pages :
							</label>
							<select name="pages" id="pages">
								<xsl:for-each select="//View/pages/page">
									<option value ="{value}">
										<xsl:if test="//View/selectedPage = value">
											<xsl:attribute name="selected">true</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="display" />
									</option>
								</xsl:for-each>
							</select>
						</xsl:if>
	                </li>
	                <li class="menu_item">
	                <label for="nbToDisplay">
	                	afficher : 
	                </label>
	                <input type="text" name="nbToDisplay" id="nbToDisplay" value="{//View/nbToDisplay}" />
                	<input type="hidden" name="refresh" value="true" />
                	<input type="hidden" name="order" id="order" value="{//View/order}"/>
                	<input type="hidden" name="orderSens" id="orderSens" value="{//View/orderSens}"/>
	                </li>
                </form>
                <li class="menu_item">
                	/<xsl:value-of select="//View/nbPatient" /> Patient(s)
                </li>
			</ul>
			<div class = "clear">
			</div>
		</div>
			<table class = "user_list" id = "user_list">
				<xsl:if test="//View/patients/patient">
					<tr class="header">
	               		<td>
		             		No Sécu.
		             	</td>
		             	<td>
		             		<a href="#" onClick="order('nomPatient')" class="classement">
		             			Nom
		             		</a>
		             	</td>
						<td>
							<a href="#" onClick="order('prenomPatient')" class="classement">
		             			Prénom
		             		</a>
		             	</td>
		             	<td>
							Nb Suivis
		             	</td>
		             	<td>
		             		Status
		             	</td>
		             	<td>
							Nouveaux suivi
		             	</td>
		             	<td>
							Liste de suivi
		             	</td>
	               </tr>
				</xsl:if>
				<xsl:for-each select="//View/patients/patient">
	               <tr>
	               		<td>
		             		<xsl:value-of select="no_secu" />
		             	</td>
		             	<td>
		             		<xsl:value-of select="name" />
		             	</td>
						<td>
		             		<xsl:value-of select="firstname" />
		             	</td>
		             	<td>
		             		<xsl:value-of select="nb_suivi" />
		             	</td>
		             	<td>
		             		<xsl:value-of select="inclus" />
		             	</td>
		             	<td>
		             		<xsl:if test="inclus = 'inclus'">
			             		<!--<a rel="boxed" href="{addsuivi_url}" title="Effectuer un suivi">
			             			<img src="{concat($sls_url_img,'Buttons/add.gif')}" alt="Effectuer un suivi" title="Effectuer un suivi" />
			             		</a>-->
								<a href="{addsuivi_url}" title="Liste de suivi">
		             			<img src="{concat($sls_url_img,'Buttons/add.gif')}" alt="Effectuer un suivi" title="Effectuer un suivi" />
		             		</a>
		             		</xsl:if>
		             	</td>
		             	<td>
		             		<a href="{listsuivi_url}" title="Liste de suivi">
		             			<img src="{concat($sls_url_img,'Buttons/view.gif')}" alt="Effectuer un suivi" title="Effectuer un suivi" />
		             		</a>
		             	</td>
	               </tr>
		       </xsl:for-each>
		   </table>
	</xsl:template>
</xsl:stylesheet>