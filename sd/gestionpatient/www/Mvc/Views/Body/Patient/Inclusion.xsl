<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Inclusion">
	<h1>Formulaire de demande d'inclusion</h1>
		<div class="content">
			<xsl:if test="count(//View/errors/error) > 0" >
				<div class = "errors">
					<ul>
						<xsl:for-each select="//View/errors/error">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			<xsl:if test="count(//View/succes/succe) > 0" >
				<div class = "succes">
					<ul>
						<xsl:for-each select="//View/succes/succe">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			<form method="POST" action="{//Statics/Site/Menu/patientInclusion_url}">
				<input type="submit" class="incl_inpt" value="Demander l'inclusion" />
				<p>
					<label for="noSecu">
						N° Sécu 
						<span class="required">*</span>
					</label>
					<input type="text" name="noSecu" maxlength="15" value="{//View/noSecu}"/> 
				</p>
				<p>
					<label for="reseau">
						Lieu de l'inclusion 
						<span class="required">*</span>
					</label>
					<select name="reseau">
						<option value="PaP">
							<xsl:if test="//View/reseau = PaP">
								<xsl:attribute name="selected">
									true
								</xsl:attribute>
							</xsl:if>
							Pointe à Pitre
						</option>
						<option value="BT">
							<xsl:if test="//View/reseau = BT">
								<xsl:attribute name="selected">
									true
								</xsl:attribute>
							</xsl:if>
							Basse-Terre
						</option>
					</select> 
				</p>
				<p>
					<label for="civilite">
						Civilité 
						<span class="required">*</span>
					</label>
					<select name="civilite">
						<OPTION VALUE="Mr">
							<xsl:if test="//View/civilite = 'Mr'">
								<xsl:attribute name="selected">
									true
								</xsl:attribute>
							</xsl:if>
							Mr
						</OPTION>
						<OPTION VALUE="Mme">
							<xsl:if test="//View/civilite = 'Mme'">
								<xsl:attribute name="selected">
									true
								</xsl:attribute>
							</xsl:if>
							Mme
						</OPTION>
						<OPTION VALUE="Mlle">
							<xsl:if test="//View/civilite = 'Mlle'">
								<xsl:attribute name="selected">
									true
								</xsl:attribute>
							</xsl:if>
							Mlle
						</OPTION>
						<OPTION VALUE="Enfant">
							<xsl:if test="//View/civilite = 'Enfant'">
								<xsl:attribute name="selected">
									true
								</xsl:attribute>
							</xsl:if>
							Enfant
						</OPTION>
					</select>
				</p>
				<p>
					<label for="nom">
						Nom
						<span class="required">*</span>
					</label>
					<input type="text" name="nom" maxlength="128" value="{//View/nom}"/> 
				</p>
				<p>
					<label for="prenom">
						Prénom
						<span class="required">*</span>
					</label>
					<input type="text" name="prenom" maxlength="128" value="{//View/prenom}"/> 
				</p>
				<p>
					<label for="rue">
						Rue
					</label>
					<input type="text" name="rue" maxlength="256" value="{//View/rue}"/> 
				</p>
				<p>
					<label for="ville">
						Ville
					</label>
					<input type="text" name="ville" maxlength="128" value="{//View/ville}"/> 
				</p>
				<p>
					<label for="cp">
						Code postal
						<span class="required">*</span>
					</label>
					<input type="text" name="cp" maxlength="5" value="{//View/cp}"/> 
				</p>
				<p>
					<label for="birthdate">
						Date de naissance
						<span class="required">*</span>
					</label>
					<input type="text" name="birthdate" id="birthdate" maxlength="15" value="{//View/birthdate}"/> JJ/MM/AAAA
				</p>
				<p>
					<label for="telephone">
						Coordonnées téléphonique (Fixe)
						<span class="required">*</span>
					</label>
					<input type="text" name="telephone" maxlength="10" value="{//View/telephone}"/> 
				</p>
				<p>
					<label for="portable">
						Coordonnées téléphonique (Portable)
					</label>
					<input type="text" name="portable" maxlength="10" value="{//View/portable}"/> 
				</p>
				<p>
					<label for="severite">
						Séverité
						<span class="required">*</span>
					</label>
					<input type="radio" name="severite" value="2">
						<xsl:if test="//View/severite = 2">
							<xsl:attribute name="checked">
								true
							</xsl:attribute>
						</xsl:if>
					</input>2
					<input type="radio" name="severite" value="3">
						<xsl:if test="//View/severite = 3">
							<xsl:attribute name="checked">
								true
							</xsl:attribute>
						</xsl:if>
					</input>3
					<input type="radio" name="severite" value="4">
						<xsl:if test="//View/severite = 4">
							<xsl:attribute name="checked">
								true
							</xsl:attribute>
						</xsl:if>
					</input>4 
				</p>
				<p>
					<label for="nbgene">
						Nombre de gênes/Mois
					</label>
					<input type="text" name="nbgene" maxlength="3" value="{//View/nbgene}"/> 
				</p>
				<div class="traitement">
					<p>
						<label for="traitCrise">
							Traitement de crise
							<span class="required">*</span>
						</label>
						<textarea id="traitCrise" name="traitCrise" rows="4">
							<xsl:value-of select="//View/traitCrise" />
						</textarea> 
					</p>
				</div>
				<div class="traitement">
					<p>
						<label for="traitFond">
							Traitement de fond
						</label>
						<textarea id="traitCrise" name="traitFond" rows="4">
							<xsl:value-of select="//View/traitFond" />
						</textarea> 
					</p>
				</div>
				<div class="traitement">
					<p>
						<label for="traitAutres">
							Autres
						</label>
						<textarea id="traitAutres" name="traitAutres" rows="4">
							<xsl:value-of select="//View/traitAutres" />
						</textarea> 
					</p>
				</div>
				<p>
					<label for="consulPrec">
						Consultations aux Urgences dans l'année précédente
					</label>
					<input type="text" name="consulPrec" maxlength="3" value="{//View/consulPrec}"/> 
				</p>
				<br />
				<p>
					<label for="hospPrec">
						Hospitalisations dans l'année précédente
					</label>
					<input type="text" name="hospPrec" maxlength="3" value="{//View/hospPrec}"/> 
				</p>
				<p>
					<label for="consultNoProg">
						Nombre de consultations non programmées
					</label>
					<input type="text" name="consultNoProg" maxlength="3" value="{//View/consultNoProg}"/> 
				</p>
				<p>
					<label for="nbStopTrav">
						Nombre d'arrêts de travail
					</label>
					<input type="text" name="nbStopTrav" maxlength="3" value="{//View/nbStopTrav}"/>
				</p>
				<p>
					<label for="dureeStopTrav">
						Durée
					</label>
					<input type="text" name="dureeStopTrav" maxlength="3" value="{//View/dureeStopTrav}"/>				
				</p>
				<p>
					<label for="nbStopSchool">
						Nombre d'absence scolaire
					</label>
					<input type="text" name="nbStopSchool" maxlength="3" value="{//View/nbStopSchool}"/>
				</p>
				<p>
					<label for="dureeStopSchool">
						Durée
					</label>
					<input type="text" name="dureeStopSchool" maxlength="3" value="{//View/dureeStopSchool}"/> 
				</p>
				<input type="hidden" value="true" name="refresh"/>
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>