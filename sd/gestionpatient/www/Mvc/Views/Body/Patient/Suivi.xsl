<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Suivi">
		<form id="suivi" name="suivi" action="{//View/post_url}" method="post">
<div id="titlePart">
<table width="700" border="0" align="center" cellspacing="0" cellpadding="0">
<!-- ligne titre-->
<tr>
<td>
<table width="100%">
<tr>
<!-- Bouton de navigation-->
<td class="navigButtonLeft" height="61">
<div onclick="change(document.getElementById('state').value, 0);" onmouseover="cursor(1);" onmouseout="cursor(0);" id="divButtonLeft" >
	Annuler
</div>
</td>
<!--Infos section-->
<td class="generalTitle" align="center">
<div id="title">Questionnaire de suivi du patient asthmatique</div>
<div id="subTitle">Par le médecin traitant</div>
<div id="userInfo"><xsl:value-of select="//Statics/Site/User/prenom"></xsl:value-of>&#160;<xsl:value-of select="//Statics/Site/User/nom"></xsl:value-of>&#160;- Patient : <u><xsl:value-of select="//View/patient_name"></xsl:value-of></u></div>
</td>
<!-- Bouton de navigation-->
<td class="navigButtonRight" height="61">
<div onclick="change(document.getElementById('state').value, 1);" onmouseover="cursor(1);" onmouseout="cursor(0);" id="divButtonRight" class="button">
	Test de contrôle
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!-- Partie A-->
<div id="questionPartA">
<table width="700px" border="0" align="center">
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td class="questionTitle">
Vérification de la technique d'inhalation de manière chronologique<br />
En respectant les étapes suivantes :
</td>
</tr>
<td class="quest">
<table class="contentQuest">
<tr>
<td class="title">
VIDER LES POUMONS
</td>
<td>
<input type="radio" name="questA_1" id="questA_1"  value="1" />OUI
<input type="radio" name="questA_1" id="questA_1b"  value="0" />NON
</td>
</tr>
<tr>
<td class="title">
INHALER
</td>
<td>
<input type="radio" name="questA_2" id="questA_2" value="1" />OUI
<input type="radio" name="questA_2" id="questA_2b" value="0" />NON
</td>
</tr>
<tr>
<td class="title">
BLOQUER
</td>
<td>
<input type="radio" name="questA_3" id="questA_3" value="1" />OUI
<input type="radio" name="questA_3" id="questA_3b" value="0" />NON
</td>
</tr>
</table>
</td>
</table>
</td>
</tr>
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td  class="questionTitle" width="560">
Sait-il différencier son traitement de fond de son traitement de secours ?
</td>
<td style="font-size:13px">
<input type="radio" name="questA_4" id="questA_4" value="1" />OUI
<input type="radio" name="questA_4" id="questA_4b" value="0" />NON
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td>
<table>
<td  class="questionTitle" width="560">
Sait-il gérer une exacerbation PAR LES PLANS D'ACTION ?
</td>
<td style="font-size:13px">
<input type="radio" name="questA_5" id="questA_5" value="1" />OUI
<input type="radio" name="questA_5" id="questA_5b" value="0" />NON
</td>
</table>
</td>
</tr>
<tr>
<td class="contentLong"> <h1>Question :</h1>
  <p>a. Tu te réveilles dans la nuit, tu tousses, tu te sens essouflé, tu fais quoi ?</p>
  <p>b. Tu as pris plusieurs fois ton médicament de secours sans résultat, tu fais quoi ensuite ?</p>
  <p>c. Cites le médicament de secours à prendre dans le plan B</p>
  <p>d. Tu es surpris par une gêne plus forte que d'habitude, ton peakflow est bas, ou tu n'arrives pas à le faire, tu fais quoi ?</p>
  <p>e. Dans ton plan C en attendant les secours, tu fais quoi ?</p></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td>
<table>
<td  class="questionTitle" width="560">
Maitrise t-il son peakflow ?
</td>
<td style="font-size:13px">
<input type="radio" name="questA_6" id="questA_6" value="1" />OUI
<input type="radio" name="questA_6" id="questA_6b" value="0" />NON
</td>
</table>
</td>
</tr>
<tr>
<td class="contentLong"> <h1>Question :</h1>
  <p>a. se met debout</p>
  <p>b. ramène le curseur à zero</p>
  <p>c.inspire profondément</p>
  <p>d. bloque sa respiration et met l'embout dans sa bouche, tiens le peakflow bien à l'horizontal</p>
  <p>e. souffle fort et court</p>
  <p>d. interprète son score, le commente et l'inscrit dans son carnet de suivi</p></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
</td>
</tr>
</table>
</div>
<!-- Div cachée contient la partie B du suivi-->
<div style="display:none" id="questionPartB">
<table width="700px" border="0" align="center" cellspacing="0" cellpadding="0">
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td>
<table>
  <td  class="questionTitle2"> Au cours des<u> 4 dernières semaines</u>, votre asthme vous a t-il géné(e) dans vos activités au travail, à l'école/université ou chez vous ?</td>
</table>
</td>
</tr>
<tr>
<td>
<table class="choiceTable"  width="100%">
<td id="questB_1_1" onmouseover="quest(1, this.id, 'B_1', 1);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_1_1', 'B_1', 1);" class="choice">
<div id="divQuestB_1_1" class="divQuest">
tout le temps<br/>
</div>
</td>
<td id="questB_1_2" onmouseover="quest(1, this.id, 'B_1', 2);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_1_2', 'B_1', 2);" class="choice">
<div id="divQuestB_1_2" class="divQuest">La plupart du temps<br/>
</div>
</td>
<td id="questB_1_3" onmouseover="quest(1, this.id, 'B_1', 3);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_1_3', 'B_1', 3);" class="choice">
<div id="divQuestB_1_3" class="divQuest"> Quelquefois
<br/>
</div>
</td>
<td id="questB_1_4" onmouseover="quest(1, this.id, 'B_1', 4);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_1_4', 'B_1', 4);" class="choice">
<div id="divQuestB_1_4" class="divQuest"> Rarement
<br/>
</div>
</td>
<td id="questB_1_5" onmouseover="quest(1, this.id, 'B_1', 5);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_1_5', 'B_1', 5);" class="choice">
<div id="divQuestB_1_5" class="divQuest"> Jamais
<br/>
</div>
</td>
<td class="point">
<table width="100%" height="100%">
<tr>
<td height="50%">
Points
</td>
</tr>
<tr>
<td height="50%">
<div class="style3" id="pointQuestB_1">
0</div>
</td>
</tr>
</table>
</td>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td>
<table>
  <td class="questionTitle2" >
Au cours des <u>4 dernières semaines</u>, avez-vous été essouflé(e)</td>
</table>
</td>
</tr>
<tr>
<td>
<table class="choiceTable"  width="100%">
<td id="questB_2_1" onmouseover="quest(1, this.id, 'B_2', 1);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_2_1', 'B_2', 1);" class="choice">
<div id="divQuestB_2_1" class="divQuest">
Plus d'une fois par jour
</div>
</td>
<td id="questB_2_2" onmouseover="quest(1, this.id, 'B_2', 2);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_2_2', 'B_2', 2);" class="choice">
<div id="divQuestB_2_2" class="divQuest"> Une fois par jours
<br />
</div>
</td>
<td id="questB_2_3" onmouseover="quest(1, this.id, 'B_2', 3);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_2_3', 'B_2', 3);" class="choice">
<div id="divQuestB_2_3" class="divQuest">
3 à six fois par semaine
</div>
</td>
<td id="questB_2_4" onmouseover="quest(1, this.id, 'B_2', 4);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_2_4', 'B_2', 4);" class="choice">
<div id="divQuestB_2_4" class="divQuest">Une à deux fois/semaine<br />
</div>
</td>
<td id="questB_2_5" onmouseover="quest(1, this.id, 'B_2', 5);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_2_5', 'B_2', 5);" class="choice">
<div id="divQuestB_2_5" class="divQuest">Jamais<br />
</div>
</td>
<td class="point">
<table width="100%" height="100%">
<tr>
<td height="50%">
Points
</td>
</tr>
<tr>
<td height="50%">
<div class="style3" id="pointQuestB_2">
0</div>
</td>
</tr>
</table>
</td>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td>
<table>
  <td  class="questionTitle2" > Au cours des <u>4 dernières semaines</u>, les symptomes de l'asthme (sifflements dans la poitrine, toux, essouflement, oppression ou douleur dans la poitrine) vous ont-ils réveillé(e) la nuit ou plus tôt que d'habitude le matin</td>
</table>
</td>
</tr>
<tr>
<td>
<table class="choiceTable"  width="100%">
<td id="questB_3_1" onmouseover="quest(1, this.id, 'B_3', 1);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_3_1', 'B_3', 1);" class="choice">
<div id="divQuestB_3_1" class="divQuest"> 4 nuits ou + par semaine
<br />
</div>
</td>
<td id="questB_3_2" onmouseover="quest(1, this.id, 'B_3', 2);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_3_2', 'B_3', 2);" class="choice">
<div id="divQuestB_3_2" class="divQuest">
2 à 3 nuits par semaine
</div>
</td>
<td id="questB_3_3" onmouseover="quest(1, this.id, 'B_3', 3);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_3_3', 'B_3', 3);" class="choice">
<div id="divQuestB_3_3" class="divQuest"> Une nuit par semaine
<br />
</div>
</td>
<td id="questB_3_4" onmouseover="quest(1, this.id, 'B_3', 4);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_3_4', 'B_3', 4);" class="choice">
<div id="divQuestB_3_4" class="divQuest">
Une à deux fois en tout
</div>
</td>
<td id="questB_3_5" onmouseover="quest(1, this.id, 'B_3', 5);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_3_5', 'B_3', 5);" class="choice">
<div id="divQuestB_3_5" class="divQuest">
Jamais
</div>
</td>
<td class="point">
<table width="100%" height="100%">
<tr>
<td height="50%">
Points
</td>
</tr>
<tr>
<td height="50%">
<div class="style3" id="pointQuestB_3">
0</div>
</td>
</tr>
</table>
</td>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td>
<table>
  <td  class="questionTitle2" >
Au cours des <u>4 dernières semaines</u>, avez vous utilisé votre inhalateur de secours ou pris un traitement par nébulisation (par exemple salbutamol, terbutaline) ?</td>
</table>
</td>
</tr>
<tr>
<td>
<table class="choiceTable"  width="100%">
<td id="questB_4_1" onmouseover="quest(1, this.id, 'B_4', 1);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_4_1', 'B_4', 1);" class="choice">
<div id="divQuestB_4_1" class="divQuest">
3 fois par jour ou plus
</div>
</td>
<td id="questB_4_2" onmouseover="quest(1, this.id, 'B_4', 2);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_4_2', 'B_4', 2);" class="choice">
<div id="divQuestB_4_2" class="divQuest"> 1 ou 2 fois par jour
<br />
</div>
</td>
<td id="questB_4_3" onmouseover="quest(1, this.id, 'B_4', 3);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_4_3', 'B_4', 3);" class="choice">
<div id="divQuestB_4_3" class="divQuest">
2 ou 3 fois par semaine
</div>
</td>
<td id="questB_4_4" onmouseover="quest(1, this.id, 'B_4', 4);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_4_4', 'B_4', 4);" class="choice">
<div id="divQuestB_4_4" class="divQuest">
1 fois par sem. ou moins
</div>
</td>
<td id="questB_4_5" onmouseover="quest(1, this.id, 'B_4', 5);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_4_5', 'B_4', 5);" class="choice">
<div id="divQuestB_4_5" class="divQuest">
Jamais
</div>
</td>
<td class="point">
<table width="100%" height="100%">
<tr>
<td height="50%">
Points
</td>
</tr>
<tr>
<td height="50%">
<div class="style3" id="pointQuestB_4">
0</div>
</td>
</tr>
</table>
</td>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="questBox" width="100%">
<tr>
<td>
<table>
  <td  class="questionTitle2" > Comment évalueriez-vous votre asthme au cours des <u>4 dernières semaines</u> ?</td>
</table>
</td>
</tr>
<tr>
<td>
<table class="choiceTable"  width="100%">
<td id="questB_5_1" onmouseover="quest(1, this.id, 'B_5', 1);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_5_1', 'B_5', 1);" class="choice">
<div id="divQuestB_5_1" class="divQuest"> Pas contrôlé du tout
<br />
</div>
</td>
<td id="questB_5_2" onmouseover="quest(1, this.id, 'B_5', 2);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_5_2', 'B_5', 2);" class="choice">
<div id="divQuestB_5_2" class="divQuest">
Très peu contrôlé
</div>
</td>
<td id="questB_5_3" onmouseover="quest(1, this.id, 'B_5', 3);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_5_3', 'B_5', 3);" class="choice">
<div id="divQuestB_5_3" class="divQuest">
Un peu contrôlé
</div>
</td>
<td id="questB_5_4" onmouseover="quest(1, this.id, 'B_5', 4);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_5_4', 'B_5', 4);" class="choice">
<div id="divQuestB_5_4" class="divQuest">
Bien contrôlé
</div>
</td>
<td id="questB_5_5" onmouseover="quest(1, this.id, 'B_5', 5);" onmouseout="quest(0, this.id);" onclick="quest(2, 'divQuestB_5_5', 'B_5', 5);" class="choice">
<div id="divQuestB_5_5" class="divQuest"> Totalement contrôlé
<br />
</div>
</td>
<td class="point">
<table width="100%" height="100%">
<tr>
<td height="50%">
Points
</td>
</tr>
<tr>
<td height="50%">
<div class="style3" id="pointQuestB_5">
0</div>
</td>
</tr>
</table>
</td>
</table>
</td>
</tr>
<tr>
<td>
<table>
<tr>
<td style="font-weight:bold">
Total :
</td>
<td>
<div class="style3" id="total" style="width:50px;text-align:center">
0</div>
</td>
<td style="font-weight:bold">
Etat Asthme :
</td>
<td>
<div id="asthmeState" style="font-weight:bold; background-color:#FFFFFF;width:95px;text-align:center">
</div>
</td>
<td><div style="margin-left:30px;font-size:14px; background-color:#FFFFFF;font-weight:bold;padding:2px">0 &lt; <span style="color:red">Non controlé</span> &lt; 15 &lt; <span style="color:#FF6600">Perfectible</span> &lt; 20 &lt; <span style="color:green">Controlé</span> &lt;= 25</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!-- Champs cachés - Contiennent les notes au question partie B-->
<input type="hidden" id="questB_1" name="questB_1" value="0"/>
<input type="hidden" id="questB_2" name="questB_2" value="0"/>
<input type="hidden" id="questB_3" name="questB_3" value="0"/>
<input type="hidden" id="questB_4" name="questB_4" value="0"/>
<input type="hidden" id="questB_5" name="questB_5" value="0"/>

<!-- Champs caché - permet de savoir sur quelle page on se situe-->
<input type="hidden" id="state" value="0" />
<input type="hidden" name="refresh" value="true" />
<input type="hidden" id="id_patient" value="{//View/patient_id}" />
</div>
<div id="hiddenDiv"></div>
</form>
	</xsl:template>
</xsl:stylesheet>