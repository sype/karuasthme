<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Setting">
		<h1>Configuration</h1>
		<div class="content">
			<xsl:if test="count(//View/errors/error) > 0" >
				<div class = "errors">
					<ul>
						<xsl:for-each select="//View/errors/error">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			<xsl:if test="count(//View/succes/succe) > 0" >
				<div class = "succes">
					<ul>
						<xsl:for-each select="//View/succes/succe">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			<form action="" method="post">
				<p>
					<label for="email">
						Email :
					</label>
					<input type="text" name="email" maxlength="256" value="{//View/email}" />
					<div class="clear"></div>
				</p>
				<p>
					<label for="oldPwd">
						Ancien mot de passe :
					</label>
					<input type="password" name="oldPwd" maxlength="16" />
					<div class="clear"></div>
				</p>
				<p>
					<label for="password">
						Nouveau mot de passe :
					</label>
					<input type="password" name="password" maxlength="16" />
					<div class="clear"></div>
				</p>
				<p>
					<label for="password">
						Confirmation nouveau mot de passe :
					</label>
					<input type="password" name="passwordconf" maxlength="16" />
					<div class="clear"></div>
				</p>
				<div class="clear"></div>
				<input type="hidden" name="refresh" value="true" />
				<p>
					<input type="submit" value="Valider" />
					<div class="clear"></div>
				</p>
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>