<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Login">
		<div class="content">
			<xsl:if test="count(//View/errors/error) > 0" >
				<div class = "errors">
					<ul>
						<xsl:for-each select="//View/errors/error">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			<form method="post" action="">
			    <p>
					<label for="name">
						Email :
			       </label>
			       <input type="text" name="email" id="email" maxlenght="256" value = "{//View/login/user_mail}"/>
			    </p>
			    <div class="clear"></div>
			    <p>
					<label for="password">
						Mot de passe :
			       </label>
			       <input type="password" name="user_pwd" id="user_pwd" maxlenght="256" value = ""/>
			       <div class="clear"></div>
			    </p>
			    <input type="hidden" name="refresh" value = "true"/>
			    <p>
			    	<input type="submit" class="submit" value = "Valider" />
			    	<div class="clear"></div>
			    </p>
			    <p>
			    	<a href="{//Statics/Site/Menu/updatemdp_url}" title="Mot de passe oublié ?">Mot de passe oublié ?</a>
			    </p>
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>