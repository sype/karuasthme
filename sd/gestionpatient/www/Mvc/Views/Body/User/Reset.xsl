<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Reset">
		<div class="content">
		<xsl:if test="count(//View/succes/succe) > 0" >
				<div class = "succes">
					<ul>
						<xsl:for-each select="//View/succes/succe">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			<xsl:if test="count(//View/errors/error) > 0" >
				<div class = "errors">
					<ul>
						<xsl:for-each select="//View/errors/error">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			<form method="post" action="">
			    <p>
					<label for="email">
						Adresse email :
			       </label>
			       <input type="text" name="email" id="email" maxlenght="256" value = ""/>
			       <div class="clear"></div>
			    </p>
			    <input type="hidden" name="refresh" value = "true"/>
			    <p>
			    	<input type="submit" class="submit" value = "Valider" />
			    	<div class="clear"></div>
			    </p>
			    <p>
			    	<a href="{$sls_url_domain}" title="Accueil">Accueil</a>
			    </p>
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>