<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="UpdMdp">
		<div class="content">
			<xsl:if test="count(//View/succes/succe) > 0" >
				<div class = "succes">
					<ul>
						<xsl:for-each select="//View/succes/succe">
							<li>
								<xsl:value-of select=".">
								</xsl:value-of>
							</li>
						</xsl:for-each>
					</ul>
				</div>
				</xsl:if>
				<xsl:if test="count(//View/errors/error) > 0" >
					<div class = "errors">
						<ul>
							<xsl:for-each select="//View/errors/error">
								<li>
									<xsl:value-of select=".">
									</xsl:value-of>
								</li>
							</xsl:for-each>
						</ul>
					</div>
				</xsl:if>
				<form method="post" action="">
				    <p>
						<label for="password">
							Mot de passe :
				       </label>
				       <input type="password" name="password" maxlenght="256" value = ""/>
				       <div class="clear"></div>
				    </p>
				    <p>
						<label for="password_onf">
							Confirmation :
				       </label>
				       <input type="password" name="password_conf" maxlenght="256" value = ""/>
				       <div class="clear"></div>
				    </p>
				    <input type="hidden" name="refresh" value = "true"/>
				    <input type="hidden" name="Token" value = "{//View/Token}"/>
				    <p>
				    	<input type="submit" class="submit" value = "Valider" />
				    	<div class="clear"></div>
				    </p>
				</form>
				<p>
			    	<a href="{$sls_url_domain}" title="Accueil">Accueil</a>
			    </p>
		</div>
	</xsl:template>
</xsl:stylesheet>