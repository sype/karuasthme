<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Index">
		<h1>Les protocoles de prise en charge de l’Asthme en Guadeloupe</h1>
			<div class="content">
				<ul>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/5-5.sls')}" target="_blank" title="Avant propos">
							Avant propos
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/6-6.sls')}" target="_blank" title="Remerciements">
							Remerciements
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/7-7.sls')}" target="_blank" title="Préface">
							Préface
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/8-9.sls')}" target="_blank" title="Bref rappel">
							Bref rappel
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/10-20.sls')}" target="_blank" title="Préface">
							Les Guidelines du GINA novembre 2006
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/22-23.sls')}" target="_blank" title="Bilan initial d’asthme de l’adulte et du grand enfant">
							Bilan initial d’asthme de l’adulte et du grand enfant
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/24-26.sls')}" target="_blank" title="Prise en charge de la crise d’Asthme de l’enfant à domicile">
							Prise en charge de la crise d’Asthme de l’enfant à domicile
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/27-29.sls')}" target="_blank" title="Prise en charge de la crise d’Asthme de l’enfant par le médecin traitant en ville">
							Prise en charge de la crise d’Asthme de l’enfant par le médecin traitant en ville
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/30-34.sls')}" target="_blank" title="Prise en charge de la crise d’Asthme aux urgences pédiatriques">
							Prise en charge de la crise d’Asthme aux urgences pédiatriques
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/35-44.sls')}" target="_blank" title="Prise en charge de la crise d’Asthme aigue grave aux urgences">
							Prise en charge de la crise d’Asthme aigue grave aux urgences
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/45-46.sls')}" target="_blank" title="Prise en charge d’une crise d’Asthme de l’adulte en ville">
							Prise en charge d’une crise d’Asthme de l’adulte en ville
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/47-53.sls')}" target="_blank" title="Protocole de régulation (SAMU) – Traitement d’un appel pour crise d’Asthme">
							Protocole de régulation (SAMU) – Traitement d’un appel pour crise d’Asthme
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/54-56.sls')}" target="_blank" title="Les cures thermales, histoire, mécanismes, efficacité">
							Les cures thermales, histoire, mécanismes, efficacité
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/57-60.sls')}" target="_blank" title="Projet d’accueil individualisé (PAI)">
							Projet d’accueil individualisé (PAI)
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/61-62.sls')}" target="_blank" title="Procédure de prise en charge globale du patient au Réseau Asthme de Guadeloupe">
							Procédure de prise en charge globale du patient au Réseau Asthme de Guadeloupe
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/63-64.sls')}" target="_blank" title="L’inclusion et le suivi éducatif du patient par le médecin traitant en ligne">
							L’inclusion et le suivi éducatif du patient par le médecin traitant en ligne
						</a>
					</li>
					<li>
						<a href="{concat($sls_url_domain,'/Protocoles/Index/Pages/1-64.sls')}" target="_blank" title="Guide complet des protocoles">
							Guide complet des protocoles
						</a>
					</li>
				</ul>
			</div>
	</xsl:template>
</xsl:stylesheet>