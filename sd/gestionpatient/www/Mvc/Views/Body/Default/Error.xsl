<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Error">
		Sorry, the wanted action doesn't exist
		<xsl:if test="count(//actions/action) &gt; 0">
			, you maybe search :
			<ul>
				<!-- Don't change anything below -->
				<xsl:for-each select="//actions/action">
					<li><a href="{href}" title="{label}"><xsl:value-of select="label" /></a></li>
				</xsl:for-each>
				<!-- /Don't change anything below -->
			</ul>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>