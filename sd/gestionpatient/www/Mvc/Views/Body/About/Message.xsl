<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:template name="Message">
		<h1>Laissez nous un message</h1>
			<div class="content">
				<xsl:if test="count(//View/errors/error) > 0" >
					<div class = "errors">
						<ul>
							<xsl:for-each select="//View/errors/error">
								<li>
									<xsl:value-of select=".">
									</xsl:value-of>
								</li>
							</xsl:for-each>
						</ul>
					</div>
				</xsl:if>
				<xsl:if test="count(//View/succes/succe) > 0" >
					<div class = "succes">
						<ul>
							<xsl:for-each select="//View/succes/succe">
								<li>
									<xsl:value-of select=".">
									</xsl:value-of>
								</li>
							</xsl:for-each>
						</ul>
					</div>
				</xsl:if>
				<form method="POST" action="">
					<p>
						<textarea name="message"></textarea>
					</p>
					<p>
						<input type="submit" class="submit" value="Envoyer" />
					</p>
					<div class="clear"></div>
					<input type="hidden"  name="refresh" value="true" />
				</form>
			</div>
	</xsl:template>
</xsl:stylesheet>